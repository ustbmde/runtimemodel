/**
 */
package edu.ustb.sei.mde.mobile.file;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Item</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.file.FileItem#getName <em>Name</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.file.FileItem#getActualPath <em>Actual Path</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mobile.file.FilePackage#getFileItem()
 * @model abstract="true"
 * @generated
 */
public interface FileItem extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see edu.ustb.sei.mde.mobile.file.FilePackage#getFileItem_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mobile.file.FileItem#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Actual Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actual Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actual Path</em>' attribute.
	 * @see #setActualPath(String)
	 * @see edu.ustb.sei.mde.mobile.file.FilePackage#getFileItem_ActualPath()
	 * @model
	 * @generated
	 */
	String getActualPath();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mobile.file.FileItem#getActualPath <em>Actual Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Actual Path</em>' attribute.
	 * @see #getActualPath()
	 * @generated
	 */
	void setActualPath(String value);

} // FileItem
