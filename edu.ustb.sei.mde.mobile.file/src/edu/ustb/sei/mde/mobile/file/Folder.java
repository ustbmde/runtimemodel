/**
 */
package edu.ustb.sei.mde.mobile.file;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Folder</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.file.Folder#getChildren <em>Children</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mobile.file.FilePackage#getFolder()
 * @model
 * @generated
 */
public interface Folder extends FileItem {
	/**
	 * Returns the value of the '<em><b>Children</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.mobile.file.FileItem}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' containment reference list.
	 * @see edu.ustb.sei.mde.mobile.file.FilePackage#getFolder_Children()
	 * @model containment="true"
	 * @generated
	 */
	EList<FileItem> getChildren();

} // Folder
