package edu.ustb.sei.mde.mobile.filemodel;

import edu.ustb.sei.mde.mobile.datastructure.Context;
import edu.ustb.sei.mde.mobile.datastructure.Wrapper;
import edu.ustb.sei.mde.mobile.datastructure.operation.Information;
import edu.ustb.sei.mde.mobile.datastructure.operation.InformationKind;
import edu.ustb.sei.mde.mobile.datastructure.operation.OperationScheduler;
import edu.ustb.sei.mde.mobile.datastructure.operation.ValueProvider;
import edu.ustb.sei.mde.mobile.datastructure.sourcepool.SourcePool;
import edu.ustb.sei.mde.mobile.datastructure.synchronizer.ElementSynchronizer;
import edu.ustb.sei.mde.mobile.datastructure.synchronizer.ModelSynchronizer;
import edu.ustb.sei.mde.mobile.datastructure.synchronizer.MultiValuedReferenceSynchronizer;
import edu.ustb.sei.mde.mobile.datastructure.synchronizer.SingleValuedAttributeSynchronizer;
import edu.ustb.sei.mde.mobile.file.FileItem;
import edu.ustb.sei.mde.mobile.file.Folder;
import java.io.File;
import java.util.List;
import java.util.function.BiFunction;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.eclipse.xtext.xbase.lib.IterableExtensions;

@SuppressWarnings("all")
public class FileModelSynchronizer extends ModelSynchronizer {
  public static class FolderSync extends ElementSynchronizer<File, Folder, Context> {
    public FolderSync() {
      super(java.io.File.class, edu.ustb.sei.mde.mobile.file.FilePackage.eINSTANCE.getFolder());
    }
    
    public Folder createView(final Context context) {
      return edu.ustb.sei.mde.mobile.file.FileFactory.eINSTANCE.createFolder();
    }
    
    public ValueProvider<?, ? extends EObject, File> createSource(final File source, final Wrapper w, final Context c) {
      ValueProvider<Object, FileItem, File> _xblockexpression = null;
      {
        Object _value = w.get("name").getValue();
        final String name = ((String) _value);
        Object _value_1 = w.get("actualPath").getValue();
        final String actualPath = ((String) _value_1);
        final BiFunction<Object, FileItem, File> _function = (Object sp, FileItem vp) -> {
          String _xifexpression = null;
          if ((vp == null)) {
            _xifexpression = actualPath;
          } else {
            String _generatePath = FileModelSynchronizer.generatePath(vp);
            String _plus = (_generatePath + Character.valueOf(File.separatorChar));
            _xifexpression = (_plus + name);
          }
          final String path = _xifexpression;
          return new File(path);
        };
        _xblockexpression = ValueProvider.<Object, FileItem, File>valueProvider(_function);
      }
      return _xblockexpression;
    }
    
    public Boolean internalAlign(final File s, final Folder v, final Context c) {
      try {
        return Boolean.valueOf(s.getCanonicalPath().equals(v.getActualPath()));
      } catch (Throwable _e) {
        throw Exceptions.sneakyThrow(_e);
      }
    }
    
    public Boolean align(final Object source, final EObject view, final Context context) {
      return super.align(source,view,context);
    }
    
    protected FolderSync.ChildrenSync folder_children;
    
    protected FileItemSync.NameSync fileItem_name;
    
    protected FileItemSync.ActualPathSync fileItem_actualPath;
    
    public static class ChildrenSync extends MultiValuedReferenceSynchronizer<File, File, Folder, FileItem, Context> {
      public ChildrenSync() {
        super(edu.ustb.sei.mde.mobile.file.FilePackage.eINSTANCE.getFolder_Children());
      }
      
      public List<File> externalGet(final File s, final Context c) {
        return IterableExtensions.<File>toList(((Iterable<File>)Conversions.doWrapArray(s.listFiles())));
      }
      
      public void externalReplace(final File s, final File osv, final File nsv, final Folder v, final FileItem vv, final Context c) {
        try {
          boolean _equals = osv.getName().equals(nsv.getName());
          boolean _not = (!_equals);
          if (_not) {
            String _canonicalPath = osv.getCanonicalPath();
            String _plus = ("rename " + _canonicalPath);
            String _plus_1 = (_plus + " to ");
            String _canonicalPath_1 = nsv.getCanonicalPath();
            String _plus_2 = (_plus_1 + _canonicalPath_1);
            InputOutput.<String>println(_plus_2);
            boolean _exists = osv.exists();
            if (_exists) {
              osv.renameTo(nsv);
            }
            vv.setActualPath(nsv.getCanonicalPath());
          }
        } catch (Throwable _e) {
          throw Exceptions.sneakyThrow(_e);
        }
      }
      
      public void externalInsert(final File s, final File sv, final Folder v, final FileItem vv, final Context c) {
        try {
          String _canonicalPath = sv.getCanonicalPath();
          String _plus = ("insert " + _canonicalPath);
          InputOutput.<String>println(_plus);
          boolean _exists = sv.exists();
          boolean _not = (!_exists);
          if (_not) {
            if ((vv instanceof edu.ustb.sei.mde.mobile.file.File)) {
              sv.createNewFile();
            } else {
              sv.mkdir();
            }
          }
          vv.setActualPath(sv.getCanonicalPath());
        } catch (Throwable _e) {
          throw Exceptions.sneakyThrow(_e);
        }
      }
      
      public void externalRemove(final File s, final File sv, final Folder v, final Context c) {
        try {
          String _canonicalPath = sv.getCanonicalPath();
          String _plus = ("delete " + _canonicalPath);
          InputOutput.<String>println(_plus);
          boolean _exists = sv.exists();
          if (_exists) {
            sv.delete();
          }
        } catch (Throwable _e) {
          throw Exceptions.sneakyThrow(_e);
        }
      }
      
      public void externalMoveIn(final File s, final File sv, final Object os, final Folder v, final FileItem vv, final Context c) {
        try {
          Object _get = c.get("tempDir");
          final String tempName = FileModelSynchronizer.convertName(vv.getActualPath(), ((String) _get));
          String _canonicalPath = sv.getCanonicalPath();
          String _plus = ("moveIn " + _canonicalPath);
          String _plus_1 = (_plus + " from ");
          String _plus_2 = (_plus_1 + tempName);
          InputOutput.<String>println(_plus_2);
          final File f = new File(tempName);
          boolean _exists = f.exists();
          if (_exists) {
            f.renameTo(sv);
          }
          vv.setActualPath(sv.getCanonicalPath());
        } catch (Throwable _e) {
          throw Exceptions.sneakyThrow(_e);
        }
      }
      
      public void externalMoveOut(final File s, final File sv, final Folder v, final FileItem vv, final EObject nv, final Context c) {
        try {
          Object _get = c.get("tempDir");
          final String tempName = FileModelSynchronizer.convertName(sv.getCanonicalPath(), ((String) _get));
          InputOutput.<String>println(((("moveOut " + sv) + " to ") + tempName));
          boolean _exists = sv.exists();
          if (_exists) {
            File _file = new File(tempName);
            boolean _renameTo = sv.renameTo(_file);
            boolean _tripleEquals = (Boolean.valueOf(_renameTo) == Boolean.valueOf(false));
            if (_tripleEquals) {
              InputOutput.<String>println("moveOut is executed but not successful");
            }
          }
        } catch (Throwable _e) {
          throw Exceptions.sneakyThrow(_e);
        }
      }
      
      public void configureInformation(final Information info) {
        InformationKind kind = info.getKind();
        super.configureInformation(info);
      }
    }
    
    public ElementSynchronizer<File, Folder, Context> getValueSynchronizerForGet(final File s) {
      ElementSynchronizer elementSynchronizer = null;
      if((elementSynchronizer=(ElementSynchronizer)((FileModelSynchronizer)this.container).folder).canHandle(s)) 
          return elementSynchronizer;
      return null;
    }
    
    public ElementSynchronizer<File, Folder, Context> getValueSynchronizerForPut(final Folder v) {
      ElementSynchronizer elementSynchronizer = null;
      if((elementSynchronizer=(ElementSynchronizer)((FileModelSynchronizer)this.container).folder).canHandle(v)) 
        return elementSynchronizer;
      return null;
    }
    
    protected boolean sourceFilter(final Object source) {
      boolean _isDirectory = ((File) source).isDirectory();
      return _isDirectory;
    }
  }
  
  public static class FileSync extends ElementSynchronizer<File, edu.ustb.sei.mde.mobile.file.File, Context> {
    public FileSync() {
      super(java.io.File.class, edu.ustb.sei.mde.mobile.file.FilePackage.eINSTANCE.getFile());
    }
    
    public edu.ustb.sei.mde.mobile.file.File createView(final Context context) {
      return edu.ustb.sei.mde.mobile.file.FileFactory.eINSTANCE.createFile();
    }
    
    public ValueProvider<?, ? extends EObject, File> createSource(final File source, final Wrapper w, final Context c) {
      ValueProvider<Object, FileItem, File> _xblockexpression = null;
      {
        Object _value = w.get("name").getValue();
        final String name = ((String) _value);
        Object _value_1 = w.get("actualPath").getValue();
        final String actualPath = ((String) _value_1);
        final BiFunction<Object, FileItem, File> _function = (Object sp, FileItem vp) -> {
          String _xifexpression = null;
          if ((vp == null)) {
            _xifexpression = actualPath;
          } else {
            String _generatePath = FileModelSynchronizer.generatePath(vp);
            String _plus = (_generatePath + Character.valueOf(File.separatorChar));
            _xifexpression = (_plus + name);
          }
          final String path = _xifexpression;
          InputOutput.<String>println(("create " + path));
          return new File(path);
        };
        _xblockexpression = ValueProvider.<Object, FileItem, File>valueProvider(_function);
      }
      return _xblockexpression;
    }
    
    public Boolean internalAlign(final File s, final edu.ustb.sei.mde.mobile.file.File v, final Context c) {
      try {
        return Boolean.valueOf(s.getCanonicalPath().equals(v.getActualPath()));
      } catch (Throwable _e) {
        throw Exceptions.sneakyThrow(_e);
      }
    }
    
    public Boolean align(final Object source, final EObject view, final Context context) {
      return super.align(source,view,context);
    }
    
    protected FileSync.SizeSync file_size;
    
    protected FileItemSync.NameSync fileItem_name;
    
    protected FileItemSync.ActualPathSync fileItem_actualPath;
    
    public static class SizeSync extends SingleValuedAttributeSynchronizer<File, Integer, edu.ustb.sei.mde.mobile.file.File, Integer, Context> {
      public SizeSync() {
        super(edu.ustb.sei.mde.mobile.file.FilePackage.eINSTANCE.getFile_Size());
      }
      
      public Integer externalGet(final File s, final Context c) {
        long _length = s.length();
        return Integer.valueOf(((int) _length));
      }
      
      public void externalSet(final File source, final Integer value, final edu.ustb.sei.mde.mobile.file.File view, final Integer viewValue, final Context context) {
        // no definition
      }
      
      public void configureInformation(final Information info) {
        InformationKind kind = info.getKind();
        super.configureInformation(info);
      }
    }
    
    public ElementSynchronizer<File, edu.ustb.sei.mde.mobile.file.File, Context> getValueSynchronizerForGet(final File s) {
      ElementSynchronizer elementSynchronizer = null;
      if((elementSynchronizer=(ElementSynchronizer)((FileModelSynchronizer)this.container).file).canHandle(s)) 
          return elementSynchronizer;
      return null;
    }
    
    public ElementSynchronizer<File, edu.ustb.sei.mde.mobile.file.File, Context> getValueSynchronizerForPut(final edu.ustb.sei.mde.mobile.file.File v) {
      ElementSynchronizer elementSynchronizer = null;
      if((elementSynchronizer=(ElementSynchronizer)((FileModelSynchronizer)this.container).file).canHandle(v)) 
        return elementSynchronizer;
      return null;
    }
    
    protected boolean sourceFilter(final Object source) {
      boolean _isFile = ((File) source).isFile();
      return _isFile;
    }
  }
  
  public static class FileItemSync extends ElementSynchronizer<File, FileItem, Context> {
    public FileItemSync() {
      super(java.io.File.class, edu.ustb.sei.mde.mobile.file.FilePackage.eINSTANCE.getFileItem());
    }
    
    public FileItem createView(final Context context) {
      return null;
    }
    
    public ValueProvider<?, ? extends EObject, File> createSource(final File source, final Wrapper wrapper, final Context context) {
      throw new java.lang.UnsupportedOperationException();
    }
    
    public Boolean internalAlign(final File source, final FileItem view, final Context context) {
      return null;
    }
    
    public Boolean align(final Object source, final EObject view, final Context context) {
      return super.align(source,view,context);
    }
    
    protected FileItemSync.NameSync fileItem_name;
    
    protected FileItemSync.ActualPathSync fileItem_actualPath;
    
    public static class NameSync extends SingleValuedAttributeSynchronizer<File, String, FileItem, String, Context> {
      public NameSync() {
        super(edu.ustb.sei.mde.mobile.file.FilePackage.eINSTANCE.getFileItem_Name());
        this.setConstructorParameter(true);
      }
      
      public String externalGet(final File s, final Context c) {
        return s.getName();
      }
      
      public void externalSet(final File source, final String value, final FileItem view, final String viewValue, final Context context) {
        // no definition
      }
      
      public void configureInformation(final Information info) {
        InformationKind kind = info.getKind();
        super.configureInformation(info);
      }
    }
    
    public static class ActualPathSync extends SingleValuedAttributeSynchronizer<File, String, FileItem, String, Context> {
      public ActualPathSync() {
        super(edu.ustb.sei.mde.mobile.file.FilePackage.eINSTANCE.getFileItem_ActualPath());
        this.setConstructorParameter(true);
      }
      
      public String externalGet(final File s, final Context c) {
        try {
          return s.getCanonicalPath();
        } catch (Throwable _e) {
          throw Exceptions.sneakyThrow(_e);
        }
      }
      
      public String dynamicCompute(final FileItem v, final Context c) {
        return FileModelSynchronizer.generatePath(v);
      }
      
      public void externalSet(final File source, final String value, final FileItem view, final String viewValue, final Context context) {
        // no definition
      }
      
      public void configureInformation(final Information info) {
        InformationKind kind = info.getKind();
        super.configureInformation(info);
      }
    }
    
    public ElementSynchronizer<File, FileItem, Context> getValueSynchronizerForGet(final File s) {
      ElementSynchronizer elementSynchronizer = null;
      if((elementSynchronizer=(ElementSynchronizer)((FileModelSynchronizer)this.container).file).canHandle(s)) 
          return elementSynchronizer;
      if((elementSynchronizer=(ElementSynchronizer)((FileModelSynchronizer)this.container).folder).canHandle(s)) 
          return elementSynchronizer;
      return null;
    }
    
    public ElementSynchronizer<File, FileItem, Context> getValueSynchronizerForPut(final FileItem v) {
      ElementSynchronizer elementSynchronizer = null;
      if((elementSynchronizer=(ElementSynchronizer)((FileModelSynchronizer)this.container).file).canHandle(v)) 
        return elementSynchronizer;
      if((elementSynchronizer=(ElementSynchronizer)((FileModelSynchronizer)this.container).folder).canHandle(v)) 
        return elementSynchronizer;
      return null;
    }
  }
  
  public FileModelSynchronizer() {
    super();
    // create element synchronizers
    this.folder = new FolderSync();
    this.folder_children = new FolderSync.ChildrenSync();
    this.addElement(this.folder);
    this.file = new FileSync();
    this.file_size = new FileSync.SizeSync();
    this.addElement(this.file);
    this.fileItem = new FileItemSync();
    this.fileItem_name = new FileItemSync.NameSync();
    this.fileItem_actualPath = new FileItemSync.ActualPathSync();
    this.addElement(this.fileItem);
    // set feature synchronizers
    this.folder.addFeatureSynchronizer(this.folder_children);
    this.folder.addSuperFeatureSynchronizer(this.fileItem_name);
    this.folder.addSuperFeatureSynchronizer(this.fileItem_actualPath);
    this.file.addFeatureSynchronizer(this.file_size);
    this.file.addSuperFeatureSynchronizer(this.fileItem_name);
    this.file.addSuperFeatureSynchronizer(this.fileItem_actualPath);
    this.fileItem.addFeatureSynchronizer(this.fileItem_name);
    this.fileItem.addFeatureSynchronizer(this.fileItem_actualPath);
    // set feature value synchronizers
    this.folder_children.setValueSynchronizer(this.fileItem);
    this.file_size.setValueSynchronizer(edu.ustb.sei.mde.mobile.datastructure.synchronizer.Synchronizer.replace());
    this.fileItem_name.setValueSynchronizer(edu.ustb.sei.mde.mobile.datastructure.synchronizer.Synchronizer.replace());
    this.fileItem_actualPath.setValueSynchronizer(edu.ustb.sei.mde.mobile.datastructure.synchronizer.Synchronizer.replace());
    
    // set root
    this.root = this.folder;
  }
  
  public void initContext(final Context context) {
    context.put("tempDir", "/Volumes/Macintosh HD Data/temp/");
  }
  
  public OperationScheduler createScheduler(final Context context) {
    SourcePool pool = (SourcePool) context.get(Context.SOURCE_POOL);
    if(pool==null) return null;
    return new edu.ustb.sei.mde.mobile.datastructure.operation.DefaultOperationScheduler(pool);
  }
  
  @Override
  public Context createContext() {
    Context context = edu.ustb.sei.mde.mobile.datastructure.Context.newInstance();
    context.init();
    initContext(context);
    return context;
  }
  
  private FolderSync folder;
  
  private FolderSync.ChildrenSync folder_children;
  
  private FileSync file;
  
  private FileSync.SizeSync file_size;
  
  private FileItemSync fileItem;
  
  private FileItemSync.NameSync fileItem_name;
  
  private FileItemSync.ActualPathSync fileItem_actualPath;
  
  public static String generatePath(final FileItem f) {
    String _xifexpression = null;
    EObject _eContainer = f.eContainer();
    boolean _tripleEquals = (_eContainer == null);
    if (_tripleEquals) {
      String _xblockexpression = null;
      {
        final String parent = f.getActualPath().substring(0, f.getActualPath().lastIndexOf(File.separatorChar));
        String _name = f.getName();
        _xblockexpression = ((parent + Character.valueOf(File.separatorChar)) + _name);
      }
      _xifexpression = _xblockexpression;
    } else {
      EObject _eContainer_1 = f.eContainer();
      String _generatePath = FileModelSynchronizer.generatePath(((FileItem) _eContainer_1));
      String _plus = (_generatePath + Character.valueOf(File.separatorChar));
      String _name = f.getName();
      _xifexpression = (_plus + _name);
    }
    return _xifexpression;
  }
  
  public static String convertName(final String c, final String t) {
    String _replaceAll = c.replaceAll("[:\\\\/]", "_");
    return (t + _replaceAll);
  }
}
