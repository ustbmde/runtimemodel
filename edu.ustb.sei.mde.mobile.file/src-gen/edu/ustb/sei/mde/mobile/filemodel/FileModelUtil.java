package edu.ustb.sei.mde.mobile.filemodel;

import edu.ustb.sei.mde.mobile.datastructure.operation.ValueProvider;
import edu.ustb.sei.mde.mobile.filemodel.FileModelSynchronizer;

@SuppressWarnings("all")
public class FileModelUtil {
  public static FileModelSynchronizer instance = new edu.ustb.sei.mde.mobile.filemodel.FileModelSynchronizer();;
  
  public static void apply(final ValueProvider root) {
    edu.ustb.sei.mde.mobile.datastructure.operation.OperationScheduler s = instance.createScheduler();
    s.schedule(root);
    s.execute(root);
  }
}
