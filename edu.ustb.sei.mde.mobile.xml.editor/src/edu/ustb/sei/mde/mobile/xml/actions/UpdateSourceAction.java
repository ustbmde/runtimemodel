package edu.ustb.sei.mde.mobile.xml.actions;

import java.util.function.Supplier;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;

import edu.ustb.sei.mde.mobile.datastructure.operation.OperationScheduler;
import edu.ustb.sei.mde.mobile.datastructure.operation.ValueProvider;
import edu.ustb.sei.mde.mobile.xml.Document;
import edu.ustb.sei.mde.mobile.xml.util.XmlmodelUtil;

public class UpdateSourceAction extends Action {
	protected Supplier<Resource> resourceSupplier;

	public UpdateSourceAction(Supplier<Resource> resourceSupplier) {
		super("Update Source");
		this.resourceSupplier = resourceSupplier;
	}
	
	@Override
	public void run() {
		Resource resource = this.resourceSupplier.get();
		Document eDoc = (Document) resource.getContents().get(0);
		if(eDoc.getFilePath()==null) {
			
		} else {
			String path = eDoc.getFilePath();
			org.w3c.dom.Document jDoc = null;
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			try {
				DocumentBuilder builder = factory.newDocumentBuilder();
				jDoc = builder.parse(path);
			} catch(Exception e) {
				jDoc = null;
			}
			
			@SuppressWarnings("unchecked")
			ValueProvider<?,?,org.w3c.dom.Document> provider = XmlmodelUtil.instance.put(jDoc, eDoc);
			OperationScheduler s = XmlmodelUtil.instance.createScheduler();
			s.schedule(provider);
			s.execute(provider);
			
			try {
				TransformerFactory tff = TransformerFactory.newInstance();
				Transformer tf = tff.newTransformer();
				tf.setOutputProperty(OutputKeys.INDENT, "yes");
				tf.transform(new DOMSource(provider.getValue()), new StreamResult(path));
			} catch (Exception e) {
				MessageDialog.openInformation(null, "Exception in put",e.getMessage());
			}
		}
	}
}
