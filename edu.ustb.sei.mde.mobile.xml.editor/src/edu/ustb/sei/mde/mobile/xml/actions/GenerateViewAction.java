package edu.ustb.sei.mde.mobile.xml.actions;

import java.util.function.Supplier;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;

import edu.ustb.sei.mde.mobile.xml.Document;
import edu.ustb.sei.mde.mobile.xml.util.XmlmodelUtil;

public class GenerateViewAction extends Action {
	protected Supplier<Resource> resourceSupplier;

	public GenerateViewAction(Supplier<Resource> resourceSupplier) {
		super("Get View");
		this.resourceSupplier = resourceSupplier;
	}

	@Override
	public void run() {
		Resource resource = this.resourceSupplier.get();
		
		EObject root = null;
		if(resource.getContents().isEmpty()==false) root = resource.getContents().get(0);
		
		if(root==null || !(root instanceof Document)) {
			MessageDialog.openError(null, "Error in get", "This model bridge requires a root element with filePath");
		} else {
			String path = ((Document)root).getFilePath();
			
			try {
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				org.w3c.dom.Document document = builder.parse(path);
				@SuppressWarnings("unchecked")
				Document newRoot = (Document) XmlmodelUtil.instance.get(document);
				newRoot.setFilePath(path);
				resource.getContents().clear();
				resource.getContents().add(newRoot);
			} catch (Exception e) {
				MessageDialog.openInformation(null, "Exception in get",e.getMessage());
			}
		}
		
	}
}
