/**
 * generated by Xtext 2.14.0
 */
package edu.ustb.sei.mde.ide;

import com.google.inject.Guice;
import com.google.inject.Injector;
import edu.ustb.sei.mde.MobileRuntimeModule;
import edu.ustb.sei.mde.MobileStandaloneSetup;
import edu.ustb.sei.mde.ide.MobileIdeModule;
import org.eclipse.xtext.util.Modules2;

/**
 * Initialization support for running Xtext languages as language servers.
 */
@SuppressWarnings("all")
public class MobileIdeSetup extends MobileStandaloneSetup {
  @Override
  public Injector createInjector() {
    MobileRuntimeModule _mobileRuntimeModule = new MobileRuntimeModule();
    MobileIdeModule _mobileIdeModule = new MobileIdeModule();
    return Guice.createInjector(Modules2.mixin(_mobileRuntimeModule, _mobileIdeModule));
  }
}
