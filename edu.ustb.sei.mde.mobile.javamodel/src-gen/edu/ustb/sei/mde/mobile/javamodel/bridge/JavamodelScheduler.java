package edu.ustb.sei.mde.mobile.javamodel.bridge;

import edu.ustb.sei.mde.mobile.datastructure.operation.ContextPredicate;
import edu.ustb.sei.mde.mobile.datastructure.operation.DefaultOperationScheduler;
import edu.ustb.sei.mde.mobile.datastructure.operation.Information;
import edu.ustb.sei.mde.mobile.datastructure.operation.OperationOrder;
import edu.ustb.sei.mde.mobile.datastructure.operation.Pattern;
import edu.ustb.sei.mde.mobile.datastructure.operation.ScheduleRule;
import edu.ustb.sei.mde.mobile.datastructure.sourcepool.SourcePool;
import edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage;
import java.util.Map;
import org.eclipse.emf.ecore.EStructuralFeature;

@SuppressWarnings("all")
public class JavamodelScheduler extends DefaultOperationScheduler {
  public JavamodelScheduler(final SourcePool pool) {
    super(pool);
  }
  
  public static class Rule0 implements ScheduleRule {
    public static class Pred0 implements ContextPredicate {
      public boolean check(final Map<String, Object> cont) {
        Object _get = cont.get("laterFeature");
        final EStructuralFeature laterFeature = ((EStructuralFeature) _get);
        return (laterFeature == JavamodelPackage.Literals.METHOD__PARAMETERS);
      }
    }
    
    public Rule0(final DefaultOperationScheduler scheduler) {
      this.formerPattern = new edu.ustb.sei.mde.mobile.datastructure.operation.Pattern(new edu.ustb.sei.mde.mobile.datastructure.operation.InformationKind[] {edu.ustb.sei.mde.mobile.datastructure.operation.InformationKind.set}, edu.ustb.sei.mde.mobile.datastructure.operation.FeatureType.attribute, new edu.ustb.sei.mde.mobile.datastructure.Pair[] {edu.ustb.sei.mde.mobile.datastructure.Pair.of("formerFeature", "Feature"),edu.ustb.sei.mde.mobile.datastructure.Pair.of("formerSource", "SourceObject")});
      this.laterPattern = new edu.ustb.sei.mde.mobile.datastructure.operation.Pattern(new edu.ustb.sei.mde.mobile.datastructure.operation.InformationKind[] {edu.ustb.sei.mde.mobile.datastructure.operation.InformationKind.set}, edu.ustb.sei.mde.mobile.datastructure.operation.FeatureType.containment, new edu.ustb.sei.mde.mobile.datastructure.Pair[] {edu.ustb.sei.mde.mobile.datastructure.Pair.of("laterFeature", "Feature"),edu.ustb.sei.mde.mobile.datastructure.Pair.of("laterSource", "SourceObject")});
      this.order=edu.ustb.sei.mde.mobile.datastructure.operation.OperationOrder.former;
      this.predicate=edu.ustb.sei.mde.mobile.datastructure.operation.ContextPredicate.and(new edu.ustb.sei.mde.mobile.datastructure.operation.IdentityPredicate("formerSource", "laterSource"), new Pred0());
    }
    
    private Pattern formerPattern;
    
    private Pattern laterPattern;
    
    private OperationOrder order;
    
    private ContextPredicate predicate;
    
    public boolean checkLeftPredicate(final Information info, final Map<String, Object> map) {
      return this.formerPattern.check(info, map);
    }
    
    public boolean checkRightPredicate(final Information info, final Map<String, Object> map) {
      return this.laterPattern.check(info, map);
    }
    
    public boolean checkBinaryPredicate(final Map<String, Object> map) {
      return this.predicate.check(map);
    }
    
    public OperationOrder getImplication() {
      return this.order;
    }
  }
  
  protected void buildUserRules() {
    edu.ustb.sei.mde.mobile.datastructure.operation.ScheduleRule rule0 = new Rule0(this);
    this.semanticRules.add(rule0);
  }
}
