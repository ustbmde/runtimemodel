package edu.ustb.sei.mde.mobile.javamodel.bridge;

import edu.ustb.sei.mde.mobile.datastructure.operation.ValueProvider;
import edu.ustb.sei.mde.mobile.javamodel.bridge.JavamodelModelSynchronizer;

@SuppressWarnings("all")
public class JavamodelModelUtil {
  public static JavamodelModelSynchronizer instance = new edu.ustb.sei.mde.mobile.javamodel.bridge.JavamodelModelSynchronizer();;
  
  public static void apply(final ValueProvider root) {
    edu.ustb.sei.mde.mobile.datastructure.operation.OperationScheduler s = instance.createScheduler();
    s.schedule(root);
    s.execute(root);
  }
}
