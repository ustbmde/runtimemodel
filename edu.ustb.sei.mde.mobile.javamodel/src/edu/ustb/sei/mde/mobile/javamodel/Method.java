/**
 */
package edu.ustb.sei.mde.mobile.javamodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Method</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.Method#getReturnType <em>Return Type</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.Method#getExceptionTypes <em>Exception Types</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.Method#getParameters <em>Parameters</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getMethod()
 * @model
 * @generated
 */
public interface Method extends JavaElement, Annotatable, Member {
	/**
	 * Returns the value of the '<em><b>Return Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Return Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Type</em>' attribute.
	 * @see #setReturnType(String)
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getMethod_ReturnType()
	 * @model
	 * @generated
	 */
	String getReturnType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mobile.javamodel.Method#getReturnType <em>Return Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Return Type</em>' attribute.
	 * @see #getReturnType()
	 * @generated
	 */
	void setReturnType(String value);

	/**
	 * Returns the value of the '<em><b>Exception Types</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exception Types</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exception Types</em>' attribute list.
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getMethod_ExceptionTypes()
	 * @model
	 * @generated
	 */
	EList<String> getExceptionTypes();

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.mobile.javamodel.LocalVariable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getMethod_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<LocalVariable> getParameters();

} // Method
