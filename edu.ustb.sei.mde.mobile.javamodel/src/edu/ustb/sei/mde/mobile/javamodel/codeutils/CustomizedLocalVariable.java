package edu.ustb.sei.mde.mobile.javamodel.codeutils;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.jdt.core.IAnnotation;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaModel;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.ILocalVariable;
import org.eclipse.jdt.core.IMember;
import org.eclipse.jdt.core.IOpenable;
import org.eclipse.jdt.core.ISourceRange;
import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.JavaModelException;

public class CustomizedLocalVariable implements ILocalVariable {

	@Override
	public boolean exists() {
		return false;
	}

	@Override
	public IJavaElement getAncestor(int ancestorType) {
		return null;
	}

	@Override
	public String getAttachedJavadoc(IProgressMonitor monitor) throws JavaModelException {
		return null;
	}

	@Override
	public IResource getCorrespondingResource() throws JavaModelException {
		return null;
	}

	@Override
	public int getElementType() {
		return IJavaElement.LOCAL_VARIABLE;
	}

	@Override
	public String getHandleIdentifier() {
		return null;
	}

	@Override
	public IJavaModel getJavaModel() {
		return null;
	}

	@Override
	public IJavaProject getJavaProject() {
		return null;
	}

	@Override
	public IOpenable getOpenable() {
		return null;
	}

	public CustomizedLocalVariable(IJavaElement parent, String elementName, String typeSignature, int flags) {
		super();
		this.parent = parent;
		this.elementName = elementName;
		this.typeSignature = typeSignature;
		this.flags = flags;
	}

	private IJavaElement parent;

	@Override
	public IJavaElement getParent() {
		return parent;
	}

	@Override
	public IPath getPath() {
		return null;
	}

	@Override
	public IJavaElement getPrimaryElement() {
		return null;
	}

	@Override
	public IResource getResource() {
		return null;
	}

	@Override
	public ISchedulingRule getSchedulingRule() {
		return null;
	}

	@Override
	public IResource getUnderlyingResource() throws JavaModelException {
		return null;
	}

	@Override
	public boolean isReadOnly() {
		return false;
	}

	@Override
	public boolean isStructureKnown() throws JavaModelException {
		return false;
	}

	@Override
	public <T> T getAdapter(Class<T> adapter) {
		return null;
	}

	@Override
	public String getSource() throws JavaModelException {
		return null;
	}

	@Override
	public ISourceRange getSourceRange() throws JavaModelException {
		return null;
	}

	@Override
	public IAnnotation getAnnotation(String name) {
		return null;
	}

	@Override
	public IAnnotation[] getAnnotations() throws JavaModelException {
		return null;
	}

	private String elementName;
	@Override
	public String getElementName() {
		return elementName;
	}

	@Override
	public ISourceRange getNameRange() {
		return null;
	}

	private String typeSignature;
	@Override
	public String getTypeSignature() {
		return typeSignature;
	}

	@Override
	public boolean isParameter() {
		return false;
	}

	private int flags;
	@Override
	public int getFlags() {
		return flags;
	}

	@Override
	public IMember getDeclaringMember() {
		return null;
	}

	@Override
	public ITypeRoot getTypeRoot() {
		return null;
	}

}
