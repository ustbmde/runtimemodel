/**
 */
package edu.ustb.sei.mde.mobile.javamodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.Type#getFields <em>Fields</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.Type#getMethods <em>Methods</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.Type#getSuperType <em>Super Type</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.Type#getSuperInterfaces <em>Super Interfaces</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getType()
 * @model
 * @generated
 */
public interface Type extends JavaElement, Annotatable, Member {
	/**
	 * Returns the value of the '<em><b>Fields</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.mobile.javamodel.Field}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fields</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fields</em>' containment reference list.
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getType_Fields()
	 * @model containment="true"
	 * @generated
	 */
	EList<Field> getFields();

	/**
	 * Returns the value of the '<em><b>Methods</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.mobile.javamodel.Method}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Methods</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Methods</em>' containment reference list.
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getType_Methods()
	 * @model containment="true"
	 * @generated
	 */
	EList<Method> getMethods();

	/**
	 * Returns the value of the '<em><b>Super Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Super Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Super Type</em>' attribute.
	 * @see #setSuperType(String)
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getType_SuperType()
	 * @model
	 * @generated
	 */
	String getSuperType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mobile.javamodel.Type#getSuperType <em>Super Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Super Type</em>' attribute.
	 * @see #getSuperType()
	 * @generated
	 */
	void setSuperType(String value);

	/**
	 * Returns the value of the '<em><b>Super Interfaces</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Super Interfaces</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Super Interfaces</em>' attribute list.
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getType_SuperInterfaces()
	 * @model
	 * @generated
	 */
	EList<String> getSuperInterfaces();

} // Type
