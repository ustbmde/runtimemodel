/**
 */
package edu.ustb.sei.mde.mobile.javamodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Project</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.JavaProject#getPackageFragmentRoots <em>Package Fragment Roots</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getJavaProject()
 * @model
 * @generated
 */
public interface JavaProject extends JavaElement {
	/**
	 * Returns the value of the '<em><b>Package Fragment Roots</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.mobile.javamodel.PackageFragmentRoot}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Fragment Roots</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Fragment Roots</em>' containment reference list.
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getJavaProject_PackageFragmentRoots()
	 * @model containment="true"
	 * @generated
	 */
	EList<PackageFragmentRoot> getPackageFragmentRoots();

} // JavaProject
