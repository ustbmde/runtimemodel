/**
 */
package edu.ustb.sei.mde.mobile.javamodel.impl;

import edu.ustb.sei.mde.mobile.javamodel.JavaModel;
import edu.ustb.sei.mde.mobile.javamodel.JavaProject;
import edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Java Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.impl.JavaModelImpl#getJavaProjects <em>Java Projects</em>}</li>
 * </ul>
 *
 * @generated
 */
public class JavaModelImpl extends MinimalEObjectImpl.Container implements JavaModel {
	/**
	 * The cached value of the '{@link #getJavaProjects() <em>Java Projects</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJavaProjects()
	 * @generated
	 * @ordered
	 */
	protected EList<JavaProject> javaProjects;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JavaModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JavamodelPackage.Literals.JAVA_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<JavaProject> getJavaProjects() {
		if (javaProjects == null) {
			javaProjects = new EObjectContainmentEList<JavaProject>(JavaProject.class, this, JavamodelPackage.JAVA_MODEL__JAVA_PROJECTS);
		}
		return javaProjects;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JavamodelPackage.JAVA_MODEL__JAVA_PROJECTS:
				return ((InternalEList<?>)getJavaProjects()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JavamodelPackage.JAVA_MODEL__JAVA_PROJECTS:
				return getJavaProjects();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JavamodelPackage.JAVA_MODEL__JAVA_PROJECTS:
				getJavaProjects().clear();
				getJavaProjects().addAll((Collection<? extends JavaProject>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JavamodelPackage.JAVA_MODEL__JAVA_PROJECTS:
				getJavaProjects().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JavamodelPackage.JAVA_MODEL__JAVA_PROJECTS:
				return javaProjects != null && !javaProjects.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //JavaModelImpl
