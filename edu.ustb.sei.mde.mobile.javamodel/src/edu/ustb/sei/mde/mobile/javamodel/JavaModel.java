/**
 */
package edu.ustb.sei.mde.mobile.javamodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Java Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.JavaModel#getJavaProjects <em>Java Projects</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getJavaModel()
 * @model
 * @generated
 */
public interface JavaModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Java Projects</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.mobile.javamodel.JavaProject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Java Projects</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Java Projects</em>' containment reference list.
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getJavaModel_JavaProjects()
	 * @model containment="true"
	 * @generated
	 */
	EList<JavaProject> getJavaProjects();

} // JavaModel
