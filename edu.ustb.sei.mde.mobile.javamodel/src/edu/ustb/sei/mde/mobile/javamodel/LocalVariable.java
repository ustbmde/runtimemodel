/**
 */
package edu.ustb.sei.mde.mobile.javamodel;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Local Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.LocalVariable#getType <em>Type</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.LocalVariable#getModifiers <em>Modifiers</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getLocalVariable()
 * @model
 * @generated
 */
public interface LocalVariable extends JavaElement, Annotatable {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getLocalVariable_Type()
	 * @model
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mobile.javamodel.LocalVariable#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

	/**
	 * Returns the value of the '<em><b>Modifiers</b></em>' attribute list.
	 * The list contents are of type {@link edu.ustb.sei.mde.mobile.javamodel.Modifier}.
	 * The literals are from the enumeration {@link edu.ustb.sei.mde.mobile.javamodel.Modifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Modifiers</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Modifiers</em>' attribute list.
	 * @see edu.ustb.sei.mde.mobile.javamodel.Modifier
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getLocalVariable_Modifiers()
	 * @model
	 * @generated
	 */
	EList<Modifier> getModifiers();

} // LocalVariable
