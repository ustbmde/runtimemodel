/**
 */
package edu.ustb.sei.mde.mobile.javamodel.impl;

import edu.ustb.sei.mde.mobile.javamodel.Annotatable;
import edu.ustb.sei.mde.mobile.javamodel.Annotation;
import edu.ustb.sei.mde.mobile.javamodel.Field;
import edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage;
import edu.ustb.sei.mde.mobile.javamodel.Member;
import edu.ustb.sei.mde.mobile.javamodel.Method;
import edu.ustb.sei.mde.mobile.javamodel.Modifier;
import edu.ustb.sei.mde.mobile.javamodel.Type;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.impl.TypeImpl#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.impl.TypeImpl#getModifiers <em>Modifiers</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.impl.TypeImpl#getJavadoc <em>Javadoc</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.impl.TypeImpl#getFields <em>Fields</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.impl.TypeImpl#getMethods <em>Methods</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.impl.TypeImpl#getSuperType <em>Super Type</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.impl.TypeImpl#getSuperInterfaces <em>Super Interfaces</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TypeImpl extends JavaElementImpl implements Type {
	/**
	 * The cached value of the '{@link #getAnnotations() <em>Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> annotations;

	/**
	 * The cached value of the '{@link #getModifiers() <em>Modifiers</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModifiers()
	 * @generated
	 * @ordered
	 */
	protected EList<Modifier> modifiers;

	/**
	 * The default value of the '{@link #getJavadoc() <em>Javadoc</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJavadoc()
	 * @generated
	 * @ordered
	 */
	protected static final String JAVADOC_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getJavadoc() <em>Javadoc</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJavadoc()
	 * @generated
	 * @ordered
	 */
	protected String javadoc = JAVADOC_EDEFAULT;

	/**
	 * The cached value of the '{@link #getFields() <em>Fields</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFields()
	 * @generated
	 * @ordered
	 */
	protected EList<Field> fields;

	/**
	 * The cached value of the '{@link #getMethods() <em>Methods</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMethods()
	 * @generated
	 * @ordered
	 */
	protected EList<Method> methods;

	/**
	 * The default value of the '{@link #getSuperType() <em>Super Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuperType()
	 * @generated
	 * @ordered
	 */
	protected static final String SUPER_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSuperType() <em>Super Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuperType()
	 * @generated
	 * @ordered
	 */
	protected String superType = SUPER_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSuperInterfaces() <em>Super Interfaces</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuperInterfaces()
	 * @generated
	 * @ordered
	 */
	protected EList<String> superInterfaces;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JavamodelPackage.Literals.TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getAnnotations() {
		if (annotations == null) {
			annotations = new EObjectContainmentEList<Annotation>(Annotation.class, this, JavamodelPackage.TYPE__ANNOTATIONS);
		}
		return annotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Modifier> getModifiers() {
		if (modifiers == null) {
			modifiers = new EDataTypeUniqueEList<Modifier>(Modifier.class, this, JavamodelPackage.TYPE__MODIFIERS);
		}
		return modifiers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getJavadoc() {
		return javadoc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setJavadoc(String newJavadoc) {
		String oldJavadoc = javadoc;
		javadoc = newJavadoc;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JavamodelPackage.TYPE__JAVADOC, oldJavadoc, javadoc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Field> getFields() {
		if (fields == null) {
			fields = new EObjectContainmentEList<Field>(Field.class, this, JavamodelPackage.TYPE__FIELDS);
		}
		return fields;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Method> getMethods() {
		if (methods == null) {
			methods = new EObjectContainmentEList<Method>(Method.class, this, JavamodelPackage.TYPE__METHODS);
		}
		return methods;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSuperType() {
		return superType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuperType(String newSuperType) {
		String oldSuperType = superType;
		superType = newSuperType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JavamodelPackage.TYPE__SUPER_TYPE, oldSuperType, superType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getSuperInterfaces() {
		if (superInterfaces == null) {
			superInterfaces = new EDataTypeUniqueEList<String>(String.class, this, JavamodelPackage.TYPE__SUPER_INTERFACES);
		}
		return superInterfaces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JavamodelPackage.TYPE__ANNOTATIONS:
				return ((InternalEList<?>)getAnnotations()).basicRemove(otherEnd, msgs);
			case JavamodelPackage.TYPE__FIELDS:
				return ((InternalEList<?>)getFields()).basicRemove(otherEnd, msgs);
			case JavamodelPackage.TYPE__METHODS:
				return ((InternalEList<?>)getMethods()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JavamodelPackage.TYPE__ANNOTATIONS:
				return getAnnotations();
			case JavamodelPackage.TYPE__MODIFIERS:
				return getModifiers();
			case JavamodelPackage.TYPE__JAVADOC:
				return getJavadoc();
			case JavamodelPackage.TYPE__FIELDS:
				return getFields();
			case JavamodelPackage.TYPE__METHODS:
				return getMethods();
			case JavamodelPackage.TYPE__SUPER_TYPE:
				return getSuperType();
			case JavamodelPackage.TYPE__SUPER_INTERFACES:
				return getSuperInterfaces();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JavamodelPackage.TYPE__ANNOTATIONS:
				getAnnotations().clear();
				getAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
			case JavamodelPackage.TYPE__MODIFIERS:
				getModifiers().clear();
				getModifiers().addAll((Collection<? extends Modifier>)newValue);
				return;
			case JavamodelPackage.TYPE__JAVADOC:
				setJavadoc((String)newValue);
				return;
			case JavamodelPackage.TYPE__FIELDS:
				getFields().clear();
				getFields().addAll((Collection<? extends Field>)newValue);
				return;
			case JavamodelPackage.TYPE__METHODS:
				getMethods().clear();
				getMethods().addAll((Collection<? extends Method>)newValue);
				return;
			case JavamodelPackage.TYPE__SUPER_TYPE:
				setSuperType((String)newValue);
				return;
			case JavamodelPackage.TYPE__SUPER_INTERFACES:
				getSuperInterfaces().clear();
				getSuperInterfaces().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JavamodelPackage.TYPE__ANNOTATIONS:
				getAnnotations().clear();
				return;
			case JavamodelPackage.TYPE__MODIFIERS:
				getModifiers().clear();
				return;
			case JavamodelPackage.TYPE__JAVADOC:
				setJavadoc(JAVADOC_EDEFAULT);
				return;
			case JavamodelPackage.TYPE__FIELDS:
				getFields().clear();
				return;
			case JavamodelPackage.TYPE__METHODS:
				getMethods().clear();
				return;
			case JavamodelPackage.TYPE__SUPER_TYPE:
				setSuperType(SUPER_TYPE_EDEFAULT);
				return;
			case JavamodelPackage.TYPE__SUPER_INTERFACES:
				getSuperInterfaces().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JavamodelPackage.TYPE__ANNOTATIONS:
				return annotations != null && !annotations.isEmpty();
			case JavamodelPackage.TYPE__MODIFIERS:
				return modifiers != null && !modifiers.isEmpty();
			case JavamodelPackage.TYPE__JAVADOC:
				return JAVADOC_EDEFAULT == null ? javadoc != null : !JAVADOC_EDEFAULT.equals(javadoc);
			case JavamodelPackage.TYPE__FIELDS:
				return fields != null && !fields.isEmpty();
			case JavamodelPackage.TYPE__METHODS:
				return methods != null && !methods.isEmpty();
			case JavamodelPackage.TYPE__SUPER_TYPE:
				return SUPER_TYPE_EDEFAULT == null ? superType != null : !SUPER_TYPE_EDEFAULT.equals(superType);
			case JavamodelPackage.TYPE__SUPER_INTERFACES:
				return superInterfaces != null && !superInterfaces.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Annotatable.class) {
			switch (derivedFeatureID) {
				case JavamodelPackage.TYPE__ANNOTATIONS: return JavamodelPackage.ANNOTATABLE__ANNOTATIONS;
				default: return -1;
			}
		}
		if (baseClass == Member.class) {
			switch (derivedFeatureID) {
				case JavamodelPackage.TYPE__MODIFIERS: return JavamodelPackage.MEMBER__MODIFIERS;
				case JavamodelPackage.TYPE__JAVADOC: return JavamodelPackage.MEMBER__JAVADOC;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Annotatable.class) {
			switch (baseFeatureID) {
				case JavamodelPackage.ANNOTATABLE__ANNOTATIONS: return JavamodelPackage.TYPE__ANNOTATIONS;
				default: return -1;
			}
		}
		if (baseClass == Member.class) {
			switch (baseFeatureID) {
				case JavamodelPackage.MEMBER__MODIFIERS: return JavamodelPackage.TYPE__MODIFIERS;
				case JavamodelPackage.MEMBER__JAVADOC: return JavamodelPackage.TYPE__JAVADOC;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (modifiers: ");
		result.append(modifiers);
		result.append(", javadoc: ");
		result.append(javadoc);
		result.append(", superType: ");
		result.append(superType);
		result.append(", superInterfaces: ");
		result.append(superInterfaces);
		result.append(')');
		return result.toString();
	}

} //TypeImpl
