/**
 */
package edu.ustb.sei.mde.mobile.javamodel;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Modifier</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getModifier()
 * @model
 * @generated
 */
public enum Modifier implements Enumerator {
	/**
	 * The '<em><b>Acc Abstract</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACC_ABSTRACT_VALUE
	 * @generated
	 * @ordered
	 */
	ACC_ABSTRACT(1024, "AccAbstract", "AccAbstract"),

	/**
	 * The '<em><b>Acc Annotation</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACC_ANNOTATION_VALUE
	 * @generated
	 * @ordered
	 */
	ACC_ANNOTATION(8192, "AccAnnotation", "AccAnnotation"),

	/**
	 * The '<em><b>Acc Annotation Default</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACC_ANNOTATION_DEFAULT_VALUE
	 * @generated
	 * @ordered
	 */
	ACC_ANNOTATION_DEFAULT(131072, "AccAnnotationDefault", "AccAnnotationDefault"),

	/**
	 * The '<em><b>Acc Default</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACC_DEFAULT_VALUE
	 * @generated
	 * @ordered
	 */
	ACC_DEFAULT(0, "AccDefault", "AccDefault"),

	/**
	 * The '<em><b>Acc Default Method</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACC_DEFAULT_METHOD_VALUE
	 * @generated
	 * @ordered
	 */
	ACC_DEFAULT_METHOD(65536, "AccDefaultMethod", "AccDefaultMethod"),

	/**
	 * The '<em><b>Acc Deprecated</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACC_DEPRECATED_VALUE
	 * @generated
	 * @ordered
	 */
	ACC_DEPRECATED(1048576, "AccDeprecated", "AccDeprecated"),

	/**
	 * The '<em><b>Acc Enum</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACC_ENUM_VALUE
	 * @generated
	 * @ordered
	 */
	ACC_ENUM(16384, "AccEnum", "AccEnum"),

	/**
	 * The '<em><b>Acc Final</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACC_FINAL_VALUE
	 * @generated
	 * @ordered
	 */
	ACC_FINAL(16, "AccFinal", "AccFinal"),

	/**
	 * The '<em><b>Acc Interface</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACC_INTERFACE_VALUE
	 * @generated
	 * @ordered
	 */
	ACC_INTERFACE(512, "AccInterface", "AccInterface"),

	/**
	 * The '<em><b>Acc Module</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACC_MODULE_VALUE
	 * @generated
	 * @ordered
	 */
	ACC_MODULE(32768, "AccModule", "AccModule"),

	/**
	 * The '<em><b>Acc Native</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACC_NATIVE_VALUE
	 * @generated
	 * @ordered
	 */
	ACC_NATIVE(256, "AccNative", "AccNative"),

	/**
	 * The '<em><b>Acc Private</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACC_PRIVATE_VALUE
	 * @generated
	 * @ordered
	 */
	ACC_PRIVATE(2, "AccPrivate", "AccPrivate"),

	/**
	 * The '<em><b>Acc Protected</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACC_PROTECTED_VALUE
	 * @generated
	 * @ordered
	 */
	ACC_PROTECTED(4, "AccProtected", "AccProtected"),

	/**
	 * The '<em><b>Acc Public</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACC_PUBLIC_VALUE
	 * @generated
	 * @ordered
	 */
	ACC_PUBLIC(1, "AccPublic", "AccPublic"),

	/**
	 * The '<em><b>Acc Static</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACC_STATIC_VALUE
	 * @generated
	 * @ordered
	 */
	ACC_STATIC(8, "AccStatic", "AccStatic"),

	/**
	 * The '<em><b>Acc Super Or Synchronized</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACC_SUPER_OR_SYNCHRONIZED_VALUE
	 * @generated
	 * @ordered
	 */
	ACC_SUPER_OR_SYNCHRONIZED(32, "AccSuperOrSynchronized", "AccSuperOrSynchronized"), /**
	 * The '<em><b>Acc Transient Or Varargs</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACC_TRANSIENT_OR_VARARGS_VALUE
	 * @generated
	 * @ordered
	 */
	ACC_TRANSIENT_OR_VARARGS(128, "AccTransientOrVarargs", "AccTransientOrVarargs"), /**
	 * The '<em><b>Acc Volatile</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACC_VOLATILE_VALUE
	 * @generated
	 * @ordered
	 */
	ACC_VOLATILE(64, "AccVolatile", "AccVolatile");

	/**
	 * The '<em><b>Acc Abstract</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Acc Abstract</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACC_ABSTRACT
	 * @model name="AccAbstract"
	 * @generated
	 * @ordered
	 */
	public static final int ACC_ABSTRACT_VALUE = 1024;

	/**
	 * The '<em><b>Acc Annotation</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Acc Annotation</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACC_ANNOTATION
	 * @model name="AccAnnotation"
	 * @generated
	 * @ordered
	 */
	public static final int ACC_ANNOTATION_VALUE = 8192;

	/**
	 * The '<em><b>Acc Annotation Default</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Acc Annotation Default</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACC_ANNOTATION_DEFAULT
	 * @model name="AccAnnotationDefault"
	 * @generated
	 * @ordered
	 */
	public static final int ACC_ANNOTATION_DEFAULT_VALUE = 131072;

	/**
	 * The '<em><b>Acc Default</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Acc Default</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACC_DEFAULT
	 * @model name="AccDefault"
	 * @generated
	 * @ordered
	 */
	public static final int ACC_DEFAULT_VALUE = 0;

	/**
	 * The '<em><b>Acc Default Method</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Acc Default Method</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACC_DEFAULT_METHOD
	 * @model name="AccDefaultMethod"
	 * @generated
	 * @ordered
	 */
	public static final int ACC_DEFAULT_METHOD_VALUE = 65536;

	/**
	 * The '<em><b>Acc Deprecated</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Acc Deprecated</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACC_DEPRECATED
	 * @model name="AccDeprecated"
	 * @generated
	 * @ordered
	 */
	public static final int ACC_DEPRECATED_VALUE = 1048576;

	/**
	 * The '<em><b>Acc Enum</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Acc Enum</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACC_ENUM
	 * @model name="AccEnum"
	 * @generated
	 * @ordered
	 */
	public static final int ACC_ENUM_VALUE = 16384;

	/**
	 * The '<em><b>Acc Final</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Acc Final</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACC_FINAL
	 * @model name="AccFinal"
	 * @generated
	 * @ordered
	 */
	public static final int ACC_FINAL_VALUE = 16;

	/**
	 * The '<em><b>Acc Interface</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Acc Interface</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACC_INTERFACE
	 * @model name="AccInterface"
	 * @generated
	 * @ordered
	 */
	public static final int ACC_INTERFACE_VALUE = 512;

	/**
	 * The '<em><b>Acc Module</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Acc Module</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACC_MODULE
	 * @model name="AccModule"
	 * @generated
	 * @ordered
	 */
	public static final int ACC_MODULE_VALUE = 32768;

	/**
	 * The '<em><b>Acc Native</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Acc Native</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACC_NATIVE
	 * @model name="AccNative"
	 * @generated
	 * @ordered
	 */
	public static final int ACC_NATIVE_VALUE = 256;

	/**
	 * The '<em><b>Acc Private</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Acc Private</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACC_PRIVATE
	 * @model name="AccPrivate"
	 * @generated
	 * @ordered
	 */
	public static final int ACC_PRIVATE_VALUE = 2;

	/**
	 * The '<em><b>Acc Protected</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Acc Protected</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACC_PROTECTED
	 * @model name="AccProtected"
	 * @generated
	 * @ordered
	 */
	public static final int ACC_PROTECTED_VALUE = 4;

	/**
	 * The '<em><b>Acc Public</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Acc Public</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACC_PUBLIC
	 * @model name="AccPublic"
	 * @generated
	 * @ordered
	 */
	public static final int ACC_PUBLIC_VALUE = 1;

	/**
	 * The '<em><b>Acc Static</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Acc Static</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACC_STATIC
	 * @model name="AccStatic"
	 * @generated
	 * @ordered
	 */
	public static final int ACC_STATIC_VALUE = 8;

	/**
	 * The '<em><b>Acc Super Or Synchronized</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Acc Super Or Synchronized</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACC_SUPER_OR_SYNCHRONIZED
	 * @model name="AccSuperOrSynchronized"
	 * @generated
	 * @ordered
	 */
	public static final int ACC_SUPER_OR_SYNCHRONIZED_VALUE = 32;

	/**
	 * The '<em><b>Acc Transient Or Varargs</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Acc Transient Or Varargs</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACC_TRANSIENT_OR_VARARGS
	 * @model name="AccTransientOrVarargs"
	 * @generated
	 * @ordered
	 */
	public static final int ACC_TRANSIENT_OR_VARARGS_VALUE = 128;

	/**
	 * The '<em><b>Acc Volatile</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Acc Volatile</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACC_VOLATILE
	 * @model name="AccVolatile"
	 * @generated
	 * @ordered
	 */
	public static final int ACC_VOLATILE_VALUE = 64;

	/**
	 * An array of all the '<em><b>Modifier</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final Modifier[] VALUES_ARRAY =
		new Modifier[] {
			ACC_ABSTRACT,
			ACC_ANNOTATION,
			ACC_ANNOTATION_DEFAULT,
			ACC_DEFAULT,
			ACC_DEFAULT_METHOD,
			ACC_DEPRECATED,
			ACC_ENUM,
			ACC_FINAL,
			ACC_INTERFACE,
			ACC_MODULE,
			ACC_NATIVE,
			ACC_PRIVATE,
			ACC_PROTECTED,
			ACC_PUBLIC,
			ACC_STATIC,
			ACC_SUPER_OR_SYNCHRONIZED,
			ACC_TRANSIENT_OR_VARARGS,
			ACC_VOLATILE,
		};

	/**
	 * A public read-only list of all the '<em><b>Modifier</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<Modifier> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Modifier</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Modifier get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Modifier result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Modifier</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Modifier getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			Modifier result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Modifier</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static Modifier get(int value) {
		switch (value) {
			case ACC_ABSTRACT_VALUE: return ACC_ABSTRACT;
			case ACC_ANNOTATION_VALUE: return ACC_ANNOTATION;
			case ACC_ANNOTATION_DEFAULT_VALUE: return ACC_ANNOTATION_DEFAULT;
			case ACC_DEFAULT_VALUE: return ACC_DEFAULT;
			case ACC_DEFAULT_METHOD_VALUE: return ACC_DEFAULT_METHOD;
			case ACC_DEPRECATED_VALUE: return ACC_DEPRECATED;
			case ACC_ENUM_VALUE: return ACC_ENUM;
			case ACC_FINAL_VALUE: return ACC_FINAL;
			case ACC_INTERFACE_VALUE: return ACC_INTERFACE;
			case ACC_MODULE_VALUE: return ACC_MODULE;
			case ACC_NATIVE_VALUE: return ACC_NATIVE;
			case ACC_PRIVATE_VALUE: return ACC_PRIVATE;
			case ACC_PROTECTED_VALUE: return ACC_PROTECTED;
			case ACC_PUBLIC_VALUE: return ACC_PUBLIC;
			case ACC_STATIC_VALUE: return ACC_STATIC;
			case ACC_SUPER_OR_SYNCHRONIZED_VALUE: return ACC_SUPER_OR_SYNCHRONIZED;
			case ACC_TRANSIENT_OR_VARARGS_VALUE: return ACC_TRANSIENT_OR_VARARGS;
			case ACC_VOLATILE_VALUE: return ACC_VOLATILE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private Modifier(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //Modifier
