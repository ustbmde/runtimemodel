/**
 */
package edu.ustb.sei.mde.mobile.javamodel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelFactory
 * @model kind="package"
 * @generated
 */
public interface JavamodelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "javamodel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.ustb.edu.cn/sei/mde/mobile/javamodel";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "javamodel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	JavamodelPackage eINSTANCE = edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl.init();

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.JavaElementImpl <em>Java Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavaElementImpl
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getJavaElement()
	 * @generated
	 */
	int JAVA_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_ELEMENT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_ELEMENT__PATH = 1;

	/**
	 * The number of structural features of the '<em>Java Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_ELEMENT_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Java Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.JavaModelImpl <em>Java Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavaModelImpl
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getJavaModel()
	 * @generated
	 */
	int JAVA_MODEL = 1;

	/**
	 * The feature id for the '<em><b>Java Projects</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_MODEL__JAVA_PROJECTS = 0;

	/**
	 * The number of structural features of the '<em>Java Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_MODEL_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Java Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.JavaProjectImpl <em>Java Project</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavaProjectImpl
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getJavaProject()
	 * @generated
	 */
	int JAVA_PROJECT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_PROJECT__NAME = JAVA_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_PROJECT__PATH = JAVA_ELEMENT__PATH;

	/**
	 * The feature id for the '<em><b>Package Fragment Roots</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_PROJECT__PACKAGE_FRAGMENT_ROOTS = JAVA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Java Project</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_PROJECT_FEATURE_COUNT = JAVA_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Java Project</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVA_PROJECT_OPERATION_COUNT = JAVA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.PackageFragmentRootImpl <em>Package Fragment Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.PackageFragmentRootImpl
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getPackageFragmentRoot()
	 * @generated
	 */
	int PACKAGE_FRAGMENT_ROOT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_FRAGMENT_ROOT__NAME = JAVA_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_FRAGMENT_ROOT__PATH = JAVA_ELEMENT__PATH;

	/**
	 * The feature id for the '<em><b>Package Fragments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_FRAGMENT_ROOT__PACKAGE_FRAGMENTS = JAVA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Package Fragment Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_FRAGMENT_ROOT_FEATURE_COUNT = JAVA_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Package Fragment Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_FRAGMENT_ROOT_OPERATION_COUNT = JAVA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.PackageFragmentImpl <em>Package Fragment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.PackageFragmentImpl
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getPackageFragment()
	 * @generated
	 */
	int PACKAGE_FRAGMENT = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_FRAGMENT__NAME = JAVA_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_FRAGMENT__PATH = JAVA_ELEMENT__PATH;

	/**
	 * The feature id for the '<em><b>Compilation Units</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_FRAGMENT__COMPILATION_UNITS = JAVA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Package Fragment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_FRAGMENT_FEATURE_COUNT = JAVA_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Package Fragment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PACKAGE_FRAGMENT_OPERATION_COUNT = JAVA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.CompilationUnitImpl <em>Compilation Unit</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.CompilationUnitImpl
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getCompilationUnit()
	 * @generated
	 */
	int COMPILATION_UNIT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPILATION_UNIT__NAME = JAVA_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPILATION_UNIT__PATH = JAVA_ELEMENT__PATH;

	/**
	 * The feature id for the '<em><b>Import Declarations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPILATION_UNIT__IMPORT_DECLARATIONS = JAVA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPILATION_UNIT__TYPES = JAVA_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Compilation Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPILATION_UNIT_FEATURE_COUNT = JAVA_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Compilation Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPILATION_UNIT_OPERATION_COUNT = JAVA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.ImportDeclarationImpl <em>Import Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.ImportDeclarationImpl
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getImportDeclaration()
	 * @generated
	 */
	int IMPORT_DECLARATION = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_DECLARATION__NAME = JAVA_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_DECLARATION__PATH = JAVA_ELEMENT__PATH;

	/**
	 * The number of structural features of the '<em>Import Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_DECLARATION_FEATURE_COUNT = JAVA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Import Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_DECLARATION_OPERATION_COUNT = JAVA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.TypeImpl <em>Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.TypeImpl
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getType()
	 * @generated
	 */
	int TYPE = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__NAME = JAVA_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__PATH = JAVA_ELEMENT__PATH;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__ANNOTATIONS = JAVA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__MODIFIERS = JAVA_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Javadoc</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__JAVADOC = JAVA_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Fields</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__FIELDS = JAVA_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Methods</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__METHODS = JAVA_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Super Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__SUPER_TYPE = JAVA_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Super Interfaces</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE__SUPER_INTERFACES = JAVA_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_FEATURE_COUNT = JAVA_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPE_OPERATION_COUNT = JAVA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.FieldImpl <em>Field</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.FieldImpl
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getField()
	 * @generated
	 */
	int FIELD = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__NAME = JAVA_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__PATH = JAVA_ELEMENT__PATH;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__ANNOTATIONS = JAVA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__MODIFIERS = JAVA_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Javadoc</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__JAVADOC = JAVA_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD__TYPE = JAVA_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_FEATURE_COUNT = JAVA_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Field</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FIELD_OPERATION_COUNT = JAVA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.MethodImpl <em>Method</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.MethodImpl
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getMethod()
	 * @generated
	 */
	int METHOD = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__NAME = JAVA_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__PATH = JAVA_ELEMENT__PATH;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__ANNOTATIONS = JAVA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__MODIFIERS = JAVA_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Javadoc</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__JAVADOC = JAVA_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Return Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__RETURN_TYPE = JAVA_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Exception Types</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__EXCEPTION_TYPES = JAVA_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD__PARAMETERS = JAVA_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Method</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_FEATURE_COUNT = JAVA_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Method</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METHOD_OPERATION_COUNT = JAVA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.LocalVariableImpl <em>Local Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.LocalVariableImpl
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getLocalVariable()
	 * @generated
	 */
	int LOCAL_VARIABLE = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_VARIABLE__NAME = JAVA_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_VARIABLE__PATH = JAVA_ELEMENT__PATH;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_VARIABLE__ANNOTATIONS = JAVA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_VARIABLE__TYPE = JAVA_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_VARIABLE__MODIFIERS = JAVA_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Local Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_VARIABLE_FEATURE_COUNT = JAVA_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Local Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCAL_VARIABLE_OPERATION_COUNT = JAVA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.AnnotationImpl <em>Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.AnnotationImpl
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getAnnotation()
	 * @generated
	 */
	int ANNOTATION = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__NAME = JAVA_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__PATH = JAVA_ELEMENT__PATH;

	/**
	 * The feature id for the '<em><b>Value Pairs</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION__VALUE_PAIRS = JAVA_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_FEATURE_COUNT = JAVA_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATION_OPERATION_COUNT = JAVA_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.AnnotatableImpl <em>Annotatable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.AnnotatableImpl
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getAnnotatable()
	 * @generated
	 */
	int ANNOTATABLE = 12;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATABLE__ANNOTATIONS = 0;

	/**
	 * The number of structural features of the '<em>Annotatable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATABLE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Annotatable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANNOTATABLE_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.MemberImpl <em>Member</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.MemberImpl
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getMember()
	 * @generated
	 */
	int MEMBER = 13;

	/**
	 * The feature id for the '<em><b>Modifiers</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER__MODIFIERS = 0;

	/**
	 * The feature id for the '<em><b>Javadoc</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER__JAVADOC = 1;

	/**
	 * The number of structural features of the '<em>Member</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Member</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEMBER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.javamodel.Modifier <em>Modifier</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.javamodel.Modifier
	 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getModifier()
	 * @generated
	 */
	int MODIFIER = 14;


	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.javamodel.JavaElement <em>Java Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Java Element</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavaElement
	 * @generated
	 */
	EClass getJavaElement();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.javamodel.JavaElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavaElement#getName()
	 * @see #getJavaElement()
	 * @generated
	 */
	EAttribute getJavaElement_Name();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.javamodel.JavaElement#getPath <em>Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Path</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavaElement#getPath()
	 * @see #getJavaElement()
	 * @generated
	 */
	EAttribute getJavaElement_Path();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.javamodel.JavaModel <em>Java Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Java Model</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavaModel
	 * @generated
	 */
	EClass getJavaModel();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.mobile.javamodel.JavaModel#getJavaProjects <em>Java Projects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Java Projects</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavaModel#getJavaProjects()
	 * @see #getJavaModel()
	 * @generated
	 */
	EReference getJavaModel_JavaProjects();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.javamodel.JavaProject <em>Java Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Java Project</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavaProject
	 * @generated
	 */
	EClass getJavaProject();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.mobile.javamodel.JavaProject#getPackageFragmentRoots <em>Package Fragment Roots</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Package Fragment Roots</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavaProject#getPackageFragmentRoots()
	 * @see #getJavaProject()
	 * @generated
	 */
	EReference getJavaProject_PackageFragmentRoots();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.javamodel.PackageFragmentRoot <em>Package Fragment Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Package Fragment Root</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.PackageFragmentRoot
	 * @generated
	 */
	EClass getPackageFragmentRoot();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.mobile.javamodel.PackageFragmentRoot#getPackageFragments <em>Package Fragments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Package Fragments</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.PackageFragmentRoot#getPackageFragments()
	 * @see #getPackageFragmentRoot()
	 * @generated
	 */
	EReference getPackageFragmentRoot_PackageFragments();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.javamodel.PackageFragment <em>Package Fragment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Package Fragment</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.PackageFragment
	 * @generated
	 */
	EClass getPackageFragment();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.mobile.javamodel.PackageFragment#getCompilationUnits <em>Compilation Units</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Compilation Units</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.PackageFragment#getCompilationUnits()
	 * @see #getPackageFragment()
	 * @generated
	 */
	EReference getPackageFragment_CompilationUnits();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.javamodel.CompilationUnit <em>Compilation Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Compilation Unit</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.CompilationUnit
	 * @generated
	 */
	EClass getCompilationUnit();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.mobile.javamodel.CompilationUnit#getImportDeclarations <em>Import Declarations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Import Declarations</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.CompilationUnit#getImportDeclarations()
	 * @see #getCompilationUnit()
	 * @generated
	 */
	EReference getCompilationUnit_ImportDeclarations();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.mobile.javamodel.CompilationUnit#getTypes <em>Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Types</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.CompilationUnit#getTypes()
	 * @see #getCompilationUnit()
	 * @generated
	 */
	EReference getCompilationUnit_Types();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.javamodel.ImportDeclaration <em>Import Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Import Declaration</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.ImportDeclaration
	 * @generated
	 */
	EClass getImportDeclaration();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.javamodel.Type <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.Type
	 * @generated
	 */
	EClass getType();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.mobile.javamodel.Type#getFields <em>Fields</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fields</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.Type#getFields()
	 * @see #getType()
	 * @generated
	 */
	EReference getType_Fields();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.mobile.javamodel.Type#getMethods <em>Methods</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Methods</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.Type#getMethods()
	 * @see #getType()
	 * @generated
	 */
	EReference getType_Methods();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.javamodel.Type#getSuperType <em>Super Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Super Type</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.Type#getSuperType()
	 * @see #getType()
	 * @generated
	 */
	EAttribute getType_SuperType();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.mobile.javamodel.Type#getSuperInterfaces <em>Super Interfaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Super Interfaces</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.Type#getSuperInterfaces()
	 * @see #getType()
	 * @generated
	 */
	EAttribute getType_SuperInterfaces();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.javamodel.Field <em>Field</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Field</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.Field
	 * @generated
	 */
	EClass getField();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.javamodel.Field#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.Field#getType()
	 * @see #getField()
	 * @generated
	 */
	EAttribute getField_Type();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.javamodel.Method <em>Method</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Method</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.Method
	 * @generated
	 */
	EClass getMethod();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.javamodel.Method#getReturnType <em>Return Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Return Type</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.Method#getReturnType()
	 * @see #getMethod()
	 * @generated
	 */
	EAttribute getMethod_ReturnType();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.mobile.javamodel.Method#getExceptionTypes <em>Exception Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Exception Types</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.Method#getExceptionTypes()
	 * @see #getMethod()
	 * @generated
	 */
	EAttribute getMethod_ExceptionTypes();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.mobile.javamodel.Method#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.Method#getParameters()
	 * @see #getMethod()
	 * @generated
	 */
	EReference getMethod_Parameters();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.javamodel.LocalVariable <em>Local Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Local Variable</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.LocalVariable
	 * @generated
	 */
	EClass getLocalVariable();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.javamodel.LocalVariable#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.LocalVariable#getType()
	 * @see #getLocalVariable()
	 * @generated
	 */
	EAttribute getLocalVariable_Type();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.mobile.javamodel.LocalVariable#getModifiers <em>Modifiers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Modifiers</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.LocalVariable#getModifiers()
	 * @see #getLocalVariable()
	 * @generated
	 */
	EAttribute getLocalVariable_Modifiers();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.javamodel.Annotation <em>Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotation</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.Annotation
	 * @generated
	 */
	EClass getAnnotation();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.mobile.javamodel.Annotation#getValuePairs <em>Value Pairs</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Value Pairs</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.Annotation#getValuePairs()
	 * @see #getAnnotation()
	 * @generated
	 */
	EAttribute getAnnotation_ValuePairs();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.javamodel.Annotatable <em>Annotatable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Annotatable</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.Annotatable
	 * @generated
	 */
	EClass getAnnotatable();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.mobile.javamodel.Annotatable#getAnnotations <em>Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Annotations</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.Annotatable#getAnnotations()
	 * @see #getAnnotatable()
	 * @generated
	 */
	EReference getAnnotatable_Annotations();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.javamodel.Member <em>Member</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Member</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.Member
	 * @generated
	 */
	EClass getMember();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.mobile.javamodel.Member#getModifiers <em>Modifiers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Modifiers</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.Member#getModifiers()
	 * @see #getMember()
	 * @generated
	 */
	EAttribute getMember_Modifiers();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.javamodel.Member#getJavadoc <em>Javadoc</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Javadoc</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.Member#getJavadoc()
	 * @see #getMember()
	 * @generated
	 */
	EAttribute getMember_Javadoc();

	/**
	 * Returns the meta object for enum '{@link edu.ustb.sei.mde.mobile.javamodel.Modifier <em>Modifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Modifier</em>'.
	 * @see edu.ustb.sei.mde.mobile.javamodel.Modifier
	 * @generated
	 */
	EEnum getModifier();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	JavamodelFactory getJavamodelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.JavaElementImpl <em>Java Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavaElementImpl
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getJavaElement()
		 * @generated
		 */
		EClass JAVA_ELEMENT = eINSTANCE.getJavaElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JAVA_ELEMENT__NAME = eINSTANCE.getJavaElement_Name();

		/**
		 * The meta object literal for the '<em><b>Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JAVA_ELEMENT__PATH = eINSTANCE.getJavaElement_Path();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.JavaModelImpl <em>Java Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavaModelImpl
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getJavaModel()
		 * @generated
		 */
		EClass JAVA_MODEL = eINSTANCE.getJavaModel();

		/**
		 * The meta object literal for the '<em><b>Java Projects</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JAVA_MODEL__JAVA_PROJECTS = eINSTANCE.getJavaModel_JavaProjects();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.JavaProjectImpl <em>Java Project</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavaProjectImpl
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getJavaProject()
		 * @generated
		 */
		EClass JAVA_PROJECT = eINSTANCE.getJavaProject();

		/**
		 * The meta object literal for the '<em><b>Package Fragment Roots</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JAVA_PROJECT__PACKAGE_FRAGMENT_ROOTS = eINSTANCE.getJavaProject_PackageFragmentRoots();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.PackageFragmentRootImpl <em>Package Fragment Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.PackageFragmentRootImpl
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getPackageFragmentRoot()
		 * @generated
		 */
		EClass PACKAGE_FRAGMENT_ROOT = eINSTANCE.getPackageFragmentRoot();

		/**
		 * The meta object literal for the '<em><b>Package Fragments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PACKAGE_FRAGMENT_ROOT__PACKAGE_FRAGMENTS = eINSTANCE.getPackageFragmentRoot_PackageFragments();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.PackageFragmentImpl <em>Package Fragment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.PackageFragmentImpl
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getPackageFragment()
		 * @generated
		 */
		EClass PACKAGE_FRAGMENT = eINSTANCE.getPackageFragment();

		/**
		 * The meta object literal for the '<em><b>Compilation Units</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PACKAGE_FRAGMENT__COMPILATION_UNITS = eINSTANCE.getPackageFragment_CompilationUnits();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.CompilationUnitImpl <em>Compilation Unit</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.CompilationUnitImpl
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getCompilationUnit()
		 * @generated
		 */
		EClass COMPILATION_UNIT = eINSTANCE.getCompilationUnit();

		/**
		 * The meta object literal for the '<em><b>Import Declarations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPILATION_UNIT__IMPORT_DECLARATIONS = eINSTANCE.getCompilationUnit_ImportDeclarations();

		/**
		 * The meta object literal for the '<em><b>Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPILATION_UNIT__TYPES = eINSTANCE.getCompilationUnit_Types();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.ImportDeclarationImpl <em>Import Declaration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.ImportDeclarationImpl
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getImportDeclaration()
		 * @generated
		 */
		EClass IMPORT_DECLARATION = eINSTANCE.getImportDeclaration();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.TypeImpl <em>Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.TypeImpl
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getType()
		 * @generated
		 */
		EClass TYPE = eINSTANCE.getType();

		/**
		 * The meta object literal for the '<em><b>Fields</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TYPE__FIELDS = eINSTANCE.getType_Fields();

		/**
		 * The meta object literal for the '<em><b>Methods</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TYPE__METHODS = eINSTANCE.getType_Methods();

		/**
		 * The meta object literal for the '<em><b>Super Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPE__SUPER_TYPE = eINSTANCE.getType_SuperType();

		/**
		 * The meta object literal for the '<em><b>Super Interfaces</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPE__SUPER_INTERFACES = eINSTANCE.getType_SuperInterfaces();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.FieldImpl <em>Field</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.FieldImpl
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getField()
		 * @generated
		 */
		EClass FIELD = eINSTANCE.getField();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FIELD__TYPE = eINSTANCE.getField_Type();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.MethodImpl <em>Method</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.MethodImpl
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getMethod()
		 * @generated
		 */
		EClass METHOD = eINSTANCE.getMethod();

		/**
		 * The meta object literal for the '<em><b>Return Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute METHOD__RETURN_TYPE = eINSTANCE.getMethod_ReturnType();

		/**
		 * The meta object literal for the '<em><b>Exception Types</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute METHOD__EXCEPTION_TYPES = eINSTANCE.getMethod_ExceptionTypes();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METHOD__PARAMETERS = eINSTANCE.getMethod_Parameters();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.LocalVariableImpl <em>Local Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.LocalVariableImpl
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getLocalVariable()
		 * @generated
		 */
		EClass LOCAL_VARIABLE = eINSTANCE.getLocalVariable();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCAL_VARIABLE__TYPE = eINSTANCE.getLocalVariable_Type();

		/**
		 * The meta object literal for the '<em><b>Modifiers</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCAL_VARIABLE__MODIFIERS = eINSTANCE.getLocalVariable_Modifiers();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.AnnotationImpl <em>Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.AnnotationImpl
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getAnnotation()
		 * @generated
		 */
		EClass ANNOTATION = eINSTANCE.getAnnotation();

		/**
		 * The meta object literal for the '<em><b>Value Pairs</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANNOTATION__VALUE_PAIRS = eINSTANCE.getAnnotation_ValuePairs();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.AnnotatableImpl <em>Annotatable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.AnnotatableImpl
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getAnnotatable()
		 * @generated
		 */
		EClass ANNOTATABLE = eINSTANCE.getAnnotatable();

		/**
		 * The meta object literal for the '<em><b>Annotations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ANNOTATABLE__ANNOTATIONS = eINSTANCE.getAnnotatable_Annotations();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.javamodel.impl.MemberImpl <em>Member</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.MemberImpl
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getMember()
		 * @generated
		 */
		EClass MEMBER = eINSTANCE.getMember();

		/**
		 * The meta object literal for the '<em><b>Modifiers</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEMBER__MODIFIERS = eINSTANCE.getMember_Modifiers();

		/**
		 * The meta object literal for the '<em><b>Javadoc</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEMBER__JAVADOC = eINSTANCE.getMember_Javadoc();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.javamodel.Modifier <em>Modifier</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.javamodel.Modifier
		 * @see edu.ustb.sei.mde.mobile.javamodel.impl.JavamodelPackageImpl#getModifier()
		 * @generated
		 */
		EEnum MODIFIER = eINSTANCE.getModifier();

	}

} //JavamodelPackage
