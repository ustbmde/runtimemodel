package edu.ustb.sei.mde.mobile.javamodel.codeutils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.SourceRange;
import org.eclipse.text.edits.TextEdit;

import edu.ustb.sei.mde.mobile.javamodel.Modifier;

public class JavaSourceUtil {
	private static final String IMPLEMENTS = "implements";
	private static final String EXTENDS = "extends";

	static public int skipComment(String source, int start) {
		int end = start;
		char ch = source.charAt(end);
		if(ch=='/') {
			char nextCh = source.charAt(end+1);
			if(nextCh=='/') {
				end = end + 2; // skip //
				while(source.charAt(end)!='\n' && end<source.length()) end++;
				end = end + 1; // skip \n
			} else if(nextCh=='*') {
				end = end + 2; // skip /*
				while((source.charAt(end)!='*' || source.charAt(end+1)!='/') && end<source.length()-1) end++;
				end = end + 2; // skip */
			} else {
				System.out.println("Unknown comment structure : "+
			source.substring(start, start+10));
				return start;
			}
		} 
		return end;
	}
	
	static public int skipSpace(String source, int start) {
		for(;start<source.length();start++) {
			char ch = source.charAt(start);
			if(ch==' '||ch=='\t'||ch=='\n') continue;
			break;
		}
		return start;
	}
	
	static public int skipSpaceAndComment(String source, int start) {
		int old;
		do {
			old = start;
			start = skipSpace(source, start);
			start = skipComment(source, start);
		} while(old!=start);
		return start;
	}
	
	static public boolean isIdentifierHeadingChar(char ch) {
		return ((ch>='a' && ch<='z') || (ch>='A' && ch<='Z') || (ch=='_'));
	}
	
	static public boolean isIdentifierChar(char ch) {
		return (isIdentifierHeadingChar(ch) || (ch>='0' && ch<='9'));
	}
	
	public static int skipChar(String source, int start) {
		char ch = source.charAt(start);
		if(ch=='\'') {
			boolean skip = false;
			start ++;
			for(;start<source.length();start++) {
				ch = source.charAt(start);
				if(skip) skip = false;
				else if(ch=='\\') {
					skip = true;
				} else if(ch=='\'') return start + 1;
			}
		}
		return start;
	}
	
	static public Range nextToken(String code, int start) {
		start = skipSpaceAndComment(code, start);
		
		char ch = code.charAt(start);
		
		if(isIdentifierHeadingChar(ch)) {
			int end = start + 1;
			while(end<code.length() && isIdentifierChar(code.charAt(end))) end++;
			return new Range(start, end-start, Range.IDENTIFIER);
		} else if(isDigit(ch)) {
			int end = start + 1;
			int state = 0; // 0--int, 1--dot, 2--float, 3--e/E, 4--+/-, 5--int
			while(end<code.length()) {
				char d = code.charAt(end);
				switch(state) {
				case 0:
					if(d=='.') state = 1;
					else if(d=='e' || d=='E') state=3;
					else if(!isDigit(d)) state = -1;
					break;
				case 1:
					if(isDigit(d)) state = 2;
					else if(d=='e' || d=='E') state = 3;
					else state = -1;
					break;
				case 2:
					if(d=='e' || d=='E') state = 3;
					else if(!isDigit(d)) state = -1;
					break;
				case 3:
					if(d=='+'||d=='-') state = 4;
					else if(isDigit(d)) state = 5;
					else state = -1;
					break;
				case 4:
					if(!isDigit(d)) state = -1;
					break;
				case 5:
					if(!isDigit(d)) state = -1;
					break;
				}
				
				if(state == -1) break;
				end++;
			}
			return new Range(start, end-start, Range.NUMBER);
		} else if(ch=='/') {
			char nextCh = code.charAt(start + 1);
			if(nextCh=='/') {// single line comment
				start = start + 2;
				while(start<code.length() && code.charAt(start)!='\n') start ++;
				return nextToken(code, start);
			} else { // multilines comment
				start = start + 2;
				char prev = code.charAt(start);
				start = start + 1;
				while(start<code.length() 
						&& !(prev=='*' && (prev=code.charAt(start))=='/'))
					start ++;
				if(start<code.length()) start = start + 1;
				return nextToken(code, start);
			}
		} else if(ch=='"') {
			int end = start + 1;
			boolean escape = false;
			while(end<code.length()) {
				if(escape) {
					escape = false;
				} else {
					char sc = code.charAt(end);
					if(sc=='\\')escape=true;
					else if(sc=='"') break;
				}
				end ++;
			}
			end ++;
			return new Range(start, end, Range.STRING);
		} else if(ch=='\'') {
			int end = start + 1;
			boolean escape = false;
			while(end<code.length()) {
				if(escape) {
					escape = false;
				} else {
					char sc = code.charAt(end);
					if(sc=='\\')escape=true;
					else if(sc=='\'') break;
				}
				end ++;
			}
			end ++;
			return new Range(start, end, Range.CHAR);
		} else {
			return new Range(start, 1, ch);
		}
	}
	
	public static boolean isDigit(char ch) {
		return (ch>='0' && ch<='9');
	}

	static public Range matchIdentifier(String code, int start) {
		Range range = nextToken(code, start);
		if(range.kind!=Range.IDENTIFIER) return Range.invalid;
		return range;
	}
	
	static public Range matchQualifiedName(String code, int start) {
		List<Range> ranges = new ArrayList<>();
		Range r = nextToken(code, start);
		for(;;) {
			if(r.kind()!=Range.IDENTIFIER) break;
			ranges.add(r);
			start = r.offset + r.length;
			r = nextToken(code, start);
			if(r.kind()==Range.SINGLE && r.singleChar()=='<') {// type parameter
				ranges.add(r);
				int level = 1;
				do {
					start = r.offset + r.length;
					r = nextToken(code, start);
					if(r.kind()==Range.SINGLE) {
						if(r.singleChar()=='<') level ++; 
						else if(r.singleChar()=='>') level--;
					}
					ranges.add(r);
				} while(level>0);
				start = r.offset + r.length;
				r = nextToken(code, start);
			}
			
			if(!(r.kind()==Range.SINGLE && (r.singleChar()=='.'||r.singleChar()=='$'))) break;
			ranges.add(r);
			start = r.offset + r.length;
			r = nextToken(code, start);
		}
		if(ranges.isEmpty()) return Range.invalid;
		else return new MultiRange(ranges, Range.QUALIFIED_NAME);
	}
	
	static public Range matchQualifiedNameList(String code, int start) {
		List<Range> ranges = new ArrayList<>();
		while(true) {
			Range r = matchQualifiedName(code, start);
			if(r==Range.invalid) break;
			ranges.add(r);
			start = r.offset + r.length;
			r = nextToken(code, start);
			if(!(r.kind()==Range.SINGLE && r.singleChar()==',')) break;
			ranges.add(r);
			start = r.offset + r.length;
		}
		
		if(ranges.isEmpty()) return Range.invalid;
		else return new MultiRange(ranges, Range.QUALIFIED_NAME_LIST);
	}
	
	// search extends cause from start
	static public Range matchExtends(String code, int start) {
		boolean afterTypeArg = false;
		List<Range> list = new ArrayList<>();
		for(;;) {
			Range r = nextToken(code, start);
			if(r.kind()==Range.SINGLE&&r.singleChar()=='<') {
				if(!afterTypeArg) {
					afterTypeArg = true;
					int level=1;
					do {
						start = r.offset+r.length;
						r = nextToken(code, start);
						if(r.kind()==Range.SINGLE) {
							if(r.singleChar()=='<') level ++; 
							else if(r.singleChar()=='>') level--;
						}
					} while(level!=0);
					start = r.offset + r.length;
				} else break;
			} else {
				if(r.kind()==Range.SINGLE&&r.singleChar()=='{') {
					break;
				} else if(r.kind==Range.IDENTIFIER && r.string(code).equals("implements")) {
					break;
				} else if(r.kind==Range.IDENTIFIER && r.string(code).equals("extends")) {
					list.add(r);
					start = r.offset + r.length;
					r = matchQualifiedNameList(code, start);
					list.add(r);
					break;
				} else {
					start = r.offset + r.length;
				}
			}
		}
		
		if(list.isEmpty()) {
			return new Range(start, 0, Range.PLACEHOLDER);
		} else {
			return new MultiRange(list, Range.EXTENDS_CLAUSE);
		}
	}
	
	static public Range matchImplements(String code, int start) {
		Range r = matchExtends(code, start);
		start = r.offset + r.length;
		List<Range> list = null;
		r = nextToken(code, start);
		if(r.kind==Range.IDENTIFIER && r.string(code).equals("implements")) {
			list = new ArrayList<>();
			list.add(r);
			start = r.offset + r.length;
			r = matchQualifiedNameList(code, start);
			list.add(r);
		}
		if(list==null) {
			return new Range(start, 0, Range.PLACEHOLDER);
		} else {
			return new MultiRange(list, Range.IMPLEMENTS_CLAUSE);
		}
	}
	
	final static public String[] modifiers = new String[] {"abstract", "default", "enum", "final", "interface", "class", "private", "protected", "public", "static", "synchronized", "transient", "volatile", "@interface"};
	final static public int[] modifierFlags = new int[] {
			Modifier.ACC_ABSTRACT_VALUE, Modifier.ACC_DEFAULT_METHOD_VALUE, Modifier.ACC_ENUM_VALUE, 
			Modifier.ACC_FINAL_VALUE, Modifier.ACC_INTERFACE_VALUE, Modifier.ACC_DEFAULT_VALUE, 
			Modifier.ACC_PRIVATE_VALUE, Modifier.ACC_PROTECTED_VALUE, Modifier.ACC_PUBLIC_VALUE, 
			Modifier.ACC_STATIC_VALUE, Modifier.ACC_SUPER_OR_SYNCHRONIZED_VALUE, Modifier.ACC_TRANSIENT_OR_VARARGS_VALUE, 
			Modifier.ACC_VOLATILE_VALUE, Modifier.ACC_ANNOTATION_VALUE};
	static public boolean isModifier(String str) {
		for(String s:modifiers)
			if(s.equals(str)) return true;
		return false;
	}
	
	static public Range matchTypeName(String code, int end) {
		int start  = 0;
		Range r;
		while(true) {
			r = nextToken(code,start);
			if(r.offset>=end) break;
			if(r.kind==Range.SINGLE && r.singleChar()=='@') {
				r = matchAnnotation(code, start);
			} else if(r.kind==Range.IDENTIFIER) {
				String str = r.string(code);
				if(!isModifier(str)) {
					r = matchQualifiedName(code, r.offset);
					return r;
				}
			}
			start = r.offset + r.length;	
		}
		return Range.invalid;
	}
	
	static public Range matchModifiers(String code, int end) {
		int start = 0;
		List<Range> ranges = new ArrayList<>();
		
		while(true) {
			Range r = nextToken(code, start);
			if(r.offset>=end) break;
			if(r.kind==Range.SINGLE && r.singleChar()=='@') {
				r = matchAnnotation(code, start);
			} else if(r.kind==Range.IDENTIFIER) {
				String str = r.string(code);
				if(isModifier(str)) {
					ranges.add(r);
				}
			}
			start = r.offset + r.length;			
		}
		
		if(ranges.isEmpty()) {
			return Range.invalid;
		} else 
			return new MultiRange(ranges, Range.MODIFIER_LIST);
	}
	
	static public Range matchAnnotation(String code, int start) {
		Range r = nextToken(code, start);
		if(r.kind==Range.SINGLE && r.singleChar()=='@') {
			List<Range> ranges = new ArrayList<>();
			ranges.add(r);
			start = r.offset + r.length;
			r=matchQualifiedName(code, start);
			if(r.kind==Range.INVALID) return r;
			ranges.add(r);
			start = r.offset + r.length;
			r = nextToken(code, start);
			if(r.kind==Range.SINGLE && r.singleChar()=='(') {
				ranges.add(r);
				int level = 1;
				do {
					start = r.offset + r.length;
					r = nextToken(code, start);
					if(r.kind==Range.SINGLE) {
						if(r.singleChar()=='(') level ++;
						else if(r.singleChar()==')') level --;
					}
				} while(level!=0);
			}
			return new MultiRange(ranges, Range.ANNOTATION);
		} else return Range.invalid;
	}
	
	static public void applyTextEdit(ICompilationUnit so, TextEdit edit) throws JavaModelException {
		if(edit!=null) {
			ICompilationUnit wc = so.getWorkingCopy(null);
			wc.applyTextEdit(edit, null);
			wc.reconcile(ICompilationUnit.NO_AST, false, null, null);
			wc.commitWorkingCopy(false, null);
			wc.discardWorkingCopy();
		}
	}
	
	static public Range matchParameterScope(String code, int start) {
		Range r = nextToken(code,start);
		if(r.kind!=Range.SINGLE || r.singleChar()!='(') return Range.invalid;
		int begin = r.offset;
		start = r.offset + r.length;
		int level  = 1;
		do {
			r = nextToken(code,start);
			if(r.kind==Range.SINGLE) {
				if(r.singleChar()=='(') level ++;
				else if(r.singleChar()==')') level --;
			}
			start = r.offset + r.length;
		} while(level!=0);
		return new Range(begin, start-begin, Range.PLACEHOLDER);
	}
	
	
	static final Pattern docpattern = Pattern.compile("\\s*/\\*\\*(.*)\\*/\\s*",Pattern.DOTALL|Pattern.MULTILINE);
	static final Pattern splitpattern = Pattern.compile("\\n\\s*\\*",Pattern.MULTILINE);
	
	static public String unboxingJavaDoc(String doc) {
		Matcher m = docpattern.matcher(doc);
		m.matches();
		String con = m.group(1);
		String[] lines = splitpattern.split(con);
		return Arrays.stream(lines).reduce((l,r)->l+"\n"+r).get();
	}
	
	static public String boxingJavaDoc(String unboxdoc) {
		String[] lines = unboxdoc.split("\\n");
		StringBuilder builder = new StringBuilder();
		builder.append("/**\n");
		for(String s : lines) {
			builder.append(" * ");
			builder.append(s);
			builder.append("\n");
		}
		builder.append("*/\n");
		return builder.toString();
	}
	
	public static void main(String[] args) {
		String doc = "/**\n" + 
				"	 * g\n"
				+ " f\n" + 
				"	 * @param doc\n" + 
				"	 * @return\n" + 
				"	 */";
		System.out.println(unboxingJavaDoc(doc));
	}
}
