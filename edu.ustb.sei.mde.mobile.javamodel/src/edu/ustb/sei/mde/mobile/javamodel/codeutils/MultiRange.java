package edu.ustb.sei.mde.mobile.javamodel.codeutils;

import java.util.List;

public class MultiRange extends Range {
	private List<Range> ranges;

	public MultiRange(List<Range> ranges, int kind) {
		super(0,0, kind);
		this.ranges = ranges;
		this.offset = this.ranges.stream().reduce(-1, 
				(o, r)->{
					if(o==-1) return r.offset;
					else return Math.min(o, r.offset);
				}, (l,r)->{
					return Math.min(l, r);
				});
		this.length = this.ranges.stream().reduce(offset, 
				(o,r)->{
					return Math.max(o, r.offset+r.length);
				},
				(l,r)->{
					return Math.max(l, r);
				}) - offset;
	}
	
	@Override
	public String string(String code) {
		if(kind==QUALIFIED_NAME || kind==QUALIFIED_NAME_LIST)
			return ranges.stream().map(r->r.string(code)).reduce((l,r)->l+r).get();
		else
			return ranges.stream().map(r->r.string(code)).reduce((l,r)->l+" "+r).get();
	}
	
	public List<Range> ranges() {
		return ranges;
	}
}
