/**
 */
package edu.ustb.sei.mde.mobile.javamodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Package Fragment Root</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.PackageFragmentRoot#getPackageFragments <em>Package Fragments</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getPackageFragmentRoot()
 * @model
 * @generated
 */
public interface PackageFragmentRoot extends JavaElement {
	/**
	 * Returns the value of the '<em><b>Package Fragments</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.mobile.javamodel.PackageFragment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Package Fragments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package Fragments</em>' containment reference list.
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getPackageFragmentRoot_PackageFragments()
	 * @model containment="true"
	 * @generated
	 */
	EList<PackageFragment> getPackageFragments();

} // PackageFragmentRoot
