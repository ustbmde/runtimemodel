/**
 */
package edu.ustb.sei.mde.mobile.javamodel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.Annotation#getValuePairs <em>Value Pairs</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getAnnotation()
 * @model
 * @generated
 */
public interface Annotation extends JavaElement {
	/**
	 * Returns the value of the '<em><b>Value Pairs</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Pairs</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Pairs</em>' attribute list.
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getAnnotation_ValuePairs()
	 * @model
	 * @generated
	 */
	EList<String> getValuePairs();

} // Annotation
