/**
 */
package edu.ustb.sei.mde.mobile.javamodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Member</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.Member#getModifiers <em>Modifiers</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.Member#getJavadoc <em>Javadoc</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getMember()
 * @model abstract="true"
 * @generated
 */
public interface Member extends EObject {
	/**
	 * Returns the value of the '<em><b>Modifiers</b></em>' attribute list.
	 * The list contents are of type {@link edu.ustb.sei.mde.mobile.javamodel.Modifier}.
	 * The literals are from the enumeration {@link edu.ustb.sei.mde.mobile.javamodel.Modifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Modifiers</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Modifiers</em>' attribute list.
	 * @see edu.ustb.sei.mde.mobile.javamodel.Modifier
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getMember_Modifiers()
	 * @model
	 * @generated
	 */
	EList<Modifier> getModifiers();

	/**
	 * Returns the value of the '<em><b>Javadoc</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Javadoc</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Javadoc</em>' attribute.
	 * @see #setJavadoc(String)
	 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getMember_Javadoc()
	 * @model
	 * @generated
	 */
	String getJavadoc();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mobile.javamodel.Member#getJavadoc <em>Javadoc</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Javadoc</em>' attribute.
	 * @see #getJavadoc()
	 * @generated
	 */
	void setJavadoc(String value);

} // Member
