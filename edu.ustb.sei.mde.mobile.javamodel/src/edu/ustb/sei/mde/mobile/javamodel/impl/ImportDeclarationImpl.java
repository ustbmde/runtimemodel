/**
 */
package edu.ustb.sei.mde.mobile.javamodel.impl;

import edu.ustb.sei.mde.mobile.javamodel.ImportDeclaration;
import edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Import Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ImportDeclarationImpl extends JavaElementImpl implements ImportDeclaration {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ImportDeclarationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JavamodelPackage.Literals.IMPORT_DECLARATION;
	}

} //ImportDeclarationImpl
