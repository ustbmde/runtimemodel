package edu.ustb.sei.mde.mobile.javamodel.codeutils;

public class Range {
	static public final int PLACEHOLDER = -2;
	static public final int INVALID = -1;
	static public final int IDENTIFIER = 0;
	static public final int QUALIFIED_NAME = 1;
	static public final int STRING = 2;
	static public final int CHAR = 3;
	static public final int SINGLE = 4;
	static public final int QUALIFIED_NAME_LIST = 5;
	public static final int NUMBER = 6;
	public static final int EXTENDS_CLAUSE = 7;
	public static final int IMPLEMENTS_CLAUSE = 8;
	public static final int ANNOTATION = 9;
	public static final int MODIFIER_LIST = 10;
	
	public Range(int offset, int length, int kind) {
		super();
		this.offset = offset;
		this.length = length;
		this.kind = kind;
	}
	
	public Range(int offset, int length, char s) {
		super();
		this.offset = offset;
		this.length = length;
		this.kind = SINGLE;
		this.singleChar = s;
	}
	
	private char singleChar;
	protected int offset;
	protected int length;
	protected int kind;
	
	
	public char singleChar() {
		return singleChar;
	}
	
	public int kind() {
		return kind;
	}
	
	public int offset() {
		return offset;
	}
	
	public int length() {
		return length;
	}
	
	static public final Range invalid = new Range(-1,0,INVALID);
	

	public String string(String code) {
		return code.substring(offset, offset+length);
	}
	
}
