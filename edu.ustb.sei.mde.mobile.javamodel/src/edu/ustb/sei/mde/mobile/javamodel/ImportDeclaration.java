/**
 */
package edu.ustb.sei.mde.mobile.javamodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Import Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage#getImportDeclaration()
 * @model
 * @generated
 */
public interface ImportDeclaration extends JavaElement {
} // ImportDeclaration
