/**
 */
package edu.ustb.sei.mde.mobile.javamodel.impl;

import edu.ustb.sei.mde.mobile.javamodel.Annotatable;
import edu.ustb.sei.mde.mobile.javamodel.Annotation;
import edu.ustb.sei.mde.mobile.javamodel.JavamodelPackage;
import edu.ustb.sei.mde.mobile.javamodel.LocalVariable;
import edu.ustb.sei.mde.mobile.javamodel.Member;
import edu.ustb.sei.mde.mobile.javamodel.Method;

import edu.ustb.sei.mde.mobile.javamodel.Modifier;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Method</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.impl.MethodImpl#getAnnotations <em>Annotations</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.impl.MethodImpl#getModifiers <em>Modifiers</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.impl.MethodImpl#getJavadoc <em>Javadoc</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.impl.MethodImpl#getReturnType <em>Return Type</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.impl.MethodImpl#getExceptionTypes <em>Exception Types</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.javamodel.impl.MethodImpl#getParameters <em>Parameters</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MethodImpl extends JavaElementImpl implements Method {
	/**
	 * The cached value of the '{@link #getAnnotations() <em>Annotations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotations()
	 * @generated
	 * @ordered
	 */
	protected EList<Annotation> annotations;

	/**
	 * The cached value of the '{@link #getModifiers() <em>Modifiers</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModifiers()
	 * @generated
	 * @ordered
	 */
	protected EList<Modifier> modifiers;

	/**
	 * The default value of the '{@link #getJavadoc() <em>Javadoc</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJavadoc()
	 * @generated
	 * @ordered
	 */
	protected static final String JAVADOC_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getJavadoc() <em>Javadoc</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getJavadoc()
	 * @generated
	 * @ordered
	 */
	protected String javadoc = JAVADOC_EDEFAULT;

	/**
	 * The default value of the '{@link #getReturnType() <em>Return Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReturnType()
	 * @generated
	 * @ordered
	 */
	protected static final String RETURN_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getReturnType() <em>Return Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReturnType()
	 * @generated
	 * @ordered
	 */
	protected String returnType = RETURN_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getExceptionTypes() <em>Exception Types</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExceptionTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<String> exceptionTypes;

	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected EList<LocalVariable> parameters;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MethodImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return JavamodelPackage.Literals.METHOD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Annotation> getAnnotations() {
		if (annotations == null) {
			annotations = new EObjectContainmentEList<Annotation>(Annotation.class, this, JavamodelPackage.METHOD__ANNOTATIONS);
		}
		return annotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Modifier> getModifiers() {
		if (modifiers == null) {
			modifiers = new EDataTypeUniqueEList<Modifier>(Modifier.class, this, JavamodelPackage.METHOD__MODIFIERS);
		}
		return modifiers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getJavadoc() {
		return javadoc;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setJavadoc(String newJavadoc) {
		String oldJavadoc = javadoc;
		javadoc = newJavadoc;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JavamodelPackage.METHOD__JAVADOC, oldJavadoc, javadoc));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getReturnType() {
		return returnType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReturnType(String newReturnType) {
		String oldReturnType = returnType;
		returnType = newReturnType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, JavamodelPackage.METHOD__RETURN_TYPE, oldReturnType, returnType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getExceptionTypes() {
		if (exceptionTypes == null) {
			exceptionTypes = new EDataTypeUniqueEList<String>(String.class, this, JavamodelPackage.METHOD__EXCEPTION_TYPES);
		}
		return exceptionTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LocalVariable> getParameters() {
		if (parameters == null) {
			parameters = new EObjectContainmentEList<LocalVariable>(LocalVariable.class, this, JavamodelPackage.METHOD__PARAMETERS);
		}
		return parameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case JavamodelPackage.METHOD__ANNOTATIONS:
				return ((InternalEList<?>)getAnnotations()).basicRemove(otherEnd, msgs);
			case JavamodelPackage.METHOD__PARAMETERS:
				return ((InternalEList<?>)getParameters()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case JavamodelPackage.METHOD__ANNOTATIONS:
				return getAnnotations();
			case JavamodelPackage.METHOD__MODIFIERS:
				return getModifiers();
			case JavamodelPackage.METHOD__JAVADOC:
				return getJavadoc();
			case JavamodelPackage.METHOD__RETURN_TYPE:
				return getReturnType();
			case JavamodelPackage.METHOD__EXCEPTION_TYPES:
				return getExceptionTypes();
			case JavamodelPackage.METHOD__PARAMETERS:
				return getParameters();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case JavamodelPackage.METHOD__ANNOTATIONS:
				getAnnotations().clear();
				getAnnotations().addAll((Collection<? extends Annotation>)newValue);
				return;
			case JavamodelPackage.METHOD__MODIFIERS:
				getModifiers().clear();
				getModifiers().addAll((Collection<? extends Modifier>)newValue);
				return;
			case JavamodelPackage.METHOD__JAVADOC:
				setJavadoc((String)newValue);
				return;
			case JavamodelPackage.METHOD__RETURN_TYPE:
				setReturnType((String)newValue);
				return;
			case JavamodelPackage.METHOD__EXCEPTION_TYPES:
				getExceptionTypes().clear();
				getExceptionTypes().addAll((Collection<? extends String>)newValue);
				return;
			case JavamodelPackage.METHOD__PARAMETERS:
				getParameters().clear();
				getParameters().addAll((Collection<? extends LocalVariable>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case JavamodelPackage.METHOD__ANNOTATIONS:
				getAnnotations().clear();
				return;
			case JavamodelPackage.METHOD__MODIFIERS:
				getModifiers().clear();
				return;
			case JavamodelPackage.METHOD__JAVADOC:
				setJavadoc(JAVADOC_EDEFAULT);
				return;
			case JavamodelPackage.METHOD__RETURN_TYPE:
				setReturnType(RETURN_TYPE_EDEFAULT);
				return;
			case JavamodelPackage.METHOD__EXCEPTION_TYPES:
				getExceptionTypes().clear();
				return;
			case JavamodelPackage.METHOD__PARAMETERS:
				getParameters().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case JavamodelPackage.METHOD__ANNOTATIONS:
				return annotations != null && !annotations.isEmpty();
			case JavamodelPackage.METHOD__MODIFIERS:
				return modifiers != null && !modifiers.isEmpty();
			case JavamodelPackage.METHOD__JAVADOC:
				return JAVADOC_EDEFAULT == null ? javadoc != null : !JAVADOC_EDEFAULT.equals(javadoc);
			case JavamodelPackage.METHOD__RETURN_TYPE:
				return RETURN_TYPE_EDEFAULT == null ? returnType != null : !RETURN_TYPE_EDEFAULT.equals(returnType);
			case JavamodelPackage.METHOD__EXCEPTION_TYPES:
				return exceptionTypes != null && !exceptionTypes.isEmpty();
			case JavamodelPackage.METHOD__PARAMETERS:
				return parameters != null && !parameters.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Annotatable.class) {
			switch (derivedFeatureID) {
				case JavamodelPackage.METHOD__ANNOTATIONS: return JavamodelPackage.ANNOTATABLE__ANNOTATIONS;
				default: return -1;
			}
		}
		if (baseClass == Member.class) {
			switch (derivedFeatureID) {
				case JavamodelPackage.METHOD__MODIFIERS: return JavamodelPackage.MEMBER__MODIFIERS;
				case JavamodelPackage.METHOD__JAVADOC: return JavamodelPackage.MEMBER__JAVADOC;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Annotatable.class) {
			switch (baseFeatureID) {
				case JavamodelPackage.ANNOTATABLE__ANNOTATIONS: return JavamodelPackage.METHOD__ANNOTATIONS;
				default: return -1;
			}
		}
		if (baseClass == Member.class) {
			switch (baseFeatureID) {
				case JavamodelPackage.MEMBER__MODIFIERS: return JavamodelPackage.METHOD__MODIFIERS;
				case JavamodelPackage.MEMBER__JAVADOC: return JavamodelPackage.METHOD__JAVADOC;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (modifiers: ");
		result.append(modifiers);
		result.append(", javadoc: ");
		result.append(javadoc);
		result.append(", returnType: ");
		result.append(returnType);
		result.append(", exceptionTypes: ");
		result.append(exceptionTypes);
		result.append(')');
		return result.toString();
	}

} //MethodImpl
