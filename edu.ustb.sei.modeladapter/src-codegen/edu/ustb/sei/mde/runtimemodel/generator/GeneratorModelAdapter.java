package edu.ustb.sei.mde.runtimemodel.generator;

import java.io.OutputStream;
import java.text.MessageFormat;

import org.eclipse.emf.codegen.ecore.CodeGenEcorePlugin;
import org.eclipse.emf.codegen.ecore.generator.AbstractGeneratorAdapter;
import org.eclipse.emf.codegen.ecore.generator.Generator.Options;
import org.eclipse.emf.codegen.jet.JETEmitter;
import org.eclipse.emf.codegen.merge.java.JControlModel;
import org.eclipse.emf.codegen.merge.java.JMerger;
import org.eclipse.emf.codegen.merge.java.facade.JCompilationUnit;
import org.eclipse.emf.codegen.merge.java.facade.JImport;
import org.eclipse.emf.codegen.merge.java.facade.JNode;
import org.eclipse.emf.codegen.util.CodeGenUtil;
import org.eclipse.emf.codegen.util.ImportManager;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.Monitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EClass;

public class GeneratorModelAdapter extends AbstractGeneratorAdapter {
	protected static final int MODEL_SERVICE_ID = 0;
	protected static final int ADAPTER_FACTORY_ID = 1;
	protected static final int ADAPTER_ID = 2;
	
	private static final JETEmitterDescriptor[] JET_EMITTER_DESCRIPTORS = {
			new JETEmitterDescriptor("ModelService.javajet", "edu.ustb.sei.mde.runtimemodel.adapter.generator.ModelService"),
			new JETEmitterDescriptor("AdapterFactory.javajet", "edu.ustb.sei.mde.runtimemodel.adapter.generator.AdapterFactory"),
			new JETEmitterDescriptor("Adapter.javajet", "edu.ustb.sei.mde.runtimemodel.adapter.generator.Adapter")
	};

	protected JETEmitterDescriptor[] getJETEmitterDescriptors() {
		return JET_EMITTER_DESCRIPTORS;
	}
	
	@Override
	public boolean canGenerate(Object object, Object projectType) {
		if(projectType == GeneratorUtils.RUNTIME_MODEL_ADAPTER)
			return true;
		return false;
	}

	@Override
	protected Diagnostic doGenerate(Object object, Object projectType, Monitor monitor) throws Exception {
		GeneratorModel model = (GeneratorModel)object;

	    monitor.beginTask("generate runtime model adapters", 2+model.getClassAdapters().size());
	    
	    // 1. generate model service
	    // 2. generate adapter factory
	    
	    generateModelService(model,monitor);
	    generateAdapterFactory(model,monitor);
	    
	    // 3. generate adapters
	    for(ClassAdapter cad : model.getClassAdapters()) {
	    	generateAdapter(model, cad, monitor);
	    }

	    return Diagnostic.OK_INSTANCE;
	}

	private void generateModelService(GeneratorModel model, Monitor monitor) {
		generateJava
        (model.getDirectory(),
         GeneratorUtils.getAdapterPackage(model),
         GeneratorUtils.getModelServiceClassName(model),
         getJETEmitter(getJETEmitterDescriptors(), MODEL_SERVICE_ID),
         new Object[]{model},
         createMonitor(monitor, 1)); 
	}
	
	private void generateAdapterFactory(GeneratorModel model, Monitor monitor) {
		generateJava
        (model.getDirectory(),
         GeneratorUtils.getAdapterPackage(model),
         GeneratorUtils.getAdapterFactoryClassName(model),
         getJETEmitter(getJETEmitterDescriptors(), ADAPTER_FACTORY_ID),
         new Object[]{model},
         createMonitor(monitor, 1)); 
	}
	
	private void generateAdapter(GeneratorModel model, ClassAdapter clazz, Monitor monitor) {
		generateJava
        (model.getDirectory(),
         GeneratorUtils.getAdapterPackage(model),
         GeneratorUtils.getAdapterClassName(clazz),
         getJETEmitter(getJETEmitterDescriptors(), ADAPTER_ID),
         new Object[]{clazz},
         createMonitor(monitor, 1)); 
	}

	
	@Override
	protected void generateJava(String targetPath, String packageName, String className, JETEmitter jetEmitter,
			Object[] arguments, Monitor monitor) {
		 try
		    {
		      monitor.beginTask("", 4);

		      URI targetDirectory = toURI(targetPath).appendSegments(packageName.split("\\."));
		      URI targetFile = targetDirectory.appendSegment(className + ".java");
		      monitor.subTask(CodeGenEcorePlugin.INSTANCE.getString("_UI_Generating_message", new Object[] { targetFile }));

		      ensureContainerExists(targetDirectory, createMonitor(monitor, 1));

		      if (arguments == null)
		      {
		        arguments = new Object[] { generatingObject };
		      }

//		      JControlModel jControlModel = getGenerator().getJControlModel();      
//		      JMerger jMerger = null;
//		      if (jControlModel.canMerge())
//		      {
//		        jMerger = new JMerger(jControlModel);
//		      }

		      createImportManager(packageName, className);
		      String targetFileContents = null;
		      String targetFileEncoding = getEncoding(targetFile);
//		      if (shouldMerge(targetFile) && exists(targetFile) && jMerger != null)
//		      {
//		        // Prime the import manager with the existing imports of the target.
//		        //
//		        jMerger.setTargetCompilationUnit(jMerger.createCompilationUnitForInputStream(createInputStream(targetFile), targetFileEncoding));
//		        JCompilationUnit targetCompilationUnit = jMerger.getTargetCompilationUnit();
//		        ImportManager importManager = getImportManager();
//		        for (JNode node : targetCompilationUnit.getChildren())
//		        {
//		          if (node instanceof JImport)
//		          {
//		            JImport jImport = (JImport)node;
//		            String qualifiedName = jImport.getQualifiedName();
//		            if (!qualifiedName.endsWith(".*"))
//		            {
//		              importManager.addImport(qualifiedName);
//		            }
//		          }
//		        }
//		        targetFileContents = jMerger.getTargetCompilationUnitContents();
//		      }

		      setLineDelimiter(getLineDelimiter(targetFile, targetFileEncoding));
		      String emitterResult = jetEmitter.generate(createMonitor(monitor, 1), arguments, getLineDelimiter());
		      boolean changed = true;
		      String newContents = emitterResult;

		      Options options = getGenerator().getOptions();

//		      if (jMerger != null)
//		      {
//		        jMerger.setFixInterfaceBrace(jControlModel.getFacadeHelper().fixInterfaceBrace());
//		        
//		        try
//		        {
//		          jMerger.setSourceCompilationUnit(jMerger.createCompilationUnitForContents(emitterResult));
//		        }
//		        catch (RuntimeException runtimeException)
//		        {
//		          if (targetFileContents != null)
//		          {
//		            throw runtimeException;
//		          }
//		          else
//		          {
//		            jMerger = null;
//		          }
//		        }
//
//		        if (jMerger != null)
//		        {
//		          // Create a code formatter for this compilation unit, if needed.
//		          //
//		          Object codeFormatter = options.codeFormatting ?
//		            createCodeFormatter(options.codeFormatterOptions, targetFile) : null;
//		  
//		          if (targetFileContents != null)
//		          {
//		            monitor.subTask(CodeGenEcorePlugin.INSTANCE.getString("_UI_ExaminingOld_message", new Object[] { targetFile }));
//		  
//		            monitor.subTask(CodeGenEcorePlugin.INSTANCE.getString("_UI_PreparingNew_message", new Object[] { targetFile }));
//		            jMerger.merge();
//		  
//		            newContents = formatCode(jMerger.getTargetCompilationUnitContents(), codeFormatter, options.commentFormatting);
//		            if (options.importOrganizing)
//		            {
//		              newContents = organizeImports(targetFile.toString(), newContents);
//		            }
//		            changed = !targetFileContents.equals(newContents);
//		  
//		            // If the target is read-only, we can ask the platform to release it, and it may be updated in the process.
//		            //
//		            if (changed && isReadOnly(targetFile) && validateEdit(targetFile, createMonitor(monitor, 1)))
//		            {
//		              jMerger.setTargetCompilationUnit(jMerger.createCompilationUnitForInputStream(createInputStream(targetFile), targetFileEncoding));
//		              jMerger.remerge();
//		              newContents = formatCode(jMerger.getTargetCompilationUnitContents(), codeFormatter, options.commentFormatting);
//		              if (options.importOrganizing)
//		              {
//		                newContents = organizeImports(targetFile.toString(), newContents);
//		              }
//		            }
//		          }
//		          else
//		          {
//		            changed = true;
//		            monitor.subTask(CodeGenEcorePlugin.INSTANCE.getString("_UI_PreparingNew_message", new Object[] { targetFile }));
//
//		            jMerger.merge();
//		            newContents = formatCode(jMerger.getTargetCompilationUnitContents(), codeFormatter, options.commentFormatting);
//		          }
//		  
//		          if (jControlModel.getFacadeHelper() != null)
//		          {
//		            jControlModel.getFacadeHelper().reset();
//		          }
//		        }
//		      }

		      if (true)
		      {
		        newContents = 
		          CodeGenUtil.convertFormat("", true, emitterResult);
		        if (targetFileContents != null)
		        {
		          monitor.subTask(CodeGenEcorePlugin.INSTANCE.getString("_UI_ExaminingOld_message", new Object[] { targetFile }));
		          changed = !targetFileContents.equals(newContents);
		        }
		        else
		        {
		          changed = true;
		        }
		      }
		      monitor.worked(1);

		      if (changed)
		      {
		        String encoding = targetFileEncoding;
		        byte[] bytes = encoding == null ? newContents.getBytes() : newContents.getBytes(encoding);

		        // Apply a redirection pattern, if specified.
		        //
		        String redirection = options.redirectionPattern;
		        boolean redirect = redirection != null && redirection.indexOf("{0}") != -1;

		        if (redirect)
		        {
		          String baseName = MessageFormat.format(redirection, new Object[] { className + ".java" });
		          targetFile = targetDirectory.appendSegment(baseName);
		          monitor.subTask(CodeGenEcorePlugin.INSTANCE.getString("_UI_UsingAlternate_message", new Object[] { targetFile }));
		        } 

		        if (isReadOnly(targetFile))
		        {
		          if (options.forceOverwrite)
		          {
		            setWriteable(targetFile);
		          }
		          else
		          {
		            targetFile = targetDirectory.appendSegment("." + className + ".java.new");
		            monitor.subTask(CodeGenEcorePlugin.INSTANCE.getString("_UI_UsingDefaultAlternate_message", new Object[] { targetFile }));
		          }
		        }

		        OutputStream outputStream = createOutputStream(targetFile);
		        outputStream.write(bytes);
		        outputStream.close();
		      }
		    }
		    catch (Exception e)
		    {
		      throw e instanceof RuntimeException ? (RuntimeException)e : new WrappedException(e);
		    }
		    finally
		    {
		      clearImportManager();
		      setLineDelimiter(null);
		      monitor.done();
		    }
	}
}
