package edu.ustb.sei.mde.runtimemodel.generator;

import java.util.regex.Pattern;

import org.eclipse.emf.codegen.ecore.generator.Generator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;

import edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel;

public class GeneratorUtils {

	static final public String RUNTIME_MODEL_ADAPTER = "RUNTIME_MODEL_ADAPTER_CODE";
	
	static public String getAdapterPackage(GeneratorModel model) {
		String base = model.getBasePackage();
		if(base==null || base.length()==0)
			return model.getAdaptedPackage().getName()+".adapter";
		else
			return base+"."+model.getAdaptedPackage().getName()+".adapter";
	}
	
	static public String getPackageName(GeneratorModel model) {
		return model.getAdaptedPackage().getName();
	}
	
	static public String getEcoreURI(GeneratorModel model) {
		return model.getAdaptedPackage().eResource().getURI().toPlatformString(true);
	}
	
	static public String upperFirst(String str) {
		String first = String.valueOf(str.charAt(0)).toUpperCase();
		return first+str.substring(1);
	}
	
	static public String lowerFirst(String str) {
		String first = String.valueOf(str.charAt(0)).toLowerCase();
		return first+str.substring(1);
	}
	
	static public String getModelServiceClassName(GeneratorModel model) {
		String ePackageName = upperFirst(getPackageName(model));
		return ePackageName+"ModelService";
	}
	
	static public String getAdapterFactoryClassName(GeneratorModel model) {
		String ePackageName = upperFirst(getPackageName(model));
		return ePackageName+"AdapterFactory";
	}

	static public Generator getGenerator(GeneratorModel model) {
		Generator generator;
		generator = new Generator();
	    generator.setInput(model);
	    
//	    generator.getOptions().mergerFacadeHelperClass = "";
	    
	    return generator;
	}

	public static String getAdapterClassName(ClassAdapter clazz) {
		String eClazzName = upperFirst(clazz.getAdaptedClass().getName());
		return eClazzName+"Adapter";
	}
	
	public static String getIDFeatureName(ClassAdapter clazz) {
		for(FeatureAdapter fa : clazz.getFeatureAdapters()) {
			if(fa.isId())
				return fa.getAdaptedFeature().getName();
		}
		return null;
	}
	
	public static String getIDFeatureType(FeatureAdapter feature) {
		ClassAdapter clazz = getReferredClass(feature);
		
		if(clazz==null)
			return null;
		
		for(FeatureAdapter fa : clazz.getFeatureAdapters()) {
			if(fa.isId()) {
				EAttribute f = (EAttribute) fa.getAdaptedFeature();
				return getObjectPrimitiveType(f.getEAttributeType().getInstanceClassName());
			}
		}
		return null;
	}
	
	public static String getObjectPrimitiveType(String t) {
		if("int".equals(t) || "EInt".equals(t))
			return "Integer";
		else if("boolean".equals(t) || "EBoolean".equals(t))
			return "Boolean";
		else if("float".equals(t) || "EFloat".equals(t))
			return "Float";
		else if("double".equals(t) || "EDouble".equals(t))
			return "Double";
		else if("long".equals(t) || "ELong".equals(t))
			return "Long";
		else return t;
	}
	
	public static ClassAdapter getReferredClass(FeatureAdapter feature) {
		GeneratorModel model = (GeneratorModel) EcoreUtil.getRootContainer(feature);
		for(ClassAdapter ca : model.getClassAdapters()) {
			if(ca.getAdaptedClass()==feature.getAdaptedFeature().getEType())
				return ca;
		}
		return null;
	}
	
	public static boolean isSet(FeatureAdapter feature, String service) {
		EStructuralFeature f = feature.eClass().getEStructuralFeature(service);
		return feature.eIsSet(f) && feature.eGet(f)!=null;
	}
	
	
    static final private String getter = "\\$([_a-zA-Z0-9]+)\\h*\\.\\h*eGet\\h*\\(\\h*([_a-zA-Z0-9]+)\\h*\\)";
	static final private String setter = "\\$([_a-zA-Z0-9]+)\\h*\\.\\h*eSet\\h*\\(\\h*([_a-zA-Z0-9]+)\\h*,\\h*(.*)\\)";
	public static String shortcutForGetSet(String code) {
		code = code.replaceAll(getter, "service.eGet($1,\"$2\")");
		code = code.replaceAll(setter, "service.eSet($1,\"$2\",$3)");
		return code;
	}
	
	static final private String id = "\\$([_a-zA-Z0-9]+)\\h*\\<(.*)\\>";
	public static String shortcutForID(String code) {
		code = code.replaceAll(id, "ObjectRepresentation.id(\"$1\",$2)");
		return code;
	}
	
	static final private String typeOf = "\\$([_a-zA-Z0-9]+)\\h*\\.\\h*isTypeOf\\(\\h*([_a-zA-Z0-9]+)\\h*\\)";
	public static String shortcutForTypeOf(String code) {
		code = code.replaceAll(typeOf, "($1).eClass().getName().equals(\"$2\")");
		return code;
	}
	
	public static String shortcut(String code) {
		String code1 = shortcutForGetSet(code);
		String code2 = shortcutForID(code1);
		String code3 = shortcutForTypeOf(code2);
		return code3;
	}

}
