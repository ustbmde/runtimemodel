package edu.ustb.sei.mde.runtimemodel.adapter.generator;

import edu.ustb.sei.mde.runtimemodel.generator.*;
import org.eclipse.emf.ecore.*;
import edu.ustb.sei.mde.runtimemodel.modeladapter.*;

public class ModelService
{
  protected static String nl;
  public static synchronized ModelService create(String lineSeparator)
  {
    nl = lineSeparator;
    ModelService result = new ModelService();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "package ";
  protected final String TEXT_2 = ";" + NL + "" + NL + "import edu.ustb.sei.mde.runtimemodel.generator.*;" + NL + "import org.eclipse.emf.ecore.*;" + NL + "import edu.ustb.sei.mde.runtimemodel.modeladapter.*;" + NL + "import org.eclipse.emf.common.util.URI;" + NL + "" + NL + "" + NL + "public class ";
  protected final String TEXT_3 = " extends ModelService {" + NL + "\tfinal public static ModelService INSTANCE = new ";
  protected final String TEXT_4 = "();" + NL + "\t" + NL + "\tpublic ";
  protected final String TEXT_5 = "() {" + NL + "\t\tsuper();" + NL + "\t}" + NL + "" + NL + "\t@Override" + NL + "\tprotected void loadMetamodel() {";
  protected final String TEXT_6 = NL + "\t\tURI uri = URI.createURI(\"";
  protected final String TEXT_7 = "\");" + NL + "\t\tloadMetamodel(uri);";
  protected final String TEXT_8 = NL + "\tEPackage ";
  protected final String TEXT_9 = ";" + NL + "\tloadMetamodel(metamodel);";
  protected final String TEXT_10 = NL + "\t";
  protected final String TEXT_11 = NL + "\t}" + NL + "" + NL + "\t@Override" + NL + "\tpublic EStructuralFeature getIDFeature(EObject obj) {" + NL + "\t\tEClass cls = obj.eClass();" + NL + "\t\tString clsName = cls.getName();" + NL + "\t\t";
  protected final String TEXT_12 = NL + "\t\tif(clsName.equals(\"";
  protected final String TEXT_13 = "\")){";
  protected final String TEXT_14 = NL + "\t\t  \tthrow new UnsupportedOperationException();";
  protected final String TEXT_15 = NL + "\t\t\treturn cls.getEStructuralFeature(\"";
  protected final String TEXT_16 = "\");";
  protected final String TEXT_17 = NL + "\t\t} else";
  protected final String TEXT_18 = " " + NL + "\t\t\tthrow new UnsupportedOperationException();" + NL + "\t}" + NL + "" + NL + "\tprivate BaseAdapterFactory factory;" + NL + "\t@Override" + NL + "\tpublic BaseAdapterFactory getRuntimeModelAdapterFactory() {" + NL + "\t\tif(factory==null) {" + NL + "\t\t\tfactory = new ";
  protected final String TEXT_19 = "(this, metamodel);" + NL + "\t\t}" + NL + "\t\treturn factory;" + NL + "\t}";
  protected final String TEXT_20 = NL + "\t@Override" + NL + "\tpublic long getAutoRefreshingRate() {" + NL + "\t\treturn ";
  protected final String TEXT_21 = ";" + NL + "\t}";
  protected final String TEXT_22 = NL;
  protected final String TEXT_23 = NL + "\t@Override" + NL + "\tpublic AbstractTimer getTimer() {" + NL + "\t\treturn new ";
  protected final String TEXT_24 = "(this);" + NL + "\t}";
  protected final String TEXT_25 = NL + "}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
GeneratorModel model = (GeneratorModel)argument;

    stringBuffer.append(TEXT_1);
    stringBuffer.append(GeneratorUtils.getAdapterPackage(model));
    stringBuffer.append(TEXT_2);
    stringBuffer.append(GeneratorUtils.getModelServiceClassName(model));
    stringBuffer.append(TEXT_3);
    stringBuffer.append(GeneratorUtils.getModelServiceClassName(model));
    stringBuffer.append(TEXT_4);
    stringBuffer.append(GeneratorUtils.getModelServiceClassName(model));
    stringBuffer.append(TEXT_5);
    
if(model.getLoadMetamodel()==null) {

    stringBuffer.append(TEXT_6);
    stringBuffer.append(GeneratorUtils.getEcoreURI(model));
    stringBuffer.append(TEXT_7);
    
} else {
	String loadCode = model.getLoadMetamodel();
if(loadCode.matches("\\s*metamodel\\s*=.*")) {

    stringBuffer.append(TEXT_8);
    stringBuffer.append(loadCode);
    stringBuffer.append(TEXT_9);
    
} else {

    stringBuffer.append(TEXT_10);
    stringBuffer.append(loadCode);
    
}
}

    stringBuffer.append(TEXT_11);
    
		for(ClassAdapter ca : model.getClassAdapters()) {
		  String id = GeneratorUtils.getIDFeatureName(ca);

    stringBuffer.append(TEXT_12);
    stringBuffer.append(ca.getAdaptedClass().getName());
    stringBuffer.append(TEXT_13);
    
		  if(id==null) {

    stringBuffer.append(TEXT_14);
    
		  } else {

    stringBuffer.append(TEXT_15);
    stringBuffer.append(id);
    stringBuffer.append(TEXT_16);
    
		  }

    stringBuffer.append(TEXT_17);
    
		}

    stringBuffer.append(TEXT_18);
    stringBuffer.append(GeneratorUtils.getAdapterFactoryClassName(model));
    stringBuffer.append(TEXT_19);
    
if(model.getRefreshingRate()!=0) {

    stringBuffer.append(TEXT_20);
    stringBuffer.append(model.getRefreshingRate());
    stringBuffer.append(TEXT_21);
    
}

    stringBuffer.append(TEXT_22);
    
if(model.getTimerClass()!=null) {

    stringBuffer.append(TEXT_23);
    stringBuffer.append(model.getTimerClass());
    stringBuffer.append(TEXT_24);
    
}

    stringBuffer.append(TEXT_25);
    return stringBuffer.toString();
  }
}
