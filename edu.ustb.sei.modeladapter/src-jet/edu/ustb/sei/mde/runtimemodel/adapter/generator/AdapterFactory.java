package edu.ustb.sei.mde.runtimemodel.adapter.generator;

import edu.ustb.sei.mde.runtimemodel.generator.*;
import org.eclipse.emf.ecore.*;
import edu.ustb.sei.mde.runtimemodel.modeladapter.*;

public class AdapterFactory
{
  protected static String nl;
  public static synchronized AdapterFactory create(String lineSeparator)
  {
    nl = lineSeparator;
    AdapterFactory result = new AdapterFactory();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "package ";
  protected final String TEXT_2 = ";" + NL + "" + NL + "import edu.ustb.sei.mde.runtimemodel.generator.*;" + NL + "import org.eclipse.emf.ecore.*;" + NL + "import edu.ustb.sei.mde.runtimemodel.modeladapter.*;" + NL + "import org.eclipse.emf.common.util.URI;" + NL + "import org.eclipse.emf.common.notify.Adapter;";
  protected final String TEXT_3 = NL + NL + "public class ";
  protected final String TEXT_4 = " extends BaseAdapterFactory {" + NL + "" + NL + "\tpublic ";
  protected final String TEXT_5 = "(ModelService modelService, EPackage modelPackage) {" + NL + "\t\tsuper(modelService, modelPackage);" + NL + "\t}" + NL + "" + NL + "\t@Override" + NL + "\tprotected Adapter createPackageAdapter(EPackage eObject) {" + NL + "\t\treturn new PackageAdapter(modelService, this);" + NL + "\t}" + NL + "" + NL + "\t@Override" + NL + "\tprotected Adapter createModelAdapter(EClass clazz, EObject eObject) {" + NL + "\t\tString clsName = clazz.getName();" + NL + "\t\t";
  protected final String TEXT_6 = NL + "\t\tif(clsName.equals(\"";
  protected final String TEXT_7 = "\")) {" + NL + "\t\t    return new ";
  protected final String TEXT_8 = "();" + NL + "\t\t} else";
  protected final String TEXT_9 = NL + "\t\treturn null;" + NL + "\t}" + NL + "" + NL + "}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
GeneratorModel model = (GeneratorModel)argument;

    stringBuffer.append(TEXT_1);
    stringBuffer.append(GeneratorUtils.getAdapterPackage(model));
    stringBuffer.append(TEXT_2);
    
String clazzName = GeneratorUtils.getAdapterFactoryClassName(model);

    stringBuffer.append(TEXT_3);
    stringBuffer.append(clazzName);
    stringBuffer.append(TEXT_4);
    stringBuffer.append(clazzName);
    stringBuffer.append(TEXT_5);
    
for(ClassAdapter ca : model.getClassAdapters()) {

    stringBuffer.append(TEXT_6);
    stringBuffer.append(ca.getAdaptedClass().getName());
    stringBuffer.append(TEXT_7);
    stringBuffer.append(GeneratorUtils.getAdapterClassName(ca));
    stringBuffer.append(TEXT_8);
    
}			

    stringBuffer.append(TEXT_9);
    return stringBuffer.toString();
  }
}
