package edu.ustb.sei.mde.runtimemodel.adapter.generator;

import edu.ustb.sei.mde.runtimemodel.generator.*;
import org.eclipse.emf.ecore.*;
import edu.ustb.sei.mde.runtimemodel.modeladapter.*;

public class Adapter
{
  protected static String nl;
  public static synchronized Adapter create(String lineSeparator)
  {
    nl = lineSeparator;
    Adapter result = new Adapter();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "package ";
  protected final String TEXT_2 = ";" + NL + "" + NL + "import edu.ustb.sei.mde.runtimemodel.generator.*;" + NL + "import org.eclipse.emf.ecore.*;" + NL + "import edu.ustb.sei.mde.runtimemodel.modeladapter.*;" + NL + "import org.eclipse.emf.common.util.URI;" + NL + "import edu.ustb.sei.mde.runtimemodel.modeladapter.FeatureCacheHolder.*;" + NL;
  protected final String TEXT_3 = NL + "import ";
  protected final String TEXT_4 = ";";
  protected final String TEXT_5 = NL + NL + "public class ";
  protected final String TEXT_6 = " extends BaseAdapter {" + NL + "\tpublic ";
  protected final String TEXT_7 = "() {" + NL + "\t}" + NL + "" + NL + "\t@Override" + NL + "\tpublic void init() {" + NL + "\t\tEClass eClass = this.getSelf().eClass();" + NL + "\t\tEPackage ePackage = eClass.getEPackage();" + NL + "\t\tPackageAdapter packAdapter = BaseAdapterFactory.getPackageAdapter(ePackage);" + NL + "\t\tModelService service = packAdapter.getModelService();" + NL + "\t\t" + NL + "\t\t// install feature container";
  protected final String TEXT_8 = NL + "{";
  protected final String TEXT_9 = NL + "\t\t  \t  TraceBasedMultiValueFeatureCacheHolder<ObjectRepresentation<";
  protected final String TEXT_10 = ">, EObject> ";
  protected final String TEXT_11 = " = new TraceBasedMultiValueFeatureCacheHolder<ObjectRepresentation<";
  protected final String TEXT_12 = ">, EObject>();";
  protected final String TEXT_13 = NL + "\t\t  \t  TraceBasedSingleValueFeatureCacheHolder<ObjectRepresentation<";
  protected final String TEXT_14 = ">, EObject> ";
  protected final String TEXT_15 = " = new TraceBasedSingleValueFeatureCacheHolder<ObjectRepresentation<";
  protected final String TEXT_16 = ">, EObject>();";
  protected final String TEXT_17 = NL + "\t\t  \t\tStateBasedMultiValueFeatureCacheHolder<";
  protected final String TEXT_18 = ",";
  protected final String TEXT_19 = "> ";
  protected final String TEXT_20 = " = new StateBasedMultiValueFeatureCacheHolder<";
  protected final String TEXT_21 = ",";
  protected final String TEXT_22 = ">();";
  protected final String TEXT_23 = NL + "\t\t  \t\tStateBasedSingleValueFeatureCacheHolder<";
  protected final String TEXT_24 = ",";
  protected final String TEXT_25 = "> ";
  protected final String TEXT_26 = " = new StateBasedSingleValueFeatureCacheHolder<";
  protected final String TEXT_27 = ",";
  protected final String TEXT_28 = ">();";
  protected final String TEXT_29 = NL + "\t\t  // install conversion service" + NL + "\t\t  ";
  protected final String TEXT_30 = NL + "\t\t  ";
  protected final String TEXT_31 = ".physicalToLogicalConverter = ";
  protected final String TEXT_32 = ";" + NL + "\t\t  ";
  protected final String TEXT_33 = ".logicalToPhysicalConverter = ";
  protected final String TEXT_34 = ";" + NL + "\t\t  ";
  protected final String TEXT_35 = NL + "\t\t    ";
  protected final String TEXT_36 = ".physicalToLogicalConverter = ConversionService.objectRepresentationToEObject();" + NL + "\t\t    ";
  protected final String TEXT_37 = ".logicalToPhysicalConverter = ConversionService.eObjectToObjectRepresentation();" + NL + "\t\t    ";
  protected final String TEXT_38 = NL + "\t\t    ";
  protected final String TEXT_39 = ".physicalToLogicalConverter = ConversionService.identity();" + NL + "\t\t    ";
  protected final String TEXT_40 = ".logicalToPhysicalConverter = ConversionService.identity();" + NL + "\t\t    ";
  protected final String TEXT_41 = NL + "\t\t  ";
  protected final String TEXT_42 = ".feature = eClass.getEStructuralFeature(\"";
  protected final String TEXT_43 = "\");" + NL + "\t\t  " + NL + "\t\t  this.addFeatureCache(";
  protected final String TEXT_44 = ");" + NL + "\t\t  " + NL + "\t\t  ";
  protected final String TEXT_45 = "\t  " + NL + "\t\t  ";
  protected final String TEXT_46 = ".getService = (self) -> {" + NL + "\t\t    ";
  protected final String TEXT_47 = NL + "\t\t  };" + NL + "\t\t  ";
  protected final String TEXT_48 = NL + "\t\t  " + NL + "\t\t  ";
  protected final String TEXT_49 = "\t  " + NL + "\t\t  ";
  protected final String TEXT_50 = ".postService = (self,val) -> {" + NL + "\t\t    ";
  protected final String TEXT_51 = NL + "\t\t  };" + NL + "\t\t  ";
  protected final String TEXT_52 = NL + "\t\t  " + NL + "\t\t  ";
  protected final String TEXT_53 = "\t  " + NL + "\t\t  ";
  protected final String TEXT_54 = ".deleteService = (self,val) -> {" + NL + "\t\t    ";
  protected final String TEXT_55 = NL + "\t\t  };" + NL + "\t\t  ";
  protected final String TEXT_56 = NL + "\t\t  " + NL + "\t\t  ";
  protected final String TEXT_57 = "\t  " + NL + "\t\t  ";
  protected final String TEXT_58 = ".putService = (self,old,val) -> {" + NL + "\t\t    ";
  protected final String TEXT_59 = NL + "\t\t  };" + NL + "\t\t  ";
  protected final String TEXT_60 = NL + "}" + NL;
  protected final String TEXT_61 = NL + "\t}" + NL + "" + NL + "}";

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
    
ClassAdapter classAdapter = (ClassAdapter)argument;
GeneratorModel model = (GeneratorModel) classAdapter.eContainer();

    stringBuffer.append(TEXT_1);
    stringBuffer.append(GeneratorUtils.getAdapterPackage(model));
    stringBuffer.append(TEXT_2);
    
for(String imp : classAdapter.getImports()) {

    stringBuffer.append(TEXT_3);
    stringBuffer.append(imp);
    stringBuffer.append(TEXT_4);
    
}

    stringBuffer.append(TEXT_5);
    stringBuffer.append(GeneratorUtils.getAdapterClassName(classAdapter));
    stringBuffer.append(TEXT_6);
    stringBuffer.append(GeneratorUtils.getAdapterClassName(classAdapter));
    stringBuffer.append(TEXT_7);
    
		for(FeatureAdapter fa : classAdapter.getFeatureAdapters()) {
		  if(fa.isId()) continue;

    stringBuffer.append(TEXT_8);
    
		  
		  EStructuralFeature feature = fa.getAdaptedFeature();
		  if(feature instanceof EReference) {
		    String idFeatureType = GeneratorUtils.getIDFeatureType(fa);
		    
		    String physicalType = fa.getConversion() == null ? idFeatureType : fa.getConversion().getPhysicalType();
		    
		  	if(feature.isMany()) {

    stringBuffer.append(TEXT_9);
    stringBuffer.append(physicalType);
    stringBuffer.append(TEXT_10);
    stringBuffer.append(fa.getAdaptedFeature().getName());
    stringBuffer.append(TEXT_11);
    stringBuffer.append(physicalType);
    stringBuffer.append(TEXT_12);
    
		  	} else {

    stringBuffer.append(TEXT_13);
    stringBuffer.append(physicalType);
    stringBuffer.append(TEXT_14);
    stringBuffer.append(fa.getAdaptedFeature().getName());
    stringBuffer.append(TEXT_15);
    stringBuffer.append(physicalType);
    stringBuffer.append(TEXT_16);
    
		  	}
		  } else {
		    String featureType = GeneratorUtils.getObjectPrimitiveType(fa.getAdaptedFeature().getEType().getInstanceClassName());
		    String physicalType = fa.getConversion() == null ? featureType : fa.getConversion().getPhysicalType();
		    
		  	if(feature.isMany()) {

    stringBuffer.append(TEXT_17);
    stringBuffer.append(physicalType);
    stringBuffer.append(TEXT_18);
    stringBuffer.append(featureType);
    stringBuffer.append(TEXT_19);
    stringBuffer.append(fa.getAdaptedFeature().getName());
    stringBuffer.append(TEXT_20);
    stringBuffer.append(physicalType);
    stringBuffer.append(TEXT_21);
    stringBuffer.append(featureType);
    stringBuffer.append(TEXT_22);
    
		  	} else {

    stringBuffer.append(TEXT_23);
    stringBuffer.append(physicalType);
    stringBuffer.append(TEXT_24);
    stringBuffer.append(featureType);
    stringBuffer.append(TEXT_25);
    stringBuffer.append(fa.getAdaptedFeature().getName());
    stringBuffer.append(TEXT_26);
    stringBuffer.append(physicalType);
    stringBuffer.append(TEXT_27);
    stringBuffer.append(featureType);
    stringBuffer.append(TEXT_28);
    
		  	}
		  }
		  
		  
    stringBuffer.append(TEXT_29);
    
		  if(fa.getConversion()!=null) {
		  
    stringBuffer.append(TEXT_30);
    stringBuffer.append(fa.getAdaptedFeature().getName());
    stringBuffer.append(TEXT_31);
    stringBuffer.append(fa.getConversion().getPhysicalToLogical());
    stringBuffer.append(TEXT_32);
    stringBuffer.append(fa.getAdaptedFeature().getName());
    stringBuffer.append(TEXT_33);
    stringBuffer.append(fa.getConversion().getLogicalToPhysical());
    stringBuffer.append(TEXT_34);
    
		  } else {
		    if(feature instanceof EReference) {
		    
    stringBuffer.append(TEXT_35);
    stringBuffer.append(fa.getAdaptedFeature().getName());
    stringBuffer.append(TEXT_36);
    stringBuffer.append(fa.getAdaptedFeature().getName());
    stringBuffer.append(TEXT_37);
    
		    } else {
		    
    stringBuffer.append(TEXT_38);
    stringBuffer.append(fa.getAdaptedFeature().getName());
    stringBuffer.append(TEXT_39);
    stringBuffer.append(fa.getAdaptedFeature().getName());
    stringBuffer.append(TEXT_40);
     
		    }
		  }

    stringBuffer.append(TEXT_41);
    stringBuffer.append(fa.getAdaptedFeature().getName());
    stringBuffer.append(TEXT_42);
    stringBuffer.append(feature.getName());
    stringBuffer.append(TEXT_43);
    stringBuffer.append(fa.getAdaptedFeature().getName());
    stringBuffer.append(TEXT_44);
    
		  	if(GeneratorUtils.isSet(fa,"get")) {
		  
    stringBuffer.append(TEXT_45);
    stringBuffer.append(fa.getAdaptedFeature().getName());
    stringBuffer.append(TEXT_46);
    stringBuffer.append(GeneratorUtils.shortcut(fa.getGet()));
    stringBuffer.append(TEXT_47);
    
		    }
		  
    stringBuffer.append(TEXT_48);
    
		  	if(GeneratorUtils.isSet(fa,"post")) {
		  
    stringBuffer.append(TEXT_49);
    stringBuffer.append(fa.getAdaptedFeature().getName());
    stringBuffer.append(TEXT_50);
    stringBuffer.append(GeneratorUtils.shortcut(fa.getPost()));
    stringBuffer.append(TEXT_51);
    
		    }
		  
    stringBuffer.append(TEXT_52);
    
		  	if(GeneratorUtils.isSet(fa,"delete")) {
		  
    stringBuffer.append(TEXT_53);
    stringBuffer.append(fa.getAdaptedFeature().getName());
    stringBuffer.append(TEXT_54);
    stringBuffer.append(GeneratorUtils.shortcut(fa.getDelete()));
    stringBuffer.append(TEXT_55);
    
		    }
		  
    stringBuffer.append(TEXT_56);
    
		  	if(GeneratorUtils.isSet(fa,"put")) {
		  
    stringBuffer.append(TEXT_57);
    stringBuffer.append(fa.getAdaptedFeature().getName());
    stringBuffer.append(TEXT_58);
    stringBuffer.append(GeneratorUtils.shortcut(fa.getPut()));
    stringBuffer.append(TEXT_59);
    
		    }
		  
    stringBuffer.append(TEXT_60);
    
		}

    stringBuffer.append(TEXT_61);
    return stringBuffer.toString();
  }
}
