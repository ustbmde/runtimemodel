package edu.ustb.sei.mde.runtimemodel.ui;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.PlatformUI;

import edu.ustb.sei.mde.runtimemodel.modeladapter.BaseAdapter;
import edu.ustb.sei.mde.runtimemodel.modeladapter.ModelService;

public class ClearTraceHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Object o = selection.getFirstElement();
		
		
		Resource res = null;
		if(o instanceof EObject) {
			res = ((EObject) o).eResource();
		} else {
			res = (Resource) o;
		}
		
		EObject eObj = res.getContents().get(0);
		
		EPackage pkg = eObj.eClass().getEPackage();
		
		ModelService s = ModelServiceTable.get(pkg);
		
		clear(s,eObj);
		
		return null;
	}
	
	private void clear(ModelService s, EObject cur) {
		BaseAdapter ada = s.getAdapter(cur);
		if(ada!=null) {
			ada.clearTraceAndUnsetFeature();
		}
		for(EObject next : cur.eContents()) {
			clear(s,next);
		}
	}

	private IStructuredSelection selection;
	
	@Override
	public boolean isEnabled() {
		selection = null;
		
		ISelection s = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getSelection();
		if(s!=null && s instanceof IStructuredSelection) {
			Object first = ((IStructuredSelection)s).getFirstElement();
			if(first != null && (first instanceof EObject  || first instanceof Resource) ) {
				selection = (IStructuredSelection) s;
				return true;
			} else
				return false;
		} else 
			return false;
	}
}
