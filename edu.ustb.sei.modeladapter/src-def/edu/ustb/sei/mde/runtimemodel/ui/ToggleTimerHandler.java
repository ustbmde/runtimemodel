package edu.ustb.sei.mde.runtimemodel.ui;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.PlatformUI;

import edu.ustb.sei.mde.runtimemodel.modeladapter.AbstractTimer;
import edu.ustb.sei.mde.runtimemodel.modeladapter.ModelService;

public class ToggleTimerHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		
		Object o = selection.getFirstElement();
		
		Resource resource = null;
		if(o instanceof EObject) {
			resource = ((EObject) o).eResource();
		} else {
			resource = ((Resource)o);
		}
		EPackage pkg = resource.getContents().get(0).eClass().getEPackage();
		ModelService s = ModelServiceTable.get(pkg);
		if(s==null) {
			return null;
		}
		
		AbstractTimer timer = AbstractTimer.getTimer(resource);
		
		if(timer==null) {
			timer = AbstractTimer.setup(resource,s);
			MessageDialog.openInformation(PlatformUI.getWorkbench().getDisplay().getActiveShell(), "Timer", "Timer is started!");
			timer.begin();
		} else {
			timer.cancel();
			timer.end();
			MessageDialog.openInformation(PlatformUI.getWorkbench().getDisplay().getActiveShell(), "Timer", "Timer is canceled!");
		}
		
		return null;
	}
	
	private IStructuredSelection selection;
	
	@Override
	public boolean isEnabled() {
		selection = null;
		
		ISelection s = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getSelection();
		
		if(s!=null && s instanceof IStructuredSelection) {
			Object first = ((IStructuredSelection)s).getFirstElement();
			if(first != null && (first instanceof EObject  || first instanceof Resource) ) {
				selection = (IStructuredSelection) s;
				return true;
			} else
				return false;
		} else 
			return false;
	}
}
