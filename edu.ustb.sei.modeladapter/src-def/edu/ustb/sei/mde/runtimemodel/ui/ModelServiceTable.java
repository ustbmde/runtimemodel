package edu.ustb.sei.mde.runtimemodel.ui;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.inject.Inject;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EPackage;

import edu.ustb.sei.mde.runtimemodel.modeladapter.ModelService;

public class ModelServiceTable {

	static final public String SERVICE_ID = "edu.ustb.sei.mde.runtimemodel.modelservice";
	
	static private Map<EPackage,ModelService> serviceMap = new HashMap<EPackage,ModelService>();
	
	 
	
	static public void refresh() {
		IExtensionRegistry registry = Platform.getExtensionRegistry();
		if(registry !=null) {
			IConfigurationElement[] config =
                    registry.getConfigurationElementsFor(SERVICE_ID);
			
			for(IConfigurationElement c : config) {
				String nsURI = c.getAttribute("nsURI");
				String clazz = c.getAttribute("class");
				
				if(nsURI!=null && checkNsURI(nsURI,clazz)) {
					// do nothing
				} else {
					try {
						Object o = c.createExecutableExtension("class");
						if(o!=null && o instanceof ModelService) {
							EPackage pkg = ((ModelService) o).getMetamodel();
							serviceMap.put(pkg, (ModelService)o);
						}
					} catch (CoreException e) {
						e.printStackTrace();
					}
				}
				
				
			}
		} else {
			System.out.println("no registry");
		}
	}

	static private boolean checkNsURI(String nsURI, String clazz) {
		for(Entry<EPackage,ModelService> e : serviceMap.entrySet()) {
			if(e.getKey().getNsURI().equals(nsURI) && e.getValue().getClass().getName().equals(clazz))
				return true;
		}
		return false;
	}
	
	static public ModelService get(EPackage pkg) {
		ModelService s = serviceMap.get(pkg);
		if(s==null) {
			refresh();
			s = serviceMap.get(pkg);
		}
		return s;
	}
}
