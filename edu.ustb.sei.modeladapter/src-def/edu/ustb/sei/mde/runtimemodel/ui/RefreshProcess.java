package edu.ustb.sei.mde.runtimemodel.ui;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.operation.IRunnableWithProgress;

import edu.ustb.sei.mde.runtimemodel.modeladapter.BaseAdapter;
import edu.ustb.sei.mde.runtimemodel.modeladapter.ModelService;

public class RefreshProcess implements IRunnableWithProgress {
	
	public RefreshProcess(EObject object, boolean isRefreshModel, ModelService s) {
		super();
		this.object = object;
		this.isRefreshModel = isRefreshModel;
		this.service = s;
	}

	private EObject object;
	private boolean isRefreshModel;
	private ModelService service;

	@Override
	public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
		BaseAdapter adapter = service.getAdapter(object);
		if(adapter!=null) {
			if(isRefreshModel) {
				monitor.beginTask("Refreshing the model", collectWorkSize());

				adapter.updateLogical(monitor);
				
			} else {
				monitor.beginTask("Refreshing the physical", collectWorkSize());
				
				adapter.updatePhysical(monitor);
			}
			
		}
	}

	
	private int collectWorkSize() {
		int size = 1;
		
		TreeIterator<EObject> it = object.eAllContents();
		
		while(it.hasNext()) {
			size++;
			it.next();
		}
		
		return size;
	}
}
