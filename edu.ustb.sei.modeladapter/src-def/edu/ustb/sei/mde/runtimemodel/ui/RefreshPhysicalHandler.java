package edu.ustb.sei.mde.runtimemodel.ui;

import java.lang.reflect.InvocationTargetException;
import java.util.Iterator;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.internal.Workbench;

import edu.ustb.sei.mde.runtimemodel.modeladapter.BaseAdapter;
import edu.ustb.sei.mde.runtimemodel.modeladapter.ModelService;

public class RefreshPhysicalHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Object o = selection.getFirstElement();
		EObject eObj = null;
		if(o instanceof EObject) {
			eObj = (EObject)o;
		} else {
			eObj = ((Resource)o).getContents().get(0);
		}
		EPackage pkg = eObj.eClass().getEPackage();
		
		ModelService s = ModelServiceTable.get(pkg);

		if(s!=null) {
			
			RefreshProcess pro = new RefreshProcess(eObj, false, s);
			
			ProgressMonitorDialog dialog = new ProgressMonitorDialog(PlatformUI.getWorkbench().getDisplay().getActiveShell());
			
			try {
				dialog.run(true, true, pro);
			} catch (Exception e) {
				MessageDialog.open(MessageDialog.ERROR, 
						PlatformUI.getWorkbench().getDisplay().getActiveShell(),
						"Error", e.getMessage(), SWT.NONE);
			}
			
		} else {
			MessageDialog.open(MessageDialog.ERROR, 
					PlatformUI.getWorkbench().getDisplay().getActiveShell(), 
					"Error", "This model is not a runtime model!\nThere is no ModelService for it.", SWT.NONE);
		}
		return null;
	}
	
	private IStructuredSelection selection;
	
	@Override
	public boolean isEnabled() {
		selection = null;
		
		ISelection s = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getSelection();
		if(s!=null && s instanceof IStructuredSelection) {
			Object first = ((IStructuredSelection)s).getFirstElement();
			if(first != null && (first instanceof EObject  || first instanceof Resource) ) {
				selection = (IStructuredSelection) s;
				return true;
			} else
				return false;
		} else 
			return false;
	}
}
