package edu.ustb.sei.mde.runtimemodel.modeladapter;

public class Trace<LT,RT> {
	public LT left;
	public RT right;
	
	static public <LT,RT> Trace<LT,RT> createTrace(LT l, RT r) {
		Trace<LT,RT> t = new Trace<LT,RT>();
		t.left = l;
		t.right = r;
		return t;
	}
}
