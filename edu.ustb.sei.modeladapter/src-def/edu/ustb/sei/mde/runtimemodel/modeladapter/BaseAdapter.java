package edu.ustb.sei.mde.runtimemodel.modeladapter;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

abstract public class BaseAdapter extends AdapterImpl {

	final static public Object RUNTIMEMODELADATPER = BaseAdapter.class; 

	@Override
	public boolean isAdapterForType(Object type) {
		return type==RUNTIMEMODELADATPER;
	}
	
	public EObject getSelf() {
		return (EObject) this.target;
	}
	
	protected Map<EStructuralFeature, FeatureCacheHolder<?,?,?,?>> cacheHolder = new HashMap<EStructuralFeature, FeatureCacheHolder<?,?,?,?>>();
	
	public void addFeatureCache(FeatureCacheHolder<?,?,?,?> cache) {
		cache.selfAdapter = this;
		cacheHolder.put(cache.feature, cache);
	}
	
	public FeatureCacheHolder<?,?,?,?> getFeatureCache(EStructuralFeature feature) {
		return cacheHolder.get(feature);
	}
	
	public abstract void init();
	
	
	public void updateLogical() {
		updateLogical(null);
	}
	
	public void updateLogical(IProgressMonitor monitor) {
		updateLogicalSelf();
		updateLogicalDescendant(monitor);
		updateLogicalReference();
		
		workOne(monitor);
	}
	
	public void updatePhysical() {
		updatePhysical(null);
	}
	
	public void updatePhysical(IProgressMonitor monitor) {
		updatePhysicalSelf();
		updatePhysicalDescendant(monitor);
		updatePhysicalReference();
		
		workOne(monitor);
	}

	public void workOne(IProgressMonitor monitor) {
		if(monitor!=null)
			monitor.worked(1);
	}

	protected void updateLogicalReference() {
		this.cacheHolder.forEach((feature,cache)->{
			if((feature instanceof EReference) 
					&& !((EReference) feature).isContainment()) {
				cache.updateLogicalFromPhysical();
			}
		});
	}
	
	protected void updatePhysicalReference() {
		this.cacheHolder.forEach((feature,cache)->{
			if((feature instanceof EReference) 
					&& !((EReference) feature).isContainment()) {
				cache.updatePhysicalFromLogical();
			}
		});
	}

	protected void updateLogicalDescendant(IProgressMonitor monitor) {
		EPackage pkg = this.getSelf().eClass().getEPackage();
		PackageAdapter pkgAdapter = BaseAdapterFactory.getPackageAdapter(pkg);
		
		for(EObject obj : this.getSelf().eContents()) {
			BaseAdapter adapter = pkgAdapter.getModelService().getAdapter(obj);
			if(adapter!=null)
				adapter.updateLogical(monitor);
			else
				workOne(monitor); // skip 
		}
	}
	
	protected void updatePhysicalDescendant(IProgressMonitor monitor) {
		EPackage pkg = this.getSelf().eClass().getEPackage();
		PackageAdapter pkgAdapter = BaseAdapterFactory.getPackageAdapter(pkg);
		
		for(EObject obj : this.getSelf().eContents()) {
			BaseAdapter adapter = pkgAdapter.getModelService().getAdapter(obj);
			if(adapter!=null)
				adapter.updatePhysical(monitor);
			else
				workOne(monitor); // skip 
		}
	}

	protected void updateLogicalSelf() {
		this.cacheHolder.forEach((feature,cache)->{
			if((feature instanceof EAttribute) 
					|| ((EReference) feature).isContainment()) {
				cache.updateLogicalFromPhysical();
			}
		});
	}
	
	protected void updatePhysicalSelf() {
		this.cacheHolder.forEach((feature,cache)->{
			if((feature instanceof EAttribute) 
					|| ((EReference) feature).isContainment()) {
				cache.updatePhysicalFromLogical();
			}
		});
	}

	public void clearTraceAndUnsetFeature() {
		this.cacheHolder.forEach((feature,cache) -> {
			cache.clear();
			EObject self = this.getSelf();
			self.eUnset(feature);
		});
	}
}
