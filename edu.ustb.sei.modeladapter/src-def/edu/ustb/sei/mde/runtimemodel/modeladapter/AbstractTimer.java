package edu.ustb.sei.mde.runtimemodel.modeladapter;

import java.util.Timer;
import java.util.TimerTask;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

abstract public class AbstractTimer implements Adapter {
	
	public AbstractTimer(ModelService s) {
		timer = new Timer();
		timerTask = createTask();
		this.service = s;
		try {
			editor = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActiveEditor();
		} catch (Exception e) {
			editor = null;
		}
	}

	public boolean isLoaded() {
		return this.resource != null && this.resource.isLoaded();
	}

	public boolean shouldCancel() {
		return isClosed() || !isLoaded();
	}

	protected boolean isClosed() {
		if (editor == null)
			return false;
		try {
			for (IWorkbenchWindow window : PlatformUI.getWorkbench().getWorkbenchWindows()) {
				for (IWorkbenchPage page : window.getPages()) {
					for (IEditorReference er : page.getEditorReferences()) {
						if (er.getEditor(false) == editor) {
							return false;
						}
					}
				}
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	abstract protected TimerTask createTask();

	protected Resource resource;
	protected ModelService service;

	@Override
	public Notifier getTarget() {
		return resource;
	}

	@Override
	public void setTarget(Notifier newTarget) {
			if (newTarget == null) {
				timerTask.cancel();
				timer.cancel();
	//			this.resource.setTrackingModification(false);
				this.resource = (Resource) newTarget;
			} else {
				this.resource = (Resource) newTarget;
	//			this.resource.setTrackingModification(true); // for non-atomic modification (e.g., moving), this does not work very well
				timer.schedule(timerTask, 0, service==null ? 1000 : service.getAutoRefreshingRate());
			}
	
		}

	@Override
	public boolean isAdapterForType(Object type) {
		// can only be created manually
		return false;
	}

	public void cancel() {
		this.resource.eAdapters().remove(this);
	}

	public Resource getResource() {
		return resource;
	}

	public static AbstractTimer setup(Resource res, ModelService s) {
		for (Adapter a : res.eAdapters()) {
			if (AbstractTimer.class.isInstance(a))
				return (AbstractTimer)a;
		}
		AbstractTimer t = s.getTimer();
		res.eAdapters().add(t);
		return t;
	}

	public static void remove(Resource res) {
		AbstractTimer t = null;
		for (Adapter a : res.eAdapters()) {
			if (AbstractTimer.class.isInstance(a)) {
				t = ((AbstractTimer) a);
			}
		}
		if (t != null) {
			t.cancel();
		}
	}

	public static AbstractTimer getTimer(Resource res) {
		for (Adapter a : res.eAdapters()) {
			if (AbstractTimer.class.isInstance(a))
				return (AbstractTimer) a;
		}
		return null;
	}

	protected Timer timer;
	protected IEditorPart editor;
	protected TimerTask timerTask;

	public ModelService getModelService() {
		return this.service;
	}
	
	public void begin() {
		
	}
	
	public void end() {
		
	}

}
