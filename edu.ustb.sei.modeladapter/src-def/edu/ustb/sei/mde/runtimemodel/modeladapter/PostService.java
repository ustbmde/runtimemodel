package edu.ustb.sei.mde.runtimemodel.modeladapter;

import org.eclipse.emf.ecore.EObject;

@FunctionalInterface
public interface PostService<PET,LET> {
	PET apply(EObject o, LET val);
}
