package edu.ustb.sei.mde.runtimemodel.modeladapter;

public class ObjectRepresentation<T> {
	public String typeName;
	public T representation;
	
	public static <T> ObjectRepresentation<T> id(String type, T data) {
		ObjectRepresentation<T> ret = new ObjectRepresentation<T>();
		ret.typeName = type;
		ret.representation = data;
		return ret;
	}
	
	public boolean equals(Object r) {
		if(r==null || !(r instanceof ObjectRepresentation<?>)) return false;
		if(this==r) return true;
		
		ObjectRepresentation<?> rid = (ObjectRepresentation<?>)r;
		if((typeName==null && rid.typeName!=null) 
				|| (typeName!=null && !typeName.equals(rid.typeName))) 
			return false;
		
		if((representation==null && rid.representation!=null) 
				|| (representation!=null && !representation.equals(rid.representation))) 
			return false;
		
		return true;
	}
}
