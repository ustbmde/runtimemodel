package edu.ustb.sei.mde.runtimemodel.modeladapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;


abstract public class FeatureCacheHolder<PET,PT,LET,LT> {
	
	public EStructuralFeature feature;
	public BaseAdapter selfAdapter;
	
	
	
	public PT getPhysicalValue() {
		if(this.getService==null) {
			return null;
		} else 
			return this.getService.apply(getSelf());
	}
	
	public void updateLogicalFromPhysical() {
		Delta<PET,PET> delta = this.calculatePhysicalDelta();
		
		//1. change, resolve conflict in the future
		for(Trace<PET,PET> c : delta.getChanged()) {
			this.changeLogical(c.left, c.right);
		}
		
		//2. del
		for(PET k : delta.getDeleted()) {
			this.deleteLogical(k);
		}
		
		//3. add
		for(PET k : delta.getAdded()) {
			this.addLogical(k);
		}
	}
	
	protected void mergeLogical() {
	}
	
	protected void addPhysical(LET k) {
		PET pk = null;
		if(this.postService!=null) {
			pk = this.postService.apply(getSelf(), k);
		}
		if(pk==null) 
			pk = this.logicalToPhysicalConverter.apply(k, getSelf());
		this.addCache(pk,k);
	}

	abstract protected void addCache(PET r, LET k);

	protected void deletePhysical(PET k) {
		if(this.deleteService!=null) {
			this.deleteService.apply(getSelf(), k);
		}
		this.delCache(k);
	}

	abstract protected void delCache(PET k);

	protected void changePhysical(PET left, LET right) {
		PET nk = null;
		if(this.putService!=null) {
			nk = this.putService.apply(getSelf(), left, right);
		}
		if(nk==null)
			nk = this.logicalToPhysicalConverter.apply(right, this.getSelf());
		this.changeCache(left,nk,right);
	}

	abstract protected void changeCache(PET left, PET newLeft, LET right);

	abstract protected void deleteLogical(PET k);
	abstract protected void addLogical(PET k);
	abstract protected void changeLogical(PET o, PET n);
	
	public void updatePhysicalFromLogical() {
		Delta<LET,PET> delta = this.calculateLogicalDelta();
		
		//1. change, resolve conflict in the future
		for(Trace<PET, LET> c : delta.getChanged()) {
			this.changePhysical(c.left, c.right);
		}
		
		//2. del
		for(PET k : delta.getDeleted()) {
			this.deletePhysical(k);
		}
		
		//3. add
		for(LET k : delta.getAdded()) {
			this.addPhysical(k);
		}
	}
	
	/**
	 * 涓婁竴娆″悓姝ュ悗淇濆瓨鐨勭粨鏋滐紝鍙互鐢ㄥ畠鏉ヨ绠梡hysical鍜宭ogical鐨刣elta
	 * @return
	 */
	abstract public PT getCacheValue();
	
	/**
	 * 褰撳墠鐨勯�昏緫鍙栧��
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public LT getLogicalValue() {
		return (LT) getSelf().eGet(feature);
	}
	
	public ConversionService<PET,LET> physicalToLogicalConverter = ConversionService.identity();
	public ConversionService<LET,PET> logicalToPhysicalConverter = ConversionService.identity();
	
	
	public GetService<PT> getService;
	public PostService<PET,LET> postService;
	public DeleteService<PET> deleteService;
	public PutService<PET,LET> putService;
	
	
	abstract public Delta<PET,PET> calculatePhysicalDelta();
	abstract public Delta<LET,PET> calculateLogicalDelta();
	
	public EObject getSelf() {
		return this.selfAdapter.getSelf();
	}

	static public class Delta<AT,DT> {
		
		public Delta() {
			added = null;
			deleted = null;
			changed = null;
		}
		private List<AT> added;
		public final List<AT> getAdded() {
			return added == null ? Collections.emptyList() : added;
		}
		public void setAdded(AT added) {
			if(this.added==null)
				this.added = new ArrayList<AT>();
			this.added.add(added);
		}
		public final List<DT> getDeleted() {
			return deleted == null ? Collections.emptyList() : deleted;
		}
		public void setDeleted(DT deleted) {
			if(this.deleted==null)
				this.deleted = new ArrayList<DT>();
			this.deleted.add(deleted);
		}
		public final List<Trace<DT,AT>> getChanged() {
			return changed == null ? Collections.emptyList() : changed;
		}
		public void setChanged(DT oldV, AT newV) {
			if(this.changed==null)
				this.changed = new ArrayList<Trace<DT,AT>>();
			this.changed.add(Trace.createTrace(oldV, newV));
		}
		private List<DT> deleted;
		private List<Trace<DT,AT>> changed;
		
		
	}
	
	static public class StateBasedSingleValueFeatureCacheHolder<PT,LT> extends FeatureCacheHolder<PT,PT,LT,LT>{
		public PT cacheValue;
		public PT getCacheValue() {
			return cacheValue;
		}
		
		@Override
		public Delta<PT,PT> calculatePhysicalDelta() {
			Delta<PT,PT> delta = new Delta<PT,PT>();
			
			PT physicalValue = this.getPhysicalValue();
			
			if(cacheValue==null) {
				if(physicalValue!=null)
					delta.setAdded(physicalValue);
			} else {
				if(physicalValue==null) {
					delta.setDeleted(cacheValue);
				} else if(!physicalValue.equals(cacheValue)) {
					delta.setChanged(cacheValue,physicalValue);
				}
			}
			
			return delta;
		}
		
		@Override
		public Delta<LT,PT> calculateLogicalDelta() {
			Delta<LT,PT> delta = new Delta<LT,PT>();
			
			LT logicalValue = this.getLogicalValue();
			
			if(cacheValue==null) {
				if(logicalValue!=null)
					delta.setAdded(logicalValue);
			} else {
				if(logicalValue==null) {
					delta.setDeleted(cacheValue);
				} else {
					PT apply = logicalToPhysicalConverter.apply(logicalValue,getSelf());
					if(!cacheValue.equals(apply)) {
						delta.setChanged(cacheValue, logicalValue);
					}	
				}
			}
			return delta;
		}

		@Override
		protected void deleteLogical(PT k) {
			LT val = this.getLogicalValue();
			PT cv = this.logicalToPhysicalConverter.apply(val,getSelf());
			if(k.equals(cv)) {
//				getSelf().eSet(feature, null);
				getSelf().eUnset(feature);
			} else {
				mergeLogical();
			}
			this.delCache(k);
		}

		@Override
		protected void addLogical(PT k) {
//			LT val = this.getLogicalValue();
			LT nv = this.physicalToLogicalConverter.apply(k,getSelf());
//			if(val==null) {
			// 这样才能和多值的情况保持一致。目前是无论如何都会新增，但是因为是单值，所以会重置取值
			// trace-based则不同
			getSelf().eSet(feature, nv);
//			} else {
//				mergeLogical();
//			}
			this.addCache(k, null);
		}

		@Override
		protected void changeLogical(PT o, PT n) {
			LT val = this.getLogicalValue();
			PT cv = this.logicalToPhysicalConverter.apply(val,getSelf());
			if(o.equals(cv)) {
				getSelf().eSet(feature, this.physicalToLogicalConverter.apply(n,getSelf()));
			} else {
				mergeLogical();
			}
			this.changeCache(null, n, null);
			this.cacheValue = n;
		}

		@Override
		protected void addCache(PT r, LT k) {
			this.cacheValue = r;
		}

		@Override
		protected void delCache(PT k) {
			this.cacheValue = null;
		}

		@Override
		protected void changeCache(PT left, PT newLeft, LT right) {
			this.cacheValue = newLeft;
		}
	}
	
	static public class StateBasedMultiValueFeatureCacheHolder<PT,LT> extends FeatureCacheHolder<PT,List<PT>,LT,List<LT>>{
		public List<PT> cacheValue;
		public List<PT> getCacheValue() {
			return cacheValue == null ? Collections.emptyList() : cacheValue;
		}
		
		protected void addCache(PT k, LT v) {
			if(cacheValue==null)
				cacheValue = new ArrayList<PT>();
			if(k!=null)
				cacheValue.add(k);
		}
		
		protected void delCache(PT k) {
			if(cacheValue==null)
				return;
			if(k!=null)
				cacheValue.remove(k);
		}
		
		
		@Override
		public Delta<PT,PT> calculatePhysicalDelta() {
			Delta<PT,PT> delta = new Delta<PT,PT>();
			
			List<PT> physicalValue = this.getPhysicalValue();
			
			if(cacheValue==null) {
				if(physicalValue!=null) {
					physicalValue.forEach(x->{delta.setAdded(x);});
				}
			} else {
				if(physicalValue==null) {
					cacheValue.forEach(x->{delta.setDeleted(x);});
				} else {
					physicalValue.forEach(x->{if(!cacheValue.contains(x)) delta.setAdded(x);});
					cacheValue.forEach(x->{if(!physicalValue.contains(x)) delta.setDeleted(x);});
				}
			}
			
			return delta;
		}
		
		@Override
		public Delta<LT,PT> calculateLogicalDelta() {
			Delta<LT,PT> delta = new Delta<LT,PT>();
			
			List<LT> logicalValue = this.getLogicalValue();
			
			if(logicalValue==null) {
				cacheValue.forEach(x->{delta.setDeleted(x);});
			} else {
				List<PT> apply = logicalValue.stream().collect(ArrayList::new, 
						(list, e)->list.add(this.logicalToPhysicalConverter.apply(e,getSelf())), 
						(l,r)->l.addAll(r));
				for(int i=0;i<apply.size();i++) {
					if(!cacheValue.contains(apply.get(i))) {
						delta.setAdded(logicalValue.get(i));
					}
				}
				cacheValue.forEach(x->{if(!apply.contains(x)) delta.setDeleted(x);});
			}
			return delta;
		}

		@Override
		protected void deleteLogical(PT k) {
			List<LT> val = this.getLogicalValue();
			LT del = null;
			for(LT v : val) {
				PT t = this.logicalToPhysicalConverter.apply(v,getSelf());
				if(k.equals(t)) {
					del = v;
					break;
				}
			}
			
			if(del!=null)
				val.remove(del);
			this.delCache(k);
		}

		@Override
		protected void addLogical(PT k) {
			List<LT> val = this.getLogicalValue();
			for(LT v : val) {
				PT t = this.logicalToPhysicalConverter.apply(v,getSelf());
				if(k.equals(t)) { // always check if it exists
					mergeLogical();
					this.addCache(k,null);
					return;
				}
			}
			
			LT nv = this.physicalToLogicalConverter.apply(k,getSelf());
			val.add(nv);
			this.addCache(k,null);
		}

		@Override
		protected void changeLogical(PT o, PT n) {
			throw new UnsupportedOperationException();
		}

		@Override
		protected void changeCache(PT left, PT newLeft, LT right) {
			List<PT> cacheList = this.getCacheValue();
			for(int i=0;i<cacheList.size();i++) {
				PT l = cacheList.get(i);
				if(left.equals(l)) {
					if(newLeft!=null)
						cacheList.set(i, newLeft);
				}
			}
		}
	}
	
	static public class TraceBasedSingleValueFeatureCacheHolder<PT,LT extends EObject> extends FeatureCacheHolder<PT,PT,LT,LT>{
		public Trace<PT,LT> cacheValue;
		public PT getCacheValue() {
			return cacheValue==null ? null : cacheValue.left;
		}
		
		private LT getCacheLogicalValue() {
			return cacheValue==null ? null : cacheValue.right;
		}
		
		protected void setCache(PT k, LT v) {
			if((k==null && v!=null) || (k!=null && v==null))
				cacheValue = null;
			
			if(cacheValue==null)
				cacheValue = Trace.createTrace(k, v);
			else {
				cacheValue.left = k;
				cacheValue.right = v;
			}
		}
		
		@Override
		public Delta<PT,PT> calculatePhysicalDelta() {
			Delta<PT,PT> delta = new Delta<PT,PT>();
			PT cacheValue = this.getCacheValue();
			PT physicalValue = this.getPhysicalValue();
			
			if(cacheValue==null) {
				if(physicalValue!=null)
					delta.setAdded(physicalValue);
			} else {
				if(physicalValue==null) {
					delta.setDeleted(cacheValue);
				} else if(!physicalValue.equals(cacheValue)) {
					delta.setChanged(cacheValue,physicalValue); // change or a par of add and delete?
				}
			}
			
			return delta;
		}
		
		@Override
		public Delta<LT,PT> calculateLogicalDelta() {
			Delta<LT,PT> delta = new Delta<LT,PT>();
			
			PT cacheValue = this.getCacheValue();
			LT cacheLogicalValue = this.getCacheLogicalValue();
			
			LT logicalValue = this.getLogicalValue();
			
			if(cacheLogicalValue==null) {
				if(logicalValue!=null)
					delta.setAdded(logicalValue);
			} else {
				if(logicalValue==null) {
					delta.setDeleted(cacheValue);
				} else {
					if(cacheLogicalValue != logicalValue) {
						delta.setAdded(logicalValue);
						delta.setDeleted(cacheValue);
					} else {
						PT apply = logicalToPhysicalConverter.apply(logicalValue,getSelf());
						if(!cacheValue.equals(apply)) {
							delta.setChanged(cacheValue, logicalValue);
						}						
					}
				}
			}
			return delta;
		}

		@Override
		public void deleteLogical(PT k) {
			LT val = this.getLogicalValue();
			PT cv = this.logicalToPhysicalConverter.apply(val,getSelf());
			if(k.equals(cv)) {
				getSelf().eSet(feature, null);
			} else {
				mergeLogical();
			}
			this.setCache(null, null);
		}

		@Override
		public void addLogical(PT k) {
			LT val = this.getLogicalValue();
			LT nv = this.physicalToLogicalConverter.apply(k,getSelf());
			if(val==null) {
				getSelf().eSet(feature, nv);
				this.setCache(k, nv);
			} else {
				// in this case, a user update has not been committed.
				// I simply update the cache, so that the user update will be recognized next round
				mergeLogical();
				this.setCache(k, val);
			}
		}

		@Override
		public void changeLogical(PT o, PT n) {
			LT val = this.getLogicalValue();
			PT cv = this.logicalToPhysicalConverter.apply(val,getSelf());
			if(o.equals(cv)) {
				LT nv = this.physicalToLogicalConverter.apply(n,getSelf());
				getSelf().eSet(feature, nv);
				this.setCache(o, nv);
			} else {
				mergeLogical();
				if(this.getCacheLogicalValue()!=val) {
					LT nv = this.physicalToLogicalConverter.apply(n,getSelf());
					this.setCache(o, nv);// set a shadow value
				}
			}
		}

		@Override
		protected void addCache(PT r, LT k) {
			this.setCache(r, k);
		}

		@Override
		protected void delCache(PT k) {
			this.setCache(null, null);
		}

		@Override
		protected void changeCache(PT left, PT newLeft, LT right) {
			this.setCache(newLeft, right);
		}
		
		public void clear() {
			this.cacheValue = null;
		}
	}
	
	static public class TraceBasedMultiValueFeatureCacheHolder<PT,LT extends EObject> extends FeatureCacheHolder<PT,List<PT>,LT,List<LT>>{
		public List<Trace<PT,LT>> cacheValue;
		public List<PT> getCacheValue() {
			if(cacheValue==null) 
				return Collections.emptyList();
			List<PT> result = cacheValue.stream()
					.collect(ArrayList::new, 
							(list,t)->{list.add(t.left);}, 
							(left,right)->{left.addAll(right);});
			return result;
		}
		
		private List<Trace<PT,LT>> getTraceList() {
			return cacheValue==null ? Collections.emptyList() : cacheValue;
		}
		
		@Override
		public Delta<PT,PT> calculatePhysicalDelta() {
			Delta<PT,PT> delta = new Delta<PT,PT>();
			
			List<PT> cacheValue = this.getCacheValue();
			List<PT> physicalValue = this.getPhysicalValue();
			
			if(cacheValue==null) {
				if(physicalValue!=null)
					physicalValue.forEach(x->{delta.setAdded(x);});
			} else {
				if(physicalValue==null) {
					cacheValue.forEach(x->{delta.setDeleted(x);});
				} else {
					physicalValue.forEach(x->{if(!cacheValue.contains(x)) delta.setAdded(x);});
					cacheValue.forEach(x->{if(!physicalValue.contains(x)) delta.setDeleted(x);});
				}
			}
			
			return delta;
		}
		
		@Override
		public Delta<LT, PT> calculateLogicalDelta() {
			Delta<LT,PT> delta = new Delta<LT,PT>();
			
			List<LT> logicalValue = this.getLogicalValue();
			List<Trace<PT,LT>> trace = this.getTraceList();
			
			if(logicalValue==null) {
				trace.forEach(x->{delta.setDeleted(x.left);});
			} else {
				trace.forEach(x->{if(!logicalValue.contains(x.right)) delta.setDeleted(x.left);});
				
				logicalValue.forEach(x->{
					PT k = this.searchCache(x);
					if(k==null) {
						delta.setAdded(x);
					} else {
						PT apply = this.logicalToPhysicalConverter.apply(x,getSelf());
						if(!k.equals(apply)) {
							delta.setChanged(k, x);
						}
					}
				});
			}
			
			return delta;
		}
		
		private PT searchCache(LT o) {
			List<Trace<PT,LT>> trace = this.getTraceList();
			for(Trace<PT,LT> t : trace) {
				if(t.right==o)
					return t.left;
			}
			return null;
		}

		@Override
		protected void deleteLogical(PT k) {
			List<LT> val = this.getLogicalValue();
			LT del = null;
			for(LT v : val) {
				PT t = this.logicalToPhysicalConverter.apply(v,getSelf());
				if(k.equals(t)) {
					del = v;
					break;
				}
			}
			
			if(del!=null)
				val.remove(del);
			this.delCache(k);
		}
		
		protected void delCache(PT k) {
			Trace<PT,LT> t = null;
			for(Trace<PT,LT> i : this.getTraceList()) {
				if(i.left.equals(k)) {
					t = i;
					break;
				}
			}
			if(t!=null) {
				if(this.cacheValue!=null) 
					this.cacheValue.remove(t);
			}
		}

		@Override
		protected void addLogical(PT k) {
			List<LT> val = this.getLogicalValue();
			for(LT v : val) {
				PT t = this.logicalToPhysicalConverter.apply(v,getSelf());
				if(k.equals(t)) {
					mergeLogical();
					this.addCache(k,v);
					return;
				}
			}
			
			LT nv = this.physicalToLogicalConverter.apply(k,getSelf());
			val.add(nv);
			this.addCache(k,nv);
		}

		protected void addCache(PT k, LT v) {
			if(k==null) return;
			
			Trace<PT,LT> t = null;
			for(Trace<PT,LT> i : this.getTraceList()) {
				if(i.left.equals(k)) {
					t = i;
					break;
				}
			}
			if(t!=null) {
				if(v!=null)
					t.right = v;
				else {
					// should I remove t?
					this.getTraceList().remove(t);
				}
			} else {
				if(this.cacheValue==null)
					this.cacheValue = new ArrayList<Trace<PT,LT>>();
				if(v!=null)
					this.cacheValue.add(Trace.createTrace(k, v));
			}
		}

		@Override
		protected void changeLogical(PT o, PT n) {
			throw new UnsupportedOperationException();
		}

		@Override
		protected void changeCache(PT left, PT newLeft, LT right) {
			Trace<PT,LT> t = null;
			for(Trace<PT,LT> i : this.getTraceList()) {
				if(i.left.equals(left)) {
					t = i;
					break;
				}
			}
			
			if(t!=null) {
				if(newLeft==null || right==null)
					this.getTraceList().remove(t);
				else {
					t.left = newLeft;
					t.right = right;
				}
			}
		}
		
		@Override
		public void clear() {
			if(this.cacheValue!=null)
				this.cacheValue.clear();
		}
	}

	public void clear() {
	}
	
}
