package edu.ustb.sei.mde.runtimemodel.modeladapter;

import org.eclipse.emf.ecore.EObject;

@FunctionalInterface
public interface GetService<PT> {
	PT apply(EObject o);
}
