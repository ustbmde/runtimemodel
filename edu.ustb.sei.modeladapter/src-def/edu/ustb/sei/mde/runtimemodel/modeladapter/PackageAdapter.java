package edu.ustb.sei.mde.runtimemodel.modeladapter;

public class PackageAdapter extends BaseAdapter {
	private ModelService modelService;
	private BaseAdapterFactory adapterFactory;
	
	public PackageAdapter(ModelService modelService, BaseAdapterFactory adapterFactory) {
		super();
		this.modelService = modelService;
		this.adapterFactory = adapterFactory;
	}
	
	
	public ModelService getModelService() {
		return modelService;
	}
	
	public BaseAdapterFactory getAdapterFactory() {
		return adapterFactory;
	}


	@Override
	public void init() {
	}
}
