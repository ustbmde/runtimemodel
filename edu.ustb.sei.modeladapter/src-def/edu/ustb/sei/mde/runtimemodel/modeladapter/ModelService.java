package edu.ustb.sei.mde.runtimemodel.modeladapter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

abstract public class ModelService {
	
	final static public String ANNOTATION_URI = "http://www.ustb.edu.cn/sei/model@runtime";
	final static public String ANNOTATION_ID = "ID";
	
	public ModelService() {
		createResourceSet();
		initResourceSet();
		loadMetamodel();
		initAdapterFactory();
	}

	@SuppressWarnings("unchecked")
	public <T> T eGet(EObject object, String feature) {
		EClass clazz = object.eClass();
		EStructuralFeature f = clazz.getEStructuralFeature(feature);
		return (T)object.eGet(f);
	}
	
	
	public <T> void eSet(EObject object, String feature, T value) {
		EClass clazz = object.eClass();
		EStructuralFeature f = clazz.getEStructuralFeature(feature);
		object.eSet(f, value);
	}
	
	protected EPackage metamodel;
	
	public EPackage getMetamodel() {
		return metamodel;
	}

	abstract protected void loadMetamodel();
	
	public EClass searchClassInMetamodel(String name) {
		return searchClassInPackage(name,metamodel);
	}
	static protected EClass searchClassInPackage(String name, EPackage pkg) {
		EClass cls = (EClass) pkg.getEClassifier(name);
		if(cls!=null) return cls;
		else {
			for(EPackage sp : pkg.getESubpackages()) {
				 cls = searchClassInPackage(name, sp);
				 if(cls!=null) return cls;
			}
			return null;
		}
	}
	
	protected ResourceSet resourceSet = null;
	
	protected void createResourceSet() {
		resourceSet = new ResourceSetImpl();
	}

	protected void initResourceSet() {
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
		Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
	}

	protected void initAdapterFactory() {
		resourceSet.getAdapterFactories().add(this.getRuntimeModelAdapterFactory());
	}
	
	public EObject createObject(String clazzName) {
		EClass cls = this.searchClassInMetamodel(clazzName);
		if(cls!=null)
			return EcoreUtil.create(cls);
		else return null;
	}

	abstract public EStructuralFeature getIDFeature(EObject obj);
	abstract public BaseAdapterFactory getRuntimeModelAdapterFactory();

	protected void loadMetamodel(URI uri) {
		Resource res = loadResource(uri);
		loadMetamodel((EPackage) res.getContents().get(0));
	}
	
	protected void loadMetamodel(EPackage metamodel) {
		this.metamodel = metamodel;
		getRuntimeModelAdapterFactory().adapt(this.metamodel);
	}

	public Resource loadResource(URI uri) {
		Resource res = this.resourceSet.getResource(uri, true);
		return res;
	}
	
	public BaseAdapter getAdapter(EObject target) {
		return (BaseAdapter) this.getRuntimeModelAdapterFactory().adapt(target);
	}
	
	public void saveModel(EObject root, URI uri) {
		Resource res = resourceSet.createResource(uri);
		res.getContents().add(root);
		try {
			Map<Object,Object> map = new HashMap<Object,Object>();
			map.put(XMLResource.OPTION_SCHEMA_LOCATION, true);
			res.save(map);
			resourceSet.getResources().remove(res);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public long getAutoRefreshingRate() {
		return 1000;
	}
	
	public AbstractTimer getTimer() {
		return new ResourceSyncTimer(this);
	}
}
