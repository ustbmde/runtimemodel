package edu.ustb.sei.mde.runtimemodel.modeladapter;

import org.eclipse.emf.ecore.EObject;

@FunctionalInterface
public interface PutService<PET,LET> {
	PET apply(EObject o, PET key, LET value);
}
