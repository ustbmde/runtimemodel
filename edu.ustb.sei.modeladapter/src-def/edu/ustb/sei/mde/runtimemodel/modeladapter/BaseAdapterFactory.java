/**
 */
package edu.ustb.sei.mde.runtimemodel.modeladapter;

import java.util.List;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.EcoreUtil;

abstract public class BaseAdapterFactory extends AdapterFactoryImpl {
	
	static public PackageAdapter getPackageAdapter(EPackage pack) {
		return (PackageAdapter) EcoreUtil.getRegisteredAdapter(pack, BaseAdapter.RUNTIMEMODELADATPER);
	}

	protected EPackage modelPackage;
	protected ModelService modelService;

	public BaseAdapterFactory(ModelService modelService, EPackage modelPackage) {
		this.modelService = modelService;
		this.modelPackage = modelPackage;
	}

	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	@Override
	public Adapter createAdapter(Notifier target) {
		if(target==this.modelPackage) {
			return createPackageAdapter((EPackage)target);
		} else 
			return doSwitch(((EObject)target).eClass(), (EObject)target);
	}
	
	abstract protected Adapter createPackageAdapter(EPackage eObject);
	
	abstract protected Adapter createModelAdapter(EClass clazz, EObject eObject);

	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}
	
	protected Adapter doSwitch(EClass clazz, EObject theEObject) {
		if(isSwitchFor(clazz.getEPackage())) {
			return createModelAdapter(clazz, theEObject);
		} else {
			List<EClass> eSuperTypes = clazz.getESuperTypes();
		      if(eSuperTypes.isEmpty()) 
		    	  return defaultCase(theEObject);
		      else {
		    	  for(EClass c : eSuperTypes) {
		    		  Adapter s = doSwitch(c, theEObject);
		    		  if(s!=null)
		    			  return s;
		    	  }
		    	  return defaultCase(theEObject);
		      }
		}
	}
	
	private Adapter defaultCase(EObject theEObject) {
		return null;
	}

	public Adapter adapt(Notifier target) {
		return super.adapt(target, BaseAdapter.RUNTIMEMODELADATPER);
	}
	
	public Object adapt(Object target) {
		return super.adapt(target, BaseAdapter.RUNTIMEMODELADATPER);
	}

	@Override
	protected void associate(Adapter adapter, Notifier target) {
		super.associate(adapter, target);
		if(adapter!=null)
			this.initAdapter((EObject)target, (BaseAdapter)adapter);
	}
	
	public void initAdapter(EObject object, BaseAdapter adapter) {
		adapter.init();
	}
}
