package edu.ustb.sei.mde.runtimemodel.modeladapter;

import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.ui.PlatformUI;

import edu.ustb.sei.mde.runtimemodel.ui.ModelServiceTable;

public class ResourceSyncTimer extends AbstractTimer {

	public ResourceSyncTimer(ModelService s) {
		super(s);
	}

	@Override
	public void notifyChanged(Notification notification) {
		if (notification.isTouch()) {
			if (this.resource != null)
				((SyncTask)this.timerTask).updatePhysical(); // currently, duplicate update cannot be avoided
		}
	}

	@Override
	protected TimerTask createTask() {
		return new SyncTask(this);
	}
}

class SyncTask extends TimerTask {

	private AbstractTimer host;

	public SyncTask(AbstractTimer resourceSyncTimer) {
		this.host = resourceSyncTimer;
		this.ignore = false;
	}

	private boolean ignore;

	public void beginIgnore() {
		ignore = true;
	}

	public void endIgnore() {
		ignore = false;
	}

	private Logger log = java.util.logging.Logger.getLogger("ResourceSyncTimer");

	@Override
	public void run() {
		log.log(Level.INFO, "SyncTask is runing");
		if (host.shouldCancel() == false) {
			if (ignore == false) {
				updateModel();
			}
		} else {
			this.host.cancel();
			log.log(Level.OFF, "the resource is unloaded");
		}
	}

	public synchronized void updateModel() {
		{
			this.beginIgnore();
			try {
				EObject root = host.getResource().getContents().get(0);
//				EPackage pkg = root.eClass().getEPackage();
				ModelService s = host.getModelService();
				if (s != null) {
					BaseAdapter a = s.getAdapter(root);
					a.updateLogical();
				}
				log.log(Level.INFO, "model is refreshed");
			} catch (Exception e) {
				log.log(Level.WARNING, "an exception is caught: " + e.getMessage());
			}
			this.endIgnore();
		}
	}

	public synchronized void updatePhysical() {
		{
			this.beginIgnore();
			try {
				EObject root = host.getResource().getContents().get(0);
//				EPackage pkg = root.eClass().getEPackage();
				ModelService s = host.getModelService();
				if (s != null) {
					BaseAdapter a = s.getAdapter(root);
					a.updatePhysical();
				}
				log.log(Level.INFO, "physical is refreshed");
			} catch (Exception e) {
				log.log(Level.WARNING, "an exception is caught: " + e.getMessage());
			}
			this.endIgnore();
		}
	}
}
