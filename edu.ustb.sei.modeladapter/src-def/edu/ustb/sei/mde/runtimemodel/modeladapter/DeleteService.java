package edu.ustb.sei.mde.runtimemodel.modeladapter;

import org.eclipse.emf.ecore.EObject;

@FunctionalInterface
public interface DeleteService<PET> {
	boolean apply(EObject o, PET val);
}
