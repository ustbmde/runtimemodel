package edu.ustb.sei.mde.runtimemodel.modeladapter;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;

@FunctionalInterface
public interface ConversionService<S, T> {
	T apply(S o, EObject context);
	
	@SuppressWarnings("rawtypes")
	static public IdentityConversion IDENTITY = new IdentityConversion();
	
	@SuppressWarnings("unchecked")
	static public <S,T> ConversionService<S, T> identity() {
		return IDENTITY;
	}
	
	@SuppressWarnings("rawtypes")
	static public ObjectRepresentationToEObjectConversion OR2O = new ObjectRepresentationToEObjectConversion();
	@SuppressWarnings("unchecked")
	static public <T> ObjectRepresentationToEObjectConversion<T> objectRepresentationToEObject() {
		return OR2O;
	}
	
	@SuppressWarnings("rawtypes")
	static public EObjectToObjectRepresentationConversion O2OR = new EObjectToObjectRepresentationConversion();
	@SuppressWarnings("rawtypes")
	static public <T> EObjectToObjectRepresentationConversion<T> eObjectToObjectRepresentation() {
		return O2OR;
	}
}

class IdentityConversion<S,T> implements ConversionService<S, T> {

	@SuppressWarnings("unchecked")
	@Override
	public T apply(S o, EObject context) {
		return (T) o;
	}
	
}

class ObjectRepresentationToEObjectConversion<T extends Object> implements ConversionService<ObjectRepresentation<T>, EObject> {

	@Override
	public EObject apply(ObjectRepresentation<T> o, EObject context) {
		EPackage pkg = context.eClass().getEPackage();
		PackageAdapter adapter = BaseAdapterFactory.getPackageAdapter(pkg);
		ModelService util = adapter.getModelService();
		EObject obj = util.createObject(o.typeName);
		EStructuralFeature feature = util.getIDFeature(obj);
		obj.eSet(feature, o.representation);
		return obj;
	}
}

class EObjectToObjectRepresentationConversion<T extends Object> implements ConversionService<EObject,ObjectRepresentation<T>> {

	@SuppressWarnings("unchecked")
	@Override
	public ObjectRepresentation<T> apply(EObject o, EObject context) {
		EPackage pkg = context.eClass().getEPackage();
		PackageAdapter adapter = BaseAdapterFactory.getPackageAdapter(pkg);
		ModelService util = adapter.getModelService();
		EStructuralFeature feature = util.getIDFeature(o);
		T r = (T) o.eGet(feature);
		ObjectRepresentation<T> representation = new ObjectRepresentation<T>();
		representation.typeName = o.eClass().getName();
		representation.representation = r;
		return representation;
	}
	
}