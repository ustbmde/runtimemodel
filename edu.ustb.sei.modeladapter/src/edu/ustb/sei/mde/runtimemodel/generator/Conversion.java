/**
 */
package edu.ustb.sei.mde.runtimemodel.generator;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Conversion</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.Conversion#getPhysicalType <em>Physical Type</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.Conversion#getPhysicalToLogical <em>Physical To Logical</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.Conversion#getLogicalToPhysical <em>Logical To Physical</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getConversion()
 * @model
 * @generated
 */
public interface Conversion extends EObject {
	/**
	 * Returns the value of the '<em><b>Physical Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical Type</em>' attribute.
	 * @see #setPhysicalType(String)
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getConversion_PhysicalType()
	 * @model required="true"
	 * @generated
	 */
	String getPhysicalType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.runtimemodel.generator.Conversion#getPhysicalType <em>Physical Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical Type</em>' attribute.
	 * @see #getPhysicalType()
	 * @generated
	 */
	void setPhysicalType(String value);

	/**
	 * Returns the value of the '<em><b>Physical To Logical</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Physical To Logical</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Physical To Logical</em>' attribute.
	 * @see #setPhysicalToLogical(String)
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getConversion_PhysicalToLogical()
	 * @model required="true"
	 * @generated
	 */
	String getPhysicalToLogical();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.runtimemodel.generator.Conversion#getPhysicalToLogical <em>Physical To Logical</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Physical To Logical</em>' attribute.
	 * @see #getPhysicalToLogical()
	 * @generated
	 */
	void setPhysicalToLogical(String value);

	/**
	 * Returns the value of the '<em><b>Logical To Physical</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Logical To Physical</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Logical To Physical</em>' attribute.
	 * @see #setLogicalToPhysical(String)
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getConversion_LogicalToPhysical()
	 * @model required="true"
	 * @generated
	 */
	String getLogicalToPhysical();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.runtimemodel.generator.Conversion#getLogicalToPhysical <em>Logical To Physical</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Logical To Physical</em>' attribute.
	 * @see #getLogicalToPhysical()
	 * @generated
	 */
	void setLogicalToPhysical(String value);

} // Conversion
