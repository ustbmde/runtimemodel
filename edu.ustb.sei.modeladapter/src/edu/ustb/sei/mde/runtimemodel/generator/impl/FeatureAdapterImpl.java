/**
 */
package edu.ustb.sei.mde.runtimemodel.generator.impl;

import edu.ustb.sei.mde.runtimemodel.generator.Conversion;
import edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter;
import edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Feature Adapter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.impl.FeatureAdapterImpl#getAdaptedFeature <em>Adapted Feature</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.impl.FeatureAdapterImpl#getGet <em>Get</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.impl.FeatureAdapterImpl#getPost <em>Post</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.impl.FeatureAdapterImpl#getPut <em>Put</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.impl.FeatureAdapterImpl#getDelete <em>Delete</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.impl.FeatureAdapterImpl#isId <em>Id</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.impl.FeatureAdapterImpl#getConversion <em>Conversion</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FeatureAdapterImpl extends MinimalEObjectImpl.Container implements FeatureAdapter {
	/**
	 * The cached value of the '{@link #getAdaptedFeature() <em>Adapted Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdaptedFeature()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature adaptedFeature;

	/**
	 * The default value of the '{@link #getGet() <em>Get</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGet()
	 * @generated
	 * @ordered
	 */
	protected static final String GET_EDEFAULT = "// get : self -> physicalValue";

	/**
	 * The cached value of the '{@link #getGet() <em>Get</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGet()
	 * @generated
	 * @ordered
	 */
	protected String get = GET_EDEFAULT;

	/**
	 * The default value of the '{@link #getPost() <em>Post</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPost()
	 * @generated
	 * @ordered
	 */
	protected static final String POST_EDEFAULT = "// post : (self, logicalValue) -> physicalValue";

	/**
	 * The cached value of the '{@link #getPost() <em>Post</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPost()
	 * @generated
	 * @ordered
	 */
	protected String post = POST_EDEFAULT;

	/**
	 * The default value of the '{@link #getPut() <em>Put</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPut()
	 * @generated
	 * @ordered
	 */
	protected static final String PUT_EDEFAULT = "// put : (self, physicalValue, logicalValue) -> physicalValue";

	/**
	 * The cached value of the '{@link #getPut() <em>Put</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPut()
	 * @generated
	 * @ordered
	 */
	protected String put = PUT_EDEFAULT;

	/**
	 * The default value of the '{@link #getDelete() <em>Delete</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDelete()
	 * @generated
	 * @ordered
	 */
	protected static final String DELETE_EDEFAULT = "// delete : (self, physicalValue) -> boolean";

	/**
	 * The cached value of the '{@link #getDelete() <em>Delete</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDelete()
	 * @generated
	 * @ordered
	 */
	protected String delete = DELETE_EDEFAULT;

	/**
	 * The default value of the '{@link #isId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isId()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ID_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isId()
	 * @generated
	 * @ordered
	 */
	protected boolean id = ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getConversion() <em>Conversion</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConversion()
	 * @generated
	 * @ordered
	 */
	protected Conversion conversion;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FeatureAdapterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GeneratorPackage.Literals.FEATURE_ADAPTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature getAdaptedFeature() {
		if (adaptedFeature != null && adaptedFeature.eIsProxy()) {
			InternalEObject oldAdaptedFeature = (InternalEObject)adaptedFeature;
			adaptedFeature = (EStructuralFeature)eResolveProxy(oldAdaptedFeature);
			if (adaptedFeature != oldAdaptedFeature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GeneratorPackage.FEATURE_ADAPTER__ADAPTED_FEATURE, oldAdaptedFeature, adaptedFeature));
			}
		}
		return adaptedFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature basicGetAdaptedFeature() {
		return adaptedFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAdaptedFeature(EStructuralFeature newAdaptedFeature) {
		EStructuralFeature oldAdaptedFeature = adaptedFeature;
		adaptedFeature = newAdaptedFeature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GeneratorPackage.FEATURE_ADAPTER__ADAPTED_FEATURE, oldAdaptedFeature, adaptedFeature));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getGet() {
		return get;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGet(String newGet) {
		String oldGet = get;
		get = newGet;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GeneratorPackage.FEATURE_ADAPTER__GET, oldGet, get));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPost() {
		return post;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPost(String newPost) {
		String oldPost = post;
		post = newPost;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GeneratorPackage.FEATURE_ADAPTER__POST, oldPost, post));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPut() {
		return put;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPut(String newPut) {
		String oldPut = put;
		put = newPut;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GeneratorPackage.FEATURE_ADAPTER__PUT, oldPut, put));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDelete() {
		return delete;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDelete(String newDelete) {
		String oldDelete = delete;
		delete = newDelete;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GeneratorPackage.FEATURE_ADAPTER__DELETE, oldDelete, delete));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(boolean newId) {
		boolean oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GeneratorPackage.FEATURE_ADAPTER__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Conversion getConversion() {
		return conversion;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConversion(Conversion newConversion, NotificationChain msgs) {
		Conversion oldConversion = conversion;
		conversion = newConversion;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, GeneratorPackage.FEATURE_ADAPTER__CONVERSION, oldConversion, newConversion);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConversion(Conversion newConversion) {
		if (newConversion != conversion) {
			NotificationChain msgs = null;
			if (conversion != null)
				msgs = ((InternalEObject)conversion).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - GeneratorPackage.FEATURE_ADAPTER__CONVERSION, null, msgs);
			if (newConversion != null)
				msgs = ((InternalEObject)newConversion).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - GeneratorPackage.FEATURE_ADAPTER__CONVERSION, null, msgs);
			msgs = basicSetConversion(newConversion, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GeneratorPackage.FEATURE_ADAPTER__CONVERSION, newConversion, newConversion));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GeneratorPackage.FEATURE_ADAPTER__CONVERSION:
				return basicSetConversion(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GeneratorPackage.FEATURE_ADAPTER__ADAPTED_FEATURE:
				if (resolve) return getAdaptedFeature();
				return basicGetAdaptedFeature();
			case GeneratorPackage.FEATURE_ADAPTER__GET:
				return getGet();
			case GeneratorPackage.FEATURE_ADAPTER__POST:
				return getPost();
			case GeneratorPackage.FEATURE_ADAPTER__PUT:
				return getPut();
			case GeneratorPackage.FEATURE_ADAPTER__DELETE:
				return getDelete();
			case GeneratorPackage.FEATURE_ADAPTER__ID:
				return isId();
			case GeneratorPackage.FEATURE_ADAPTER__CONVERSION:
				return getConversion();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GeneratorPackage.FEATURE_ADAPTER__ADAPTED_FEATURE:
				setAdaptedFeature((EStructuralFeature)newValue);
				return;
			case GeneratorPackage.FEATURE_ADAPTER__GET:
				setGet((String)newValue);
				return;
			case GeneratorPackage.FEATURE_ADAPTER__POST:
				setPost((String)newValue);
				return;
			case GeneratorPackage.FEATURE_ADAPTER__PUT:
				setPut((String)newValue);
				return;
			case GeneratorPackage.FEATURE_ADAPTER__DELETE:
				setDelete((String)newValue);
				return;
			case GeneratorPackage.FEATURE_ADAPTER__ID:
				setId((Boolean)newValue);
				return;
			case GeneratorPackage.FEATURE_ADAPTER__CONVERSION:
				setConversion((Conversion)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GeneratorPackage.FEATURE_ADAPTER__ADAPTED_FEATURE:
				setAdaptedFeature((EStructuralFeature)null);
				return;
			case GeneratorPackage.FEATURE_ADAPTER__GET:
				setGet(GET_EDEFAULT);
				return;
			case GeneratorPackage.FEATURE_ADAPTER__POST:
				setPost(POST_EDEFAULT);
				return;
			case GeneratorPackage.FEATURE_ADAPTER__PUT:
				setPut(PUT_EDEFAULT);
				return;
			case GeneratorPackage.FEATURE_ADAPTER__DELETE:
				setDelete(DELETE_EDEFAULT);
				return;
			case GeneratorPackage.FEATURE_ADAPTER__ID:
				setId(ID_EDEFAULT);
				return;
			case GeneratorPackage.FEATURE_ADAPTER__CONVERSION:
				setConversion((Conversion)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GeneratorPackage.FEATURE_ADAPTER__ADAPTED_FEATURE:
				return adaptedFeature != null;
			case GeneratorPackage.FEATURE_ADAPTER__GET:
				return GET_EDEFAULT == null ? get != null : !GET_EDEFAULT.equals(get);
			case GeneratorPackage.FEATURE_ADAPTER__POST:
				return POST_EDEFAULT == null ? post != null : !POST_EDEFAULT.equals(post);
			case GeneratorPackage.FEATURE_ADAPTER__PUT:
				return PUT_EDEFAULT == null ? put != null : !PUT_EDEFAULT.equals(put);
			case GeneratorPackage.FEATURE_ADAPTER__DELETE:
				return DELETE_EDEFAULT == null ? delete != null : !DELETE_EDEFAULT.equals(delete);
			case GeneratorPackage.FEATURE_ADAPTER__ID:
				return id != ID_EDEFAULT;
			case GeneratorPackage.FEATURE_ADAPTER__CONVERSION:
				return conversion != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (get: ");
		result.append(get);
		result.append(", post: ");
		result.append(post);
		result.append(", put: ");
		result.append(put);
		result.append(", delete: ");
		result.append(delete);
		result.append(", id: ");
		result.append(id);
		result.append(')');
		return result.toString();
	}

} //FeatureAdapterImpl
