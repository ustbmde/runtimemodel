/**
 */
package edu.ustb.sei.mde.runtimemodel.generator;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getClassAdapters <em>Class Adapters</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getAdaptedPackage <em>Adapted Package</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getDirectory <em>Directory</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getBasePackage <em>Base Package</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getLoadMetamodel <em>Load Metamodel</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getRefreshingRate <em>Refreshing Rate</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getTimerClass <em>Timer Class</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getGeneratorModel()
 * @model
 * @generated
 */
public interface GeneratorModel extends EObject {
	/**
	 * Returns the value of the '<em><b>Class Adapters</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.runtimemodel.generator.ClassAdapter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Adapters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Adapters</em>' containment reference list.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getGeneratorModel_ClassAdapters()
	 * @model containment="true"
	 * @generated
	 */
	EList<ClassAdapter> getClassAdapters();

	/**
	 * Returns the value of the '<em><b>Adapted Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Adapted Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Adapted Package</em>' reference.
	 * @see #setAdaptedPackage(EPackage)
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getGeneratorModel_AdaptedPackage()
	 * @model required="true"
	 * @generated
	 */
	EPackage getAdaptedPackage();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getAdaptedPackage <em>Adapted Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Adapted Package</em>' reference.
	 * @see #getAdaptedPackage()
	 * @generated
	 */
	void setAdaptedPackage(EPackage value);

	/**
	 * Returns the value of the '<em><b>Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Directory</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Directory</em>' attribute.
	 * @see #setDirectory(String)
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getGeneratorModel_Directory()
	 * @model
	 * @generated
	 */
	String getDirectory();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getDirectory <em>Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Directory</em>' attribute.
	 * @see #getDirectory()
	 * @generated
	 */
	void setDirectory(String value);

	/**
	 * Returns the value of the '<em><b>Base Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Package</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Package</em>' attribute.
	 * @see #setBasePackage(String)
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getGeneratorModel_BasePackage()
	 * @model
	 * @generated
	 */
	String getBasePackage();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getBasePackage <em>Base Package</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Package</em>' attribute.
	 * @see #getBasePackage()
	 * @generated
	 */
	void setBasePackage(String value);

	/**
	 * Returns the value of the '<em><b>Load Metamodel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Load Metamodel</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Load Metamodel</em>' attribute.
	 * @see #setLoadMetamodel(String)
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getGeneratorModel_LoadMetamodel()
	 * @model
	 * @generated
	 */
	String getLoadMetamodel();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getLoadMetamodel <em>Load Metamodel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Load Metamodel</em>' attribute.
	 * @see #getLoadMetamodel()
	 * @generated
	 */
	void setLoadMetamodel(String value);

	/**
	 * Returns the value of the '<em><b>Refreshing Rate</b></em>' attribute.
	 * The default value is <code>"1000"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Refreshing Rate</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Refreshing Rate</em>' attribute.
	 * @see #setRefreshingRate(long)
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getGeneratorModel_RefreshingRate()
	 * @model default="1000"
	 * @generated
	 */
	long getRefreshingRate();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getRefreshingRate <em>Refreshing Rate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Refreshing Rate</em>' attribute.
	 * @see #getRefreshingRate()
	 * @generated
	 */
	void setRefreshingRate(long value);

	/**
	 * Returns the value of the '<em><b>Timer Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timer Class</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timer Class</em>' attribute.
	 * @see #setTimerClass(String)
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getGeneratorModel_TimerClass()
	 * @model
	 * @generated
	 */
	String getTimerClass();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getTimerClass <em>Timer Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timer Class</em>' attribute.
	 * @see #getTimerClass()
	 * @generated
	 */
	void setTimerClass(String value);

} // GeneratorModel
