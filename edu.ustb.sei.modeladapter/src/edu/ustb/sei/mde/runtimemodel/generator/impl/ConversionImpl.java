/**
 */
package edu.ustb.sei.mde.runtimemodel.generator.impl;

import edu.ustb.sei.mde.runtimemodel.generator.Conversion;
import edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Conversion</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.impl.ConversionImpl#getPhysicalType <em>Physical Type</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.impl.ConversionImpl#getPhysicalToLogical <em>Physical To Logical</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.impl.ConversionImpl#getLogicalToPhysical <em>Logical To Physical</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConversionImpl extends MinimalEObjectImpl.Container implements Conversion {
	/**
	 * The default value of the '{@link #getPhysicalType() <em>Physical Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalType()
	 * @generated
	 * @ordered
	 */
	protected static final String PHYSICAL_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPhysicalType() <em>Physical Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalType()
	 * @generated
	 * @ordered
	 */
	protected String physicalType = PHYSICAL_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getPhysicalToLogical() <em>Physical To Logical</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalToLogical()
	 * @generated
	 * @ordered
	 */
	protected static final String PHYSICAL_TO_LOGICAL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPhysicalToLogical() <em>Physical To Logical</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhysicalToLogical()
	 * @generated
	 * @ordered
	 */
	protected String physicalToLogical = PHYSICAL_TO_LOGICAL_EDEFAULT;

	/**
	 * The default value of the '{@link #getLogicalToPhysical() <em>Logical To Physical</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLogicalToPhysical()
	 * @generated
	 * @ordered
	 */
	protected static final String LOGICAL_TO_PHYSICAL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLogicalToPhysical() <em>Logical To Physical</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLogicalToPhysical()
	 * @generated
	 * @ordered
	 */
	protected String logicalToPhysical = LOGICAL_TO_PHYSICAL_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConversionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GeneratorPackage.Literals.CONVERSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPhysicalType() {
		return physicalType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalType(String newPhysicalType) {
		String oldPhysicalType = physicalType;
		physicalType = newPhysicalType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GeneratorPackage.CONVERSION__PHYSICAL_TYPE, oldPhysicalType, physicalType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPhysicalToLogical() {
		return physicalToLogical;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhysicalToLogical(String newPhysicalToLogical) {
		String oldPhysicalToLogical = physicalToLogical;
		physicalToLogical = newPhysicalToLogical;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GeneratorPackage.CONVERSION__PHYSICAL_TO_LOGICAL, oldPhysicalToLogical, physicalToLogical));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLogicalToPhysical() {
		return logicalToPhysical;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLogicalToPhysical(String newLogicalToPhysical) {
		String oldLogicalToPhysical = logicalToPhysical;
		logicalToPhysical = newLogicalToPhysical;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GeneratorPackage.CONVERSION__LOGICAL_TO_PHYSICAL, oldLogicalToPhysical, logicalToPhysical));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GeneratorPackage.CONVERSION__PHYSICAL_TYPE:
				return getPhysicalType();
			case GeneratorPackage.CONVERSION__PHYSICAL_TO_LOGICAL:
				return getPhysicalToLogical();
			case GeneratorPackage.CONVERSION__LOGICAL_TO_PHYSICAL:
				return getLogicalToPhysical();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GeneratorPackage.CONVERSION__PHYSICAL_TYPE:
				setPhysicalType((String)newValue);
				return;
			case GeneratorPackage.CONVERSION__PHYSICAL_TO_LOGICAL:
				setPhysicalToLogical((String)newValue);
				return;
			case GeneratorPackage.CONVERSION__LOGICAL_TO_PHYSICAL:
				setLogicalToPhysical((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GeneratorPackage.CONVERSION__PHYSICAL_TYPE:
				setPhysicalType(PHYSICAL_TYPE_EDEFAULT);
				return;
			case GeneratorPackage.CONVERSION__PHYSICAL_TO_LOGICAL:
				setPhysicalToLogical(PHYSICAL_TO_LOGICAL_EDEFAULT);
				return;
			case GeneratorPackage.CONVERSION__LOGICAL_TO_PHYSICAL:
				setLogicalToPhysical(LOGICAL_TO_PHYSICAL_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GeneratorPackage.CONVERSION__PHYSICAL_TYPE:
				return PHYSICAL_TYPE_EDEFAULT == null ? physicalType != null : !PHYSICAL_TYPE_EDEFAULT.equals(physicalType);
			case GeneratorPackage.CONVERSION__PHYSICAL_TO_LOGICAL:
				return PHYSICAL_TO_LOGICAL_EDEFAULT == null ? physicalToLogical != null : !PHYSICAL_TO_LOGICAL_EDEFAULT.equals(physicalToLogical);
			case GeneratorPackage.CONVERSION__LOGICAL_TO_PHYSICAL:
				return LOGICAL_TO_PHYSICAL_EDEFAULT == null ? logicalToPhysical != null : !LOGICAL_TO_PHYSICAL_EDEFAULT.equals(logicalToPhysical);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (physicalType: ");
		result.append(physicalType);
		result.append(", physicalToLogical: ");
		result.append(physicalToLogical);
		result.append(", logicalToPhysical: ");
		result.append(logicalToPhysical);
		result.append(')');
		return result.toString();
	}

} //ConversionImpl
