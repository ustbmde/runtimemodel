/**
 */
package edu.ustb.sei.mde.runtimemodel.generator.impl;

import edu.ustb.sei.mde.runtimemodel.generator.ClassAdapter;
import edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter;
import edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class Adapter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.impl.ClassAdapterImpl#getFeatureAdapters <em>Feature Adapters</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.impl.ClassAdapterImpl#getAdaptedClass <em>Adapted Class</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.impl.ClassAdapterImpl#getImports <em>Imports</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ClassAdapterImpl extends MinimalEObjectImpl.Container implements ClassAdapter {
	/**
	 * The cached value of the '{@link #getFeatureAdapters() <em>Feature Adapters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeatureAdapters()
	 * @generated
	 * @ordered
	 */
	protected EList<FeatureAdapter> featureAdapters;

	/**
	 * The cached value of the '{@link #getAdaptedClass() <em>Adapted Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdaptedClass()
	 * @generated
	 * @ordered
	 */
	protected EClass adaptedClass;

	/**
	 * The cached value of the '{@link #getImports() <em>Imports</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImports()
	 * @generated
	 * @ordered
	 */
	protected EList<String> imports;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClassAdapterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GeneratorPackage.Literals.CLASS_ADAPTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FeatureAdapter> getFeatureAdapters() {
		if (featureAdapters == null) {
			featureAdapters = new EObjectContainmentEList<FeatureAdapter>(FeatureAdapter.class, this, GeneratorPackage.CLASS_ADAPTER__FEATURE_ADAPTERS);
		}
		return featureAdapters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAdaptedClass() {
		if (adaptedClass != null && adaptedClass.eIsProxy()) {
			InternalEObject oldAdaptedClass = (InternalEObject)adaptedClass;
			adaptedClass = (EClass)eResolveProxy(oldAdaptedClass);
			if (adaptedClass != oldAdaptedClass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GeneratorPackage.CLASS_ADAPTER__ADAPTED_CLASS, oldAdaptedClass, adaptedClass));
			}
		}
		return adaptedClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass basicGetAdaptedClass() {
		return adaptedClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAdaptedClass(EClass newAdaptedClass) {
		EClass oldAdaptedClass = adaptedClass;
		adaptedClass = newAdaptedClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GeneratorPackage.CLASS_ADAPTER__ADAPTED_CLASS, oldAdaptedClass, adaptedClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getImports() {
		if (imports == null) {
			imports = new EDataTypeUniqueEList<String>(String.class, this, GeneratorPackage.CLASS_ADAPTER__IMPORTS);
		}
		return imports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GeneratorPackage.CLASS_ADAPTER__FEATURE_ADAPTERS:
				return ((InternalEList<?>)getFeatureAdapters()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GeneratorPackage.CLASS_ADAPTER__FEATURE_ADAPTERS:
				return getFeatureAdapters();
			case GeneratorPackage.CLASS_ADAPTER__ADAPTED_CLASS:
				if (resolve) return getAdaptedClass();
				return basicGetAdaptedClass();
			case GeneratorPackage.CLASS_ADAPTER__IMPORTS:
				return getImports();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GeneratorPackage.CLASS_ADAPTER__FEATURE_ADAPTERS:
				getFeatureAdapters().clear();
				getFeatureAdapters().addAll((Collection<? extends FeatureAdapter>)newValue);
				return;
			case GeneratorPackage.CLASS_ADAPTER__ADAPTED_CLASS:
				setAdaptedClass((EClass)newValue);
				return;
			case GeneratorPackage.CLASS_ADAPTER__IMPORTS:
				getImports().clear();
				getImports().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GeneratorPackage.CLASS_ADAPTER__FEATURE_ADAPTERS:
				getFeatureAdapters().clear();
				return;
			case GeneratorPackage.CLASS_ADAPTER__ADAPTED_CLASS:
				setAdaptedClass((EClass)null);
				return;
			case GeneratorPackage.CLASS_ADAPTER__IMPORTS:
				getImports().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GeneratorPackage.CLASS_ADAPTER__FEATURE_ADAPTERS:
				return featureAdapters != null && !featureAdapters.isEmpty();
			case GeneratorPackage.CLASS_ADAPTER__ADAPTED_CLASS:
				return adaptedClass != null;
			case GeneratorPackage.CLASS_ADAPTER__IMPORTS:
				return imports != null && !imports.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (imports: ");
		result.append(imports);
		result.append(')');
		return result.toString();
	}

} //ClassAdapterImpl
