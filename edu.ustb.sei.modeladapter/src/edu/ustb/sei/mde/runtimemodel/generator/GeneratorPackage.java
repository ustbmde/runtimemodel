/**
 */
package edu.ustb.sei.mde.runtimemodel.generator;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorFactory
 * @model kind="package"
 * @generated
 */
public interface GeneratorPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "generator";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.ustb.edu.cn/sei/mde/runtimemodel/generator";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "gm";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	GeneratorPackage eINSTANCE = edu.ustb.sei.mde.runtimemodel.generator.impl.GeneratorPackageImpl.init();

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.runtimemodel.generator.impl.GeneratorModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.runtimemodel.generator.impl.GeneratorModelImpl
	 * @see edu.ustb.sei.mde.runtimemodel.generator.impl.GeneratorPackageImpl#getGeneratorModel()
	 * @generated
	 */
	int GENERATOR_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Class Adapters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATOR_MODEL__CLASS_ADAPTERS = 0;

	/**
	 * The feature id for the '<em><b>Adapted Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATOR_MODEL__ADAPTED_PACKAGE = 1;

	/**
	 * The feature id for the '<em><b>Directory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATOR_MODEL__DIRECTORY = 2;

	/**
	 * The feature id for the '<em><b>Base Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATOR_MODEL__BASE_PACKAGE = 3;

	/**
	 * The feature id for the '<em><b>Load Metamodel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATOR_MODEL__LOAD_METAMODEL = 4;

	/**
	 * The feature id for the '<em><b>Refreshing Rate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATOR_MODEL__REFRESHING_RATE = 5;

	/**
	 * The feature id for the '<em><b>Timer Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATOR_MODEL__TIMER_CLASS = 6;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATOR_MODEL_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GENERATOR_MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.runtimemodel.generator.impl.ClassAdapterImpl <em>Class Adapter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.runtimemodel.generator.impl.ClassAdapterImpl
	 * @see edu.ustb.sei.mde.runtimemodel.generator.impl.GeneratorPackageImpl#getClassAdapter()
	 * @generated
	 */
	int CLASS_ADAPTER = 1;

	/**
	 * The feature id for the '<em><b>Feature Adapters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ADAPTER__FEATURE_ADAPTERS = 0;

	/**
	 * The feature id for the '<em><b>Adapted Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ADAPTER__ADAPTED_CLASS = 1;

	/**
	 * The feature id for the '<em><b>Imports</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ADAPTER__IMPORTS = 2;

	/**
	 * The number of structural features of the '<em>Class Adapter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ADAPTER_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Class Adapter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_ADAPTER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.runtimemodel.generator.impl.FeatureAdapterImpl <em>Feature Adapter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.runtimemodel.generator.impl.FeatureAdapterImpl
	 * @see edu.ustb.sei.mde.runtimemodel.generator.impl.GeneratorPackageImpl#getFeatureAdapter()
	 * @generated
	 */
	int FEATURE_ADAPTER = 2;

	/**
	 * The feature id for the '<em><b>Adapted Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_ADAPTER__ADAPTED_FEATURE = 0;

	/**
	 * The feature id for the '<em><b>Get</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_ADAPTER__GET = 1;

	/**
	 * The feature id for the '<em><b>Post</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_ADAPTER__POST = 2;

	/**
	 * The feature id for the '<em><b>Put</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_ADAPTER__PUT = 3;

	/**
	 * The feature id for the '<em><b>Delete</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_ADAPTER__DELETE = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_ADAPTER__ID = 5;

	/**
	 * The feature id for the '<em><b>Conversion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_ADAPTER__CONVERSION = 6;

	/**
	 * The number of structural features of the '<em>Feature Adapter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_ADAPTER_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Feature Adapter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FEATURE_ADAPTER_OPERATION_COUNT = 0;


	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.runtimemodel.generator.impl.ConversionImpl <em>Conversion</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.runtimemodel.generator.impl.ConversionImpl
	 * @see edu.ustb.sei.mde.runtimemodel.generator.impl.GeneratorPackageImpl#getConversion()
	 * @generated
	 */
	int CONVERSION = 3;

	/**
	 * The feature id for the '<em><b>Physical Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONVERSION__PHYSICAL_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Physical To Logical</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONVERSION__PHYSICAL_TO_LOGICAL = 1;

	/**
	 * The feature id for the '<em><b>Logical To Physical</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONVERSION__LOGICAL_TO_PHYSICAL = 2;

	/**
	 * The number of structural features of the '<em>Conversion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONVERSION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Conversion</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONVERSION_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel
	 * @generated
	 */
	EClass getGeneratorModel();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getClassAdapters <em>Class Adapters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Class Adapters</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getClassAdapters()
	 * @see #getGeneratorModel()
	 * @generated
	 */
	EReference getGeneratorModel_ClassAdapters();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getAdaptedPackage <em>Adapted Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Adapted Package</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getAdaptedPackage()
	 * @see #getGeneratorModel()
	 * @generated
	 */
	EReference getGeneratorModel_AdaptedPackage();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getDirectory <em>Directory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Directory</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getDirectory()
	 * @see #getGeneratorModel()
	 * @generated
	 */
	EAttribute getGeneratorModel_Directory();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getBasePackage <em>Base Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base Package</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getBasePackage()
	 * @see #getGeneratorModel()
	 * @generated
	 */
	EAttribute getGeneratorModel_BasePackage();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getLoadMetamodel <em>Load Metamodel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Load Metamodel</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getLoadMetamodel()
	 * @see #getGeneratorModel()
	 * @generated
	 */
	EAttribute getGeneratorModel_LoadMetamodel();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getRefreshingRate <em>Refreshing Rate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Refreshing Rate</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getRefreshingRate()
	 * @see #getGeneratorModel()
	 * @generated
	 */
	EAttribute getGeneratorModel_RefreshingRate();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getTimerClass <em>Timer Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Timer Class</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel#getTimerClass()
	 * @see #getGeneratorModel()
	 * @generated
	 */
	EAttribute getGeneratorModel_TimerClass();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.runtimemodel.generator.ClassAdapter <em>Class Adapter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class Adapter</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.ClassAdapter
	 * @generated
	 */
	EClass getClassAdapter();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.runtimemodel.generator.ClassAdapter#getFeatureAdapters <em>Feature Adapters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Feature Adapters</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.ClassAdapter#getFeatureAdapters()
	 * @see #getClassAdapter()
	 * @generated
	 */
	EReference getClassAdapter_FeatureAdapters();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.runtimemodel.generator.ClassAdapter#getAdaptedClass <em>Adapted Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Adapted Class</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.ClassAdapter#getAdaptedClass()
	 * @see #getClassAdapter()
	 * @generated
	 */
	EReference getClassAdapter_AdaptedClass();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.runtimemodel.generator.ClassAdapter#getImports <em>Imports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Imports</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.ClassAdapter#getImports()
	 * @see #getClassAdapter()
	 * @generated
	 */
	EAttribute getClassAdapter_Imports();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter <em>Feature Adapter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Feature Adapter</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter
	 * @generated
	 */
	EClass getFeatureAdapter();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getAdaptedFeature <em>Adapted Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Adapted Feature</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getAdaptedFeature()
	 * @see #getFeatureAdapter()
	 * @generated
	 */
	EReference getFeatureAdapter_AdaptedFeature();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getGet <em>Get</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Get</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getGet()
	 * @see #getFeatureAdapter()
	 * @generated
	 */
	EAttribute getFeatureAdapter_Get();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getPost <em>Post</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Post</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getPost()
	 * @see #getFeatureAdapter()
	 * @generated
	 */
	EAttribute getFeatureAdapter_Post();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getPut <em>Put</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Put</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getPut()
	 * @see #getFeatureAdapter()
	 * @generated
	 */
	EAttribute getFeatureAdapter_Put();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getDelete <em>Delete</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Delete</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getDelete()
	 * @see #getFeatureAdapter()
	 * @generated
	 */
	EAttribute getFeatureAdapter_Delete();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#isId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#isId()
	 * @see #getFeatureAdapter()
	 * @generated
	 */
	EAttribute getFeatureAdapter_Id();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getConversion <em>Conversion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Conversion</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getConversion()
	 * @see #getFeatureAdapter()
	 * @generated
	 */
	EReference getFeatureAdapter_Conversion();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.runtimemodel.generator.Conversion <em>Conversion</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conversion</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.Conversion
	 * @generated
	 */
	EClass getConversion();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.runtimemodel.generator.Conversion#getPhysicalType <em>Physical Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Physical Type</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.Conversion#getPhysicalType()
	 * @see #getConversion()
	 * @generated
	 */
	EAttribute getConversion_PhysicalType();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.runtimemodel.generator.Conversion#getPhysicalToLogical <em>Physical To Logical</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Physical To Logical</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.Conversion#getPhysicalToLogical()
	 * @see #getConversion()
	 * @generated
	 */
	EAttribute getConversion_PhysicalToLogical();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.runtimemodel.generator.Conversion#getLogicalToPhysical <em>Logical To Physical</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Logical To Physical</em>'.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.Conversion#getLogicalToPhysical()
	 * @see #getConversion()
	 * @generated
	 */
	EAttribute getConversion_LogicalToPhysical();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	GeneratorFactory getGeneratorFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.runtimemodel.generator.impl.GeneratorModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.runtimemodel.generator.impl.GeneratorModelImpl
		 * @see edu.ustb.sei.mde.runtimemodel.generator.impl.GeneratorPackageImpl#getGeneratorModel()
		 * @generated
		 */
		EClass GENERATOR_MODEL = eINSTANCE.getGeneratorModel();

		/**
		 * The meta object literal for the '<em><b>Class Adapters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GENERATOR_MODEL__CLASS_ADAPTERS = eINSTANCE.getGeneratorModel_ClassAdapters();

		/**
		 * The meta object literal for the '<em><b>Adapted Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GENERATOR_MODEL__ADAPTED_PACKAGE = eINSTANCE.getGeneratorModel_AdaptedPackage();

		/**
		 * The meta object literal for the '<em><b>Directory</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GENERATOR_MODEL__DIRECTORY = eINSTANCE.getGeneratorModel_Directory();

		/**
		 * The meta object literal for the '<em><b>Base Package</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GENERATOR_MODEL__BASE_PACKAGE = eINSTANCE.getGeneratorModel_BasePackage();

		/**
		 * The meta object literal for the '<em><b>Load Metamodel</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GENERATOR_MODEL__LOAD_METAMODEL = eINSTANCE.getGeneratorModel_LoadMetamodel();

		/**
		 * The meta object literal for the '<em><b>Refreshing Rate</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GENERATOR_MODEL__REFRESHING_RATE = eINSTANCE.getGeneratorModel_RefreshingRate();

		/**
		 * The meta object literal for the '<em><b>Timer Class</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GENERATOR_MODEL__TIMER_CLASS = eINSTANCE.getGeneratorModel_TimerClass();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.runtimemodel.generator.impl.ClassAdapterImpl <em>Class Adapter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.runtimemodel.generator.impl.ClassAdapterImpl
		 * @see edu.ustb.sei.mde.runtimemodel.generator.impl.GeneratorPackageImpl#getClassAdapter()
		 * @generated
		 */
		EClass CLASS_ADAPTER = eINSTANCE.getClassAdapter();

		/**
		 * The meta object literal for the '<em><b>Feature Adapters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_ADAPTER__FEATURE_ADAPTERS = eINSTANCE.getClassAdapter_FeatureAdapters();

		/**
		 * The meta object literal for the '<em><b>Adapted Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLASS_ADAPTER__ADAPTED_CLASS = eINSTANCE.getClassAdapter_AdaptedClass();

		/**
		 * The meta object literal for the '<em><b>Imports</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS_ADAPTER__IMPORTS = eINSTANCE.getClassAdapter_Imports();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.runtimemodel.generator.impl.FeatureAdapterImpl <em>Feature Adapter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.runtimemodel.generator.impl.FeatureAdapterImpl
		 * @see edu.ustb.sei.mde.runtimemodel.generator.impl.GeneratorPackageImpl#getFeatureAdapter()
		 * @generated
		 */
		EClass FEATURE_ADAPTER = eINSTANCE.getFeatureAdapter();

		/**
		 * The meta object literal for the '<em><b>Adapted Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_ADAPTER__ADAPTED_FEATURE = eINSTANCE.getFeatureAdapter_AdaptedFeature();

		/**
		 * The meta object literal for the '<em><b>Get</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_ADAPTER__GET = eINSTANCE.getFeatureAdapter_Get();

		/**
		 * The meta object literal for the '<em><b>Post</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_ADAPTER__POST = eINSTANCE.getFeatureAdapter_Post();

		/**
		 * The meta object literal for the '<em><b>Put</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_ADAPTER__PUT = eINSTANCE.getFeatureAdapter_Put();

		/**
		 * The meta object literal for the '<em><b>Delete</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_ADAPTER__DELETE = eINSTANCE.getFeatureAdapter_Delete();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FEATURE_ADAPTER__ID = eINSTANCE.getFeatureAdapter_Id();

		/**
		 * The meta object literal for the '<em><b>Conversion</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FEATURE_ADAPTER__CONVERSION = eINSTANCE.getFeatureAdapter_Conversion();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.runtimemodel.generator.impl.ConversionImpl <em>Conversion</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.runtimemodel.generator.impl.ConversionImpl
		 * @see edu.ustb.sei.mde.runtimemodel.generator.impl.GeneratorPackageImpl#getConversion()
		 * @generated
		 */
		EClass CONVERSION = eINSTANCE.getConversion();

		/**
		 * The meta object literal for the '<em><b>Physical Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONVERSION__PHYSICAL_TYPE = eINSTANCE.getConversion_PhysicalType();

		/**
		 * The meta object literal for the '<em><b>Physical To Logical</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONVERSION__PHYSICAL_TO_LOGICAL = eINSTANCE.getConversion_PhysicalToLogical();

		/**
		 * The meta object literal for the '<em><b>Logical To Physical</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONVERSION__LOGICAL_TO_PHYSICAL = eINSTANCE.getConversion_LogicalToPhysical();

	}

} //GeneratorPackage
