/**
 */
package edu.ustb.sei.mde.runtimemodel.generator.impl;

import edu.ustb.sei.mde.runtimemodel.generator.ClassAdapter;
import edu.ustb.sei.mde.runtimemodel.generator.GeneratorModel;
import edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.impl.GeneratorModelImpl#getClassAdapters <em>Class Adapters</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.impl.GeneratorModelImpl#getAdaptedPackage <em>Adapted Package</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.impl.GeneratorModelImpl#getDirectory <em>Directory</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.impl.GeneratorModelImpl#getBasePackage <em>Base Package</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.impl.GeneratorModelImpl#getLoadMetamodel <em>Load Metamodel</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.impl.GeneratorModelImpl#getRefreshingRate <em>Refreshing Rate</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.impl.GeneratorModelImpl#getTimerClass <em>Timer Class</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GeneratorModelImpl extends MinimalEObjectImpl.Container implements GeneratorModel {
	/**
	 * The cached value of the '{@link #getClassAdapters() <em>Class Adapters</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassAdapters()
	 * @generated
	 * @ordered
	 */
	protected EList<ClassAdapter> classAdapters;

	/**
	 * The cached value of the '{@link #getAdaptedPackage() <em>Adapted Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdaptedPackage()
	 * @generated
	 * @ordered
	 */
	protected EPackage adaptedPackage;

	/**
	 * The default value of the '{@link #getDirectory() <em>Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDirectory()
	 * @generated
	 * @ordered
	 */
	protected static final String DIRECTORY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDirectory() <em>Directory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDirectory()
	 * @generated
	 * @ordered
	 */
	protected String directory = DIRECTORY_EDEFAULT;

	/**
	 * The default value of the '{@link #getBasePackage() <em>Base Package</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBasePackage()
	 * @generated
	 * @ordered
	 */
	protected static final String BASE_PACKAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBasePackage() <em>Base Package</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBasePackage()
	 * @generated
	 * @ordered
	 */
	protected String basePackage = BASE_PACKAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getLoadMetamodel() <em>Load Metamodel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadMetamodel()
	 * @generated
	 * @ordered
	 */
	protected static final String LOAD_METAMODEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLoadMetamodel() <em>Load Metamodel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoadMetamodel()
	 * @generated
	 * @ordered
	 */
	protected String loadMetamodel = LOAD_METAMODEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getRefreshingRate() <em>Refreshing Rate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRefreshingRate()
	 * @generated
	 * @ordered
	 */
	protected static final long REFRESHING_RATE_EDEFAULT = 1000L;

	/**
	 * The cached value of the '{@link #getRefreshingRate() <em>Refreshing Rate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRefreshingRate()
	 * @generated
	 * @ordered
	 */
	protected long refreshingRate = REFRESHING_RATE_EDEFAULT;

	/**
	 * The default value of the '{@link #getTimerClass() <em>Timer Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimerClass()
	 * @generated
	 * @ordered
	 */
	protected static final String TIMER_CLASS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTimerClass() <em>Timer Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimerClass()
	 * @generated
	 * @ordered
	 */
	protected String timerClass = TIMER_CLASS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GeneratorModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return GeneratorPackage.Literals.GENERATOR_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ClassAdapter> getClassAdapters() {
		if (classAdapters == null) {
			classAdapters = new EObjectContainmentEList<ClassAdapter>(ClassAdapter.class, this, GeneratorPackage.GENERATOR_MODEL__CLASS_ADAPTERS);
		}
		return classAdapters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage getAdaptedPackage() {
		if (adaptedPackage != null && adaptedPackage.eIsProxy()) {
			InternalEObject oldAdaptedPackage = (InternalEObject)adaptedPackage;
			adaptedPackage = (EPackage)eResolveProxy(oldAdaptedPackage);
			if (adaptedPackage != oldAdaptedPackage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, GeneratorPackage.GENERATOR_MODEL__ADAPTED_PACKAGE, oldAdaptedPackage, adaptedPackage));
			}
		}
		return adaptedPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage basicGetAdaptedPackage() {
		return adaptedPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAdaptedPackage(EPackage newAdaptedPackage) {
		EPackage oldAdaptedPackage = adaptedPackage;
		adaptedPackage = newAdaptedPackage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GeneratorPackage.GENERATOR_MODEL__ADAPTED_PACKAGE, oldAdaptedPackage, adaptedPackage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDirectory() {
		return directory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDirectory(String newDirectory) {
		String oldDirectory = directory;
		directory = newDirectory;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GeneratorPackage.GENERATOR_MODEL__DIRECTORY, oldDirectory, directory));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBasePackage() {
		return basePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBasePackage(String newBasePackage) {
		String oldBasePackage = basePackage;
		basePackage = newBasePackage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GeneratorPackage.GENERATOR_MODEL__BASE_PACKAGE, oldBasePackage, basePackage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLoadMetamodel() {
		return loadMetamodel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLoadMetamodel(String newLoadMetamodel) {
		String oldLoadMetamodel = loadMetamodel;
		loadMetamodel = newLoadMetamodel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GeneratorPackage.GENERATOR_MODEL__LOAD_METAMODEL, oldLoadMetamodel, loadMetamodel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getRefreshingRate() {
		return refreshingRate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRefreshingRate(long newRefreshingRate) {
		long oldRefreshingRate = refreshingRate;
		refreshingRate = newRefreshingRate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GeneratorPackage.GENERATOR_MODEL__REFRESHING_RATE, oldRefreshingRate, refreshingRate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTimerClass() {
		return timerClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimerClass(String newTimerClass) {
		String oldTimerClass = timerClass;
		timerClass = newTimerClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, GeneratorPackage.GENERATOR_MODEL__TIMER_CLASS, oldTimerClass, timerClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case GeneratorPackage.GENERATOR_MODEL__CLASS_ADAPTERS:
				return ((InternalEList<?>)getClassAdapters()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case GeneratorPackage.GENERATOR_MODEL__CLASS_ADAPTERS:
				return getClassAdapters();
			case GeneratorPackage.GENERATOR_MODEL__ADAPTED_PACKAGE:
				if (resolve) return getAdaptedPackage();
				return basicGetAdaptedPackage();
			case GeneratorPackage.GENERATOR_MODEL__DIRECTORY:
				return getDirectory();
			case GeneratorPackage.GENERATOR_MODEL__BASE_PACKAGE:
				return getBasePackage();
			case GeneratorPackage.GENERATOR_MODEL__LOAD_METAMODEL:
				return getLoadMetamodel();
			case GeneratorPackage.GENERATOR_MODEL__REFRESHING_RATE:
				return getRefreshingRate();
			case GeneratorPackage.GENERATOR_MODEL__TIMER_CLASS:
				return getTimerClass();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case GeneratorPackage.GENERATOR_MODEL__CLASS_ADAPTERS:
				getClassAdapters().clear();
				getClassAdapters().addAll((Collection<? extends ClassAdapter>)newValue);
				return;
			case GeneratorPackage.GENERATOR_MODEL__ADAPTED_PACKAGE:
				setAdaptedPackage((EPackage)newValue);
				return;
			case GeneratorPackage.GENERATOR_MODEL__DIRECTORY:
				setDirectory((String)newValue);
				return;
			case GeneratorPackage.GENERATOR_MODEL__BASE_PACKAGE:
				setBasePackage((String)newValue);
				return;
			case GeneratorPackage.GENERATOR_MODEL__LOAD_METAMODEL:
				setLoadMetamodel((String)newValue);
				return;
			case GeneratorPackage.GENERATOR_MODEL__REFRESHING_RATE:
				setRefreshingRate((Long)newValue);
				return;
			case GeneratorPackage.GENERATOR_MODEL__TIMER_CLASS:
				setTimerClass((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case GeneratorPackage.GENERATOR_MODEL__CLASS_ADAPTERS:
				getClassAdapters().clear();
				return;
			case GeneratorPackage.GENERATOR_MODEL__ADAPTED_PACKAGE:
				setAdaptedPackage((EPackage)null);
				return;
			case GeneratorPackage.GENERATOR_MODEL__DIRECTORY:
				setDirectory(DIRECTORY_EDEFAULT);
				return;
			case GeneratorPackage.GENERATOR_MODEL__BASE_PACKAGE:
				setBasePackage(BASE_PACKAGE_EDEFAULT);
				return;
			case GeneratorPackage.GENERATOR_MODEL__LOAD_METAMODEL:
				setLoadMetamodel(LOAD_METAMODEL_EDEFAULT);
				return;
			case GeneratorPackage.GENERATOR_MODEL__REFRESHING_RATE:
				setRefreshingRate(REFRESHING_RATE_EDEFAULT);
				return;
			case GeneratorPackage.GENERATOR_MODEL__TIMER_CLASS:
				setTimerClass(TIMER_CLASS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case GeneratorPackage.GENERATOR_MODEL__CLASS_ADAPTERS:
				return classAdapters != null && !classAdapters.isEmpty();
			case GeneratorPackage.GENERATOR_MODEL__ADAPTED_PACKAGE:
				return adaptedPackage != null;
			case GeneratorPackage.GENERATOR_MODEL__DIRECTORY:
				return DIRECTORY_EDEFAULT == null ? directory != null : !DIRECTORY_EDEFAULT.equals(directory);
			case GeneratorPackage.GENERATOR_MODEL__BASE_PACKAGE:
				return BASE_PACKAGE_EDEFAULT == null ? basePackage != null : !BASE_PACKAGE_EDEFAULT.equals(basePackage);
			case GeneratorPackage.GENERATOR_MODEL__LOAD_METAMODEL:
				return LOAD_METAMODEL_EDEFAULT == null ? loadMetamodel != null : !LOAD_METAMODEL_EDEFAULT.equals(loadMetamodel);
			case GeneratorPackage.GENERATOR_MODEL__REFRESHING_RATE:
				return refreshingRate != REFRESHING_RATE_EDEFAULT;
			case GeneratorPackage.GENERATOR_MODEL__TIMER_CLASS:
				return TIMER_CLASS_EDEFAULT == null ? timerClass != null : !TIMER_CLASS_EDEFAULT.equals(timerClass);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (directory: ");
		result.append(directory);
		result.append(", basePackage: ");
		result.append(basePackage);
		result.append(", loadMetamodel: ");
		result.append(loadMetamodel);
		result.append(", refreshingRate: ");
		result.append(refreshingRate);
		result.append(", timerClass: ");
		result.append(timerClass);
		result.append(')');
		return result.toString();
	}

} //GeneratorModelImpl
