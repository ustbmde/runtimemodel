/**
 */
package edu.ustb.sei.mde.runtimemodel.generator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Adapter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getAdaptedFeature <em>Adapted Feature</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getGet <em>Get</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getPost <em>Post</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getPut <em>Put</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getDelete <em>Delete</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#isId <em>Id</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getConversion <em>Conversion</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getFeatureAdapter()
 * @model
 * @generated
 */
public interface FeatureAdapter extends EObject {
	/**
	 * Returns the value of the '<em><b>Adapted Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Adapted Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Adapted Feature</em>' reference.
	 * @see #setAdaptedFeature(EStructuralFeature)
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getFeatureAdapter_AdaptedFeature()
	 * @model required="true"
	 * @generated
	 */
	EStructuralFeature getAdaptedFeature();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getAdaptedFeature <em>Adapted Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Adapted Feature</em>' reference.
	 * @see #getAdaptedFeature()
	 * @generated
	 */
	void setAdaptedFeature(EStructuralFeature value);

	/**
	 * Returns the value of the '<em><b>Get</b></em>' attribute.
	 * The default value is <code>"// get : self -> physicalValue"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Get</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Get</em>' attribute.
	 * @see #setGet(String)
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getFeatureAdapter_Get()
	 * @model default="// get : self -> physicalValue"
	 * @generated
	 */
	String getGet();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getGet <em>Get</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Get</em>' attribute.
	 * @see #getGet()
	 * @generated
	 */
	void setGet(String value);

	/**
	 * Returns the value of the '<em><b>Post</b></em>' attribute.
	 * The default value is <code>"// post : (self, logicalValue) -> physicalValue"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Post</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Post</em>' attribute.
	 * @see #setPost(String)
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getFeatureAdapter_Post()
	 * @model default="// post : (self, logicalValue) -> physicalValue"
	 * @generated
	 */
	String getPost();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getPost <em>Post</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Post</em>' attribute.
	 * @see #getPost()
	 * @generated
	 */
	void setPost(String value);

	/**
	 * Returns the value of the '<em><b>Put</b></em>' attribute.
	 * The default value is <code>"// put : (self, physicalValue, logicalValue) -> physicalValue"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Put</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Put</em>' attribute.
	 * @see #setPut(String)
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getFeatureAdapter_Put()
	 * @model default="// put : (self, physicalValue, logicalValue) -> physicalValue"
	 * @generated
	 */
	String getPut();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getPut <em>Put</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Put</em>' attribute.
	 * @see #getPut()
	 * @generated
	 */
	void setPut(String value);

	/**
	 * Returns the value of the '<em><b>Delete</b></em>' attribute.
	 * The default value is <code>"// delete : (self, physicalValue) -> boolean"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Delete</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Delete</em>' attribute.
	 * @see #setDelete(String)
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getFeatureAdapter_Delete()
	 * @model default="// delete : (self, physicalValue) -> boolean"
	 * @generated
	 */
	String getDelete();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getDelete <em>Delete</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Delete</em>' attribute.
	 * @see #getDelete()
	 * @generated
	 */
	void setDelete(String value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(boolean)
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getFeatureAdapter_Id()
	 * @model
	 * @generated
	 */
	boolean isId();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#isId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #isId()
	 * @generated
	 */
	void setId(boolean value);

	/**
	 * Returns the value of the '<em><b>Conversion</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Conversion</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Conversion</em>' containment reference.
	 * @see #setConversion(Conversion)
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getFeatureAdapter_Conversion()
	 * @model containment="true"
	 * @generated
	 */
	Conversion getConversion();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter#getConversion <em>Conversion</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Conversion</em>' containment reference.
	 * @see #getConversion()
	 * @generated
	 */
	void setConversion(Conversion value);

} // FeatureAdapter
