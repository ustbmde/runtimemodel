/**
 */
package edu.ustb.sei.mde.runtimemodel.generator;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Adapter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.ClassAdapter#getFeatureAdapters <em>Feature Adapters</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.ClassAdapter#getAdaptedClass <em>Adapted Class</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.runtimemodel.generator.ClassAdapter#getImports <em>Imports</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getClassAdapter()
 * @model
 * @generated
 */
public interface ClassAdapter extends EObject {
	/**
	 * Returns the value of the '<em><b>Feature Adapters</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature Adapters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature Adapters</em>' containment reference list.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getClassAdapter_FeatureAdapters()
	 * @model containment="true"
	 * @generated
	 */
	EList<FeatureAdapter> getFeatureAdapters();

	/**
	 * Returns the value of the '<em><b>Adapted Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Adapted Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Adapted Class</em>' reference.
	 * @see #setAdaptedClass(EClass)
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getClassAdapter_AdaptedClass()
	 * @model required="true"
	 * @generated
	 */
	EClass getAdaptedClass();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.runtimemodel.generator.ClassAdapter#getAdaptedClass <em>Adapted Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Adapted Class</em>' reference.
	 * @see #getAdaptedClass()
	 * @generated
	 */
	void setAdaptedClass(EClass value);

	/**
	 * Returns the value of the '<em><b>Imports</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imports</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imports</em>' attribute list.
	 * @see edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage#getClassAdapter_Imports()
	 * @model
	 * @generated
	 */
	EList<String> getImports();

} // ClassAdapter
