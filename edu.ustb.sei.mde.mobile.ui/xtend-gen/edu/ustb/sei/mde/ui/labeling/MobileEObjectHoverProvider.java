package edu.ustb.sei.mde.ui.labeling;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.ui.editor.hover.html.DefaultEObjectHoverProvider;

@SuppressWarnings("all")
public class MobileEObjectHoverProvider extends DefaultEObjectHoverProvider {
  @Override
  protected String getDocumentation(final EObject o) {
    String _documentation = super.getDocumentation(o);
    String _plus = (_documentation + "\n");
    return (_plus + "customize");
  }
}
