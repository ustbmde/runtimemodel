package edu.ustb.sei.mde.ui.labeling

import org.eclipse.xtext.ui.editor.hover.html.DefaultEObjectHoverProvider
import org.eclipse.emf.ecore.EObject

class MobileEObjectHoverProvider extends DefaultEObjectHoverProvider {
	
	override protected getDocumentation(EObject o) {
		super.getDocumentation(o)+'\n'+'customize'
	}
	
}