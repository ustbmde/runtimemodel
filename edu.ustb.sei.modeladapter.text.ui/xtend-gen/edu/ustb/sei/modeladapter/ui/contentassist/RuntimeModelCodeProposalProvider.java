/**
 * generated by Xtext 2.11.0
 */
package edu.ustb.sei.modeladapter.ui.contentassist;

import edu.ustb.sei.modeladapter.ui.contentassist.AbstractRuntimeModelCodeProposalProvider;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.AbstractElement;
import org.eclipse.xtext.Assignment;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext;
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor;

/**
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#content-assist
 * on how to customize the content assistant.
 */
@SuppressWarnings("all")
public class RuntimeModelCodeProposalProvider extends AbstractRuntimeModelCodeProposalProvider {
  @Override
  public void complete_CODE(final EObject model, final RuleCall ruleCall, final ContentAssistContext context, final ICompletionProposalAcceptor acceptor) {
    super.complete_CODE(model, ruleCall, context, acceptor);
    AbstractElement first = context.getFirstSetGrammarElements().get(0);
    String _xifexpression = null;
    if ((first instanceof Assignment)) {
      _xifexpression = ((Assignment) first).getFeature();
    } else {
      _xifexpression = null;
    }
    String feature = _xifexpression;
    boolean _equals = "loadMetamodel".equals(feature);
    if (_equals) {
      acceptor.accept(this.createCompletionProposal("⟦ metamodel = expression ⟧", context));
      acceptor.accept(this.createCompletionProposal("⟦ /* Java code */ ⟧", context));
    } else {
      boolean _equals_1 = "get".equals(feature);
      if (_equals_1) {
        acceptor.accept(this.createCompletionProposal("⟦ /* (self) -> physical value */ ⟧", context));
      } else {
        boolean _equals_2 = "post".equals(feature);
        if (_equals_2) {
          acceptor.accept(this.createCompletionProposal("⟦ /* (self, val) -> physical Value */ ⟧", context));
        } else {
          boolean _equals_3 = "put".equals(feature);
          if (_equals_3) {
            acceptor.accept(this.createCompletionProposal("⟦ /* (self, old, val) -> physicalValue */ ⟧", context));
          } else {
            boolean _equals_4 = "delete".equals(feature);
            if (_equals_4) {
              acceptor.accept(this.createCompletionProposal("⟦ /* (self, val) -> boolean */ ⟧", context));
            } else {
              boolean _equals_5 = "from".equals(feature);
              if (_equals_5) {
                acceptor.accept(this.createCompletionProposal("⟦ (pval,self) -> lval  ⟧", context));
              } else {
                boolean _equals_6 = "to".equals(feature);
                if (_equals_6) {
                  acceptor.accept(this.createCompletionProposal("⟦ (lval, self) -> pval ⟧", context));
                } else {
                  acceptor.accept(this.createCompletionProposal("⟦ ⟧", context));
                }
              }
            }
          }
        }
      }
    }
  }
}
