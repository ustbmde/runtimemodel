package edu.ustb.sei.mde.mobile.swt.actions;

import java.util.function.Supplier;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;

import edu.ustb.sei.mde.mobile.swt.Display;
import edu.ustb.sei.mde.mobile.swt.Shell;
import edu.ustb.sei.mde.mobile.swtmodel.SwtModelUtil;



public class GenerateViewAction extends Action {
	protected Supplier<Resource> resourceSupplier;

	public GenerateViewAction(Supplier<Resource> resourceSupplier) {
		super("Get View");
		this.resourceSupplier = resourceSupplier;
	}

	@Override
	public void run() {
		Resource resource = this.resourceSupplier.get();
		EObject root;
		if(resource.getContents().isEmpty()==false) root = resource.getContents().get(0);
		else root = null;
		
		if(root==null || !(root instanceof Shell)) {
			MessageDialog.openError(null, "Error in get", "This model bridge requires a root Shell");
		} else {
			org.eclipse.swt.widgets.Shell sd = null;
			
			sd = (org.eclipse.swt.widgets.Shell) ((Shell)root).getSwtObject();
			if(sd!=null) {
				@SuppressWarnings("unchecked")
				Shell newRoot = (Shell) SwtModelUtil.instance.get(sd);
				newRoot.setSwtObject(sd);
				resource.getContents().clear();
				resource.getContents().add(newRoot);
			}
		}
		
	}
}
