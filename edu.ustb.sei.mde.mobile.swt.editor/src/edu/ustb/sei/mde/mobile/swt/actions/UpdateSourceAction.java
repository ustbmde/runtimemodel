package edu.ustb.sei.mde.mobile.swt.actions;

import java.util.function.Supplier;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.action.Action;

import edu.ustb.sei.mde.mobile.datastructure.operation.ValueProvider;
import edu.ustb.sei.mde.mobile.swt.Display;
import edu.ustb.sei.mde.mobile.swt.Shell;
import edu.ustb.sei.mde.mobile.swtmodel.SwtModelUtil;

public class UpdateSourceAction extends Action {
	protected Supplier<Resource> resourceSupplier;

	public UpdateSourceAction(Supplier<Resource> resourceSupplier) {
		super("Update Source");
		this.resourceSupplier = resourceSupplier;
	}
	
	@Override
	public void run() {
		Resource resource = this.resourceSupplier.get();
		Shell root = (Shell) resource.getContents().get(0);
		org.eclipse.swt.widgets.Shell sd = (org.eclipse.swt.widgets.Shell) root.getSwtObject();
		@SuppressWarnings("unchecked")
		ValueProvider<?,?,org.eclipse.swt.widgets.Shell> provider = SwtModelUtil.instance.put(sd, root);
		SwtModelUtil.apply(provider);
		root.setSwtObject(provider.getValue());
	}
}
