/**
 */
package edu.ustb.sei.filesystem;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Folder</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.filesystem.Folder#getItems <em>Items</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.filesystem.FilesystemPackage#getFolder()
 * @model
 * @generated
 */
public interface Folder extends FileItem {
	/**
	 * Returns the value of the '<em><b>Items</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.filesystem.FileItem}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Items</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Items</em>' containment reference list.
	 * @see edu.ustb.sei.filesystem.FilesystemPackage#getFolder_Items()
	 * @model containment="true"
	 * @generated
	 */
	EList<FileItem> getItems();

} // Folder
