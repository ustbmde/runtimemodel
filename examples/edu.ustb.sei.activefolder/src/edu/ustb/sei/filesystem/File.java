/**
 */
package edu.ustb.sei.filesystem;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>File</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.filesystem.FilesystemPackage#getFile()
 * @model
 * @generated
 */
public interface File extends FileItem {
} // File
