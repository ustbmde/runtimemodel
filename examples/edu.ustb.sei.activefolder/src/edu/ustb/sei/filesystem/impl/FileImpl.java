/**
 */
package edu.ustb.sei.filesystem.impl;

import edu.ustb.sei.filesystem.File;
import edu.ustb.sei.filesystem.FilesystemPackage;

import java.io.IOException;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>File</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class FileImpl extends FileItemImpl implements File {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FileImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FilesystemPackage.Literals.FILE;
	}

	
	@Override
	public void updateURI() {
		String uri = this.computeURI();
		this.setUri(uri);
	}
	
	@Override
	public boolean exists() {
		String uri = this.computeURI();
		java.io.File f = new java.io.File(uri);
		return f.isFile();
	}
	
	@Override
	public boolean create() {
		String uri = this.computeURI();
		java.io.File f = new java.io.File(uri);
		if(!f.isFile()) {
			try {
				f.createNewFile();
			} catch (IOException e) {
				return false;
			}
		}
		return true;
	}
	
	@Override
	public boolean delete() {
		String uri = this.computeURI();
		java.io.File f = new java.io.File(uri);
		if(f.isFile()) {
			f.delete();
		}
		return true;
	}
	
	@Override
	public boolean move() {
		String oldURI = this.getUri();
		String newURI = this.computeURI();
		if(oldURI==null)
			return false;
		if(oldURI.equals(newURI))
			return true;
		
		java.io.File oldFile = new java.io.File(oldURI);
		if(oldFile.isFile()) {
			java.io.File newFile = new java.io.File(newURI);
			if(oldFile.renameTo(newFile)) {
				this.setUri(newURI);
				return true;
			} else
				return false;
		} else
			return false;
	}
} //FileImpl
