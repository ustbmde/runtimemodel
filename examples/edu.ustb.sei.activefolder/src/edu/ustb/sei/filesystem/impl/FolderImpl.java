/**
 */
package edu.ustb.sei.filesystem.impl;

import edu.ustb.sei.filesystem.FileItem;
import edu.ustb.sei.filesystem.FilesystemPackage;
import edu.ustb.sei.filesystem.Folder;
import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Folder</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.filesystem.impl.FolderImpl#getItems <em>Items</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FolderImpl extends FileItemImpl implements Folder {
	/**
	 * The cached value of the '{@link #getItems() <em>Items</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getItems()
	 * @generated
	 * @ordered
	 */
	protected EList<FileItem> items;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FolderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FilesystemPackage.Literals.FOLDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<FileItem> getItems() {
		if (items == null) {
			items = new EObjectContainmentEList<FileItem>(FileItem.class, this, FilesystemPackage.FOLDER__ITEMS);
		}
		return items;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FilesystemPackage.FOLDER__ITEMS:
				return ((InternalEList<?>)getItems()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FilesystemPackage.FOLDER__ITEMS:
				return getItems();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FilesystemPackage.FOLDER__ITEMS:
				getItems().clear();
				getItems().addAll((Collection<? extends FileItem>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FilesystemPackage.FOLDER__ITEMS:
				getItems().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FilesystemPackage.FOLDER__ITEMS:
				return items != null && !items.isEmpty();
		}
		return super.eIsSet(featureID);
	}
	
	@Override
	public void updateURI() {
		String oldUri = this.getUri();
		String uri = this.computeURI();

		if(oldUri==null) {
			this.setUri(uri);
			return;
		} else {
			EObject root = EcoreUtil.getRootContainer(this);
			TreeIterator<EObject> iter = root.eAllContents();
			while(iter.hasNext()) {
				EObject cur = iter.next();
				if(cur instanceof FileItem) {
					if(((FileItem) cur).getUri()!=null) {
						String tUri = ((FileItem) cur).getUri();
						if(tUri.startsWith(oldUri)) {
							tUri = uri + tUri.substring(oldUri.length());
							((FileItem) cur).setUri(tUri);
						}
					}
				} 
			}
		}
	}

	@Override
	public boolean exists() {
		String uri = this.computeURI();
		java.io.File f = new java.io.File(uri);
		return f.isDirectory();
	}
	
	@Override
	public boolean create() {
		String uri = this.computeURI();
		java.io.File f = new java.io.File(uri);
		if(!f.isDirectory()) {
			f.mkdirs();
			this.updateURI();
		}
		return true;
	}
	
	@Override
	public boolean delete() {
		String uri = this.computeURI();
		java.io.File f = new java.io.File(uri);
		if(f.isDirectory()) {
			f.delete();
		}
		return true;
	}
	
	@Override
	public boolean move() {
		String oldURI = this.getUri();
		if(oldURI==null)
			return false;
		java.io.File oldFile = new java.io.File(oldURI);
		if(oldFile.isDirectory()) {
			String newURI = this.computeURI();
			java.io.File newFile = new java.io.File(newURI);
			if(oldFile.renameTo(newFile)) {
				this.updateURI();
				return true;
			} else
				return false;
		} else
			return false;
	}
	
} //FolderImpl
