package edu.ustb.sei.filesystem.adapter;

import edu.ustb.sei.mde.runtimemodel.generator.*;
import org.eclipse.emf.ecore.*;
import edu.ustb.sei.mde.runtimemodel.modeladapter.*;
import org.eclipse.emf.common.util.URI;


public class FilesystemModelService extends ModelService {
final public static ModelService INSTANCE = new FilesystemModelService();

public FilesystemModelService() {
super();
}

@Override
protected void loadMetamodel() {
EPackage metamodel=edu.ustb.sei.filesystem.FilesystemPackage.eINSTANCE;
loadMetamodel(metamodel);
}

@Override
public EStructuralFeature getIDFeature(EObject obj) {
EClass cls = obj.eClass();
String clsName = cls.getName();

if(clsName.equals("BaseFolder")){
  	throw new UnsupportedOperationException();
} else
if(clsName.equals("File")){
return cls.getEStructuralFeature("name");
} else
if(clsName.equals("Folder")){
return cls.getEStructuralFeature("name");
} else
if(clsName.equals("FileItem")){
return cls.getEStructuralFeature("name");
} else 
throw new UnsupportedOperationException();
}

private BaseAdapterFactory factory;
@Override
public BaseAdapterFactory getRuntimeModelAdapterFactory() {
if(factory==null) {
factory = new FilesystemAdapterFactory(this, metamodel);
}
return factory;
}
@Override
public long getAutoRefreshingRate() {
return 1000;
}
}