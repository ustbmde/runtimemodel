package edu.ustb.sei.filesystem.adapter;

import edu.ustb.sei.mde.runtimemodel.generator.*;
import org.eclipse.emf.ecore.*;
import edu.ustb.sei.mde.runtimemodel.modeladapter.*;
import org.eclipse.emf.common.util.URI;
import edu.ustb.sei.mde.runtimemodel.modeladapter.FeatureCacheHolder.*;

import edu.ustb.sei.filesystem.*;

public class FileAdapter extends BaseAdapter {
public FileAdapter() {
}

@Override
public void init() {
EClass eClass = this.getSelf().eClass();
EPackage ePackage = eClass.getEPackage();
PackageAdapter packAdapter = BaseAdapterFactory.getPackageAdapter(ePackage);
ModelService service = packAdapter.getModelService();

// install feature container
{
  		StateBasedSingleValueFeatureCacheHolder<java.lang.String,java.lang.String> uri = new StateBasedSingleValueFeatureCacheHolder<java.lang.String,java.lang.String>();
  // install conversion service
  
    uri.physicalToLogicalConverter = ConversionService.identity();
    uri.logicalToPhysicalConverter = ConversionService.identity();
    
  uri.feature = eClass.getEStructuralFeature("uri");
  
  this.addFeatureCache(uri);
  
  	  
  uri.getService = (self) -> {
    // get : self -> physicalValue
File f = (File)self;
return f.computeLastURI();
  };
  
  
  
  
  
  
  
}

}

}