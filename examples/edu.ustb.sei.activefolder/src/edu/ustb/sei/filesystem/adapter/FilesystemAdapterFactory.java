package edu.ustb.sei.filesystem.adapter;

import edu.ustb.sei.mde.runtimemodel.generator.*;
import org.eclipse.emf.ecore.*;
import edu.ustb.sei.mde.runtimemodel.modeladapter.*;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.notify.Adapter;

public class FilesystemAdapterFactory extends BaseAdapterFactory {

public FilesystemAdapterFactory(ModelService modelService, EPackage modelPackage) {
super(modelService, modelPackage);
}

@Override
protected Adapter createPackageAdapter(EPackage eObject) {
return new PackageAdapter(modelService, this);
}

@Override
protected Adapter createModelAdapter(EClass clazz, EObject eObject) {
String clsName = clazz.getName();

if(clsName.equals("BaseFolder")) {
    return new BaseFolderAdapter();
} else
if(clsName.equals("File")) {
    return new FileAdapter();
} else
if(clsName.equals("Folder")) {
    return new FolderAdapter();
} else
if(clsName.equals("FileItem")) {
    return new FileItemAdapter();
} else
return null;
}

}