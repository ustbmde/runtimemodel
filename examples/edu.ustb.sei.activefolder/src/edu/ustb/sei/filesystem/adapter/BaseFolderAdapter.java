package edu.ustb.sei.filesystem.adapter;

import edu.ustb.sei.mde.runtimemodel.generator.*;
import org.eclipse.emf.ecore.*;
import edu.ustb.sei.mde.runtimemodel.modeladapter.*;
import org.eclipse.emf.common.util.URI;
import edu.ustb.sei.mde.runtimemodel.modeladapter.FeatureCacheHolder.*;

import edu.ustb.sei.filesystem.*;

public class BaseFolderAdapter extends BaseAdapter {
public BaseFolderAdapter() {
}

@Override
public void init() {
EClass eClass = this.getSelf().eClass();
EPackage ePackage = eClass.getEPackage();
PackageAdapter packAdapter = BaseAdapterFactory.getPackageAdapter(ePackage);
ModelService service = packAdapter.getModelService();

// install feature container
{
  	  TraceBasedSingleValueFeatureCacheHolder<ObjectRepresentation<java.lang.String>, EObject> base = new TraceBasedSingleValueFeatureCacheHolder<ObjectRepresentation<java.lang.String>, EObject>();
  // install conversion service
  
    base.physicalToLogicalConverter = ConversionService.objectRepresentationToEObject();
    base.logicalToPhysicalConverter = ConversionService.eObjectToObjectRepresentation();
    
  base.feature = eClass.getEStructuralFeature("base");
  
  this.addFeatureCache(base);
  
  	  
  base.getService = (self) -> {
    // get : self -> physicalValue
BaseFolder baseFolder = (BaseFolder)self;
String baseUri = baseFolder.getBaseUri();
java.io.File file = new java.io.File(baseUri);
if(file.exists()) {
return ObjectRepresentation.id("Folder",file.getName());
} else {
return null;
}
  };
  
  
  	  
  base.postService = (self,val) -> {
    // post : (self, logicalValue) -> physicalValue
FileItem item = (FileItem)val;
String uri = item.computeURI();
java.io.File f = new java.io.File(uri);
if(!f.exists()) {
f.mkdirs();
}

BaseFolder bf = (BaseFolder)self;
bf.setBaseUri(uri);
item.updateURI();

return ObjectRepresentation.id("Folder",item.getName());
  };
  
  
  	  
  base.deleteService = (self,val) -> {
    // delete : (self, physicalValue) -> boolean
BaseFolder f = (BaseFolder)self;
String baseUri = f.getBaseUri();
java.io.File ff = new java.io.File(baseUri);
ff.delete();
return true;
  };
  
  
  	  
  base.putService = (self,old,val) -> {
    // put : (self, physicalValue, logicalValue) -> physicalValue
BaseFolder bf = (BaseFolder)self;
String baseUri = bf.getBaseUri();
FileItem item = (FileItem)val;
String newUri = item.computeURI();

java.io.File oldFile = new java.io.File(baseUri);
java.io.File newFile = new java.io.File(newUri);

oldFile.renameTo(newFile);

bf.setBaseUri(newUri);
item.updateURI();

return ObjectRepresentation.id("Folder", item.getName());
  };
  
}

}

}