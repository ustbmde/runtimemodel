package edu.ustb.sei.filesystem.adapter;

import edu.ustb.sei.mde.runtimemodel.generator.*;
import org.eclipse.emf.ecore.*;
import edu.ustb.sei.mde.runtimemodel.modeladapter.*;
import org.eclipse.emf.common.util.URI;
import edu.ustb.sei.mde.runtimemodel.modeladapter.FeatureCacheHolder.*;

import edu.ustb.sei.filesystem.*;
import java.util.*;

public class FolderAdapter extends BaseAdapter {
public FolderAdapter() {
}

@Override
public void init() {
EClass eClass = this.getSelf().eClass();
EPackage ePackage = eClass.getEPackage();
PackageAdapter packAdapter = BaseAdapterFactory.getPackageAdapter(ePackage);
ModelService service = packAdapter.getModelService();

// install feature container
{
  	  TraceBasedMultiValueFeatureCacheHolder<ObjectRepresentation<java.lang.String>, EObject> items = new TraceBasedMultiValueFeatureCacheHolder<ObjectRepresentation<java.lang.String>, EObject>();
  // install conversion service
  
    items.physicalToLogicalConverter = ConversionService.objectRepresentationToEObject();
    items.logicalToPhysicalConverter = ConversionService.eObjectToObjectRepresentation();
    
  items.feature = eClass.getEStructuralFeature("items");
  
  this.addFeatureCache(items);
  
  	  
  items.getService = (self) -> {
    // get : self -> physicalValue
List<ObjectRepresentation<String>> list = new ArrayList<ObjectRepresentation<String>>();
Folder folder = (Folder)self;
String uri = folder.computeLastURI();

java.io.File fd = new java.io.File(uri);

if(fd.exists()) {
java.io.File[] l = fd.listFiles();
for(java.io.File f : l) {
String type = f.isDirectory() ? "Folder" : "File";
list.add(ObjectRepresentation.id(type,f.getName()));
}
return list;
} else {
return list;
}
  };
  
  
  	  
  items.postService = (self,val) -> {
    // post : (self, logicalValue) -> physicalValue

// A. is moved from another place
// B. move and updateURI
// C. or simply create

Folder folder = (Folder)self;
FileItem curItem = (FileItem)val;

if(curItem.exists()) {
curItem.updateURI();
return ObjectRepresentation.id(curItem.eClass().getName(), curItem.getName());
} else {

if(curItem.move()==false) {
curItem.create();
}
return ObjectRepresentation.id(curItem.eClass().getName(), curItem.getName());
}
  };
  
  
  	  
  items.deleteService = (self,val) -> {
    // delete : (self, physicalValue) -> boolean
// A. is moved to another place
// B. move and updateURI
// C. or simply delete

Folder folder = (Folder)self;
String type = val.typeName;
String value = val.representation;

String oldURI = folder.computeURI() + java.io.File.separator+value;
java.io.File f = new java.io.File(oldURI);

FileItem item = folder.search(oldURI);

if(item==null) {
// 不是移动，或者已经移动过了
if(("Folder".equals(type) && f.isDirectory())||("File".equals(type) && f.isFile())) f.delete();
return true;
} else {
//首先尝试移动，不成功则删除
if(item.move()==false) {
f.delete();
}
return true;
}

  };
  
  
  	  
  items.putService = (self,old,val) -> {
    // put : (self, physicalValue, logicalValue) -> physicalValue
FileItem item = (FileItem)val;
item.move();
return ObjectRepresentation.id(old.typeName, item.getName());
  };
  
} {
  		StateBasedSingleValueFeatureCacheHolder<java.lang.String,java.lang.String> uri = new StateBasedSingleValueFeatureCacheHolder<java.lang.String,java.lang.String>();
  // install conversion service
  
    uri.physicalToLogicalConverter = ConversionService.identity();
    uri.logicalToPhysicalConverter = ConversionService.identity();
    
  uri.feature = eClass.getEStructuralFeature("uri");
  
  this.addFeatureCache(uri);
  
  	  
  uri.getService = (self) -> {
    // get : self -> physicalValue
Folder f = (Folder)self;
return f.computeLastURI();
  };
  
  
  
  
  
  
  
}

}

}