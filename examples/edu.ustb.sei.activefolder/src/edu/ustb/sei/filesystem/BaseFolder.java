/**
 */
package edu.ustb.sei.filesystem;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Base Folder</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.filesystem.BaseFolder#getBaseUri <em>Base Uri</em>}</li>
 *   <li>{@link edu.ustb.sei.filesystem.BaseFolder#getBase <em>Base</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.filesystem.FilesystemPackage#getBaseFolder()
 * @model
 * @generated
 */
public interface BaseFolder extends EObject {
	/**
	 * Returns the value of the '<em><b>Base Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Uri</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Uri</em>' attribute.
	 * @see #setBaseUri(String)
	 * @see edu.ustb.sei.filesystem.FilesystemPackage#getBaseFolder_BaseUri()
	 * @model
	 * @generated
	 */
	String getBaseUri();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.filesystem.BaseFolder#getBaseUri <em>Base Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Uri</em>' attribute.
	 * @see #getBaseUri()
	 * @generated
	 */
	void setBaseUri(String value);

	/**
	 * Returns the value of the '<em><b>Base</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base</em>' containment reference.
	 * @see #setBase(Folder)
	 * @see edu.ustb.sei.filesystem.FilesystemPackage#getBaseFolder_Base()
	 * @model containment="true"
	 * @generated
	 */
	Folder getBase();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.filesystem.BaseFolder#getBase <em>Base</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base</em>' containment reference.
	 * @see #getBase()
	 * @generated
	 */
	void setBase(Folder value);

} // BaseFolder
