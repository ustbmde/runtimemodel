/**
 */
package edu.ustb.sei.filesystem;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.filesystem.FilesystemFactory
 * @model kind="package"
 * @generated
 */
public interface FilesystemPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "filesystem";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.ustb.edu.cn/sei/filesystem";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "fs";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FilesystemPackage eINSTANCE = edu.ustb.sei.filesystem.impl.FilesystemPackageImpl.init();

	/**
	 * The meta object id for the '{@link edu.ustb.sei.filesystem.impl.BaseFolderImpl <em>Base Folder</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.filesystem.impl.BaseFolderImpl
	 * @see edu.ustb.sei.filesystem.impl.FilesystemPackageImpl#getBaseFolder()
	 * @generated
	 */
	int BASE_FOLDER = 0;

	/**
	 * The feature id for the '<em><b>Base Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FOLDER__BASE_URI = 0;

	/**
	 * The feature id for the '<em><b>Base</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FOLDER__BASE = 1;

	/**
	 * The number of structural features of the '<em>Base Folder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FOLDER_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Base Folder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BASE_FOLDER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.filesystem.impl.FileItemImpl <em>File Item</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.filesystem.impl.FileItemImpl
	 * @see edu.ustb.sei.filesystem.impl.FilesystemPackageImpl#getFileItem()
	 * @generated
	 */
	int FILE_ITEM = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_ITEM__NAME = 0;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_ITEM__URI = 1;

	/**
	 * The number of structural features of the '<em>File Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_ITEM_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Compute URI</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_ITEM___COMPUTE_URI = 0;

	/**
	 * The operation id for the '<em>Update URI</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_ITEM___UPDATE_URI = 1;

	/**
	 * The operation id for the '<em>Exists</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_ITEM___EXISTS = 2;

	/**
	 * The operation id for the '<em>Create</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_ITEM___CREATE = 3;

	/**
	 * The operation id for the '<em>Delete</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_ITEM___DELETE = 4;

	/**
	 * The operation id for the '<em>Move</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_ITEM___MOVE = 5;

	/**
	 * The operation id for the '<em>Search</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_ITEM___SEARCH__STRING = 6;

	/**
	 * The operation id for the '<em>Compute Last URI</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_ITEM___COMPUTE_LAST_URI = 7;

	/**
	 * The number of operations of the '<em>File Item</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_ITEM_OPERATION_COUNT = 8;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.filesystem.impl.FileImpl <em>File</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.filesystem.impl.FileImpl
	 * @see edu.ustb.sei.filesystem.impl.FilesystemPackageImpl#getFile()
	 * @generated
	 */
	int FILE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__NAME = FILE_ITEM__NAME;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__URI = FILE_ITEM__URI;

	/**
	 * The number of structural features of the '<em>File</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_FEATURE_COUNT = FILE_ITEM_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Compute URI</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE___COMPUTE_URI = FILE_ITEM___COMPUTE_URI;

	/**
	 * The operation id for the '<em>Update URI</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE___UPDATE_URI = FILE_ITEM___UPDATE_URI;

	/**
	 * The operation id for the '<em>Exists</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE___EXISTS = FILE_ITEM___EXISTS;

	/**
	 * The operation id for the '<em>Create</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE___CREATE = FILE_ITEM___CREATE;

	/**
	 * The operation id for the '<em>Delete</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE___DELETE = FILE_ITEM___DELETE;

	/**
	 * The operation id for the '<em>Move</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE___MOVE = FILE_ITEM___MOVE;

	/**
	 * The operation id for the '<em>Search</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE___SEARCH__STRING = FILE_ITEM___SEARCH__STRING;

	/**
	 * The operation id for the '<em>Compute Last URI</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE___COMPUTE_LAST_URI = FILE_ITEM___COMPUTE_LAST_URI;

	/**
	 * The number of operations of the '<em>File</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_OPERATION_COUNT = FILE_ITEM_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.filesystem.impl.FolderImpl <em>Folder</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.filesystem.impl.FolderImpl
	 * @see edu.ustb.sei.filesystem.impl.FilesystemPackageImpl#getFolder()
	 * @generated
	 */
	int FOLDER = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLDER__NAME = FILE_ITEM__NAME;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLDER__URI = FILE_ITEM__URI;

	/**
	 * The feature id for the '<em><b>Items</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLDER__ITEMS = FILE_ITEM_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Folder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLDER_FEATURE_COUNT = FILE_ITEM_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Compute URI</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLDER___COMPUTE_URI = FILE_ITEM___COMPUTE_URI;

	/**
	 * The operation id for the '<em>Update URI</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLDER___UPDATE_URI = FILE_ITEM___UPDATE_URI;

	/**
	 * The operation id for the '<em>Exists</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLDER___EXISTS = FILE_ITEM___EXISTS;

	/**
	 * The operation id for the '<em>Create</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLDER___CREATE = FILE_ITEM___CREATE;

	/**
	 * The operation id for the '<em>Delete</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLDER___DELETE = FILE_ITEM___DELETE;

	/**
	 * The operation id for the '<em>Move</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLDER___MOVE = FILE_ITEM___MOVE;

	/**
	 * The operation id for the '<em>Search</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLDER___SEARCH__STRING = FILE_ITEM___SEARCH__STRING;

	/**
	 * The operation id for the '<em>Compute Last URI</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLDER___COMPUTE_LAST_URI = FILE_ITEM___COMPUTE_LAST_URI;

	/**
	 * The number of operations of the '<em>Folder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOLDER_OPERATION_COUNT = FILE_ITEM_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.filesystem.BaseFolder <em>Base Folder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Base Folder</em>'.
	 * @see edu.ustb.sei.filesystem.BaseFolder
	 * @generated
	 */
	EClass getBaseFolder();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.filesystem.BaseFolder#getBaseUri <em>Base Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Base Uri</em>'.
	 * @see edu.ustb.sei.filesystem.BaseFolder#getBaseUri()
	 * @see #getBaseFolder()
	 * @generated
	 */
	EAttribute getBaseFolder_BaseUri();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.filesystem.BaseFolder#getBase <em>Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Base</em>'.
	 * @see edu.ustb.sei.filesystem.BaseFolder#getBase()
	 * @see #getBaseFolder()
	 * @generated
	 */
	EReference getBaseFolder_Base();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.filesystem.FileItem <em>File Item</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>File Item</em>'.
	 * @see edu.ustb.sei.filesystem.FileItem
	 * @generated
	 */
	EClass getFileItem();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.filesystem.FileItem#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.ustb.sei.filesystem.FileItem#getName()
	 * @see #getFileItem()
	 * @generated
	 */
	EAttribute getFileItem_Name();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.filesystem.FileItem#getUri <em>Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uri</em>'.
	 * @see edu.ustb.sei.filesystem.FileItem#getUri()
	 * @see #getFileItem()
	 * @generated
	 */
	EAttribute getFileItem_Uri();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.filesystem.FileItem#computeURI() <em>Compute URI</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Compute URI</em>' operation.
	 * @see edu.ustb.sei.filesystem.FileItem#computeURI()
	 * @generated
	 */
	EOperation getFileItem__ComputeURI();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.filesystem.FileItem#updateURI() <em>Update URI</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update URI</em>' operation.
	 * @see edu.ustb.sei.filesystem.FileItem#updateURI()
	 * @generated
	 */
	EOperation getFileItem__UpdateURI();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.filesystem.FileItem#exists() <em>Exists</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Exists</em>' operation.
	 * @see edu.ustb.sei.filesystem.FileItem#exists()
	 * @generated
	 */
	EOperation getFileItem__Exists();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.filesystem.FileItem#create() <em>Create</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create</em>' operation.
	 * @see edu.ustb.sei.filesystem.FileItem#create()
	 * @generated
	 */
	EOperation getFileItem__Create();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.filesystem.FileItem#delete() <em>Delete</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Delete</em>' operation.
	 * @see edu.ustb.sei.filesystem.FileItem#delete()
	 * @generated
	 */
	EOperation getFileItem__Delete();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.filesystem.FileItem#move() <em>Move</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move</em>' operation.
	 * @see edu.ustb.sei.filesystem.FileItem#move()
	 * @generated
	 */
	EOperation getFileItem__Move();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.filesystem.FileItem#search(java.lang.String) <em>Search</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Search</em>' operation.
	 * @see edu.ustb.sei.filesystem.FileItem#search(java.lang.String)
	 * @generated
	 */
	EOperation getFileItem__Search__String();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.filesystem.FileItem#computeLastURI() <em>Compute Last URI</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Compute Last URI</em>' operation.
	 * @see edu.ustb.sei.filesystem.FileItem#computeLastURI()
	 * @generated
	 */
	EOperation getFileItem__ComputeLastURI();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.filesystem.File <em>File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>File</em>'.
	 * @see edu.ustb.sei.filesystem.File
	 * @generated
	 */
	EClass getFile();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.filesystem.Folder <em>Folder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Folder</em>'.
	 * @see edu.ustb.sei.filesystem.Folder
	 * @generated
	 */
	EClass getFolder();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.filesystem.Folder#getItems <em>Items</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Items</em>'.
	 * @see edu.ustb.sei.filesystem.Folder#getItems()
	 * @see #getFolder()
	 * @generated
	 */
	EReference getFolder_Items();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FilesystemFactory getFilesystemFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link edu.ustb.sei.filesystem.impl.BaseFolderImpl <em>Base Folder</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.filesystem.impl.BaseFolderImpl
		 * @see edu.ustb.sei.filesystem.impl.FilesystemPackageImpl#getBaseFolder()
		 * @generated
		 */
		EClass BASE_FOLDER = eINSTANCE.getBaseFolder();

		/**
		 * The meta object literal for the '<em><b>Base Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BASE_FOLDER__BASE_URI = eINSTANCE.getBaseFolder_BaseUri();

		/**
		 * The meta object literal for the '<em><b>Base</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BASE_FOLDER__BASE = eINSTANCE.getBaseFolder_Base();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.filesystem.impl.FileItemImpl <em>File Item</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.filesystem.impl.FileItemImpl
		 * @see edu.ustb.sei.filesystem.impl.FilesystemPackageImpl#getFileItem()
		 * @generated
		 */
		EClass FILE_ITEM = eINSTANCE.getFileItem();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FILE_ITEM__NAME = eINSTANCE.getFileItem_Name();

		/**
		 * The meta object literal for the '<em><b>Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FILE_ITEM__URI = eINSTANCE.getFileItem_Uri();

		/**
		 * The meta object literal for the '<em><b>Compute URI</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FILE_ITEM___COMPUTE_URI = eINSTANCE.getFileItem__ComputeURI();

		/**
		 * The meta object literal for the '<em><b>Update URI</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FILE_ITEM___UPDATE_URI = eINSTANCE.getFileItem__UpdateURI();

		/**
		 * The meta object literal for the '<em><b>Exists</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FILE_ITEM___EXISTS = eINSTANCE.getFileItem__Exists();

		/**
		 * The meta object literal for the '<em><b>Create</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FILE_ITEM___CREATE = eINSTANCE.getFileItem__Create();

		/**
		 * The meta object literal for the '<em><b>Delete</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FILE_ITEM___DELETE = eINSTANCE.getFileItem__Delete();

		/**
		 * The meta object literal for the '<em><b>Move</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FILE_ITEM___MOVE = eINSTANCE.getFileItem__Move();

		/**
		 * The meta object literal for the '<em><b>Search</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FILE_ITEM___SEARCH__STRING = eINSTANCE.getFileItem__Search__String();

		/**
		 * The meta object literal for the '<em><b>Compute Last URI</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation FILE_ITEM___COMPUTE_LAST_URI = eINSTANCE.getFileItem__ComputeLastURI();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.filesystem.impl.FileImpl <em>File</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.filesystem.impl.FileImpl
		 * @see edu.ustb.sei.filesystem.impl.FilesystemPackageImpl#getFile()
		 * @generated
		 */
		EClass FILE = eINSTANCE.getFile();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.filesystem.impl.FolderImpl <em>Folder</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.filesystem.impl.FolderImpl
		 * @see edu.ustb.sei.filesystem.impl.FilesystemPackageImpl#getFolder()
		 * @generated
		 */
		EClass FOLDER = eINSTANCE.getFolder();

		/**
		 * The meta object literal for the '<em><b>Items</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FOLDER__ITEMS = eINSTANCE.getFolder_Items();

	}

} //FilesystemPackage
