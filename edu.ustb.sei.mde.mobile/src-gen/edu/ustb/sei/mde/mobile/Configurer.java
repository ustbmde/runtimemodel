/**
 * generated by Xtext 2.20.0
 */
package edu.ustb.sei.mde.mobile;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.xtext.xbase.XExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Configurer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.Configurer#getInfo <em>Info</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.Configurer#getBody <em>Body</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mobile.MobilePackage#getConfigurer()
 * @model
 * @generated
 */
public interface Configurer extends EObject
{
  /**
   * Returns the value of the '<em><b>Info</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Info</em>' attribute.
   * @see #setInfo(String)
   * @see edu.ustb.sei.mde.mobile.MobilePackage#getConfigurer_Info()
   * @model
   * @generated
   */
  String getInfo();

  /**
   * Sets the value of the '{@link edu.ustb.sei.mde.mobile.Configurer#getInfo <em>Info</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Info</em>' attribute.
   * @see #getInfo()
   * @generated
   */
  void setInfo(String value);

  /**
   * Returns the value of the '<em><b>Body</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Body</em>' containment reference.
   * @see #setBody(XExpression)
   * @see edu.ustb.sei.mde.mobile.MobilePackage#getConfigurer_Body()
   * @model containment="true"
   * @generated
   */
  XExpression getBody();

  /**
   * Sets the value of the '{@link edu.ustb.sei.mde.mobile.Configurer#getBody <em>Body</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Body</em>' containment reference.
   * @see #getBody()
   * @generated
   */
  void setBody(XExpression value);

} // Configurer
