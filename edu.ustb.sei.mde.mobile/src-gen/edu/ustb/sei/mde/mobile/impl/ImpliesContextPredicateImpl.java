/**
 * generated by Xtext 2.20.0
 */
package edu.ustb.sei.mde.mobile.impl;

import edu.ustb.sei.mde.mobile.ContextPredicate;
import edu.ustb.sei.mde.mobile.ImpliesContextPredicate;
import edu.ustb.sei.mde.mobile.MobilePackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Implies Context Predicate</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.impl.ImpliesContextPredicateImpl#getLeft <em>Left</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.impl.ImpliesContextPredicateImpl#getRight <em>Right</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ImpliesContextPredicateImpl extends ContextPredicateImpl implements ImpliesContextPredicate
{
  /**
   * The cached value of the '{@link #getLeft() <em>Left</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLeft()
   * @generated
   * @ordered
   */
  protected ContextPredicate left;

  /**
   * The cached value of the '{@link #getRight() <em>Right</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRight()
   * @generated
   * @ordered
   */
  protected ContextPredicate right;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ImpliesContextPredicateImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MobilePackage.Literals.IMPLIES_CONTEXT_PREDICATE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ContextPredicate getLeft()
  {
    return left;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLeft(ContextPredicate newLeft, NotificationChain msgs)
  {
    ContextPredicate oldLeft = left;
    left = newLeft;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MobilePackage.IMPLIES_CONTEXT_PREDICATE__LEFT, oldLeft, newLeft);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setLeft(ContextPredicate newLeft)
  {
    if (newLeft != left)
    {
      NotificationChain msgs = null;
      if (left != null)
        msgs = ((InternalEObject)left).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MobilePackage.IMPLIES_CONTEXT_PREDICATE__LEFT, null, msgs);
      if (newLeft != null)
        msgs = ((InternalEObject)newLeft).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MobilePackage.IMPLIES_CONTEXT_PREDICATE__LEFT, null, msgs);
      msgs = basicSetLeft(newLeft, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MobilePackage.IMPLIES_CONTEXT_PREDICATE__LEFT, newLeft, newLeft));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ContextPredicate getRight()
  {
    return right;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRight(ContextPredicate newRight, NotificationChain msgs)
  {
    ContextPredicate oldRight = right;
    right = newRight;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MobilePackage.IMPLIES_CONTEXT_PREDICATE__RIGHT, oldRight, newRight);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setRight(ContextPredicate newRight)
  {
    if (newRight != right)
    {
      NotificationChain msgs = null;
      if (right != null)
        msgs = ((InternalEObject)right).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MobilePackage.IMPLIES_CONTEXT_PREDICATE__RIGHT, null, msgs);
      if (newRight != null)
        msgs = ((InternalEObject)newRight).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MobilePackage.IMPLIES_CONTEXT_PREDICATE__RIGHT, null, msgs);
      msgs = basicSetRight(newRight, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MobilePackage.IMPLIES_CONTEXT_PREDICATE__RIGHT, newRight, newRight));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MobilePackage.IMPLIES_CONTEXT_PREDICATE__LEFT:
        return basicSetLeft(null, msgs);
      case MobilePackage.IMPLIES_CONTEXT_PREDICATE__RIGHT:
        return basicSetRight(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MobilePackage.IMPLIES_CONTEXT_PREDICATE__LEFT:
        return getLeft();
      case MobilePackage.IMPLIES_CONTEXT_PREDICATE__RIGHT:
        return getRight();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MobilePackage.IMPLIES_CONTEXT_PREDICATE__LEFT:
        setLeft((ContextPredicate)newValue);
        return;
      case MobilePackage.IMPLIES_CONTEXT_PREDICATE__RIGHT:
        setRight((ContextPredicate)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MobilePackage.IMPLIES_CONTEXT_PREDICATE__LEFT:
        setLeft((ContextPredicate)null);
        return;
      case MobilePackage.IMPLIES_CONTEXT_PREDICATE__RIGHT:
        setRight((ContextPredicate)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MobilePackage.IMPLIES_CONTEXT_PREDICATE__LEFT:
        return left != null;
      case MobilePackage.IMPLIES_CONTEXT_PREDICATE__RIGHT:
        return right != null;
    }
    return super.eIsSet(featureID);
  }

} //ImpliesContextPredicateImpl
