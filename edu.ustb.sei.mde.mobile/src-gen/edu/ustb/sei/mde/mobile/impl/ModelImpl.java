/**
 * generated by Xtext 2.20.0
 */
package edu.ustb.sei.mde.mobile.impl;

import edu.ustb.sei.mde.mobile.AdapterModel;
import edu.ustb.sei.mde.mobile.Helper;
import edu.ustb.sei.mde.mobile.MobilePackage;
import edu.ustb.sei.mde.mobile.Model;
import edu.ustb.sei.mde.mobile.Option;
import edu.ustb.sei.mde.mobile.TypeMapping;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.xtext.xtype.XImportSection;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.impl.ModelImpl#getName <em>Name</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.impl.ModelImpl#getImportSection <em>Import Section</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.impl.ModelImpl#getAdapter <em>Adapter</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.impl.ModelImpl#getOption <em>Option</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.impl.ModelImpl#getHelpers <em>Helpers</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.impl.ModelImpl#getTypeMappings <em>Type Mappings</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModelImpl extends MinimalEObjectImpl.Container implements Model
{
  /**
   * The default value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected static final String NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getName()
   * @generated
   * @ordered
   */
  protected String name = NAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getImportSection() <em>Import Section</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getImportSection()
   * @generated
   * @ordered
   */
  protected XImportSection importSection;

  /**
   * The cached value of the '{@link #getAdapter() <em>Adapter</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAdapter()
   * @generated
   * @ordered
   */
  protected AdapterModel adapter;

  /**
   * The cached value of the '{@link #getOption() <em>Option</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOption()
   * @generated
   * @ordered
   */
  protected Option option;

  /**
   * The cached value of the '{@link #getHelpers() <em>Helpers</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHelpers()
   * @generated
   * @ordered
   */
  protected EList<Helper> helpers;

  /**
   * The cached value of the '{@link #getTypeMappings() <em>Type Mappings</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getTypeMappings()
   * @generated
   * @ordered
   */
  protected EList<TypeMapping> typeMappings;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ModelImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MobilePackage.Literals.MODEL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getName()
  {
    return name;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setName(String newName)
  {
    String oldName = name;
    name = newName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MobilePackage.MODEL__NAME, oldName, name));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public XImportSection getImportSection()
  {
    return importSection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetImportSection(XImportSection newImportSection, NotificationChain msgs)
  {
    XImportSection oldImportSection = importSection;
    importSection = newImportSection;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MobilePackage.MODEL__IMPORT_SECTION, oldImportSection, newImportSection);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setImportSection(XImportSection newImportSection)
  {
    if (newImportSection != importSection)
    {
      NotificationChain msgs = null;
      if (importSection != null)
        msgs = ((InternalEObject)importSection).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MobilePackage.MODEL__IMPORT_SECTION, null, msgs);
      if (newImportSection != null)
        msgs = ((InternalEObject)newImportSection).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MobilePackage.MODEL__IMPORT_SECTION, null, msgs);
      msgs = basicSetImportSection(newImportSection, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MobilePackage.MODEL__IMPORT_SECTION, newImportSection, newImportSection));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public AdapterModel getAdapter()
  {
    return adapter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAdapter(AdapterModel newAdapter, NotificationChain msgs)
  {
    AdapterModel oldAdapter = adapter;
    adapter = newAdapter;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MobilePackage.MODEL__ADAPTER, oldAdapter, newAdapter);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setAdapter(AdapterModel newAdapter)
  {
    if (newAdapter != adapter)
    {
      NotificationChain msgs = null;
      if (adapter != null)
        msgs = ((InternalEObject)adapter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MobilePackage.MODEL__ADAPTER, null, msgs);
      if (newAdapter != null)
        msgs = ((InternalEObject)newAdapter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MobilePackage.MODEL__ADAPTER, null, msgs);
      msgs = basicSetAdapter(newAdapter, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MobilePackage.MODEL__ADAPTER, newAdapter, newAdapter));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Option getOption()
  {
    return option;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetOption(Option newOption, NotificationChain msgs)
  {
    Option oldOption = option;
    option = newOption;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MobilePackage.MODEL__OPTION, oldOption, newOption);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setOption(Option newOption)
  {
    if (newOption != option)
    {
      NotificationChain msgs = null;
      if (option != null)
        msgs = ((InternalEObject)option).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MobilePackage.MODEL__OPTION, null, msgs);
      if (newOption != null)
        msgs = ((InternalEObject)newOption).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MobilePackage.MODEL__OPTION, null, msgs);
      msgs = basicSetOption(newOption, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MobilePackage.MODEL__OPTION, newOption, newOption));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<Helper> getHelpers()
  {
    if (helpers == null)
    {
      helpers = new EObjectContainmentEList<Helper>(Helper.class, this, MobilePackage.MODEL__HELPERS);
    }
    return helpers;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<TypeMapping> getTypeMappings()
  {
    if (typeMappings == null)
    {
      typeMappings = new EObjectContainmentEList<TypeMapping>(TypeMapping.class, this, MobilePackage.MODEL__TYPE_MAPPINGS);
    }
    return typeMappings;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MobilePackage.MODEL__IMPORT_SECTION:
        return basicSetImportSection(null, msgs);
      case MobilePackage.MODEL__ADAPTER:
        return basicSetAdapter(null, msgs);
      case MobilePackage.MODEL__OPTION:
        return basicSetOption(null, msgs);
      case MobilePackage.MODEL__HELPERS:
        return ((InternalEList<?>)getHelpers()).basicRemove(otherEnd, msgs);
      case MobilePackage.MODEL__TYPE_MAPPINGS:
        return ((InternalEList<?>)getTypeMappings()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MobilePackage.MODEL__NAME:
        return getName();
      case MobilePackage.MODEL__IMPORT_SECTION:
        return getImportSection();
      case MobilePackage.MODEL__ADAPTER:
        return getAdapter();
      case MobilePackage.MODEL__OPTION:
        return getOption();
      case MobilePackage.MODEL__HELPERS:
        return getHelpers();
      case MobilePackage.MODEL__TYPE_MAPPINGS:
        return getTypeMappings();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MobilePackage.MODEL__NAME:
        setName((String)newValue);
        return;
      case MobilePackage.MODEL__IMPORT_SECTION:
        setImportSection((XImportSection)newValue);
        return;
      case MobilePackage.MODEL__ADAPTER:
        setAdapter((AdapterModel)newValue);
        return;
      case MobilePackage.MODEL__OPTION:
        setOption((Option)newValue);
        return;
      case MobilePackage.MODEL__HELPERS:
        getHelpers().clear();
        getHelpers().addAll((Collection<? extends Helper>)newValue);
        return;
      case MobilePackage.MODEL__TYPE_MAPPINGS:
        getTypeMappings().clear();
        getTypeMappings().addAll((Collection<? extends TypeMapping>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MobilePackage.MODEL__NAME:
        setName(NAME_EDEFAULT);
        return;
      case MobilePackage.MODEL__IMPORT_SECTION:
        setImportSection((XImportSection)null);
        return;
      case MobilePackage.MODEL__ADAPTER:
        setAdapter((AdapterModel)null);
        return;
      case MobilePackage.MODEL__OPTION:
        setOption((Option)null);
        return;
      case MobilePackage.MODEL__HELPERS:
        getHelpers().clear();
        return;
      case MobilePackage.MODEL__TYPE_MAPPINGS:
        getTypeMappings().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MobilePackage.MODEL__NAME:
        return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
      case MobilePackage.MODEL__IMPORT_SECTION:
        return importSection != null;
      case MobilePackage.MODEL__ADAPTER:
        return adapter != null;
      case MobilePackage.MODEL__OPTION:
        return option != null;
      case MobilePackage.MODEL__HELPERS:
        return helpers != null && !helpers.isEmpty();
      case MobilePackage.MODEL__TYPE_MAPPINGS:
        return typeMappings != null && !typeMappings.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (name: ");
    result.append(name);
    result.append(')');
    return result.toString();
  }

} //ModelImpl
