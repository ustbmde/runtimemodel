/**
 * generated by Xtext 2.20.0
 */
package edu.ustb.sei.mde.mobile.impl;

import edu.ustb.sei.mde.mobile.InformationPair;
import edu.ustb.sei.mde.mobile.MobilePackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Information Pair</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.impl.InformationPairImpl#getVar <em>Var</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.impl.InformationPairImpl#getInfoKey <em>Info Key</em>}</li>
 * </ul>
 *
 * @generated
 */
public class InformationPairImpl extends MinimalEObjectImpl.Container implements InformationPair
{
  /**
   * The default value of the '{@link #getVar() <em>Var</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVar()
   * @generated
   * @ordered
   */
  protected static final String VAR_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getVar() <em>Var</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVar()
   * @generated
   * @ordered
   */
  protected String var = VAR_EDEFAULT;

  /**
   * The default value of the '{@link #getInfoKey() <em>Info Key</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInfoKey()
   * @generated
   * @ordered
   */
  protected static final String INFO_KEY_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getInfoKey() <em>Info Key</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInfoKey()
   * @generated
   * @ordered
   */
  protected String infoKey = INFO_KEY_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected InformationPairImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MobilePackage.Literals.INFORMATION_PAIR;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getVar()
  {
    return var;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setVar(String newVar)
  {
    String oldVar = var;
    var = newVar;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MobilePackage.INFORMATION_PAIR__VAR, oldVar, var));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getInfoKey()
  {
    return infoKey;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setInfoKey(String newInfoKey)
  {
    String oldInfoKey = infoKey;
    infoKey = newInfoKey;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MobilePackage.INFORMATION_PAIR__INFO_KEY, oldInfoKey, infoKey));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MobilePackage.INFORMATION_PAIR__VAR:
        return getVar();
      case MobilePackage.INFORMATION_PAIR__INFO_KEY:
        return getInfoKey();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MobilePackage.INFORMATION_PAIR__VAR:
        setVar((String)newValue);
        return;
      case MobilePackage.INFORMATION_PAIR__INFO_KEY:
        setInfoKey((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MobilePackage.INFORMATION_PAIR__VAR:
        setVar(VAR_EDEFAULT);
        return;
      case MobilePackage.INFORMATION_PAIR__INFO_KEY:
        setInfoKey(INFO_KEY_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MobilePackage.INFORMATION_PAIR__VAR:
        return VAR_EDEFAULT == null ? var != null : !VAR_EDEFAULT.equals(var);
      case MobilePackage.INFORMATION_PAIR__INFO_KEY:
        return INFO_KEY_EDEFAULT == null ? infoKey != null : !INFO_KEY_EDEFAULT.equals(infoKey);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (var: ");
    result.append(var);
    result.append(", infoKey: ");
    result.append(infoKey);
    result.append(')');
    return result.toString();
  }

} //InformationPairImpl
