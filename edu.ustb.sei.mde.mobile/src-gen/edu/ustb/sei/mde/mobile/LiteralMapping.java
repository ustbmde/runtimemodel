/**
 * generated by Xtext 2.20.0
 */
package edu.ustb.sei.mde.mobile;

import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.xtext.xbase.XExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Literal Mapping</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.LiteralMapping#getEcoreLiteral <em>Ecore Literal</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.LiteralMapping#getJavaLiteral <em>Java Literal</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mobile.MobilePackage#getLiteralMapping()
 * @model
 * @generated
 */
public interface LiteralMapping extends EObject
{
  /**
   * Returns the value of the '<em><b>Ecore Literal</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ecore Literal</em>' reference.
   * @see #setEcoreLiteral(EEnumLiteral)
   * @see edu.ustb.sei.mde.mobile.MobilePackage#getLiteralMapping_EcoreLiteral()
   * @model
   * @generated
   */
  EEnumLiteral getEcoreLiteral();

  /**
   * Sets the value of the '{@link edu.ustb.sei.mde.mobile.LiteralMapping#getEcoreLiteral <em>Ecore Literal</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ecore Literal</em>' reference.
   * @see #getEcoreLiteral()
   * @generated
   */
  void setEcoreLiteral(EEnumLiteral value);

  /**
   * Returns the value of the '<em><b>Java Literal</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Java Literal</em>' containment reference.
   * @see #setJavaLiteral(XExpression)
   * @see edu.ustb.sei.mde.mobile.MobilePackage#getLiteralMapping_JavaLiteral()
   * @model containment="true"
   * @generated
   */
  XExpression getJavaLiteral();

  /**
   * Sets the value of the '{@link edu.ustb.sei.mde.mobile.LiteralMapping#getJavaLiteral <em>Java Literal</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Java Literal</em>' containment reference.
   * @see #getJavaLiteral()
   * @generated
   */
  void setJavaLiteral(XExpression value);

} // LiteralMapping
