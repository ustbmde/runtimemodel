/**
 * generated by Xtext 2.20.0
 */
package edu.ustb.sei.mde.mobile;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Adapter Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.AdapterModel#getMetamodel <em>Metamodel</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.AdapterModel#getBasePackage <em>Base Package</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mobile.MobilePackage#getAdapterModel()
 * @model
 * @generated
 */
public interface AdapterModel extends EObject
{
  /**
   * Returns the value of the '<em><b>Metamodel</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Metamodel</em>' reference.
   * @see #setMetamodel(EPackage)
   * @see edu.ustb.sei.mde.mobile.MobilePackage#getAdapterModel_Metamodel()
   * @model
   * @generated
   */
  EPackage getMetamodel();

  /**
   * Sets the value of the '{@link edu.ustb.sei.mde.mobile.AdapterModel#getMetamodel <em>Metamodel</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Metamodel</em>' reference.
   * @see #getMetamodel()
   * @generated
   */
  void setMetamodel(EPackage value);

  /**
   * Returns the value of the '<em><b>Base Package</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Base Package</em>' attribute.
   * @see #setBasePackage(String)
   * @see edu.ustb.sei.mde.mobile.MobilePackage#getAdapterModel_BasePackage()
   * @model
   * @generated
   */
  String getBasePackage();

  /**
   * Sets the value of the '{@link edu.ustb.sei.mde.mobile.AdapterModel#getBasePackage <em>Base Package</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Base Package</em>' attribute.
   * @see #getBasePackage()
   * @generated
   */
  void setBasePackage(String value);

} // AdapterModel
