package edu.ustb.sei.mde.scoping;

import edu.ustb.sei.mde.mobile.AdapterModel;
import edu.ustb.sei.mde.mobile.MobilePackage;
import java.util.Collections;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.linking.impl.DefaultLinkingService;
import org.eclipse.xtext.linking.impl.IllegalNodeException;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.xbase.lib.Exceptions;

@SuppressWarnings("all")
public class MobileLinkingService extends DefaultLinkingService {
  @Override
  public List<EObject> getLinkedObjects(final EObject context, final EReference ref, final INode node) throws IllegalNodeException {
    List<EObject> _xblockexpression = null;
    {
      if (((context instanceof AdapterModel) && (ref == MobilePackage.eINSTANCE.getAdapterModel_Metamodel()))) {
        try {
          final String uri = this.getCrossRefNodeAsString(node);
          final IScope scope = this.getScope(context, ref);
          Iterable<IEObjectDescription> _allElements = scope.getAllElements();
          for (final IEObjectDescription d : _allElements) {
            boolean _equals = d.getQualifiedName().getFirstSegment().equals(uri);
            if (_equals) {
              return Collections.<EObject>singletonList(d.getEObjectOrProxy());
            }
          }
          final EPackage p = EcoreUtil2.loadEPackage(uri, this.getClass().getClassLoader());
          if ((p != null)) {
            return Collections.<EObject>singletonList(p);
          }
        } catch (final Throwable _t) {
          if (_t instanceof Exception) {
            return Collections.<EObject>emptyList();
          } else {
            throw Exceptions.sneakyThrow(_t);
          }
        }
      }
      _xblockexpression = super.getLinkedObjects(context, ref, node);
    }
    return _xblockexpression;
  }
}
