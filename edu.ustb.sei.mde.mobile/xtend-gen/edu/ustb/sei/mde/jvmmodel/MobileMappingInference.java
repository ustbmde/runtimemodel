package edu.ustb.sei.mde.jvmmodel;

import edu.ustb.sei.mde.mobile.Model;
import java.util.List;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.xtext.common.types.JvmDeclaredType;
import org.eclipse.xtext.common.types.JvmFeature;
import org.eclipse.xtext.common.types.JvmField;
import org.eclipse.xtext.common.types.JvmGenericArrayTypeReference;
import org.eclipse.xtext.common.types.JvmOperation;
import org.eclipse.xtext.common.types.JvmParameterizedTypeReference;
import org.eclipse.xtext.common.types.JvmType;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.eclipse.xtext.common.types.JvmVisibility;
import org.eclipse.xtext.xbase.lib.Exceptions;

@SuppressWarnings("all")
public class MobileMappingInference {
  public String inferGetterCode(final JvmType type, final String owner, final EStructuralFeature feature, final Model model) {
    try {
      if ((type instanceof JvmDeclaredType)) {
        final JvmDeclaredType declType = ((JvmDeclaredType) type);
        Iterable<JvmFeature> _allFeatures = declType.getAllFeatures();
        for (final JvmFeature jf : _allFeatures) {
          if ((jf instanceof JvmField)) {
            if ((((((JvmField)jf).getVisibility() == JvmVisibility.PUBLIC) && (Boolean.valueOf(((JvmField)jf).isStatic()) == Boolean.valueOf(false))) && ((JvmField) jf).getSimpleName().equals(feature.getName()))) {
              final JvmTypeReference jfType = ((JvmField)jf).getType();
              if ((feature instanceof EAttribute)) {
                String _xifexpression = null;
                Class<?> _instanceClass = ((EAttribute) feature).getEAttributeType().getInstanceClass();
                boolean _tripleNotEquals = (_instanceClass != null);
                if (_tripleNotEquals) {
                  _xifexpression = ((EAttribute) feature).getEAttributeType().getInstanceClass().getCanonicalName();
                } else {
                  _xifexpression = null;
                }
                final String ectName = _xifexpression;
                boolean _isMany = ((EAttribute)feature).isMany();
                if (_isMany) {
                  if ((jfType instanceof JvmGenericArrayTypeReference)) {
                    final String jctName = ((JvmGenericArrayTypeReference) jfType).getComponentType().getQualifiedName();
                    boolean _equals = jctName.equals(ectName);
                    if (_equals) {
                      String _simpleName = ((JvmField) jf).getSimpleName();
                      String _plus = ((("java.util.Arrays.asList(" + owner) + ".") + _simpleName);
                      return (_plus + ")");
                    }
                  } else {
                    if ((jfType instanceof JvmParameterizedTypeReference)) {
                      int _size = ((JvmParameterizedTypeReference) jfType).getArguments().size();
                      boolean _tripleEquals = (_size == 1);
                      if (_tripleEquals) {
                        final String containerName = ((JvmParameterizedTypeReference) jfType).getQualifiedName();
                        final Class<?> cls = this.getClass().getClassLoader().loadClass(containerName);
                        if (((cls != null) && List.class.isAssignableFrom(cls))) {
                          String _simpleName_1 = ((JvmField) jf).getSimpleName();
                          return ((owner + ".") + _simpleName_1);
                        }
                      }
                    }
                  }
                } else {
                  final String jTypeName = jfType.getQualifiedName();
                  boolean _equals_1 = jTypeName.equals(ectName);
                  if (_equals_1) {
                    String _simpleName_2 = ((JvmField) jf).getSimpleName();
                    return ((owner + ".") + _simpleName_2);
                  }
                }
              } else {
              }
              String _simpleName_3 = ((JvmField) jf).getSimpleName();
              String _plus_1 = ((owner + "./*") + _simpleName_3);
              return (_plus_1 + "*/");
            }
          } else {
            if ((jf instanceof JvmOperation)) {
            }
          }
        }
        return null;
      } else {
        return null;
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
