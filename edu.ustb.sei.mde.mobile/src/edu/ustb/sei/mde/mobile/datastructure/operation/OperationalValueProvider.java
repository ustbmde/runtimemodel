package edu.ustb.sei.mde.mobile.datastructure.operation;

import java.util.ArrayList;
import java.util.List;

public class OperationalValueProvider<S, V, SV> extends ValueProvider<S, V, SV> {
	
	public OperationalValueProvider(ValueProvider<S, V, SV> host,
			ExternalOperation<S, V, SV> externalOperation) {
		super();
		this.host = host;
		this.externalOperations = new ArrayList<ExternalOperation<S,V,SV>>();
		this.externalOperations.add(externalOperation);
	}

	protected ValueProvider<S, V, SV> host;
	protected List<ExternalOperation<S, V, SV>> externalOperations;
	
	public void performExternalOperations(S s, V v) {
		SV value = getValue(s,v);
		this.externalOperations.forEach(o->o.execute(s, v, value));
	}
	
	public void appendExternalOperation(ExternalOperation<S, V, SV> op) {
		this.externalOperations.add(op);
	}

	public OperationalValueProvider() {
		super();
		this.externalOperations = new ArrayList<ExternalOperation<S,V,SV>>();
	}

	@Override
	public boolean isValueReady() {
		return host.isValueReady();
	}

	@Override
	public SV getValue(S s, V v) {
		return host.getValue(s,v);
	}

	@Override
	public ValueProvider<S, V, SV> getCore() {
		return host.getCore();
	}
	
	@Override
	public boolean hasExternalOperations() {
		return !this.externalOperations.isEmpty();
	}

}
