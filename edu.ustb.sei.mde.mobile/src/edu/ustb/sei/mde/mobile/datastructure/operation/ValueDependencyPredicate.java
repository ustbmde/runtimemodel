package edu.ustb.sei.mde.mobile.datastructure.operation;

import java.util.Map;

public class ValueDependencyPredicate implements ContextPredicate {
	public ValueDependencyPredicate(OperationScheduler scheduler, String leftKey, String rightKey,
			boolean leftDependsOnRight) {
		super();
		this.scheduler = scheduler;
		this.leftKey = leftKey;
		this.rightKey = rightKey;
		this.leftDependsOnRight = leftDependsOnRight;
	}

	protected OperationScheduler scheduler;
	protected String leftKey;
	protected String rightKey;
	protected boolean leftDependsOnRight;

	@Override
	public boolean check(Map<String, Object> context) {
		ValueProvider<?, ?, ?> leftVar = (ValueProvider<?, ?, ?>) context.get(leftKey);
		ValueProvider<?, ?, ?> rightVar = (ValueProvider<?, ?, ?>) context.get(rightKey);
		
		if(leftDependsOnRight) return scheduler.getValueDependencies().isDependentOn(leftVar, rightVar);
		else return scheduler.getValueDependencies().isDependentOn(rightVar, leftVar);
	}
	
}