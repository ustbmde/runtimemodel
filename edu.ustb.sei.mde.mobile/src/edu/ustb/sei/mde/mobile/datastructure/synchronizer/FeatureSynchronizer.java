package edu.ustb.sei.mde.mobile.datastructure.synchronizer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;

import edu.ustb.sei.mde.mobile.datastructure.Context;
import edu.ustb.sei.mde.mobile.datastructure.TraceLink;
import edu.ustb.sei.mde.mobile.datastructure.TraceSystem;
import edu.ustb.sei.mde.mobile.datastructure.operation.Information;
import edu.ustb.sei.mde.mobile.datastructure.operation.ValueProvider;

@SuppressWarnings("unused")
public abstract class FeatureSynchronizer<ST, SFT, SFET, VT extends EObject, VFT, VFET,C extends Context> implements Synchronizer<SFT, VFT, C>{
	protected EStructuralFeature feature;
	protected FeatureKind kind;
	protected boolean constructorParameter;
	
	protected FeatureSynchronizer(EStructuralFeature feature) {
		makeNormal();
		this.constructorParameter = false;
		this.feature = feature;
	}
	
	enum FeatureKind {
		key,
		keyFactor,
		normal
	}
	
	public void setValueSynchronizer(Synchronizer<SFET, VFET,C> valueSynchronizer) {
		this.valueSynchronizer = valueSynchronizer;
	}
	
	
	public Synchronizer<SFET, VFET, C> getValueSynchronizer() {
		return valueSynchronizer;
	}


	protected ElementSynchronizer<ST,VT,C> container;
	
	public abstract VFT get(SFT source, C context);
	public abstract SFT put(SFT source, VFT view, C context);
	
	@SuppressWarnings("unchecked")
	final protected void link(ST sourceContainer, VT viewContainer, C context) {
		if(getFeature() instanceof EReference) {
			EReference reference = (EReference) getFeature();
			
			if(reference.isContainment()) {
				if(reference.isMany()) {
					ElementSynchronizer<SFET,EObject,C> syn = (ElementSynchronizer<SFET,EObject,C>) this.valueSynchronizer;
					List<SFET> svalues = (List<SFET>) this.externalGet(sourceContainer, context);
					List<VFET> vvalues = new ArrayList<>((List<VFET>) viewContainer.eGet(getFeature()));
					
					svalues.forEach(s->{
						int i = 0;
						for(VFET v : vvalues) {
							if(syn.align(s, (EObject) v, context)!=Boolean.FALSE) {
								syn.link(s, (EObject) v, context);
								break;
							}
							i++;
						}
						if(i!=vvalues.size()) vvalues.remove(i);
					});
				} else {
					ElementSynchronizer<SFT,EObject,C> syn = (ElementSynchronizer<SFT,EObject,C>) this.valueSynchronizer;
					SFT s = (SFT) this.externalGet(sourceContainer, context);
					VFT v = (VFT) viewContainer.eGet(getFeature());
					if(syn.align(s, (EObject) v, context)!=Boolean.FALSE) {
						syn.link(s, (EObject) v, context);
					}
				}
			}
		}
	}
	
	@Override
	abstract public <SO, VO> ValueProvider<SO, VO, SFT> put(ValueProvider<SO, VO, SFT> source, VFT view, C context);

	public abstract SFT externalGet(ST source, C context);
	// write though operations
	public abstract void externalSet(ST source, SFT value, VT view, VFT viewValue, C context);
	
	protected Synchronizer<SFET, VFET,C> valueSynchronizer;
	
	public void configureInformation(Information info) {
		
	}
	public boolean isKey() {
		return kind==FeatureKind.key;
	}
	
	
	public void makeKey() {
		kind = FeatureKind.key;
	}
	
	public void makeKeyFactor() {
		kind = FeatureKind.keyFactor;
	}
	
	public void makeNormal() {
		kind = FeatureKind.normal;
	}
	public boolean isConstructorParameter() {
		return constructorParameter;
	}
	public void setConstructorParameter(boolean constructorParameter) {
		this.constructorParameter = constructorParameter;
	}
	public EStructuralFeature getFeature() {
		return feature;
	}
	public void setFeature(EStructuralFeature feature) {
		this.feature = feature;
	}
	
	public boolean isContainment() {
		return false;
	}
	
	@SuppressWarnings("unchecked")
	public VFT dynamicCompute(VT viewObject, Context context) {
		return (VFT) viewObject.eGet(this.feature);
	}
}
