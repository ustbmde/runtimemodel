package edu.ustb.sei.mde.mobile.datastructure.synchronizer;


import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import edu.ustb.sei.mde.mobile.datastructure.Context;

public abstract class ReferenceSynchronizer<ST, SFT, SFET, VT extends EObject, VFT, VFET, C extends Context>
		extends FeatureSynchronizer<ST, SFT, SFET, VT, VFT, VFET, C> {

	public ReferenceSynchronizer(EReference feature) {
		super(feature);
	}

	@Override
	public boolean isContainment() {
		return ((EReference) this.feature).isContainment();
	}
	
	public EClass getEcoreType() {
		return ((EReference) this.feature).getEReferenceType();
	}
	
	public Class<?> getJavaType() {
		return ((ElementSynchronizer<?, ?, ?>) this.valueSynchronizer).javaClass;
	}
	
	public abstract void externalReplace(ST source, SFET oldValue, SFET newValue, VT view, VFET viewValue, C context);
	public abstract void externalInsert(ST source, SFET value, VT view, VFET viewValue, C context);
	public abstract void externalRemove(ST source, SFET value, VT view, C context);
	
	
//	public void externalPostMoveOut(ST source, SFET sv, VT view, VFET vv, EObject newContainer, C context) {
//		
//	}
	
	public void externalMoveOut(ST source, SFET sourceValue, VT view, VFET viewValue, EObject newContainer, C context) {
		externalRemove(source, sourceValue, view, context);
	}
	
	
//	public void externalPreMoveIn(ST source, SFET sv, Object oldSource, VT view, VFET vv, C context) {}
	public void externalMoveIn(ST source, SFET sourceValue, Object oldSourceValue, VT view, VFET viewValue, C context) {
		externalInsert(source, sourceValue, view, viewValue, context);
	}
}
