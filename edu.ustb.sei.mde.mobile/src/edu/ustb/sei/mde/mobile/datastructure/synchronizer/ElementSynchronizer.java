package edu.ustb.sei.mde.mobile.datastructure.synchronizer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import edu.ustb.sei.mde.mobile.datastructure.Context;
import edu.ustb.sei.mde.mobile.datastructure.Pair;
import edu.ustb.sei.mde.mobile.datastructure.TraceLink;
import edu.ustb.sei.mde.mobile.datastructure.TraceSystem;
import edu.ustb.sei.mde.mobile.datastructure.Wrapper;
import edu.ustb.sei.mde.mobile.datastructure.operation.ValueProvider;

public abstract class ElementSynchronizer<ST, VT extends EObject, C extends Context> implements Synchronizer<ST, VT, C>{
	protected ElementSynchronizer(Class<?> javaClass, EClass ecoreClass) {
		super();
		this.ecoreClass = ecoreClass;
		this.javaClass = javaClass;
	}
	
	protected boolean sourceFilter(Object self) {
		return true;
	}
	protected ModelSynchronizer<?, ?, C> container;
	
	protected abstract ValueProvider<?, ?, ST> createSource(ST source, Wrapper wrapper, C context);
	protected abstract VT createView(C context);
	protected EClass ecoreClass;
	protected Class<?> javaClass;
	
	public boolean isEqual(ST oa, ST ob) {
		return oa==ob || (oa!=null && oa.equals(ob));
	}
	
	protected Wrapper createWrapper(ValueProvider<?, ?, ST> source, C context) {
		Wrapper wrapper = new Wrapper();
		if(source!=null && source.isValueReady() && source.getValue()!=null) {
			ST so = source.getValue(null,null);
			this.features.forEach(f->{
				Object sv = f.externalGet(so, context);
				wrapper.put(f.getFeature().getName(), ValueProvider.value(sv));
			});
		} else {
			this.features.forEach(f->{
				if(f.getFeature().isMany()) {
					wrapper.put(f.getFeature().getName(), ValueProvider.value(new ArrayList<>()));
				} else {
					wrapper.put(f.getFeature().getName(), ValueProvider.nullValue());
				}
				
			});
		}
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public Boolean align(Object source, EObject view, C context) {
		if((source==null || this.canHandle(source)) && this.canHandle(view))
			return internalAlign((ST) source, (VT) view, context);
		else 
			return false;
	}
	
	public Boolean internalAlign(ST source, VT view, C context) {
		return null;
	}
	
	protected List<FeatureSynchronizer<ST,?,?,VT,?,?,C>> features = new ArrayList<>();
	
//	@SuppressWarnings("unchecked")
//	public <SFT,SFET,VFT,VFET> FeatureSynchronizer<ST, SFT, SFET, VT, VFT, VFET,C> getFeature(String name) {
//		for(FeatureSynchronizer<ST,?,?,VT,?,?,C> f : features) {
//			if(f.getFeature().getName().equals(name))
//				return (FeatureSynchronizer<ST, SFT, SFET, VT, VFT, VFET, C>) f;
//		}
//		return null;
//	}
	
	@SuppressWarnings("unchecked")
	public <SFT,SFET,VFT,VFET> FeatureSynchronizer<ST, SFT, SFET, VT, VFT, VFET,C> getFeature(EStructuralFeature feature) {
		for(FeatureSynchronizer<ST,?,?,VT,?,?,C> f : features) {
			if(f.getFeature()==feature)
				return (FeatureSynchronizer<ST, SFT, SFET, VT, VFT, VFET, C>) f;
		}
		return null;
	}
	
	final public void addFeatureSynchronizer(FeatureSynchronizer<ST,?,?,VT,?,?,C> f) {
		f.container = this;
		int i=0;
		for(i=0;i<features.size();i++) {
			if(features.get(i).getFeature()==f.getFeature())
				break;
		}
		if(i==features.size()) features.add(f);
		else features.set(i, f);
	}
	
	@SuppressWarnings("all")
	final public void addSuperFeatureSynchronizer(FeatureSynchronizer f) {
		int i=0;
		for(i=0;i<features.size();i++) {
			if(features.get(i).getFeature()==f.getFeature())
				break;
		}
		if(i==features.size()) features.add(f);
		else features.set(i, f);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public VT get(ST source, C context) {
		TraceSystem trace = (TraceSystem) context.get(Context.TRACE_LINK);
		VT tracedView = (VT) trace.resolve(ecoreClass.getName(), source);
		
		VT view;
		if(tracedView!=null && !isDeepRefreshing(context)) return tracedView;
		if(tracedView!=null) view = tracedView;
		else {
			view = createView(context);
			trace.add(TraceLink.forwardTrace(ecoreClass.getName(), source, view));
		}
		// we may refreshing view properties multiple times, but it is fine
		this.features.forEach(f->{
			Object sv = f.externalGet(source, context);
			if(sv==null) {
				view.eUnset(f.getFeature());
			} else {
				Object vv = ((FeatureSynchronizer<ST,Object,?,VT,Object,?,C>)f).get(sv, context);
				view.eSet(f.getFeature(), vv);				
			}
		});
		
		return view;
	}
	
	@Override
	public ST put(ST source, VT view, C context) {
		throw new UnsupportedOperationException(); // short-cut
	}
	
	private boolean isDeepRefreshing(C context) {
		boolean refreshMode = (Boolean) context.get(Context.REFRESH_MODE);
		return refreshMode;
	}
	
	/**
	 * @param source must be null or a value
	 */
	@SuppressWarnings({ "unchecked"})
	@Override
	public <SO,VO> ValueProvider<SO,VO, ST> put(ValueProvider<SO, VO, ST> source, VT view, C context) {
		// Remember that a trace link in put indicates a conversion 
		TraceSystem trace = (TraceSystem) context.get(Context.TRACE_LINK);
		ValueProvider<SO, VO, ST> tracedUpdatedSource = (ValueProvider<SO, VO, ST>) trace.resolve(ecoreClass.getName(), source, view);
		// we must ensure that one view node enters deepRefreshing only once; 
		boolean deepRefreshing = isDeepRefreshing(context);
		if(tracedUpdatedSource!=null && !deepRefreshing) return tracedUpdatedSource;
		
		Wrapper wrapper = createWrapper(source, context);
		
		Object internalSource = null;
		if(source==null) internalSource = null;
		else internalSource = source.getValue();
		
		if(tracedUpdatedSource!=null) 
			source = tracedUpdatedSource;
		else {
			boolean consChanged = false;
			for(FeatureSynchronizer<ST, ?, ?, VT, ?, ?, C> f : this.features) {
				// Phase 1: compute constructor parameters
				if(!f.isConstructorParameter()) continue;
				ValueProvider<ST, VT, Object> sourceValue = (ValueProvider<ST, VT, Object>) wrapper.get(f.getFeature().getName());
//				Object viewValue = view.eGet(f.getFeature());
				Object viewValue = f.dynamicCompute(view, context);
				
				if((sourceValue!=null && (!sourceValue.isValueReady() || sourceValue.getValue()!=null)) || viewValue!=null) {
					ValueProvider<ST, VT, Object> newSourceValue = ((FeatureSynchronizer<ST, Object, ?, VT, Object, ?, C>)f).put(sourceValue, viewValue, context);
					// Now we allow that a construct parameter has a private putter
					if(newSourceValue.isValueReady()) {
						if(!ValueProvider.isValueEqual(newSourceValue, sourceValue)) {
							consChanged = true;
						}
					}
					wrapper.put(f.getFeature().getName(), newSourceValue);
				} else {
					wrapper.put(f.getFeature().getName(), ValueProvider.nullValue());
				}
				
			}
			if(source==null || source.isNullValue()) {
				source = (ValueProvider<SO, VO, ST>) this.createSource(null, wrapper, context);
			} else if(consChanged) {
				source = (ValueProvider<SO, VO, ST>) this.createSource(source.getValue(), wrapper, context);
			}
			trace.add(TraceLink.backwardTrace(ecoreClass.getName(), internalSource, view, source));
			// Till now, we created and registered a placeholder
		}
		
		if(!deepRefreshing) return source;
		else { 
			// We ensure that only in deep refreshing mode, relationships are updated
			// Deep Refreshing Mode
			// In this mode, we compute other features
			for(FeatureSynchronizer<ST, ?, ?, VT, ?, ?, C> f : this.features) {
				// Phase 2: compute non-constructor parameters.
				// Please note that the trace link has been pushed
				if(f.constructorParameter) continue;
				ValueProvider<ST, VT, Object> sourceValue = (ValueProvider<ST, VT, Object>) wrapper.get(f.getFeature().getName());
//				Object viewValue = view.eGet(f.getFeature());
				Object viewValue = f.dynamicCompute(view, context);
				
				if((sourceValue!=null && (!sourceValue.isValueReady() || sourceValue.getValue()!=null)) || viewValue!=null) {
					ValueProvider<ST, VT, Object> sourceValueAction = ((FeatureSynchronizer<ST, Object, ?, VT, Object, ?, C>)f).put(sourceValue, viewValue, context);
					wrapper.put(f.getFeature().getName(), sourceValueAction);
				} else {
					wrapper.put(f.getFeature().getName(), ValueProvider.nullValue());
				}
				
			}
			List<Pair<EStructuralFeature, ValueProvider<ST, VT, ?>>> actions = this.features.stream()
					.map(f->{
						ValueProvider<?,?,?> o = wrapper.get(f.getFeature().getName());
						if(!o.isValueReady() || o.hasExternalOperations()) {
							return Pair.<EStructuralFeature, ValueProvider<ST, VT, ?>>of(f.getFeature(), (ValueProvider<ST, VT, ?>)o);
						} else 
							return null;
					})
					.filter(o->o!=null)
					.collect(Collectors.toList());
			
			ValueProvider<SO, VO, ST> sourceElement = ValueProvider.element(source, view, actions, wrapper);
			return sourceElement;
		}
		
	}
	
//	@SuppressWarnings("unchecked")
//	public <SO,VO> ValueProvider<SO,VO, ST> createFromView(VT view, C context) {
//		TraceSystem trace = (TraceSystem) context.get(Context.TRACE_LINK);
//		Map<String, Object> wrapper = createWrapper(null, context);
//		for(FeatureSynchronizer<ST, ?, ?, VT, ?, ?, C> f : this.features) {
//			if(f.isConstructorParameter()==false) continue;
//			
//			Object sourceValue = wrapper.get(f.getFeature().getName());
//			Object viewValue = view.eGet(f.getFeature());
//			
//			ValueProvider<ST, VT, Object> sourceValueAction = ((FeatureSynchronizer<ST, Object, ?, VT, Object, ?, C>)f).put(ValueProvider.value(sourceValue), viewValue, context);
//			
//			if(f.isConstructorParameter() && sourceValueAction.isValueReady()) {
//				Object newValue = sourceValueAction.getValue(null, null);
//				if(!newValue.equals(sourceValue)) {
//					wrapper.put(f.getFeature().getName(), sourceValueAction);
//					if(f.isConstructorParameter()) {
//						wrapper.put(f.getFeature().getName(), sourceValueAction.getValue());
//					}
//				}
//			} else {
//				return null;
//			}
//		}
//		ValueProvider<SO, VO, ST> internalSource = (ValueProvider<SO, VO, ST>) this.createSource(null, wrapper, context);;
//		trace.add(TraceLink.backwardTrace(ecoreClass.getName(), null, view, internalSource));
//		return internalSource;
//	}
	
	final protected void link(ST source, VT view, C context) {
		TraceSystem trace = (TraceSystem) context.get(Context.TRACE_LINK);
		trace.add(TraceLink.forwardTrace(ecoreClass.getName(), source, view));
		this.features.forEach(f->{
			f.link(source, view, context);
		});
	}
	
//	@SuppressWarnings("unchecked")
//	final protected List<Object> collectRemovedObject(Object seed, Context context) {
//		List<Object> result = new ArrayList<>();
//		result.add(seed);
//		result.addAll(this.container.collectRemovedObject(seed, (ElementSynchronizer<Object, EObject, Context>) this, context));
//		return result;
//	}
	
	public List<FeatureSynchronizer<ST,?,?,VT,?,?,C>> getAllFeatures() {
		return this.features;
	}
	
//	public boolean canBeInstantiate(EObject view) {
//		return this.features.stream().filter(f->f.isConstructorParameter()).allMatch(f->{
//			return view.eIsSet(f.feature) || view.eGet(f.feature)!=null;
//		});
//	}
	
	public boolean canHandle(Object s) {
		return this.javaClass.isInstance(s) && this.sourceFilter(s);
	}
	
	public boolean canHandle(EObject v) {
		return this.ecoreClass.isSuperTypeOf(v.eClass());
	}
	
	public ElementSynchronizer<ST, VT, C> getValueSynchronizerForGet(ST s) {
		return this;
	}
	
	public ElementSynchronizer<ST, VT, C> getValueSynchronizerForPut(VT v) {
		return this;
	}
}