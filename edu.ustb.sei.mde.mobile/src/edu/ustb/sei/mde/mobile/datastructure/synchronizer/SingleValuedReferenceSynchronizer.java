package edu.ustb.sei.mde.mobile.datastructure.synchronizer;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import edu.ustb.sei.mde.mobile.datastructure.Context;
import edu.ustb.sei.mde.mobile.datastructure.Pair;
import edu.ustb.sei.mde.mobile.datastructure.operation.ExternalOperation;
import edu.ustb.sei.mde.mobile.datastructure.operation.Information;
import edu.ustb.sei.mde.mobile.datastructure.operation.InformationKind;
import edu.ustb.sei.mde.mobile.datastructure.operation.ValueProvider;

public abstract class SingleValuedReferenceSynchronizer<ST, SFT, VT extends EObject, VFT extends EObject,C extends Context> extends ReferenceSynchronizer<ST, SFT, SFT, VT, VFT, VFT, C> {

	protected SingleValuedReferenceSynchronizer(EReference feature) {
		super(feature);
	}

	@SuppressWarnings("unchecked")
	@Override
	public VFT get(SFT source, C context) {
		C newScope = (C) context.newScope();
		newScope.put(Context.REFRESH_MODE, ((EReference)this.getFeature()).isContainment());
		
		if(source==null) return null;
		else return ((ElementSynchronizer<SFT, VFT, C>)this.valueSynchronizer).getValueSynchronizerForGet(source).get(source, newScope);
	}
	
	@Override
	public SFT put(SFT source, VFT view, C context) {
		throw new UnsupportedOperationException();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <SO, VO> ValueProvider<SO, VO, SFT> put(ValueProvider<SO, VO, SFT> source, VFT view, C context) {
		C newScope = (C) context.newScope();
		newScope.put(Context.REFRESH_MODE, ((EReference)this.getFeature()).isContainment());
		
		ValueProvider<SO, VO, SFT> newSource = null;
		if(view!=null) newSource=((ElementSynchronizer<SFT, VFT, C>)this.valueSynchronizer).getValueSynchronizerForPut(view).put(source, view, newScope);
		
		/*
		 * value with external operations
		 */
		if(newSource!=null) {
			if(source==null) {
				return ValueProvider.operation(newSource,ExternalOperation.operation((s, v, sv) -> {
					externalSet((ST) s, sv, (VT) v, view, context);
				}, this::configureInformation, InformationKind.set, 
						Pair.of(Information.VALUE, newSource), 
						Pair.of(Information.VIEW_VALUE, view), 
						Pair.of(Information.FEATURE, this.getFeature())));
			} else if(!newSource.equals(source)) {
				return ValueProvider.operation(newSource,ExternalOperation.operation((s, v, sv) -> {
					externalReplace((ST) s, source.getValue(), sv, (VT) v, view, context);
				}, this::configureInformation, InformationKind.replace, 
						Pair.of(Information.VALUE, newSource), 
						Pair.of(Information.OLD_VALUE, source.getValue()),
						Pair.of(Information.VIEW_VALUE, view), 
						Pair.of(Information.FEATURE, this.getFeature())));
			} else if(newSource.hasExternalOperations())
				return newSource;
			else 
				return source;
		} else {
			if(source==null || (source.isValueReady()&&source.getValue()==null)) return source;
			else {
				return ValueProvider.operation(ValueProvider.nullValue(), ExternalOperation.operation((s, v, sv) -> {
					externalSet((ST) s, sv, (VT) v, view, context);
				}, this::configureInformation, InformationKind.set, 
						Pair.of(Information.VALUE, newSource), 
						Pair.of(Information.VIEW_VALUE, view), 
						Pair.of(Information.FEATURE, this.getFeature())));
			}
		}
	}
	
	@Override
	public void externalReplace(ST source, SFT oldValue, SFT newValue, VT view, VFT viewValue, C context) {
		externalSet(source, newValue, view, viewValue, context);
	}

	@Override
	final public void externalInsert(ST source, SFT value, VT view, VFT viewValue, C context) {
		throw new UnsupportedOperationException();
	}

	@Override
	final public void externalRemove(ST source, SFT value, VT view, C context) {
		throw new UnsupportedOperationException();
	}
	
}