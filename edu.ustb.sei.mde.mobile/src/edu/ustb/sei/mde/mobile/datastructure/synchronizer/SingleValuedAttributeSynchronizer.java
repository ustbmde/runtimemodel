package edu.ustb.sei.mde.mobile.datastructure.synchronizer;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.mde.mobile.datastructure.Context;
import edu.ustb.sei.mde.mobile.datastructure.Pair;
import edu.ustb.sei.mde.mobile.datastructure.operation.ExternalOperation;
import edu.ustb.sei.mde.mobile.datastructure.operation.Information;
import edu.ustb.sei.mde.mobile.datastructure.operation.InformationKind;
import edu.ustb.sei.mde.mobile.datastructure.operation.ValueProvider;

public abstract class SingleValuedAttributeSynchronizer<ST, SFT, VT extends EObject, VFT,C extends Context> extends AttributeSynchronizer<ST, SFT, SFT, VT, VFT, VFT, C> {

	protected SingleValuedAttributeSynchronizer(EAttribute feature) {
		super(feature);
	}

	@Override
	public VFT get(SFT source, C context) {
		return this.valueSynchronizer.get(source, context);
	}
	
	@Override
	public SFT put(SFT source, VFT view, C context) {
		return this.valueSynchronizer.put(source, view, context);
	}
	
	// FIXME: it replace must be defined in the parent class
	public void externalReplace(ST source, SFT oldValue, SFT newValue, VT view, VFT viewValue, C context) {
		externalSet(source, newValue, view, viewValue, context);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <SO, VO> ValueProvider<SO, VO, SFT> put(ValueProvider<SO, VO, SFT> source, VFT view, C context) {
		if(source.isValueReady()) {
			/*
			 * value with external operations
			 */
			SFT newSource = put(source.getValue(), view, context);
			// ASSUMPTION: we assume that the default value of this feature is null
			if(source.getValue()==null && newSource!=null) {
				ValueProvider<SO, VO, SFT> result = ValueProvider.value(newSource);
				return ValueProvider.operation(result, ExternalOperation.operation((s, v, sv) -> {
					externalSet((ST) s, sv, (VT) v, view, context);
				}, this::configureInformation, InformationKind.set, Pair.of(Information.VALUE, result), 
						Pair.of(Information.VIEW_VALUE, view), Pair.of(Information.FEATURE, this.getFeature())));
			} else {
				if(!(newSource==source.getValue() || (newSource!=null && newSource.equals(source.getValue())))) {
					ValueProvider<SO, VO, SFT> result = ValueProvider.value(newSource);
					return ValueProvider.operation(result, ExternalOperation.operation((s, v, sv) -> {
						externalReplace((ST) s, source.getValue(), sv, (VT) v, view, context);
					}, this::configureInformation, InformationKind.replace, Pair.of(Information.VALUE, result), 
							Pair.of(Information.OLD_VALUE, source.getValue()),
							Pair.of(Information.VIEW_VALUE, view), 
							Pair.of(Information.FEATURE, this.getFeature())));
				} else {
					return source;
				}				
			}
			
		} else {
			throw new UnsupportedOperationException();
		}
	}	
}