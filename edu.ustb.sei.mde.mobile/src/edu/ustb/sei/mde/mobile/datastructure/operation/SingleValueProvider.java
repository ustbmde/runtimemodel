package edu.ustb.sei.mde.mobile.datastructure.operation;

import java.util.Optional;
import java.util.function.BiFunction;

public class SingleValueProvider<S, V, SV> extends ValueProvider<S, V, SV> {
	public SingleValueProvider(SV value) {
		super();
		this.value = Optional.ofNullable(value);
		this.provider = null;
	}
	
	public SingleValueProvider(BiFunction<S, V, SV> provider) {
		super();
		this.value = null;
		this.provider = provider;
	}
	
	protected Optional<SV> value;
	protected BiFunction<S, V, SV> provider;
	
	@Override
	public boolean isValueReady() {
		return value!=null; // && value.isPresent();
	}
	@Override
	public SV getValue(S s, V v) {
		if(isValueReady()) {
			if(value.isPresent())
				return value.get();
			else return null;
		}
		else {
			SV rawValue = provider.apply(s, v);
			value = Optional.ofNullable(rawValue);
			return rawValue;
		}
	}
	
	@Override
	public void explain(StringBuilder builder, int indent) {
		indent(builder, indent);
		if(isValueReady()) {
			builder.append(value);
		} else {
			builder.append("value creator");
		}
	}
	@Override
	public ValueProvider<S, V, SV> getCore() {
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj!=null && obj instanceof SingleValueProvider) {
			if(this==obj) return true;
			else if(this.isValueReady() && ((SingleValueProvider<?,?,?>) obj).isValueReady())
				return this.getValue() == ((SingleValueProvider<?,?,?>) obj).getValue() 
				|| (this.getValue()!=null && this.getValue().equals(((SingleValueProvider<?,?,?>) obj).getValue()));
			else return false;
		} else return super.equals(obj);
	}
	
	@Override
	public int hashCode() {
		if(isValueReady()) {
			Object v = this.getValue();
			if(v==null) return 0;
			else return v.hashCode();
		} else return super.hashCode();
	}
	
	@Override
	public String toString() {
		if(isValueReady()) return value.toString();
		else return "create value";
	}
	
	@Override
	public boolean hasExternalOperations() {
		return false;
	}
	
	@Override
	public boolean isNullValue() {
		return isValueReady() && getValue()==null;
	}
}
