package edu.ustb.sei.mde.mobile.datastructure.operation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import edu.ustb.sei.mde.mobile.datastructure.Pair;

public interface OperationVisitor {
	default void visit(ValueProvider<?,?,?> root) {
		visit(null, null, root);
	}
	
	default void visit(ValueProvider<?,?,?> hostObject, EObject viewObject, SingleValueProvider<?, ?, ?> provider) {}
	
	default void visit(ValueProvider<?,?,?> hostObject, EObject viewObject, OperationalValueProvider<?, ?, ?> provider) {
		visit(hostObject, null, provider.host);
		provider.externalOperations.forEach(o->{
			visit(hostObject, viewObject, o);			
		});
	}
	
	default void visit(ValueProvider<?, ?, ?> sourceObject, EObject viewObject, ValueProvider<?, ?, ?> value) {
		if(value instanceof OperationalCollectionValueProvider) {
			visit(sourceObject, viewObject, (OperationalCollectionValueProvider<?,?,?>)value);
		} else if(value instanceof OperationalElementValueProvider) {
			visit(sourceObject, viewObject, (OperationalElementValueProvider<?,?,?,?>)value);
		} else if(value instanceof OperationalValueProvider) {
			visit(sourceObject, viewObject, (OperationalValueProvider<?,?,?>)value);
		} else if(value instanceof SingleValueProvider) {
			visit(sourceObject, viewObject, (SingleValueProvider<?,?,?>)value);
		}
	}

	default void visit(ValueProvider<?,?,?> hostObject, EObject viewObject, OperationalElementValueProvider<?, ?, ?, ?> provider) {
		visit(hostObject, viewObject, (OperationalValueProvider<?, ?, ?>) provider);
		provider.propertyProviders.forEach(v->{
			visit(provider, (EObject) provider.view, v.second);
		});
	}
	
	
	default void visit(ValueProvider<?,?,?> hostObject, EObject viewObject, OperationalCollectionValueProvider<?, ?, ?> provider) {
		visit(hostObject, viewObject, (OperationalValueProvider<?, ?, ?>) provider);
		provider.valueProviders.forEach(v->{
			visit(hostObject, viewObject, v);
		});
	}
	
	default void visit(ValueProvider<?,?,?> hostObject, EObject viewObject, ExternalOperation<?, ?, ?> operation) {
		if (operation instanceof CompoundExternalOperation) {
		} else {
			operation.getInformation().put(Information.SOURCE_OBJECT, hostObject);
			operation.getInformation().put(Information.VIEW_OBJECT, viewObject);
			operation.doConfigure();
			visitInformation(operation, operation.getInformation());
		}
	}
	
	default void visitInformation(ExternalOperation<?, ?, ?> operation, Information info) {
		System.out.println(info.toString());
	}
	
	// FIXME: we support setting the object collection now. Will this affect the value dependency?
	static public class ValueDependencyCollector implements OperationVisitor {
		protected HashMap<ValueProvider<?,?,?>, Set<ValueProvider<?,?,?>>> valueDependencies = new HashMap<>();

		protected void addDependency(ValueProvider<?, ?, ?> client, ValueProvider<?, ?, ?> host) {
			if(host==null) return;
			Set<ValueProvider<?,?,?>> set = valueDependencies.get(client.getCore());
			if(set==null) {
				set = new HashSet<>();
				valueDependencies.put(client.getCore(), set);
			}
			set.add(host.getCore());
		}
		
		@Override
		public void visit(ValueProvider<?, ?, ?> hostObject, EObject viewObject,
				OperationalCollectionValueProvider<?, ?, ?> provider) {
			if(provider.hasCollectionOperation) {
				provider.valueProviders.forEach(v->{
					addDependency(v, provider); // to ensure that any operation on provider can be scheduled
				});
			}
			OperationVisitor.super.visit(hostObject, viewObject, provider);
		}
		
		@Override
		public void visit(ValueProvider<?, ?, ?> hostObject, EObject viewObject, OperationalElementValueProvider<?, ?, ?, ?> provider) {
			addDependency(provider, hostObject);
			visit(hostObject, viewObject, (OperationalValueProvider<?, ?, ?>) provider);
			provider.propertyProviders.forEach(v->{
				if(v.first instanceof EReference && ((EReference)v.first).isContainment())
					visit(provider, (EObject) provider.view, v.second);
			});
		}
		
		@Override
		public void visit(ValueProvider<?, ?, ?> hostObject, EObject viewObject, OperationalValueProvider<?, ?, ?> provider) {
			addDependency(provider, hostObject);
			OperationVisitor.super.visit(hostObject, viewObject, provider);
		}
		
		@Override
		public void visit(ValueProvider<?, ?, ?> hostObject, EObject viewObject, SingleValueProvider<?, ?, ?> provider) {
			addDependency(provider, hostObject);
			OperationVisitor.super.visit(hostObject, viewObject, provider);
		}
		
		public boolean isDependentOn(ValueProvider<?, ?, ?> client, ValueProvider<?, ?, ?> host) {
			return isDependentOn(client, host, new HashSet<>());
		}
		
		private boolean isDependentOn(ValueProvider<?, ?, ?> client, ValueProvider<?, ?, ?> host, Set<ValueProvider<?, ?, ?>> checked) {
			ValueProvider<?, ?, ?> core = client.getCore();
			if(checked.contains(core)) return false;
			else {
				checked.add(core);
				Set<ValueProvider<?,?,?>> set = valueDependencies.getOrDefault(core, Collections.emptySet());
				if(core.equals(host.getCore())) return true;
				else if(set.contains(host.getCore())) return true;
				else {
					for(ValueProvider<?, ?, ?> e : set) {
						if(isDependentOn(e, host, checked)) return true;
					}
					return false;
				}
			}
		}
		
		@Override
		public void visit(ValueProvider<?, ?, ?> hostObject, EObject viewObject, ExternalOperation<?, ?, ?> operation) {}
		
		@Override
		public void visitInformation(ExternalOperation<?, ?, ?> operation, Information info) {}
	}
	
	static public class OperationCollector implements OperationVisitor {
		protected List<Pair<ExternalOperation<?, ?, ?>, Information>> operationInformation = new ArrayList<>();
		@Override
		public void visitInformation(ExternalOperation<?, ?, ?> operation, Information info) {
			operationInformation.add(Pair.of(operation, info));
			// FIXME: what will happen to the downstream operations if the parent operation is cancelled
			// FIXME: we should also handle duplicate operations!!
			if (info.getKind() == InformationKind.moveOut) {
				operationInformation.stream().filter(p -> (p.second.getKind() == InformationKind.insert 
						|| p.second.getKind() == InformationKind.moveIn)
						&& p.second.isContainment() && info.getViewValue()==p.second.getViewValue())
						.forEach(p -> {
							if(p.second.getKind()==InformationKind.moveIn) {
								operation.setAssociatedOperation(p.first);
							} else { // we cancel insertion. But this should not happen
								p.first.cancel();
								p.first.setPairedOperation(operation);
							}
						});
			} else if (info.getKind() == InformationKind.insert && info.isContainment()) {
				operationInformation.stream().filter(p -> p.second.getKind() == InformationKind.moveOut
						&& info.getViewValue().equals(p.second.getViewValue())).forEach(p -> {
							operation.cancel();
							operation.setPairedOperation(p.first);
						});
			} else if(info.getKind() == InformationKind.moveIn) {
				operationInformation.stream().filter(p -> (p.second.getKind() == InformationKind.remove 
						|| p.second.getKind() == InformationKind.moveOut)
						&& p.second.isContainment() && info.getOldValue().equals(p.second.getOldValue()))
						.forEach(p -> {
							if(p.second.getKind()==InformationKind.moveOut) {
								operation.setAssociatedOperation(p.first);
							} else {
								p.first.cancel();
								p.first.setPairedOperation(operation);
							}
						});
			} else if (info.getKind() == InformationKind.remove && info.isContainment()) { 
				operationInformation.stream().filter(p -> p.second.getKind() == InformationKind.moveIn
						&& info.getOldValue().equals(p.second.getOldValue())).forEach(p -> {
							operation.cancel();
							operation.setPairedOperation(p.first);
						});
			}
		}
	}
	
	
}
