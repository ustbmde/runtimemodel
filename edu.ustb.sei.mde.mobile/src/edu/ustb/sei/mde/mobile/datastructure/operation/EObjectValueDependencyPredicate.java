package edu.ustb.sei.mde.mobile.datastructure.operation;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;

public class EObjectValueDependencyPredicate implements ContextPredicate {

	public EObjectValueDependencyPredicate(String leftKey, String rightKey, boolean leftDependsOnRight) {
		super();
		this.leftKey = leftKey;
		this.rightKey = rightKey;
		this.leftDependsOnRight = leftDependsOnRight;
	}
	@Override
	public boolean check(Map<String, Object> context) {
		EObject left = (EObject) context.get(leftKey);
		EObject right = (EObject) context.get(rightKey);
		
		if(leftDependsOnRight) {
			while(left!=right && left!=null) {
				left = left.eContainer();
			}
			return left!=null;
		} else {
			while(left!=right && right!=null) {
				right = right.eContainer();
			}
			return right!=null;
		}
	}
	
	
	protected String leftKey;
	protected String rightKey;
	protected boolean leftDependsOnRight;
}