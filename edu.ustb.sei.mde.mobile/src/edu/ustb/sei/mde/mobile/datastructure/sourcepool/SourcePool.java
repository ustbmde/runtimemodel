package edu.ustb.sei.mde.mobile.datastructure.sourcepool;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SourcePool {
	protected List<SourcePoolObject> objects;
	
	protected SourcePoolObject[] roots;
	
	public SourcePool(List<SourcePoolObject> pool) {
		objects = new ArrayList<>(pool);
		roots = objects.stream().filter(s->s.getContainer()==null).toArray(i->new SourcePoolObject[i]);
	}
	
	public Iterator<SourcePoolObject> allObjects() {
		return objects.iterator();
	}
	
	public SourcePoolObject getPoolObject(Object sourceValue) {
		Iterator<SourcePoolObject> it = this.allObjects();
		while(it.hasNext()) {
			SourcePoolObject s = it.next();
			if(s.getSourceObject().equals(sourceValue)) return s;
		}
		return null;
	}
	
	public boolean isSameOrContainer(Object child, Object parent) {
		SourcePoolObject sc = this.getPoolObject(child);
		SourcePoolObject sp = this.getPoolObject(parent);
		
		if(sc==null || sp==null) return false;
		
		while(sc!=sp && sc!=null) {
			sc = sc.getContainer();
		}
		
		return sc!=null;
	}
}
