package edu.ustb.sei.mde.mobile.datastructure.operation;

import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EStructuralFeature;

import edu.ustb.sei.mde.mobile.datastructure.Pair;
import edu.ustb.sei.mde.mobile.datastructure.Wrapper;

public class OperationalElementValueProvider<SP, VP, S, V> extends OperationalValueProvider<SP, VP, S> {
	public OperationalElementValueProvider(ValueProvider<SP, VP, S> host, V view,
			List<Pair<EStructuralFeature, ValueProvider<S, V, ?>>> properties, Wrapper wrapper) {
		super();
		this.host = host;
		this.view = view;
		this.propertyProviders = properties;
		this.wrapper = wrapper;
		
		List<ValueProvider<S, V, ?>> collect = properties.stream().map(p->p.second).collect(Collectors.toList());

		this.externalOperations.add(new CompoundExternalOperation<SP,VP,S,V>(collect, view));
	}

	public V view;
	public List<Pair<EStructuralFeature, ValueProvider<S, V, ?>>> propertyProviders;
	public Wrapper wrapper;
	
	@Override
	public boolean isValueReady() {
		return host.isValueReady();
	}
	@Override
	public S getValue(SP s, VP v) {
		S value = host.getValue(s, v);
		propertyProviders.forEach(o->{
			o.second.getValue(value, view);
			// o.second must be ready now
			wrapper.put(o.first.getName(), o.second);
		});
		return value;
	}
	
	@Override
	public void explain(StringBuilder builder, int indent) {
		indent(builder,indent);
		builder.append("element : {\n");
		host.explain(builder, indent+1);
		indent(builder,indent);
		builder.append("} {\n");
		
		this.propertyProviders.forEach(p->{
			indent(builder,indent+1);
			builder.append(p.first);
			builder.append("=>\n");
			p.second.explain(builder, indent+2);
		});
		indent(builder,indent);
		builder.append("} {\n");
		
		this.externalOperations.forEach(o->{
			indent(builder, indent+1);
			builder.append(o.printableInformation());
			builder.append("\n");
		});
		indent(builder, indent);
		builder.append("}\n");
	}

}
