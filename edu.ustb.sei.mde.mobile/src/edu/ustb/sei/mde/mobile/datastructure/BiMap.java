package edu.ustb.sei.mde.mobile.datastructure;

import java.util.HashMap;

public class BiMap<F, S> {
	private HashMap<F, S> forwardMap;
	private HashMap<S, F> backwardMap;
	
	public BiMap() {
		forwardMap = new HashMap<>();
		backwardMap = new HashMap<>();
	}
	
	
	public S fowardGet(F k) {
		return forwardMap.get(k);
	}
	public F backwardGet(S k) {
		return backwardMap.get(k);
	}
	
	public void put(F f, S s) {
		S oldS = fowardGet(f);
		F oldF = backwardGet(s);
		
		this.forwardMap.remove(oldF);
		this.backwardMap.remove(oldS);
		
		this.forwardMap.put(f, s);
		this.backwardMap.put(s, f);
	}

}
