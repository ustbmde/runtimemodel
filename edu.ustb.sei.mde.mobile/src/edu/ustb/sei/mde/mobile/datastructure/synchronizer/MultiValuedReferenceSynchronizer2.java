package edu.ustb.sei.mde.mobile.datastructure.synchronizer;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import edu.ustb.sei.mde.mobile.datastructure.Context;
import edu.ustb.sei.mde.mobile.datastructure.Pair;
import edu.ustb.sei.mde.mobile.datastructure.operation.ExternalOperation;
import edu.ustb.sei.mde.mobile.datastructure.operation.Information;
import edu.ustb.sei.mde.mobile.datastructure.operation.InformationKind;
import edu.ustb.sei.mde.mobile.datastructure.operation.ValueProvider;

abstract public class MultiValuedReferenceSynchronizer2<ST, SFET, VT extends EObject, VFET extends EObject, C extends Context> extends MultiValuedReferenceSynchronizer<ST, SFET, VT, VFET, C> {

	protected MultiValuedReferenceSynchronizer2(EReference feature) {
		super(feature);
	}

	@Override
	public void externalRemove(ST source, SFET value, VT view, C context) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void externalInsert(ST source, SFET value, VT view, VFET viewValue, C context) {
		throw new UnsupportedOperationException();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <SO, VO> ValueProvider<SO, VO, List<SFET>> put(ValueProvider<SO, VO, List<SFET>> source, List<VFET> view,
			C context) {
		C newScope = (C) context.newScope();
		newScope.put(Context.REFRESH_MODE, ((EReference)this.getFeature()).isContainment());
		
		List<ValueProvider<SO, VO, SFET>> newSources = new ArrayList<>();
		List<SFET> oldSource = new ArrayList<>(source.getValue());
		/*
		 * collection value provide
		 */
		if (this.valueSynchronizer instanceof ElementSynchronizer) {
			view.forEach(v->{
				ElementSynchronizer<SFET, VFET, C> syn = ((ElementSynchronizer<SFET, VFET, C>)this.valueSynchronizer).getValueSynchronizerForPut(v);
				int i = 0;
				for (; i < oldSource.size(); i++) {
					SFET s = oldSource.get(i);
					if (syn.align(s, v, newScope) != Boolean.FALSE)
						break;
				}
				if(i==oldSource.size()) {
					ValueProvider<SO, VO, SFET> newSource = syn.put((ValueProvider<SO, VO, SFET> ) null, v, newScope);
					newSources.add(newSource);
				} else {
					ValueProvider<SO, VO, SFET> old = ValueProvider.value(oldSource.get(i));
					ValueProvider<SO, VO, SFET> newSource = syn.put(old, v, newScope);
					newSources.add(newSource);
					oldSource.remove(i);
				}
			});
			
			ValueProvider<SO, VO, List<SFET>> result = ValueProvider.collectionWithOperation(newSources);
			
			return ValueProvider.operation(result, ExternalOperation.operation((s, v, sv) -> {
				externalSet((ST) s, sv, (VT) v, view, context);
			}, this::configureInformation, InformationKind.set, 
					Pair.of(Information.VALUE, result), 
					Pair.of(Information.VIEW_VALUE, view), 
					Pair.of(Information.FEATURE, this.getFeature())));
		} else 
			throw new UnsupportedOperationException();
	}
}
