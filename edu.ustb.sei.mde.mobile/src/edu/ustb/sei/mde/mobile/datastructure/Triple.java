package edu.ustb.sei.mde.mobile.datastructure;

public class Triple<F, S, T> {
	final public F first;
	final public S second;
	final public T third;
	
	protected Triple(F first, S second, T third) {
		super();
		this.first = first;
		this.second = second;
		this.third = third;
	}
	
	static public <F,S,T> Triple<F,S,T> of(F f, S s, T t) {
		return new Triple<F, S, T>(f, s, t);
	}
}
