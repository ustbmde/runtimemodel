package edu.ustb.sei.mde.mobile.datastructure;

import java.util.HashMap;

import edu.ustb.sei.mde.mobile.datastructure.operation.ValueProvider;

public class Wrapper extends HashMap<String, ValueProvider<?, ?, ?>> {
	private static final long serialVersionUID = -6011180432779710853L;

	@SuppressWarnings("unchecked")
	public <T> T getValue(String key) {
		ValueProvider<?, ?, ?> v = get(key);
		if(v!=null)
			return (T) v.getValue();
		else return null;
	}
}
