package edu.ustb.sei.mde.mobile.datastructure.synchronizer;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.mde.mobile.datastructure.Context;
import edu.ustb.sei.mde.mobile.datastructure.Pair;
import edu.ustb.sei.mde.mobile.datastructure.operation.ExternalOperation;
import edu.ustb.sei.mde.mobile.datastructure.operation.Information;
import edu.ustb.sei.mde.mobile.datastructure.operation.InformationKind;
import edu.ustb.sei.mde.mobile.datastructure.operation.ValueProvider;

public abstract class MultiValuedAttributeSynchronizer<ST, SFET, VT extends EObject, VFET, C extends Context> extends AttributeSynchronizer<ST, List<SFET>, SFET, VT, List<VFET>, VFET, C> {
	protected MultiValuedAttributeSynchronizer(EAttribute feature) {
		super(feature);
	}

	@Override
	public List<VFET> get(List<SFET> source, C context) {
		List<VFET> result = new ArrayList<>();
		source.forEach(s->result.add(this.valueSynchronizer.get(s, context)));
		return result;
	}
	
	@Override
	public List<SFET> put(List<SFET> source, List<VFET> view, C context) {
		List<SFET> result = new ArrayList<>();
		for(int i=0;i<view.size();i++) {
			if(i<source.size()) {
				result.add(this.valueSynchronizer.put(source.get(i), view.get(i), context));
			} else {
				result.add(this.valueSynchronizer.put((SFET) null, view.get(i), context));
			}
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <SO, VO> ValueProvider<SO, VO, List<SFET>> put(ValueProvider<SO, VO, List<SFET>> source,
			List<VFET> view, C context) {
		if(source.isValueReady()) {
			/*
			 * value with external operations
			 */
			List<SFET> newSource = put(source.getValue(), view, context);
			if(!ValueProvider.isValueEqual(newSource, source)) {
				ValueProvider<SO, VO, List<SFET>> result = ValueProvider.value(newSource);
				return ValueProvider.operation(result, ExternalOperation.operation((s, v, sv) -> {
					externalSet((ST) s, sv, (VT) v, view, context);
				}, this::configureInformation, InformationKind.set, 
						Pair.of(Information.VALUE, result), 
						Pair.of(Information.VIEW_VALUE, view), 
						Pair.of(Information.FEATURE, this.getFeature())));
			} else {
				return source;
			}
		} else {
			throw new UnsupportedOperationException();
		}
	}
	
//	@Override
//	final public void externalSet(ST source, List<SFET> value, VT view , C context) {
//		throw new UnsupportedOperationException();
//	}
}