package edu.ustb.sei.mde.mobile.datastructure.operation;

import java.util.HashMap;
import java.util.Map;

public interface ScheduleRule {
	default String message() {
		return "ScheduleRule";
	}
	OperationOrder getImplication();
	boolean checkLeftPredicate(Information left, Map<String, Object> context);
	boolean checkRightPredicate(Information right, Map<String, Object> context);
	boolean checkBinaryPredicate(Map<String, Object> context);
	
	default OperationOrder check(Information left, Information right) {
		Map<String, Object> context = new HashMap<>();
		
		if(checkLeftPredicate(left, context) 
				&& checkRightPredicate(right, context) 
				&& checkBinaryPredicate(context))
			return getImplication();
		else return OperationOrder.unknown;
	}
	
	static ScheduleRule simpleRule(String message, InformationPredicate leftPredicate, 
			InformationPredicate rightPredicate, ContextPredicate binaryPredicate, OperationOrder order) {
		return new ScheduleRule() {
			@Override
			public String message() {
				return message;
			}
			@Override
			public OperationOrder getImplication() {
				return order;
			}
			
			@Override
			public boolean checkRightPredicate(Information right, Map<String, Object> context) {
				return rightPredicate.check(right, context);
			}
			
			@Override
			public boolean checkLeftPredicate(Information left, Map<String, Object> context) {
				return leftPredicate.check(left, context);
			}
			
			@Override
			public boolean checkBinaryPredicate(Map<String, Object> context) {
				return binaryPredicate.check(context);
			}
		};
	}
}