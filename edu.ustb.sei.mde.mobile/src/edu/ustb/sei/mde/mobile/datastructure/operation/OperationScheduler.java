package edu.ustb.sei.mde.mobile.datastructure.operation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.mde.mobile.datastructure.Pair;
import edu.ustb.sei.mde.mobile.datastructure.operation.OperationVisitor.OperationCollector;
import edu.ustb.sei.mde.mobile.datastructure.operation.OperationVisitor.ValueDependencyCollector;

public interface OperationScheduler {
	OperationOrder compare(ExternalOperation<?, ?, ?> former, ExternalOperation<?, ?, ?> later);
	ValueDependencyCollector getValueDependencies();
	OperationCollector getOperations();
	void schedule(ValueProvider<?, ?, ?> root);
	List<Pair<ExternalOperation<?, ?, ?>, Set<ExternalOperation<?, ?, ?>>>> getScheduleCopy();
	
	default void execute(ValueProvider<?, ?, ?> root) {
		// prepare and construct values
		root.getValue();
		
		// execute scheduled operations
		List<Pair<ExternalOperation<?, ?, ?>, Set<ExternalOperation<?, ?, ?>>>> schedule = getScheduleCopy();
		
		List<Pair<ExternalOperation<?, ?, ?>, Set<ExternalOperation<?, ?, ?>>>> executed = new ArrayList<>();
		while(!schedule.isEmpty()) {
			executed.clear();
			
			for(Pair<ExternalOperation<?, ?, ?>, Set<ExternalOperation<?, ?, ?>>> pair : schedule) {
				if(pair.second.isEmpty()) {
					@SuppressWarnings("unchecked")
					ExternalOperation<Object, EObject, Object> operation = (ExternalOperation<Object, EObject, Object>) pair.first;
					ValueProvider<?, ?, Object> value = operation.getInformation().getValue();

					if(operation.getInformation().getKind()==InformationKind.global_pre || operation.getInformation().getKind()==InformationKind.global_post) {
						operation.execute(null, null, value.getValue());
					} else {
						ValueProvider<?, ?, Object> sourceObject = operation.getInformation().getSourceObject();
						EObject viewObject = pair.first.getInformation().getViewObject();
						operation.execute(sourceObject.getValue(), viewObject, value.getValue());
					}
					executed.add(pair);
				}
			}
			
			if(executed.isEmpty()) {
				throw new RuntimeException();
			} else {
				schedule.removeAll(executed);

				Set<ExternalOperation<?, ?, ?>> execOps = executed.stream().map(p->p.first).collect(Collectors.toSet());
				schedule.forEach(p->{
					p.second.removeAll(execOps);
				});
			}
		}
	}
	
}
