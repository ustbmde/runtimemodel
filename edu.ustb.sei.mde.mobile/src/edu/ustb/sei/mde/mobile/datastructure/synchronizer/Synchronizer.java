package edu.ustb.sei.mde.mobile.datastructure.synchronizer;

import edu.ustb.sei.mde.mobile.datastructure.Context;
import edu.ustb.sei.mde.mobile.datastructure.operation.ValueProvider;

public interface Synchronizer<S, V, C extends Context> {
	V get(S source, C context);
	S put(S source, V view, C context);
	
	/**
	 * this version of get is defined for the sake of well-formedness 
	 * @param source
	 * @param context
	 * @return
	 */
	default <SP,VP> V get(ValueProvider<SP, VP, S> source, C context) {
		assert source.isValueReady() == true;
		return get(source.getValue(null, null), context);
	}

	<SO,VO> ValueProvider<SO, VO, S> put(ValueProvider<SO, VO, S> source, V view, C context);
	
	
	static public interface BijectiveSynchronizer<S, V, C extends Context> extends Synchronizer<S, V, C> {
		S put(V view, C context);
		
		@Override
		default S put(S source, V view, C context) {
			return put(view, context);
		}
		
		default <SO,VO> ValueProvider<SO, VO, S> put(ValueProvider<SO, VO, S> source, V view, C context) {
			return ValueProvider.value(put(view, context));
		}
	}
	
	static public interface PrimitiveValueSynchronizer<S, V, C extends Context>  extends Synchronizer<S, V, C> {
		default <SO,VO> ValueProvider<SO, VO, S> put(ValueProvider<SO, VO, S> source, V view, C context) {
			return ValueProvider.value(put(source.getValue(), view, context));
		}
	}
	
	@SuppressWarnings("unchecked")
	static public <S,C extends Context> Replace<S,C> replace() {
		return (Replace<S,C>) Replace.replace;
	}
	
	public static class Replace<S, C extends Context> implements PrimitiveValueSynchronizer<S, S, C> {
		@SuppressWarnings("rawtypes")
		static final private Replace replace = new Replace();

		@Override
		public S get(S source, C context) {
			return source;
		}

		@Override
		public S put(S source, S view, C context) {
			return view;
		}

		@Override
		public <SO, VO> ValueProvider<SO, VO, S> put(ValueProvider<SO, VO, S> source, S view, C context) {
			return ValueProvider.value(view);
		}
		
	}
}