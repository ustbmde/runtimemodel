package edu.ustb.sei.mde.mobile.datastructure.operation;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

public class Information extends HashMap<String, Object> {
	public Information(ExternalOperation<?, ?, ?> owner) {
		super();
		this.owner = owner;
	}
	
	
	
	
	public static final String PRODUCED_OBJECTS = "ProducedObjects";
	public static final String DESTROYED_OBJECTS = "DestroyedObjects";
	public static final String REQUIRED_OBJECTS = "RequiredObjects";
	
	/**
	 * used in every operation
	 */
	public static final String SOURCE_OBJECT = "SourceObject";
	
	/**
	 * used in moveIn
	 */
	public static final String OLD_SOURCE_OBJECT = "OldSourceObject";
	
	/**
	 * used in every operation
	 */
	public static final String VIEW_OBJECT = "ViewObject";

	/**
	 * used in moveOut
	 */
	public static final String NEW_VIEW_OBJECT = "NewViewObject";
	
	/**
	 * used in every operation
	 */
	public static final String VALUE = "Value";
	
	/**
	 * used in moveIn, replace
	 */
	public static final String OLD_VALUE = "OldValue";

	/**
	 * used in every operation
	 */
	public static final String VIEW_VALUE = "ViewValue";
	
	public static final String FEATURE = "Feature";
	
	/**
	 * it is set when the paired operation canceled this operation
	 */
	public static final String PAIRED_OPERATION = "PairedOperation";
	
	/**
	 * used in moveIn/moveOut/moveIn_pre/moveOut_post
	 */
	public static final String ASSOCIATED_OPERATION = "AssociatedOperation";
	
	public static final String CONTEXT = "Context";
	
	public static final String PREV_SILBING = "PrevSilbing";
	
	private static final long serialVersionUID = -9000674792223200901L;
	
	protected InformationKind kind;
	
	
	public static final String[] PRINTABLE = {FEATURE, SOURCE_OBJECT, VALUE,  OLD_SOURCE_OBJECT, OLD_VALUE, VIEW_OBJECT, NEW_VIEW_OBJECT, VIEW_VALUE};

	public InformationKind getKind() {
		return kind;
	}

	public void setKind(InformationKind kind) {
		this.kind = kind;
	}

	@SuppressWarnings("unchecked")
	public <SP, VP, S> ValueProvider<SP, VP, S> getSourceObject() {
		return (ValueProvider<SP, VP, S>) get(SOURCE_OBJECT);
	}
	
	public void setSourceObject(ValueProvider<?, ?, ?> source) {
		put(SOURCE_OBJECT, source);
	}
	
	@SuppressWarnings("unchecked")
	public <S> S getOldSourceObject() {
		return (S) get(OLD_SOURCE_OBJECT);
	}
	
	public void setOldSourceObject(Object source) {
		put(OLD_SOURCE_OBJECT, source);
	}
	
	@SuppressWarnings("unchecked")
	public <SP, VP, S> ValueProvider<SP, VP, S> getValue() {
		return (ValueProvider<SP, VP, S>) get(VALUE);
	}
	
	public void setValue(ValueProvider<?, ?, ?> value) {
		put(VALUE, value);
	}
	
	@SuppressWarnings("unchecked")
	public <S> S getOldValue() {
		return (S) get(OLD_VALUE);
	}
	
	public void setOldValue(Object value) {
		put(OLD_VALUE, value);
	}
	
	public EObject getNewViewObject() {
		return (EObject) get(NEW_VIEW_OBJECT);
	}
	
	public void setNewViewObject(EObject viewObject) {
		put(NEW_VIEW_OBJECT, viewObject);
	}
	
	/**
	 * @return List, EObject, or null
	 */
	public Object getViewValue() {
		return get(VIEW_VALUE);
	}
	
	public void setViewValue(Object viewValue) {
		put(VIEW_VALUE, viewValue);
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getDestroyedObjects() {
		return (List<Object>) getOrDefault(DESTROYED_OBJECTS, Collections.emptyList());
	}
	
	public void setDestroyedObjects(List<Object> objects) {
		put(DESTROYED_OBJECTS, objects);
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getProducedObjects() {
		return (List<Object>) getOrDefault(PRODUCED_OBJECTS, Collections.emptyList());
	}
	
	public void setProducedObjects(List<Object> objects) {
		put(PRODUCED_OBJECTS, objects);
	}
	
	@SuppressWarnings("unchecked")
	public List<Object> getRequiredObjects() {
		return (List<Object>) getOrDefault(REQUIRED_OBJECTS, Collections.emptyList());
	}
	
	public void setRequiredObjects(List<Object> objects) {
		put(REQUIRED_OBJECTS, objects);
	}
	
	
	public EStructuralFeature getFeature() {
		return (EStructuralFeature) get(FEATURE);
	}
	
	public boolean isContainment() {
		EStructuralFeature feature = this.getFeature();
		return feature instanceof EReference && ((EReference)feature).isContainment();
	}
	
	public void setFeature(EStructuralFeature f) {
		put(FEATURE, f);
	}

	public void setPairedOperation(ExternalOperation<?, ?, ?> operation) {
		put(PAIRED_OPERATION, operation);
		operation.information.put(PAIRED_OPERATION, this.owner);
	}

	public EObject getViewObject() {
		return (EObject) get(VIEW_OBJECT);
	}
	
	public void setViewObject(EObject o) {
		put(VIEW_OBJECT, o);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append(this.kind.name());
		sb.append("]\t");
		for(String s : PRINTABLE) {
			Object v = get(s);
			if(v!=null) {
				sb.append(s);
				sb.append("=");
				sb.append(v.toString());
				sb.append(",");
			}
		}
		return sb.toString();
	}
	
	public void setAssociatedOperation(ExternalOperation<?, ?, ?> operation) {
		put(ASSOCIATED_OPERATION, operation);
		operation.information.put(ASSOCIATED_OPERATION, this.owner);
	}
	
	private ExternalOperation<?, ?, ?> owner;
	
}
