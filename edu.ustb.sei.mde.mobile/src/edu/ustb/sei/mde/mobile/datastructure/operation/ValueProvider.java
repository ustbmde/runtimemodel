package edu.ustb.sei.mde.mobile.datastructure.operation;

import java.util.List;
import java.util.function.BiFunction;

import org.eclipse.emf.ecore.EStructuralFeature;

import edu.ustb.sei.mde.mobile.datastructure.Pair;
import edu.ustb.sei.mde.mobile.datastructure.Wrapper;

public abstract class ValueProvider<S,V,SV> {
	
	
	public abstract boolean isValueReady();
	
	public abstract SV getValue(S s, V v);
	
	public SV getValue() {
		return getValue(null, null);
	}
	
	public boolean isNullValue() {
		if(isValueReady()) {
			return this.getValue()==null;
		}
		else return this.getCore().isNullValue();
	}
	
	public void performExternalOperations(S s, V v) {
	}
	
	static public <S, V, SV> ValueProvider<S, V, SV> nullValue() {
		return new SingleValueProvider<>((SV)null);
	}
	
	
	@SuppressWarnings("unchecked")
	static public <S, V, SV> ValueProvider<S, V, SV> value(SV value) {
		if(value instanceof ValueProvider) {
			System.out.println("value!");
			return (ValueProvider<S, V, SV>) value;
		}
		else return new SingleValueProvider<>(value);
	}
	
	static public <S, V, SV> ValueProvider<S, V, SV> valueProvider(BiFunction<S, V, SV> function) {
		return new SingleValueProvider<>(function);
	}
	
	static public <S, V, SV, VV> ValueProvider<S, V, SV> element(ValueProvider<S, V, SV> objectProvider, VV view, List<Pair<EStructuralFeature, ValueProvider<SV, VV, ?>>> propertyProviders, Wrapper wrapper) {
		return new OperationalElementValueProvider<S, V, SV, VV>(objectProvider, view, propertyProviders, wrapper);
	}
	
	static public <S, V, SV> ValueProvider<S, V, SV> operation(ValueProvider<S, V, SV> host, ExternalOperation<S, V, SV> operation) {
		return new OperationalValueProvider<>(host, operation);
	}
	
	static public <S, V, SV> ValueProvider<S, V, List<SV>> collection(List<ValueProvider<S, V, SV>> providers) {
		return new OperationalCollectionValueProvider<>(providers);
	}
	
	static public <S, V, SV> ValueProvider<S, V, List<SV>> collectionWithOperation(List<ValueProvider<S, V, SV>> providers) {
		return new OperationalCollectionValueProvider<>(providers, true);
	}
	
	public void explain(StringBuilder builder, int indent) {
		
	}
	
	static StringBuilder indent(StringBuilder builder, int indent) {
		for(int i=0;i<indent;i++) builder.append(' ');
		return builder;
	}
	
	public abstract ValueProvider<S, V, SV> getCore();
	
	@Override
	public boolean equals(Object obj) {
		if(obj==null || !(obj instanceof ValueProvider)) return false;
		else {
			ValueProvider<S, V, SV> thisCore = this.getCore();
			ValueProvider<?, ?, ?> objCore = ((ValueProvider<?,?,?>) obj).getCore();
			if(thisCore==objCore) return true;
			else if(thisCore==this && objCore==obj) return false; // cannot go down
			else return thisCore.equals(objCore);
		}
	}
	
	@Override
	public String toString() {
		ValueProvider<S, V, SV> core = this.getCore();
		if(core==this) return super.toString();
		else return core.toString();
	}

	public boolean hasExternalOperations() {
		return false;
	}
	
	static public boolean isValueEqual(ValueProvider<?, ?, ?> l, ValueProvider<?, ?, ?> r) {
		if(l==r) return true;
		if(l==null || r==null) return false;
		Object lv = l.getValue();
		Object rv = r.getValue();
		return lv==rv || (lv!=null && lv.equals(rv));
	}
	
	static public boolean isValueEqual(Object lv, ValueProvider<?, ?, ?> r) {
		if(r==null) return false;
		Object rv = r.getValue();
		return lv==rv || (lv!=null && lv.equals(rv));
	}
}
