package edu.ustb.sei.mde.mobile.datastructure.operation;

import java.util.ArrayList;
import java.util.List;

public class OperationalCollectionValueProvider<S, V, SV> extends OperationalValueProvider<S, V, List<SV>> {
	
	public OperationalCollectionValueProvider(List<ValueProvider<S, V, SV>> providers) {
		this(providers, false);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public OperationalCollectionValueProvider(List<ValueProvider<S, V, SV>> providers, boolean hasCollectionOperation) {
		super();
		this.valueProviders = providers;
		this.externalOperations.add(new CompoundExternalOperation(providers));
		this.value = null;
		this.hasCollectionOperation = hasCollectionOperation;
	}
	
	/**
	 * if hasCollectionOperation is true, it means the collection value also explicitly participates in an operation;
	 * otherwise, the operation over the collection is fully delegated to every element provider
	 */
	protected boolean hasCollectionOperation;

	protected List<ValueProvider<S, V, SV>> valueProviders;

	protected List<SV> value;
	
	@Override
	public boolean isValueReady() {
		return valueProviders.stream().allMatch(v->v.isValueReady());
	}

	@Override
	public List<SV> getValue(S s, V v) {
		if(value==null) {
			value = new ArrayList<>();
			valueProviders.forEach(val->{
				SV x =val.getValue(s, v);
				if(x!=null) value.add(x);
			});
		}
		return value;
	}
	
	@Override
	public void explain(StringBuilder builder, int indent) {
		this.valueProviders.forEach(v->{
			v.explain(builder, indent+1);
		});
		
		indent(builder,indent);
		builder.append("{\n");
		
		this.externalOperations.forEach(o->{
			indent(builder, indent+1);
			builder.append(o.printableInformation());
			builder.append("\n");
		});
		indent(builder, indent);
		builder.append("}\n");
	}
	
	@Override
	public ValueProvider<S, V, List<SV>> getCore() {
		return this;
	}
}
