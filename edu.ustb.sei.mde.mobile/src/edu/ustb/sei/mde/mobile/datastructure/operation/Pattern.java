package edu.ustb.sei.mde.mobile.datastructure.operation;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

import edu.ustb.sei.mde.mobile.datastructure.Pair;

public class Pattern implements InformationPredicate{
	public Pattern(InformationKind kind, FeatureType type, Pair<String,String>[] keyMap) {
		this(new InformationKind[] {kind}, type, keyMap);
	}
	
	public Pattern(InformationKind kind, Pair<String,String>[] keyMap) {
		this(kind, FeatureType.containment, keyMap);
	}
	
	public Pattern(InformationKind[] kinds, Pair<String,String>[] keyMap) {
		this(kinds, FeatureType.containment, keyMap);
	}
	
	public Pattern(InformationKind[] kinds, FeatureType type, Pair<String,String>[] keyMap) {
		super();
		this.kinds = kinds;
		this.featureType = type;
		this.keyMap = new HashMap<String, String>();
		for(Pair<String, String> m : keyMap) {
			this.keyMap.put(m.first, m.second);
		}
	}
	
	protected InformationKind[] kinds;
	protected Map<String,String> keyMap;
	
	protected FeatureType featureType;
	
	public boolean check(Information info, Map<String,Object> context) {
		if(checkKind(info) && checkFeatureType(info)) {
			fillContext(info, context);
			return true;
		} else 
			return false;
	}

	private boolean checkKind(Information info) {
		for(InformationKind kind : kinds) {
			if(info.getKind()==kind) {
				return true;
			}
		}
		return false;
	}

	private boolean checkFeatureType(Information info) {
		if(any()) return true;
		
		EStructuralFeature f = info.getFeature();
		if(containmentOnly()) {
			if(f instanceof EReference && ((EReference)f).isContainment()) 
				return true;
			else return false;
		} else if(attributeOnly()) {
			if(f instanceof EAttribute) 
				return true;
			else return false;
		} else if (noncontainmentOnly()) {
			if(f instanceof EReference && !((EReference)f).isContainment()) 
				return true;
			else return false;
		} else return true;
	}
	
	private boolean any() {
		return this.featureType == FeatureType.any;
	}

	private boolean containmentOnly() {
		return this.featureType == FeatureType.containment;
	}
	
	private boolean attributeOnly() {
		return this.featureType == FeatureType.attribute;
	}
	
	private boolean noncontainmentOnly() {
		return this.featureType == FeatureType.noncontainment;
	}
	
	

	@SuppressWarnings("unchecked")
	protected <S> S getValue(Information info, String varName) {
		String key = this.keyMap.get(varName);
		if(key==null) throw new RuntimeException();
		return (S) info.get(key);
	}

	private void fillContext(Information info, Map<String, Object> context) {
		this.keyMap.forEach((v, k)->{
			context.put(v, this.getValue(info, v));
		});
	}
}