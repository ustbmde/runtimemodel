package edu.ustb.sei.mde.mobile.datastructure.synchronizer;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import edu.ustb.sei.mde.mobile.datastructure.Context;
import edu.ustb.sei.mde.mobile.datastructure.MobileUtil;
import edu.ustb.sei.mde.mobile.datastructure.Pair;
import edu.ustb.sei.mde.mobile.datastructure.Triple;
import edu.ustb.sei.mde.mobile.datastructure.operation.ExternalOperation;
import edu.ustb.sei.mde.mobile.datastructure.operation.ExternalOperation.FunctionBasedExternalOperation;
import edu.ustb.sei.mde.mobile.datastructure.operation.Information;
import edu.ustb.sei.mde.mobile.datastructure.operation.InformationKind;
import edu.ustb.sei.mde.mobile.datastructure.operation.ValueProvider;

public abstract class MultiValuedReferenceSynchronizer<ST, SFET, VT extends EObject, VFET extends EObject, C extends Context> extends ReferenceSynchronizer<ST, List<SFET>, SFET, VT, List<VFET>, VFET, C> {
	protected MultiValuedReferenceSynchronizer(EReference feature) {
		super(feature);
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<VFET> get(List<SFET> source, C context) {
		C newScope = (C) context.newScope();
		newScope.put(Context.REFRESH_MODE, ((EReference)this.getFeature()).isContainment());
		List<VFET> result = new ArrayList<>();
		source.forEach(s->result.add(((ElementSynchronizer<SFET, VFET, C>)this.valueSynchronizer).getValueSynchronizerForGet(s).get(s, context)));
		return result;
	}
	
	@Override
	public List<SFET> put(List<SFET> source, List<VFET> view, C context) {
		throw new UnsupportedOperationException();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <SO, VO> ValueProvider<SO, VO, List<SFET>> put(ValueProvider<SO, VO, List<SFET>> source,
			List<VFET> view, C context) {
		EReference ref = (EReference) getFeature();
		
		C newScope = (C) context.newScope();
		newScope.put(Context.REFRESH_MODE, ((EReference)this.getFeature()).isContainment());
		
		List<ValueProvider<SO, VO, SFET>> actions = new ArrayList<>();
		
		/*
		 * collection value provide
		 */
		if (this.valueSynchronizer instanceof ElementSynchronizer) {
			List<Pair<SFET, VFET>> aligned = new ArrayList<>();
			List<SFET> removedSource = new ArrayList<>(source.getValue());
			List<VFET> insertedView = new ArrayList<>();
			List<Triple<SFET, VFET, EObject>> movedOutView = new ArrayList<>();
			List<Triple<Object, SFET, VFET>> movedInView = new ArrayList<>();
			
			List<ValueProvider<?, ?, SFET>> newSources = new ArrayList<>();

			view.forEach(v -> {
				ValueProvider<?, ?, SFET> previous;
				if(newSources.isEmpty()==false) 
					previous = newSources.get(newSources.size()-1);
				else previous = null;
				
				ElementSynchronizer<SFET, VFET, C> syn = ((ElementSynchronizer<SFET, VFET, C>)this.valueSynchronizer).getValueSynchronizerForPut(v);
				int i = 0;
				for (; i < removedSource.size(); i++) {
					SFET s = removedSource.get(i);
					if (syn.align(s, v, newScope) != Boolean.FALSE)
						break;
				}
				if (i == removedSource.size()) { // additive
					Triple<Object, SFET, VFET> t;
					
					if(ref.isContainment()) {
						t = findMoveIn(v, syn, newScope);						
					} else {
						t = null;
					}

					if (t != null) {
						movedInView.add(t);
						
						ValueProvider<SO, VO, SFET> curSource = ValueProvider.value(t.second);
						ValueProvider<SO, VO, SFET> newSource = syn.put(curSource, t.third, newScope);
						
						FunctionBasedExternalOperation<SO, VO, SFET> moveInOperation = ExternalOperation.operation((so,vo,value)->{
							C nc = (C) newScope.newScope();
							nc.put(Context.PREV_SILBING, previous);
							externalMoveIn((ST) so, value, t.first, (VT) vo, t.third, nc);
						}, this::configureInformation, InformationKind.moveIn, 
								Pair.of(Information.OLD_VALUE, t.second),
								Pair.of(Information.VALUE, newSource), 
								Pair.of(Information.OLD_SOURCE_OBJECT, t.first),
								Pair.of(Information.VIEW_VALUE, t.third),
								Pair.of(Information.FEATURE, this.feature),
								Pair.of(Information.PREV_SILBING, previous));
						
						ValueProvider<SO, VO, SFET> moveInAction = ValueProvider.operation(newSource, moveInOperation);
						
						actions.add(moveInAction);
						newSources.add(newSource.getCore());

//						FunctionBasedExternalOperation<SO, VO, SFET> moveInPreOperation = ExternalOperation.operation((so,vo,value)->{
//							externalPreMoveIn((ST) so, t.second, t.first, (VT) vo, t.third, newScope);
//						}, this::configureInformation, InformationKind.moveIn_pre, 
//								Pair.of(Information.OLD_VALUE, t.second),
//								Pair.of(Information.VALUE, newSource), 
//								Pair.of(Information.OLD_SOURCE_OBJECT, t.first),
//								Pair.of(Information.VIEW_VALUE, t.third),
//								Pair.of(Information.FEATURE, this.feature));
//						
//						((OperationalValueProvider<SO, VO, SFET>) moveInAction).appendExternalOperation(moveInPreOperation);
//						
//						moveInPreOperation.setAssociatedOperation(moveInOperation);
						
					} else {
						/*
						 * value with external operation
						 */
						ValueProvider<SO, VO, SFET> newSource = syn.put((ValueProvider<SO, VO, SFET> ) null, v, newScope);
						actions.add(ValueProvider.operation(newSource,ExternalOperation.operation((so,vo,value)->{
							C nc = (C) newScope.newScope();
							nc.put(Context.PREV_SILBING, previous);
							this.externalInsert((ST) so, value, (VT) vo, v, nc);
						}, this::configureInformation, InformationKind.insert, 
								Pair.of(Information.VALUE, newSource),
								Pair.of(Information.VIEW_VALUE, v),
								Pair.of(Information.FEATURE, this.getFeature()),
								Pair.of(Information.PREV_SILBING, previous))));
						
						insertedView.add(v);
						newSources.add(newSource.getCore());
					}
				} else {
					/*
					 * value with external operation
					 */
					Pair<SFET, VFET> of = Pair.of(removedSource.get(i), v);
					ValueProvider<SO, VO, SFET> curSource = ValueProvider.value(of.first);
					ValueProvider<SO, VO, SFET> newSource = syn.put(curSource, of.second, newScope);
					
					actions.add(ValueProvider.operation(newSource,ExternalOperation.operation((so,vo,value)->{
						if(value!=of.first) {
							C nc = (C) newScope.newScope();
							nc.put(Context.PREV_SILBING, previous);
							externalReplace((ST) so, of.first, value, (VT) vo, v, nc);
						}
					}, this::configureInformation, InformationKind.replace, 
							Pair.of(Information.VALUE, newSource), 
							Pair.of(Information.VIEW_VALUE, v),
							Pair.of(Information.OLD_VALUE, of.first),
							Pair.of(Information.FEATURE, this.getFeature()),
							Pair.of(Information.PREV_SILBING, previous))));
					
					aligned.add(of);
					removedSource.remove(i);
					newSources.add(newSource.getCore());
				}
			});

			if (ref.isContainment()) {
				for (int i = removedSource.size() - 1; i >= 0; i--) {
					SFET s = removedSource.get(i);
					ElementSynchronizer<SFET, VFET, C> syn = ((ElementSynchronizer<SFET, VFET, C>)this.valueSynchronizer).getValueSynchronizerForGet(s);
					Triple<SFET, VFET, EObject> triple = findMoveOut(s, syn, newScope);
					if (triple != null) {
						removedSource.remove(i);
						movedOutView.add(triple);
					}
				}
			}

			movedOutView.forEach(t -> {
				/*
				 * value with external operation
				 */
//				ElementSynchronizer<SFET, VFET, C> syn = ((ElementSynchronizer<SFET, VFET, C>)this.valueSynchronizer).getValueSynchronizerForPut(t.second);
				ValueProvider<SO, VO, SFET> movedValue = ValueProvider.value(t.first);
				
				C shallowScope = (C) newScope.newScope();
				shallowScope.put(Context.REFRESH_MODE, false);
				// FIXME: moveOut may replace an object (causing object creation). 
				// However, when this creation is applied, it passes the old view parent to the creator
//				ValueProvider<SO, VO, SFET> newMovedValue = syn.put(movedValue, t.second, shallowScope);
				FunctionBasedExternalOperation<SO, VO, SFET> moveOutOperation = ExternalOperation.operation((so,vo,value)->{
					this.externalMoveOut((ST) so, t.first, (VT) vo, t.second, t.third, shallowScope);
				}, this::configureInformation, InformationKind.moveOut, 
						Pair.of(Information.OLD_VALUE, t.first),
						Pair.of(Information.VALUE, movedValue),
						Pair.of(Information.VIEW_VALUE, t.second),
						Pair.of(Information.NEW_VIEW_OBJECT, t.third),
						Pair.of(Information.FEATURE, this.getFeature()));
				ValueProvider<SO, VO, SFET> moveOutAction = ValueProvider.operation(movedValue, moveOutOperation);
				actions.add(moveOutAction);
				
//				FunctionBasedExternalOperation<SO, VO, SFET> moveOutPostOperation = ExternalOperation.operation((so,vo,value)->{
//					this.externalPostMoveOut((ST) so, value, (VT) vo, t.second, t.third, shallowScope);
//				}, this::configureInformation, InformationKind.moveOut_post, 
//						Pair.of(Information.OLD_VALUE, t.first),
//						Pair.of(Information.VALUE, newMovedValue),
//						Pair.of(Information.VIEW_VALUE, t.second),
//						Pair.of(Information.NEW_VIEW_OBJECT, t.third),
//						Pair.of(Information.FEATURE, this.getFeature()));
//				((OperationalValueProvider<SO, VO, SFET>)moveOutAction).appendExternalOperation(moveOutPostOperation);
//				
//				moveOutPostOperation.setAssociatedOperation(moveOutOperation);
				
			});

			removedSource.forEach(s -> {
				/*
				 * value with external operation
				 */
				ValueProvider<SO, VO, SFET> removedValue = ValueProvider.value(s);
				actions.add(ValueProvider.operation(removedValue,ExternalOperation.operation((so,vo,value)->{
					this.externalRemove((ST) so, s, (VT) vo, newScope);
				}, this::configureInformation, InformationKind.remove,
						Pair.of(Information.OLD_VALUE, removedValue.getValue()), 
						Pair.of(Information.VALUE, removedValue), 
						Pair.of(Information.FEATURE, this.getFeature()))));
			});

		}
		
		
		return ValueProvider.collection(actions);
	}


	protected Triple<SFET, VFET, EObject> findMoveOut(SFET s, ElementSynchronizer<SFET, VFET, C> syn, C newScope) {
		return MobileUtil.findMovedOut(s, this.getEcoreType(), syn::align, syn::canHandle, newScope);
	}


	protected Triple<Object, SFET, VFET> findMoveIn(VFET v, ElementSynchronizer<SFET, VFET, C> syn, C newScope) {
		return MobileUtil.findMovedIn(v, this.getJavaType(), syn::align, syn::canHandle, newScope);
	}
	
	@Override
	public void externalSet(ST source, List<SFET> value, VT view, List<VFET> viewValue, C context) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void externalReplace(ST source, SFET oldValue, SFET newValue, VT view, VFET viewValue, C context) {
		externalRemove(source, oldValue, view, context);
		externalInsert(source, newValue, view, viewValue, context);
	}
}