package edu.ustb.sei.mde.mobile.datastructure.operation;

public enum OperationOrder {
	former,
	later,
	unknown
}