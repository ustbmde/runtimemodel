package edu.ustb.sei.mde.mobile.datastructure.sourcepool;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.ustb.sei.mde.mobile.datastructure.synchronizer.FeatureSynchronizer;

public class SourcePoolObject {
	protected Object sourceObject;
	public SourcePoolObject getContainer() {
		return container;
	}

	public void setContainer(SourcePoolObject container) {
		this.container = container;
	}

	public FeatureSynchronizer<?, ?, ?, ?, ?, ?, ?> getContainerFeature() {
		return containerFeature;
	}

	public void setContainerFeature(FeatureSynchronizer<?, ?, ?, ?, ?, ?, ?> containerFeature) {
		this.containerFeature = containerFeature;
	}

	public Object getSourceObject() {
		return sourceObject;
	}

	public Map<FeatureSynchronizer<?, ?, ?, ?, ?, ?, ?>, List<SourcePoolObject>> getChildren() {
		return children;
	}

	protected SourcePoolObject container;
	protected FeatureSynchronizer<?, ?, ?, ?, ?, ?, ?> containerFeature;
	protected Map<FeatureSynchronizer<?, ?, ?, ?, ?, ?, ?>, List<SourcePoolObject>> children;
	
	public SourcePoolObject(Object sourceObject) {
		this.sourceObject = sourceObject;
		children = new HashMap<>();
	}
}
