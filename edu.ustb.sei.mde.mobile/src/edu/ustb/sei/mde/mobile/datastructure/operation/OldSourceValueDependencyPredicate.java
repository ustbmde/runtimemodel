package edu.ustb.sei.mde.mobile.datastructure.operation;

import java.util.Map;

import edu.ustb.sei.mde.mobile.datastructure.sourcepool.SourcePool;

public class OldSourceValueDependencyPredicate implements ContextPredicate {

	public OldSourceValueDependencyPredicate(SourcePool sourcePool, String leftKey, String rightKey, boolean leftDependsOnRight) {
		super();
		this.leftKey = leftKey;
		this.rightKey = rightKey;
		this.leftDependsOnRight = leftDependsOnRight;
		this.sourcePool = sourcePool;
	}
	@SuppressWarnings("rawtypes")
	@Override
	public boolean check(Map<String, Object> context) {
		Object left = context.get(leftKey);
		Object right = context.get(rightKey);
		
		if(left instanceof ValueProvider) {
			if(((ValueProvider) left).isValueReady())
				left = ((ValueProvider) left).getValue();
			else return false;
		}
		
		if(right instanceof ValueProvider) {
			if(((ValueProvider) right).isValueReady())
				right = ((ValueProvider) right).getValue();
			else return false;
		}
		
		if(leftDependsOnRight)
			return sourcePool.isSameOrContainer(left, right);
		else return sourcePool.isSameOrContainer(right, left);
	}
	
	
	protected String leftKey;
	protected String rightKey;
	protected boolean leftDependsOnRight;
	protected SourcePool sourcePool;
}