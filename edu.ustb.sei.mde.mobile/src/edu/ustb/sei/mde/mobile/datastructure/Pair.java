package edu.ustb.sei.mde.mobile.datastructure;

public class Pair<K, V> {
	public final K first;
	public final V second;
	
	
	protected Pair(K first, V second) {
		super();
		this.first = first;
		this.second = second;
	}
	
	static public <K,V> Pair<K,V> of(K f, V s) {
		return new Pair<K,V>(f,s);
	}
	
	@Override
	public String toString() {
		return first+"=>"+second;
	}
}
