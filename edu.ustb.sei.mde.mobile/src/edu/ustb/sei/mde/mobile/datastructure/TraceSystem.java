package edu.ustb.sei.mde.mobile.datastructure;

import java.util.ArrayList;
//import java.util.HashSet;
import java.util.List;
//import java.util.Set;
import java.util.function.Consumer;

import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.mde.mobile.datastructure.operation.ValueProvider;

public class TraceSystem {
	private List<TraceLink> traces;
	
	public TraceSystem() {
		traces = new ArrayList<TraceLink>();
//		visitedViewObjects = new HashSet<>();
	}
	
	public void add(TraceLink link) {
		traces.add(link);
//		visitedViewObjects.add(link.view);
	}
	
	public EObject resolve(String name, Object source) {
		for(TraceLink l : traces) {
			if(l.matchForward(name, source))
				return l.view;
		}
		return null;
	}
	
//	public void startTracingView() {
//		this.visitedViewObjects.clear();
//	}
//	public boolean isVisited(EObject view) {
//		return this.visitedViewObjects.contains(view);
//	}
//	public void markVisited(EObject view) {
//		this.visitedViewObjects.add(view);
//	}
//	private Set<EObject> visitedViewObjects;
	
	public ValueProvider<?, ?, ?> resolveFromView(EObject view) {
		for(TraceLink l : traces) {
			if(l.view==view) return (ValueProvider<?, ?, ?>) l.updatedSource;
		}
		return null;
	}
	
	public ValueProvider<?, ?, ?> resolve(String name, ValueProvider<?, ?, ?> source, EObject view) {
		if(source!=null && source.isValueReady()) {
			return (ValueProvider<?, ?, ?>) resolve(name, source.getValue(), view);
		} else {
			return (ValueProvider<?, ?, ?>) resolve(name, (Object) source, view);
		}
	}
	
	public Object resolve(String name, Object source, EObject view) {
		for(TraceLink l : traces) {
			if(l.matchBackward(name, source, view))
				return l.updatedSource;
		}
		return null;
	}
	
	public void forEach(Consumer<TraceLink> op) {
		this.traces.forEach(op);
	}
}
