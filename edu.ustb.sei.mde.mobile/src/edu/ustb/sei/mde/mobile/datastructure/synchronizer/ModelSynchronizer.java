package edu.ustb.sei.mde.mobile.datastructure.synchronizer;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.mde.mobile.datastructure.Context;
import edu.ustb.sei.mde.mobile.datastructure.MobileUtil;
import edu.ustb.sei.mde.mobile.datastructure.operation.DefaultOperationScheduler;
import edu.ustb.sei.mde.mobile.datastructure.operation.OperationScheduler;
import edu.ustb.sei.mde.mobile.datastructure.operation.ValueProvider;
import edu.ustb.sei.mde.mobile.datastructure.sourcepool.SourcePool;

public abstract class ModelSynchronizer<S,V extends EObject,C extends Context> {
	protected ModelSynchronizer() {
		elements = new ArrayList<>();
	}
	
	protected ElementSynchronizer<S,V,C> root;
	protected List<ElementSynchronizer<?,?,C>> elements;
	
	public void addElement(ElementSynchronizer<?,?,C> e) {
		int at = elements.size();
		
		for(int i=elements.size()-1;i>=0;i--) {
			ElementSynchronizer<?,?,C> sync = elements.get(i);
			if(sync.ecoreClass.isSuperTypeOf(e.ecoreClass)) {
				at = i;
			}
		}
		elements.add(at, e);
		e.container = this;
	}
	
//	@SuppressWarnings("unchecked")
//	public <SO,VO extends EObject> ElementSynchronizer<SO, VO, C> getElement(String name) {
//		for(ElementSynchronizer<?,?,C> sync : this.elements) {
//			if(sync.ecoreClass.getName().equals(name))
//				return (ElementSynchronizer<SO, VO, C>) sync;
//		}
//		return null;
//	}
	
	@SuppressWarnings("unchecked")
	public <SO,VO extends EObject> ElementSynchronizer<SO, VO, C> getElement(EClass clazz) {
		for(ElementSynchronizer<?,?,C> sync : this.elements) {
			if(sync.ecoreClass.isSuperTypeOf(clazz))
				return (ElementSynchronizer<SO, VO, C>) sync;
		}
		return null;
	}
	
	protected abstract C createContext();
	
	public V get(S sourceRoot) {
		C context = createContext();
		return get(sourceRoot, context);	
	}
	
	public V get(S sourceRoot, C context) {
		context.put(Context.REFRESH_MODE, true);
		return root.get(sourceRoot,context);		
	}
	
	public V get(S sourceRoot, V viewRoot) {
		C context = createContext();
		return get(sourceRoot, viewRoot, context);	
	}
	
	public V get(S sourceRoot, V viewRoot, C context) {
		context.put(Context.REFRESH_MODE, true);
		link(sourceRoot, viewRoot, context);
		return root.get(sourceRoot,context);		
	}
	
	/**
	 * Warning: this method should not be called before put! put will automatically create links
	 * @param sourceRoot
	 * @param viewRoot
	 * @param context
	 */
	public void link(S sourceRoot, V viewRoot, C context) {
		root.link(sourceRoot, viewRoot, context);
//		((TraceSystem) context.get(Context.TRACE_LINK)).startTracingView();
	}
	
	protected C lastPutContext;
	public ValueProvider<?, ?, S> put(S sourceRoot, V viewRoot) {
		lastPutContext = createContext();
		return put(sourceRoot, viewRoot, lastPutContext);
	}
	
	@SuppressWarnings("unchecked")
	public ValueProvider<?, ?, S> put(S sourceRoot, V viewRoot, C context) {
		context.put(Context.SOURCE_POOL, MobileUtil.constructSourcePool(sourceRoot, (ElementSynchronizer<Object, ?, Context>) this.root.getValueSynchronizerForPut(viewRoot), context));
		context.put(Context.VIEW_POOL, viewRoot);
		context.put(Context.REFRESH_MODE, true);
		ValueProvider<?, ?, S> var = root.put(ValueProvider.value(sourceRoot), viewRoot, context);
		return var;
	}
	
	final public OperationScheduler createScheduler() {
		return createScheduler(lastPutContext);
	}
	
	public OperationScheduler createScheduler(C context) {
		SourcePool pool = (SourcePool) context.get(Context.SOURCE_POOL);
		if(pool==null) return null;
		return new DefaultOperationScheduler(pool);
	}

	@SuppressWarnings("unchecked")
	final public <ST, VT extends EObject> ElementSynchronizer<ST, VT, C> find(String name) {
		for(ElementSynchronizer<?, ?, C> e : this.elements) {
			if(e.ecoreClass.getName().equals(name)) return (ElementSynchronizer<ST, VT, C>) e;
		}
		return null;
	}
	
//	@SuppressWarnings("unchecked")
//	final protected List<Object> collectRemovedObject(Object seed, ElementSynchronizer<Object, EObject, Context> startingPoint, Context context) {
//		List<Object> finalResult = new ArrayList<>();
//		startingPoint.features.stream().filter(f -> f.isContainment()).map(f -> {
//			if (f.feature.isMany()) {
//				List<Object> vs = (List<Object>) f.externalGet(seed, context);
//				if (vs != null) {
//					List<Object> result = new ArrayList<>();
//					vs.forEach(v -> {
//						result.addAll(((ElementSynchronizer<?, ?, ?>) f.valueSynchronizer).collectRemovedObject(v, context));
//					});
//					return result;
//				} else
//					return (List<Object>) Collections.emptyList();
//			} else {
//				Object v = f.externalGet(seed, context);
//				if (v != null)
//					return ((ElementSynchronizer<?, ?, ?>) f.valueSynchronizer).collectRemovedObject(v, context);
//				else
//					return (List<Object>) Collections.emptyList();
//			}
//
//		}).forEach(l -> {
//			finalResult.addAll(l);
//		});
//		return finalResult;
//	}
}
