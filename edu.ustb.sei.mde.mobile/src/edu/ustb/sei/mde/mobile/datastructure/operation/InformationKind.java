package edu.ustb.sei.mde.mobile.datastructure.operation;

public enum InformationKind {
	set,
	replace,
	insert,
	remove,
	moveIn,
//	moveIn_pre,
	moveOut,
//	moveOut_post,
	global_pre,
	global_post;
	
	static final InformationKind[] all = new InformationKind[] {set, replace, insert, remove, moveIn, moveOut};//, moveIn_pre, moveOut_post};
}