package edu.ustb.sei.mde.mobile.datastructure.operation;

import java.util.List;

public class CompoundExternalOperation<S, V, SV, VV> extends ExternalOperation<S, V, SV> {
	
	public CompoundExternalOperation(List<ValueProvider<SV, VV, ?>> providers, VV view) {
		super();
		this.providers = providers;
		this.view = view;
		collective = false;
	}
	
	public CompoundExternalOperation(List<ValueProvider<SV, VV, ?>> providers) {
		super();
		this.providers = providers;
		this.view = null;
		collective = true;
	}
	
	@Override
	public String printableInformation() {
		return "Compound";
	}

	protected List<ValueProvider<SV, VV, ?>> providers;
	protected VV view;
	private boolean collective;
	
	@SuppressWarnings("unchecked")
	@Override
	public void execute(S sourceObject, V viewObject, SV value) {
		if(collective) {
			providers.forEach(o->{
				o.performExternalOperations((SV)sourceObject, (VV)viewObject);
			});
		} else {
			providers.forEach(o->{
				o.performExternalOperations(value, view);
			});
		}
	}
	
}