package edu.ustb.sei.mde.mobile.datastructure.operation;

import java.util.function.Consumer;

import edu.ustb.sei.mde.mobile.datastructure.Pair;

public abstract class ExternalOperation<S, V, SV> {
	
	public ExternalOperation() {
		information = new Information(this);
		canceled = false;
	}
	
	protected Consumer<Information> configurer;
	
	protected boolean canceled;
	
	public void cancel() {
		this.canceled = true;
		System.out.println("An operation ("+information+") is cancelled");
		ExternalOperation<?, ?, ?> op = (ExternalOperation<?, ?, ?>) information.get(Information.ASSOCIATED_OPERATION);
		if(op!=null) op.canceled = true;
	}
	
	public abstract void execute(S sourceObject, V viewObject, SV value);
		
	static public <S,V,SV> FunctionBasedExternalOperation<S,V,SV> operation(ExternalOperationFunction<S, V, SV> function, Consumer<Information> configurer) {
		FunctionBasedExternalOperation<S,V,SV> op = new FunctionBasedExternalOperation<>(function);
		op.configurer = configurer;
		return op;
	}
	
	@SuppressWarnings("rawtypes")
	static public <S,V,SV> FunctionBasedExternalOperation<S,V,SV> operation(ExternalOperationFunction<S, V, SV> function, 
			Consumer<Information> configurer, InformationKind kind, Pair... info) {
		FunctionBasedExternalOperation<S,V,SV> op = new FunctionBasedExternalOperation<>(function);
		op.configurer = configurer;
		op.getInformation().setKind(kind);
		for(Pair p : info) {
			op.getInformation().put(p.first.toString(), p.second);
		}
		return op;
	}
	
	public String printableInformation() {
		return information.toString();
	}
	
	public void doConfigure() {
		if(this.configurer!=null)
			this.configurer.accept(this.getInformation());
	}
	
	protected Information information;
	
	public Information getInformation() {
		return information;
	}
	
	static public class FunctionBasedExternalOperation<S,V,SV> extends ExternalOperation<S, V, SV> {
		
		public FunctionBasedExternalOperation(ExternalOperationFunction<S, V, SV> function) {
			super();
			this.function = function;
		}

		protected ExternalOperationFunction<S,V,SV> function;
		
		@Override
		public void execute(S sourceObject, V viewObject, SV value) {
			if(canceled==false)
				function.execute(sourceObject, viewObject, value);
		}
	}
	
	@FunctionalInterface
	static public interface ExternalOperationFunction<S,V,SV> {
		void execute(S s, V v, SV value);
	}

	public void setPairedOperation(ExternalOperation<?, ?, ?> operation) {
		this.getInformation().setPairedOperation(operation);
	}
	
	public void setAssociatedOperation(ExternalOperation<?, ?, ?> operation) {
		this.getInformation().setAssociatedOperation(operation);
	}
	
	
	@Override
	public String toString() {
		return information.toString();
	}
}
