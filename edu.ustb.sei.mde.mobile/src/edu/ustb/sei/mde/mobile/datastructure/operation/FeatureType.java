package edu.ustb.sei.mde.mobile.datastructure.operation;

public enum FeatureType {
	any,
	attribute,
	containment,
	noncontainment
}