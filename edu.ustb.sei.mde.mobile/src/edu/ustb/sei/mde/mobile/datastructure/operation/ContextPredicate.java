package edu.ustb.sei.mde.mobile.datastructure.operation;

import java.util.Map;

public interface ContextPredicate {
	boolean check(Map<String, Object> context);
	
	static ContextPredicate TRUE = new ContextPredicate() {
		@Override
		public boolean check(Map<String, Object> context) {
			return true;
		}
	};
	
	static ContextPredicate and(ContextPredicate... preds) {
		return new ContextPredicate() {

			@Override
			public boolean check(Map<String, Object> context) {
				for(ContextPredicate p : preds) {
					if(p.check(context)==false) return false;
				}
				return true;
			}
			
		};
	}
	
	static ContextPredicate or(ContextPredicate... preds) {
		return new ContextPredicate() {
			@Override
			public boolean check(Map<String, Object> context) {
				for(ContextPredicate p : preds) {
					if(p.check(context)) return true;
				}
				return false;
			}
		};
	}
	
	static ContextPredicate not(ContextPredicate p) {
		return new ContextPredicate() {
			@Override
			public boolean check(Map<String, Object> context) {
				return !p.check(context);
			}
		};
	}
}