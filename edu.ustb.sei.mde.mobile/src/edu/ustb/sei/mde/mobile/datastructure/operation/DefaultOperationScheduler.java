package edu.ustb.sei.mde.mobile.datastructure.operation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import edu.ustb.sei.mde.mobile.datastructure.Pair;
import edu.ustb.sei.mde.mobile.datastructure.operation.OperationVisitor.OperationCollector;
import edu.ustb.sei.mde.mobile.datastructure.operation.OperationVisitor.ValueDependencyCollector;
import edu.ustb.sei.mde.mobile.datastructure.sourcepool.SourcePool;

public class DefaultOperationScheduler implements OperationScheduler {
	
	protected List<Pair<ExternalOperation<?, ?, ?>, Set<ExternalOperation<?, ?, ?>>>> schedule = new ArrayList<>();
	protected List<ScheduleRule> semanticRules = new ArrayList<>();
	
	protected ValueDependencyCollector valueDependency = new ValueDependencyCollector();
	protected OperationCollector operationCollector = new OperationCollector();
	protected SourcePool pool;
	
	public DefaultOperationScheduler(SourcePool pool) {
		this.pool = pool;
		buildHardRule();
		buildUserRules();
		buildDefaultRules();
		buildGlobalRules();
	}
	
	@SuppressWarnings("unchecked")
	final private void buildHardRule() {
		Pattern formerMoveOut = new Pattern(InformationKind.moveOut, new Pair[] {Pair.of("v1", Information.OLD_VALUE)});
		Pattern laterMoveIn = new Pattern(InformationKind.moveIn, new Pair[] {Pair.of("v2", Information.OLD_VALUE)});
		IdentityPredicate pred1 = new IdentityPredicate("v1", "v2");
		ScheduleRule rule1 = ScheduleRule.simpleRule("moveOut < moveIn", formerMoveOut, laterMoveIn, pred1, OperationOrder.former);
		this.semanticRules.add(rule1);
		
//		Pattern formerMoveOut = new Pattern(InformationKind.moveOut, new Pair[] {Pair.of("v1", Information.VALUE)});
//		Pattern laterMoveOut_post = new Pattern(InformationKind.moveOut_post, new Pair[] {Pair.of("v2", Information.VALUE)});
//		IdentityPredicate pred2 = new IdentityPredicate("v1", "v2");
//		ScheduleRule rule2 = ScheduleRule.simpleRule("moveOut < moveOutPost", formerMoveOut, laterMoveOut_post, pred2, OperationOrder.former);
//		this.semanticRules.add(rule2);
	}

	public SourcePool getSourcePool() {
		return pool;
	}
	
	public List<Pair<ExternalOperation<?, ?, ?>, Set<ExternalOperation<?, ?, ?>>>> getScheduleCopy() {
		List<Pair<ExternalOperation<?, ?, ?>, Set<ExternalOperation<?, ?, ?>>>> copy = new ArrayList<>();
		
		this.schedule.forEach(p->{
			copy.add(Pair.of(p.first, new HashSet<>(p.second)));
		});
		
		return copy;
	}
	
	protected void buildUserRules() {
	}
	
	/*
	 * insert(*,v1) before insert(v2,*) when v1 -> v2
	 * insert(*,v1) before set(v2,*), replace when v1 -> v2
	 * insert(*,v1) before moveIn(v2,*,*) when v1 -> v2
	 * insert(*,v1) unknown moveOut(v2,*,*), remove(v2,*) because there must be a moveIn(v3,*,*) between the two operations
	 * 
	 * remove(*,v1) after remove(v2,*)
	 * remove(*,v1) after moveOut(v2,*,*)/moveIn(*,v2,*)
	 * remove(*,v1) unknown others
	 * 
	 * set(*,v1) ?
	 * 
	 * replace(*,v1,v1') before insert(v2,*), set, replace, moveIn
	 *                   after remove, moveOut
	 * 
	 * moveIn(*,v1,*) before insert(v2,*), set(v2,*), moveIn(v2,*,*)
	 *                after moveOut(v2,*,*)/moveIn(*,v2,*)
	 * 
	 * moveOut(*,v1,*) after remove(v2,*), moveOut(v2,*,*)/moveIn(*,v2,*)
	 */
	final private void buildDefaultRules() {
		buildSetRules();
		buildReplaceRules();
		buildAdditiveRules();
		buildRemovalRules();
		buildNormalRules();		
	}

	@SuppressWarnings("unchecked")
	private void buildNormalRules() {
		IdentityPredicate pred = new IdentityPredicate("v1", "v2");

		Pattern former = new Pattern(InformationKind.all, FeatureType.attribute, new Pair[] {Pair.of("v1", Information.VIEW_OBJECT)}); 
		Pattern later = new Pattern(InformationKind.all, FeatureType.containment, new Pair[] {Pair.of("v2", Information.VIEW_OBJECT)}); 
		ScheduleRule rule1 = ScheduleRule.simpleRule("[NormalRule] Attributes should be updated after containment references", former, later, pred, OperationOrder.later);
		this.semanticRules.add(rule1);

		Pattern later2 = new Pattern(InformationKind.all, FeatureType.noncontainment, new Pair[] {Pair.of("v2", Information.VIEW_OBJECT)}); 
		ScheduleRule rule2 = ScheduleRule.simpleRule("[NormalRule] Attributes should be updated before noncontainment references", former, later2, pred, OperationOrder.former);
		this.semanticRules.add(rule2);
	}

	@SuppressWarnings("unchecked")
	final private void buildRemovalRules() {
		Pattern formerRemoval = new Pattern(new InformationKind[] {InformationKind.remove, InformationKind.moveOut}, new Pair[] {Pair.of("v1", Information.VALUE)});
		
		Pattern laterRemove = new Pattern(InformationKind.remove, new Pair[] {Pair.of("v2", Information.SOURCE_OBJECT)});
		Pattern laterMoveOut = new Pattern(InformationKind.moveOut, new Pair[] {Pair.of("v2", Information.SOURCE_OBJECT)});
		ValueDependencyPredicate pred1 = new ValueDependencyPredicate(this, "v1", "v2", false);
		ScheduleRule rule1 = ScheduleRule.simpleRule("[RemovalRule] Removal of children should be performed before the removal of parents (1)", formerRemoval, InformationPredicate.or(laterRemove, laterMoveOut), pred1, OperationOrder.later);
		this.semanticRules.add(rule1);
		
//		Pattern laterMoveIn_pre = new Pattern(InformationKind.moveIn_pre, new Pair[] {Pair.of("v2", Information.OLD_SOURCE_OBJECT)});
//		OldSourceValueDependencyPredicate pred2 = new OldSourceValueDependencyPredicate(getSourcePool(), "v1'", "v2", false);
//		ScheduleRule rule2 = ScheduleRule.simpleRule("[RemovalRule] Removal of children should be performed before the removal of parents (2)", formerRemoval, laterMoveIn_pre, pred2, OperationOrder.later);
//		this.semanticRules.add(rule2);
	}

	@SuppressWarnings("unchecked")
	final private void buildAdditiveRules() {
		Pattern formerAdditive = new Pattern(new InformationKind[] {InformationKind.insert, InformationKind.moveIn}, new Pair[] {Pair.of("v1", Information.VALUE)});
		
		Pattern laterSet = new Pattern(InformationKind.set, FeatureType.any, new Pair[] {Pair.of("v2", Information.SOURCE_OBJECT)});
		Pattern laterReplace = new Pattern(InformationKind.replace, new Pair[] {Pair.of("v2", Information.SOURCE_OBJECT)});
		Pattern laterInsert = new Pattern(InformationKind.insert, new Pair[] {Pair.of("v2", Information.SOURCE_OBJECT)});
		Pattern laterMoveIn = new Pattern(InformationKind.moveIn, new Pair[] {Pair.of("v2", Information.SOURCE_OBJECT)});
		
		ValueDependencyPredicate pred1 = new ValueDependencyPredicate(this, "v1", "v2", false);
		ScheduleRule rule1 = ScheduleRule.simpleRule("[AdditiveRule] Insertion of children should be performed after the insertion of parents (1)", formerAdditive, InformationPredicate.or(laterInsert, laterSet, laterMoveIn, laterReplace), pred1, OperationOrder.former);
		this.semanticRules.add(rule1);

//		Pattern laterMoveOut_post = new Pattern(InformationKind.moveOut_post, new Pair[] {Pair.of("vv2", Information.NEW_VIEW_OBJECT)}); // check insert
//		EObjectValueDependencyPredicate pred2 = new EObjectValueDependencyPredicate("vv1", "vv2", false);
//		ScheduleRule rule2 = ScheduleRule.simpleRule("[AdditiveRule] Insertion of children should be performed after the insertion of parents (2)", formerAdditive, laterMoveOut_post, pred2, OperationOrder.former);
//		this.semanticRules.add(rule2);
	}

	@SuppressWarnings("unchecked")
	final private void buildReplaceRules() {
		Pattern formerReplace = new Pattern(InformationKind.replace, new Pair[] {Pair.of("v1", Information.VALUE), Pair.of("v1'", Information.OLD_VALUE)});
		
		Pattern laterSet = new Pattern(InformationKind.set, FeatureType.any, new Pair[] {Pair.of("v2", Information.SOURCE_OBJECT)});
		Pattern laterReplace = new Pattern(InformationKind.replace, new Pair[] {Pair.of("v2", Information.SOURCE_OBJECT)});
		Pattern laterInsert = new Pattern(InformationKind.insert, new Pair[] {Pair.of("v2", Information.SOURCE_OBJECT)});
		Pattern laterMoveIn = new Pattern(InformationKind.moveIn, new Pair[] {Pair.of("v2", Information.SOURCE_OBJECT)});
		
		ValueDependencyPredicate pred1 = new ValueDependencyPredicate(this, "v1", "v2", false);
		ScheduleRule rule1 = ScheduleRule.simpleRule("[ReplaceRule] Insertion of children should be performed after the replacement (1)", formerReplace, InformationPredicate.or(laterInsert, laterSet, laterMoveIn, laterReplace), pred1, OperationOrder.former);
		this.semanticRules.add(rule1);

//		Pattern laterMoveOut_post = new Pattern(InformationKind.moveOut_post, new Pair[] {Pair.of("vv2", Information.NEW_VIEW_OBJECT)}); // check insert
//		EObjectValueDependencyPredicate pred2 = new EObjectValueDependencyPredicate("vv1", "vv2", false);
//		ScheduleRule rule2 = ScheduleRule.simpleRule("[ReplaceRule] Insertion of children should be performed after the replacement (1)", formerReplace, laterMoveOut_post, pred2, OperationOrder.former);
//		this.semanticRules.add(rule2);
		
		Pattern laterRemove = new Pattern(InformationKind.remove, new Pair[] {Pair.of("v2", Information.SOURCE_OBJECT)});
		Pattern laterMoveOut = new Pattern(InformationKind.moveOut, new Pair[] {Pair.of("v2", Information.SOURCE_OBJECT)});
		ScheduleRule rule3 = ScheduleRule.simpleRule("[ReplaceRule] Removal of children should be performed before the replacement (1)", formerReplace, InformationPredicate.or(laterRemove, laterMoveOut), pred1, OperationOrder.later);
		this.semanticRules.add(rule3);
		
//		Pattern laterMoveIn_pre = new Pattern(InformationKind.moveIn_pre, new Pair[] {Pair.of("v2", Information.OLD_SOURCE_OBJECT)});
//		OldSourceValueDependencyPredicate pred3 = new OldSourceValueDependencyPredicate(getSourcePool(), "v1'", "v2", false);
//		ScheduleRule rule4 = ScheduleRule.simpleRule("[ReplaceRule] Removal of children should be performed before the replacement (2)", formerReplace, laterMoveIn_pre, pred3, OperationOrder.later);
//		this.semanticRules.add(rule4);
	}

	@SuppressWarnings("unchecked")
	final private void buildSetRules() {
		Pattern formerSet = new Pattern(InformationKind.set, new Pair[] {Pair.of("v1", Information.VALUE)});
		
		Pattern laterSet = new Pattern(InformationKind.set, FeatureType.any, new Pair[] {Pair.of("v2", Information.SOURCE_OBJECT)});
		Pattern laterReplace = new Pattern(InformationKind.replace, new Pair[] {Pair.of("v2", Information.SOURCE_OBJECT)});
		Pattern laterInsert = new Pattern(InformationKind.insert, new Pair[] {Pair.of("v2", Information.SOURCE_OBJECT)});
		Pattern laterMoveIn = new Pattern(InformationKind.moveIn, new Pair[] {Pair.of("v2", Information.SOURCE_OBJECT)});
		
		ValueDependencyPredicate pred1 = new ValueDependencyPredicate(this, "v1", "v2", false);
		ScheduleRule rule1 = ScheduleRule.simpleRule("[SetRule] Children additive updates should be performed after the setting of parents", 
				formerSet, InformationPredicate.or(laterInsert, laterSet, laterMoveIn, laterReplace), pred1, OperationOrder.former);
		this.semanticRules.add(rule1);

//		Pattern laterMoveOut_post = new Pattern(InformationKind.moveOut_post, new Pair[] {Pair.of("vv2", Information.NEW_VIEW_OBJECT)}); // check insert
//		EObjectValueDependencyPredicate pred2 = new EObjectValueDependencyPredicate("vv1", "vv2", false);
//		ScheduleRule rule2 = ScheduleRule.simpleRule("[SetRule] Children destructive updates should be performed after the setting of parents (1)",
//				formerSet, laterMoveOut_post, pred2, OperationOrder.former);
//		this.semanticRules.add(rule2);
		
		Pattern laterRemove = new Pattern(InformationKind.remove, new Pair[] {Pair.of("v2", Information.SOURCE_OBJECT)});
		Pattern laterMoveOut = new Pattern(InformationKind.moveOut, new Pair[] {Pair.of("v2", Information.SOURCE_OBJECT)});
		ScheduleRule rule3 = ScheduleRule.simpleRule("[SetRule] Children destructive updates should be performed after the setting of parents (2)",
				formerSet, InformationPredicate.or(laterRemove, laterMoveOut), pred1, OperationOrder.later);
		this.semanticRules.add(rule3);
		
//		Pattern laterMoveIn_pre = new Pattern(InformationKind.moveIn_pre, new Pair[] {Pair.of("v2", Information.OLD_SOURCE_OBJECT)});
//		OldSourceValueDependencyPredicate pred3 = new OldSourceValueDependencyPredicate(getSourcePool(), "v1", "v2", false);
//		ScheduleRule rule4 = ScheduleRule.simpleRule("[SetRule] Children destructive updates should be performed after the setting of parents (3)",
//				formerSet, laterMoveIn_pre, pred3, OperationOrder.later);
//		this.semanticRules.add(rule4);
	}

	@SuppressWarnings("unchecked")
	final private void buildGlobalRules() {
		
		Pattern former = new Pattern(InformationKind.global_post, FeatureType.any, new Pair[] {});
		Pattern former2 = new Pattern(InformationKind.global_pre, FeatureType.any, new Pair[] {});
		Pattern later = new Pattern(InformationKind.all, FeatureType.any, new Pair[] {});
		
		ScheduleRule rule1 = ScheduleRule.simpleRule("[GlobalRule] * < Global post",
				former, later, ContextPredicate.TRUE, OperationOrder.later);
		this.semanticRules.add(rule1);
		
		ScheduleRule rule2 = ScheduleRule.simpleRule("[GlobalRule] Global pre < *", former2, later, ContextPredicate.TRUE, OperationOrder.former);
		this.semanticRules.add(rule2);
	}
	
	protected void addDependency(ExternalOperation<?, ?, ?> client, ExternalOperation<?, ?, ?> target) {
		java.util.Optional<Pair<ExternalOperation<?, ?, ?>, Set<ExternalOperation<?, ?, ?>>>> optionalPair = schedule.stream().filter(p->p.first==client).findFirst();
		Set<ExternalOperation<?, ?, ?>> set;
		if(optionalPair.isPresent()) {
			set = optionalPair.get().second;
		} else {
			set = new HashSet<>();
			schedule.add(Pair.of(client, set));
		}
		if(target!=null) set.add(target);
	}

	@Override
	public OperationOrder compare(ExternalOperation<?, ?, ?> former, ExternalOperation<?, ?, ?> later) {
		Information formerInformation = former.getInformation();
		Information laterInformation = later.getInformation();
		
		OperationOrder idOrder = checkIDOrder(formerInformation, laterInformation);
		if(idOrder!=OperationOrder.unknown) return idOrder;
		else
			return checkSemanticOrder(formerInformation, laterInformation);
	}
	
	
	private OperationOrder checkSemanticOrder(Information formerInformation, Information laterInformation) {
		for(ScheduleRule r : this.semanticRules) {
			OperationOrder o = r.check(formerInformation, laterInformation);
			if(o!=OperationOrder.unknown) {
				if(formerInformation == laterInformation) {
					System.out.println("error: "+r.message());
					System.out.println(formerInformation.toString());
				}
				return o;
			}
		}
		return OperationOrder.unknown;
	}

	private OperationOrder checkIDOrder(Information formerInformation, Information laterInformation) {
		List<Object> formerDes = formerInformation.getDestroyedObjects();
		List<Object> formerPrd = formerInformation.getProducedObjects();
		List<Object> formerReq = formerInformation.getRequiredObjects();
		InformationKind formerKind = formerInformation.getKind();
		
		List<Object> laterDes = laterInformation.getDestroyedObjects();
		List<Object> laterPrd = laterInformation.getProducedObjects();
		List<Object> laterReq = laterInformation.getRequiredObjects();
		InformationKind laterKind = laterInformation.getKind();
		
		if(Collections.disjoint(formerDes, laterPrd)) {
			if(Collections.disjoint(laterDes, formerPrd)) {
				/*
				 * if later.require and former.des join
				 * 		if later is remove/moveOut, then later first
				 * 		else unknown (another operation is expected to be able to produce later.require)
				 * if later.require and former.prd join
				 * 		if later is insert, set, replace, then former first
				 * 		else unknown (another operation is expected to be able to destroy later.require)
				 */
				
				if(!Collections.disjoint(laterReq, formerDes)) {
					if(laterKind==InformationKind.remove || laterKind==InformationKind.moveOut)
						return OperationOrder.later;
				}
				
				if(!Collections.disjoint(laterReq, formerPrd)) {
					if(laterKind==InformationKind.insert || laterKind==InformationKind.set || laterKind==InformationKind.replace)
						return OperationOrder.former;
				}
				
				if(!Collections.disjoint(formerReq, laterDes)) {
					if(formerKind==InformationKind.remove || laterKind==InformationKind.moveOut)
						return OperationOrder.former;
				}
				
				if(!Collections.disjoint(formerReq, laterPrd)) {
					if(formerKind==InformationKind.insert || laterKind==InformationKind.set || laterKind==InformationKind.replace)
						return OperationOrder.later;
				}
				
				return OperationOrder.unknown;
			} else {
				return OperationOrder.later;
			}
		} else {
			if(Collections.disjoint(laterDes, formerPrd)) {
				return OperationOrder.former;
			} else {
				throw new UnsupportedOperationException();
			}
		}
	}

	protected boolean isValueDepend(ValueProvider<?, ?, ?> client, ValueProvider<?, ?, ?> target) {
		return valueDependency.isDependentOn(client, target);
	}

	@Override
	public ValueDependencyCollector getValueDependencies() {
		return this.valueDependency;
	}

	@Override
	public OperationCollector getOperations() {
		return this.operationCollector;
	}

	@Override
	public void schedule(ValueProvider<?, ?, ?> root) {
		this.getValueDependencies().visit(root); // collect value dependencies
		this.getOperations().visit(root); // collect and complete operation information
		
		List<Pair<ExternalOperation<?, ?, ?>, Information>> operations = this.getOperations().operationInformation.stream()
				.filter(p->!p.first.canceled)
				.collect(Collectors.toList());
		
		for(Pair<ExternalOperation<?, ?, ?>, Information> former : operations) {
			this.addDependency(former.first, null); // ensure existence
			for(Pair<ExternalOperation<?, ?, ?>, Information> later : operations) {
				OperationOrder order = compare(former.first, later.first);
				if (former == later) { // as a runtime check
					if(order!=OperationOrder.unknown)
						System.out.println("error in schedule");
				} else {
					if (order == OperationOrder.former) {
						this.addDependency(later.first, former.first);
					} else if (order == OperationOrder.later) {
						this.addDependency(former.first, later.first);
					}
				}
			}
		}
		
		// TODO: dead lock detection!
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		this.schedule.forEach(a->{
			builder.append(a.toString());
			builder.append("\n");
		});
		
		return builder.toString();
	}
}