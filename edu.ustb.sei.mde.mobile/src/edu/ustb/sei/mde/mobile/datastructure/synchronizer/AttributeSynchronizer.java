package edu.ustb.sei.mde.mobile.datastructure.synchronizer;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.mde.mobile.datastructure.Context;

public abstract class AttributeSynchronizer<ST, SFT, SFET, VT extends EObject, VFT, VFET, C extends Context>
		extends FeatureSynchronizer<ST, SFT, SFET, VT, VFT, VFET, C> {

	public AttributeSynchronizer(EAttribute feature) {
		super(feature);
	}

}
