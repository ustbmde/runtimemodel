package edu.ustb.sei.mde.mobile.datastructure;

import org.eclipse.emf.ecore.EObject;

public class TraceLink {
	public String ruleName;
	public Object originalSource;
	public EObject view;
	// TODO: updatedSource should be a ValueProvider
	public Object updatedSource;
	
	protected TraceLink(String ruleName, Object originalSource, EObject view, Object updatedSource) {
		super();
		this.ruleName = ruleName;
		this.originalSource = originalSource;
		this.view = view;
		this.updatedSource = updatedSource;
	}
	
	
	static public TraceLink forwardTrace(String name, Object source, EObject view) {
		return new TraceLink(name, source, view, source);
	}
	
	static public TraceLink backwardTrace(String name, Object originalSource, EObject view, Object updatedSource) {
		return new TraceLink(name, originalSource, view, updatedSource);
	}
	
	public boolean matchForward(String name, Object source) {
		return ruleName.equals(name) && source.equals(originalSource);
	}
	
	public boolean matchBackward(String name, Object source, EObject view) {
		return ruleName.equals(name) 
				&& ((originalSource==source || (originalSource!=null && originalSource.equals(source)))
						||(updatedSource==source)) // since source may be the updated version ?
				&& view==this.view;
	}
}
