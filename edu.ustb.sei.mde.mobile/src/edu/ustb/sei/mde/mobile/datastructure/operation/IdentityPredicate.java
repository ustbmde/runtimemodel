package edu.ustb.sei.mde.mobile.datastructure.operation;

import java.util.Map;

public class IdentityPredicate implements ContextPredicate {
	public IdentityPredicate(String leftKey, String rightKey) {
		super();
		this.leftKey = leftKey;
		this.rightKey = rightKey;
	}

	protected String leftKey;
	protected String rightKey;

	@Override
	public boolean check(Map<String, Object> context) {
		Object left = context.get(leftKey);
		Object right = context.get(rightKey);
		return left==right;
	}

}
