package edu.ustb.sei.mde.mobile.datastructure;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.mde.mobile.datastructure.sourcepool.SourcePool;
import edu.ustb.sei.mde.mobile.datastructure.sourcepool.SourcePoolObject;
import edu.ustb.sei.mde.mobile.datastructure.synchronizer.ElementSynchronizer;

public class MobileUtil {
	@SuppressWarnings("unchecked")
	static public <SV, VV extends EObject,C extends Context> Triple<SV, VV, EObject> findMovedOut(SV value, EClass eClass, Alignment<SV,VV, C> align, Predicate<EObject> filter, C context) {
		EObject viewPoolRoot = (EObject) context.get(Context.VIEW_POOL);
		Object oldAlignMode = context.put(Context.ALIGN_GLOBAL, true);
		if (viewPoolRoot != null) {
			TreeIterator<EObject> it = viewPoolRoot.eAllContents();
			while (it.hasNext()) {
				EObject o = it.next();
				if (filter.test(o)) {
					// FIXME
					if (align.align(value, (VV) o, context) == Boolean.TRUE) {
						context.put(Context.ALIGN_GLOBAL, oldAlignMode);
						return Triple.of(value, (VV) o, o.eContainer());
					}
				}
			}
		}
		context.put(Context.ALIGN_GLOBAL, oldAlignMode);
		return null;
	}
	
	@SuppressWarnings("unchecked")
	static public <SV, VV extends EObject,C extends Context> Triple<Object, SV, VV> findMovedIn(VV value, Class<?> jClass, Alignment<SV,VV, C> align, Predicate<Object> filter, C context) {
		SourcePool sourcePoolRoot = (SourcePool) context.get(Context.SOURCE_POOL);
		Object oldAlignMode = context.put(Context.ALIGN_GLOBAL, true);
		if(sourcePoolRoot!=null) {
			Iterator<SourcePoolObject> it = sourcePoolRoot.allObjects();
			while(it.hasNext()) {
				SourcePoolObject poolObject = it.next();
				Object sourceObject = poolObject.getSourceObject();
				if(filter.test(sourceObject)) {
					if(align.align((SV) sourceObject, value, context) == Boolean.TRUE ) {
						context.put(Context.ALIGN_GLOBAL, oldAlignMode);
						return Triple.of(poolObject.getContainer().getSourceObject(), (SV) sourceObject, value);
					}
				}
			}
		}
		context.put(Context.ALIGN_GLOBAL, oldAlignMode);
		return null;
	}
	
	static public SourcePool constructSourcePool(Object sourceRoot, ElementSynchronizer<Object, ?, Context> element, Context context) {
		List<SourcePoolObject> objects = internalConstructSourcePool(sourceRoot, element, context);
		SourcePool pool = new SourcePool(objects);
		return pool;
	}
	
	@SuppressWarnings("unchecked")
	static private List<SourcePoolObject> internalConstructSourcePool(Object sourceRoot, ElementSynchronizer<Object, ?, Context> element, Context context) {
		List<SourcePoolObject> result = new ArrayList<>();
		SourcePoolObject map = new SourcePoolObject(sourceRoot);
		element.getAllFeatures().stream().filter(f->f.isContainment()).forEach(f->{
			List<Object> children = null;
			if(f.getFeature().isMany()) {
				if(sourceRoot!=null)
					children = (List<Object>) f.externalGet(sourceRoot, context);
				else
					children = Collections.emptyList();
			} else {
				if(sourceRoot!=null) {
					Object externalGet = f.externalGet(sourceRoot, context);
					if(externalGet!=null)
						children = Collections.singletonList(externalGet);
					else children = Collections.emptyList();
					
				}
				else
					children = Collections.emptyList();
			}
			
			children.forEach(c->{
				List<SourcePoolObject> childrenPool = internalConstructSourcePool(c, ((ElementSynchronizer<Object, ?, Context>) f.getValueSynchronizer()).getValueSynchronizerForGet(c), context);
				childrenPool.forEach(p->{
					if(p.getContainer()==null) {
						p.setContainer(map);
						p.setContainerFeature(f);
					}
				});
				map.getChildren().put(f, childrenPool);
				result.addAll(childrenPool);			
			});
		});
		result.add(map);
		return result;
	}
	
	@FunctionalInterface
	public static interface Alignment<SV,VV, C> {
		boolean align(SV s, VV v, C c);
	}
}
