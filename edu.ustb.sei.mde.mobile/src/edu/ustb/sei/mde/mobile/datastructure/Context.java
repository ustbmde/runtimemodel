package edu.ustb.sei.mde.mobile.datastructure;

import java.util.HashMap;
import java.util.function.Supplier;

public interface Context {
	static public final String TRACE_LINK = "TRACE_SYSTEM";
	static public final String REFRESH_MODE = "REFRESH_MODE";
	public static final String SOURCE_POOL = "__SOURCE_POOL";
	public static final String VIEW_POOL = "__VIEW_POOL";
	public static final String ALIGN_GLOBAL = "__ALIGN_GLOBAL";
	public static final String PREV_SILBING = "__PREV_SILBING";
	
	Object get(Object key);
	Object getWithConstructor(Object key, java.util.function.Supplier<? extends Object> cons);
	Object put(Object key, Object value);
	Context newScope();
	
	
	static public Context newInstance() {
		HashMapBasedContext c = new HashMapBasedContext();
		c.init();
		return c;
	}
	
	default void init() {
		put(Context.TRACE_LINK, new TraceSystem());
		put(Context.REFRESH_MODE, true);
		put(Context.ALIGN_GLOBAL, false);
	}
	
	static class HashMapBasedContext extends HashMap<Object,Object> implements Context {
		private static final long serialVersionUID = -2384954576848981820L;
		private Context parent;
		
		@Override
		public Context newScope() {
			HashMapBasedContext c = new HashMapBasedContext();
			c.parent = this;
			return c;
		}
		
		@Override
		public Object get(Object key) {
			Object v = super.get(key);
			if(v==null && parent!=null) {
				return parent.get(key);
			} else return v;
		}

		@Override
		public Object getWithConstructor(Object key, Supplier<? extends Object> cons) {
			Object v = get(key);
			if(v==null) {
				v = cons.get();
				put(key,v);
			}
			return v;
		}
	}
}