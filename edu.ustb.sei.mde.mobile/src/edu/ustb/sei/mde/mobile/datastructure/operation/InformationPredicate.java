package edu.ustb.sei.mde.mobile.datastructure.operation;

import java.util.Map;

public interface InformationPredicate {
	boolean check(Information info, Map<String,Object> context);
	
	static InformationPredicate or(InformationPredicate... pred) {
		return new InformationPredicate() {
			@Override
			public boolean check(Information info, Map<String,Object> context) {
				for(InformationPredicate p : pred) {
					if(p.check(info, context)) return true;
				}
				return false;
			}
		};
	}
	
	static InformationPredicate and(InformationPredicate... pred) {
		return new InformationPredicate() {
			@Override
			public boolean check(Information info, Map<String,Object> context) {
				for(InformationPredicate p : pred) {
					if(p.check(info, context)==false) return false;
				}
				return true;
			}
		};
	}
	
	static InformationPredicate not(InformationPredicate p) {
		return new InformationPredicate() {
			@Override
			public boolean check(Information info, Map<String,Object> context) {
				return !p.check(info, context);
			}
		};
	}
}