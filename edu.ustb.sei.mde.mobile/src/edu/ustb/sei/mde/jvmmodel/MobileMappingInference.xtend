package edu.ustb.sei.mde.jvmmodel

import org.eclipse.emf.ecore.EStructuralFeature
import org.eclipse.xtext.common.types.JvmType
import org.eclipse.xtext.common.types.JvmDeclaredType
import org.eclipse.xtext.common.types.JvmFeature
import org.eclipse.xtext.common.types.JvmOperation
import org.eclipse.xtext.common.types.JvmField
import org.eclipse.emf.ecore.EAttribute
import org.eclipse.xtext.common.types.JvmVisibility
import org.eclipse.xtext.common.types.JvmGenericArrayTypeReference
import org.eclipse.xtext.common.types.JvmParameterizedTypeReference
import java.util.List
import edu.ustb.sei.mde.mobile.Model

class MobileMappingInference {
	def String inferGetterCode(JvmType type, String owner, EStructuralFeature feature, Model model) {
		if(type instanceof JvmDeclaredType) {
			val declType = type as JvmDeclaredType;
			for(JvmFeature jf : declType.allFeatures) {
				if(jf instanceof JvmField) {
					if(jf.visibility===JvmVisibility.PUBLIC && jf.static===false
						&& (jf as JvmField).simpleName.equals(feature.name)
					) { // name aligned
						val jfType = jf.type;
						if(feature instanceof EAttribute) {
							val ectName = if((feature as EAttribute).EAttributeType.instanceClass!==null) (feature as EAttribute).EAttributeType.instanceClass.canonicalName else null; 
							if(feature.many) {
								if(jfType instanceof JvmGenericArrayTypeReference) {
									val jctName = (jfType as JvmGenericArrayTypeReference).componentType.qualifiedName;
									if(jctName.equals(ectName)) {
										// convert array!
										return 'java.util.Arrays.asList('+owner+'.'+(jf as JvmField).simpleName+')';
									}
								} else if(jfType instanceof JvmParameterizedTypeReference) {
									if((jfType as JvmParameterizedTypeReference).arguments.size===1) {
										val containerName = (jfType as JvmParameterizedTypeReference).qualifiedName;
										val Class<?> cls = this.class.classLoader.loadClass(containerName);
										if(cls!==null && List.isAssignableFrom(cls)) {
											return owner+'.'+(jf as JvmField).simpleName;
										}
									}
								}
							} else {
								val jTypeName = jfType.qualifiedName;
								if(jTypeName.equals(ectName)) {
									return owner+'.'+(jf as JvmField).simpleName;
								}
							}
						} else {
							
						}
						
						return owner+'./*'+(jf as JvmField).simpleName+'*/';
					}
				} else if(jf instanceof JvmOperation) {
					
				}
			}
			return null
		} else {
			return null
		}
	}
}