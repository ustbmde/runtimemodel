/*
 * generated by Xtext 2.14.0
 */
package edu.ustb.sei.mde


/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
class MobileStandaloneSetup extends MobileStandaloneSetupGenerated {

	def static void doSetup() {
		new MobileStandaloneSetup().createInjectorAndDoEMFRegistration()
	}
}
