package edu.ustb.sei.mde.scoping

import org.eclipse.xtext.linking.impl.DefaultLinkingService
import org.eclipse.xtext.nodemodel.INode
import org.eclipse.xtext.linking.impl.IllegalNodeException
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.EReference
import edu.ustb.sei.mde.mobile.AdapterModel
import edu.ustb.sei.mde.mobile.MobilePackage
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.emf.ecore.EPackage
import java.util.Collections
import org.eclipse.xtext.scoping.IScope
import org.eclipse.xtext.resource.IEObjectDescription

class MobileLinkingService extends DefaultLinkingService {
	
	override getLinkedObjects(EObject context, EReference ref, INode node) throws IllegalNodeException {
		if(context instanceof AdapterModel 
			&& ref===MobilePackage.eINSTANCE.adapterModel_Metamodel
		) {
			try {
				val String uri = node.crossRefNodeAsString;
				val IScope scope = getScope(context, ref);

				for (IEObjectDescription d : scope.allElements) {
					if (d.qualifiedName.firstSegment.equals(uri))
						return Collections.singletonList(d.EObjectOrProxy);
				}

				val EPackage p = EcoreUtil2.loadEPackage(uri, this.class.classLoader);
				if(p !== null) return Collections.singletonList(p);

			} catch (Exception e) {
				return Collections.emptyList();
			}
		}
		super.getLinkedObjects(context, ref, node)
	}
	
}