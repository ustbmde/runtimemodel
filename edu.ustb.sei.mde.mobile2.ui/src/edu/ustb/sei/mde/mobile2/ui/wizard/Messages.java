package edu.ustb.sei.mde.mobile2.ui.wizard;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "edu.ustb.sei.mde.mobile2.ui.wizard.messages"; //$NON-NLS-1$
	
	public static String HelloWorldProject_Label;
	public static String HelloWorldProject_Description;
	public static String SimpleMobileProject_Label;
	public static String SimpleMobileProject_Description;
	public static String SimpleMobile2Project_Label;
	public static String SimpleMobile2Project_Description;
	
	static {
	// initialize resource bundle
	NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
