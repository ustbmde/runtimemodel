/**
 */
package edu.ustb.sei.mde.mobile.xml.provider;


import edu.ustb.sei.mde.mobile.xml.Document;
import edu.ustb.sei.mde.mobile.xml.XmlFactory;
import edu.ustb.sei.mde.mobile.xml.XmlPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link edu.ustb.sei.mde.mobile.xml.Document} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class DocumentItemProvider extends NodeItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DocumentItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addXmlEncodingPropertyDescriptor(object);
			addXmlVersionPropertyDescriptor(object);
			addFilePathPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Xml Encoding feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addXmlEncodingPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Document_xmlEncoding_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Document_xmlEncoding_feature", "_UI_Document_type"),
				 XmlPackage.Literals.DOCUMENT__XML_ENCODING,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Xml Version feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addXmlVersionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Document_xmlVersion_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Document_xmlVersion_feature", "_UI_Document_type"),
				 XmlPackage.Literals.DOCUMENT__XML_VERSION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the File Path feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFilePathPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_Document_filePath_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_Document_filePath_feature", "_UI_Document_type"),
				 XmlPackage.Literals.DOCUMENT__FILE_PATH,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(XmlPackage.Literals.NODE_CONTAINER__CHILDREN);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Document.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Document"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((Document)object).getXmlEncoding();
		return label == null || label.length() == 0 ?
			getString("_UI_Document_type") :
			getString("_UI_Document_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Document.class)) {
			case XmlPackage.DOCUMENT__XML_ENCODING:
			case XmlPackage.DOCUMENT__XML_VERSION:
			case XmlPackage.DOCUMENT__FILE_PATH:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case XmlPackage.DOCUMENT__CHILDREN:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(XmlPackage.Literals.NODE_CONTAINER__CHILDREN,
				 XmlFactory.eINSTANCE.createDocument()));

		newChildDescriptors.add
			(createChildParameter
				(XmlPackage.Literals.NODE_CONTAINER__CHILDREN,
				 XmlFactory.eINSTANCE.createProcessingInstruction()));

		newChildDescriptors.add
			(createChildParameter
				(XmlPackage.Literals.NODE_CONTAINER__CHILDREN,
				 XmlFactory.eINSTANCE.createEntityReference()));

		newChildDescriptors.add
			(createChildParameter
				(XmlPackage.Literals.NODE_CONTAINER__CHILDREN,
				 XmlFactory.eINSTANCE.createElement()));

		newChildDescriptors.add
			(createChildParameter
				(XmlPackage.Literals.NODE_CONTAINER__CHILDREN,
				 XmlFactory.eINSTANCE.createCharacterData()));

		newChildDescriptors.add
			(createChildParameter
				(XmlPackage.Literals.NODE_CONTAINER__CHILDREN,
				 XmlFactory.eINSTANCE.createText()));

		newChildDescriptors.add
			(createChildParameter
				(XmlPackage.Literals.NODE_CONTAINER__CHILDREN,
				 XmlFactory.eINSTANCE.createComment()));

		newChildDescriptors.add
			(createChildParameter
				(XmlPackage.Literals.NODE_CONTAINER__CHILDREN,
				 XmlFactory.eINSTANCE.createNotation()));

		newChildDescriptors.add
			(createChildParameter
				(XmlPackage.Literals.NODE_CONTAINER__CHILDREN,
				 XmlFactory.eINSTANCE.createCDATASection()));
	}

}
