package edu.ustb.sei.state.adapter;

import edu.ustb.sei.mde.runtimemodel.generator.*;
import org.eclipse.emf.ecore.*;
import edu.ustb.sei.mde.runtimemodel.modeladapter.*;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.common.notify.Adapter;

public class StateAdapterFactory extends BaseAdapterFactory {

public StateAdapterFactory(ModelService modelService, EPackage modelPackage) {
super(modelService, modelPackage);
}

@Override
protected Adapter createPackageAdapter(EPackage eObject) {
return new PackageAdapter(modelService, this);
}

@Override
protected Adapter createModelAdapter(EClass clazz, EObject eObject) {
String clsName = clazz.getName();

return null;
}

}