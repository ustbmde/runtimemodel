package edu.ustb.sei.state.adapter;

import edu.ustb.sei.mde.runtimemodel.generator.*;
import org.eclipse.emf.ecore.*;
import edu.ustb.sei.mde.runtimemodel.modeladapter.*;
import org.eclipse.emf.common.util.URI;


public class StateModelService extends ModelService {
final public static ModelService INSTANCE = new StateModelService();

public StateModelService() {
super();
}

@Override
protected void loadMetamodel() {
EPackage metamodel=edu.ustb.sei.state.StatePackage.eINSTANCE;
loadMetamodel(metamodel);
}

@Override
public EStructuralFeature getIDFeature(EObject obj) {
EClass cls = obj.eClass();
String clsName = cls.getName();
 
throw new UnsupportedOperationException();
}

private BaseAdapterFactory factory;
@Override
public BaseAdapterFactory getRuntimeModelAdapterFactory() {
if(factory==null) {
factory = new StateAdapterFactory(this, metamodel);
}
return factory;
}
@Override
public long getAutoRefreshingRate() {
return 1000;
}

@Override
public AbstractTimer getTimer() {
return new edu.ustb.sei.state.adapter.StateTimer(this);
}
}