package edu.ustb.sei.state.adapter;

import java.util.TimerTask;

import org.eclipse.emf.common.notify.Notification;

import edu.ustb.sei.mde.runtimemodel.modeladapter.AbstractTimer;
import edu.ustb.sei.mde.runtimemodel.modeladapter.ModelService;
import edu.ustb.sei.state.Statechart;

public class StateTimer extends AbstractTimer {

	public StateTimer(ModelService s) {
		super(s);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void notifyChanged(Notification notification) {
		// TODO Auto-generated method stub

	}

	@Override
	protected TimerTask createTask() {
		return new StateTimerTask(this);
	}
	
	@Override
	public void begin() {
		((StateTimerTask)this.timerTask).begin();
	}
	
	@Override
	public void end() {
		// TODO Auto-generated method stub
		super.end();
	}

}

class StateTimerTask extends TimerTask {
	public StateTimerTask(AbstractTimer timer) {
		this.host = timer;
	}
	
	private AbstractTimer host;
	
	public void begin() {
		Statechart chart = (Statechart) host.getResource().getContents().get(0);
		if(chart.getCurrentState()==null) 
			chart.doTransition(chart.getInitial());
		else
			chart.doTransition(chart.getCurrentState());
	}

	@Override
	public void run() {
		Statechart chart = (Statechart) host.getResource().getContents().get(0);
		chart.run();
		if(chart.getCurrentState()==chart.getFinal()) {
			this.cancel();
		}
	}
	
	@Override
	public boolean cancel() {
		Statechart chart = (Statechart) host.getResource().getContents().get(0);
		if(chart.getCurrentState()!=null)
			chart.getCurrentState().leave();
		return super.cancel();
	}
	
}
