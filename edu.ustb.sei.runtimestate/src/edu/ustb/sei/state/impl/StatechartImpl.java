/**
 */
package edu.ustb.sei.state.impl;

import edu.ustb.sei.state.FinalState;
import edu.ustb.sei.state.InitialState;
import edu.ustb.sei.state.State;
import edu.ustb.sei.state.StatePackage;
import edu.ustb.sei.state.Statechart;
import edu.ustb.sei.state.Transition;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import java.util.Timer;
import java.util.logging.Logger;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.emf.henshin.interpreter.EGraph;
import org.eclipse.emf.henshin.interpreter.Engine;
import org.eclipse.emf.henshin.interpreter.UnitApplication;
import org.eclipse.emf.henshin.interpreter.impl.EGraphImpl;
import org.eclipse.emf.henshin.interpreter.impl.EngineImpl;
import org.eclipse.emf.henshin.interpreter.impl.UnitApplicationImpl;
import org.eclipse.emf.henshin.model.Unit;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>STATECHART</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.state.impl.StatechartImpl#getStates <em>States</em>}</li>
 *   <li>{@link edu.ustb.sei.state.impl.StatechartImpl#getInitial <em>Initial</em>}</li>
 *   <li>{@link edu.ustb.sei.state.impl.StatechartImpl#getFinal <em>Final</em>}</li>
 *   <li>{@link edu.ustb.sei.state.impl.StatechartImpl#getHost <em>Host</em>}</li>
 *   <li>{@link edu.ustb.sei.state.impl.StatechartImpl#getCurrentState <em>Current State</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StatechartImpl extends MinimalEObjectImpl.Container implements Statechart {
	/**
	 * The cached value of the '{@link #getStates() <em>States</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStates()
	 * @generated
	 * @ordered
	 */
	protected EList<State> states;

	/**
	 * The cached value of the '{@link #getInitial() <em>Initial</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitial()
	 * @generated
	 * @ordered
	 */
	protected InitialState initial;

	/**
	 * The cached value of the '{@link #getFinal() <em>Final</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinal()
	 * @generated
	 * @ordered
	 */
	protected FinalState final_;

	/**
	 * The cached value of the '{@link #getHost() <em>Host</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHost()
	 * @generated
	 * @ordered
	 */
	protected EObject host;

	/**
	 * The cached value of the '{@link #getCurrentState() <em>Current State</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentState()
	 * @generated
	 * @ordered
	 */
	protected State currentState;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StatechartImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatePackage.Literals.STATECHART;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<State> getStates() {
		if (states == null) {
			states = new EObjectContainmentEList<State>(State.class, this, StatePackage.STATECHART__STATES);
		}
		return states;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InitialState getInitial() {
		return initial;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInitial(InitialState newInitial, NotificationChain msgs) {
		InitialState oldInitial = initial;
		initial = newInitial;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatePackage.STATECHART__INITIAL, oldInitial, newInitial);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitial(InitialState newInitial) {
		if (newInitial != initial) {
			NotificationChain msgs = null;
			if (initial != null)
				msgs = ((InternalEObject)initial).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatePackage.STATECHART__INITIAL, null, msgs);
			if (newInitial != null)
				msgs = ((InternalEObject)newInitial).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatePackage.STATECHART__INITIAL, null, msgs);
			msgs = basicSetInitial(newInitial, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatePackage.STATECHART__INITIAL, newInitial, newInitial));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FinalState getFinal() {
		return final_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFinal(FinalState newFinal, NotificationChain msgs) {
		FinalState oldFinal = final_;
		final_ = newFinal;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatePackage.STATECHART__FINAL, oldFinal, newFinal);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFinal(FinalState newFinal) {
		if (newFinal != final_) {
			NotificationChain msgs = null;
			if (final_ != null)
				msgs = ((InternalEObject)final_).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatePackage.STATECHART__FINAL, null, msgs);
			if (newFinal != null)
				msgs = ((InternalEObject)newFinal).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatePackage.STATECHART__FINAL, null, msgs);
			msgs = basicSetFinal(newFinal, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatePackage.STATECHART__FINAL, newFinal, newFinal));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getHost() {
		if (host != null && host.eIsProxy()) {
			InternalEObject oldHost = (InternalEObject)host;
			host = eResolveProxy(oldHost);
			if (host != oldHost) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatePackage.STATECHART__HOST, oldHost, host));
			}
		}
		return host;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetHost() {
		return host;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setHost(EObject newHost) {
		EGraph graph = this.getHostGraph();

		EObject oldHost = host;
		host = newHost;
		
		
		if(graph!=null && oldHost!=newHost) {
			graph.clear();
			if(newHost!=null)
				graph.addGraph(newHost);
		}
		
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatePackage.STATECHART__HOST, oldHost, host));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getCurrentState() {
		if (currentState != null && currentState.eIsProxy()) {
			InternalEObject oldCurrentState = (InternalEObject)currentState;
			currentState = (State)eResolveProxy(oldCurrentState);
			if (currentState != oldCurrentState) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatePackage.STATECHART__CURRENT_STATE, oldCurrentState, currentState));
			}
		}
		return currentState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetCurrentState() {
		return currentState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentState(State newCurrentState) {
		State oldCurrentState = currentState;
		currentState = newCurrentState;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatePackage.STATECHART__CURRENT_STATE, oldCurrentState, currentState));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void run() {
		if(this.getCurrentState()!=null) {
			Transition t = this.getCurrentState().pick();
			if(t!=null)
				this.doTransition(t.getNext());
		}
	}

	
	public void doTransition(State nextState) {
		
		State curState = this.getCurrentState();
		if(curState!=null)
			curState.leave();
		
		if(curState!=nextState) {
			if(curState!=null)
				doAction(curState.getExit());
			
			doAction(nextState.getEnter());
		}
		
		this.setCurrentState(nextState);
		
		nextState.enter();

		doAction(nextState.getBody());
	}

	public void doAction(Unit body) {
		if(body==null)
			return;

		Engine engine = this.getEngine();
		if(engine==null)
			return;
		EGraph graph = this.getHostGraph();
		if(graph==null)
			return;
		
		UnitApplication application = new UnitApplicationImpl(engine, graph, body, null);
		application.execute(null);
		
		try {
			this.getHost().eResource().save(null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatePackage.STATECHART__STATES:
				return ((InternalEList<?>)getStates()).basicRemove(otherEnd, msgs);
			case StatePackage.STATECHART__INITIAL:
				return basicSetInitial(null, msgs);
			case StatePackage.STATECHART__FINAL:
				return basicSetFinal(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatePackage.STATECHART__STATES:
				return getStates();
			case StatePackage.STATECHART__INITIAL:
				return getInitial();
			case StatePackage.STATECHART__FINAL:
				return getFinal();
			case StatePackage.STATECHART__HOST:
				if (resolve) return getHost();
				return basicGetHost();
			case StatePackage.STATECHART__CURRENT_STATE:
				if (resolve) return getCurrentState();
				return basicGetCurrentState();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatePackage.STATECHART__STATES:
				getStates().clear();
				getStates().addAll((Collection<? extends State>)newValue);
				return;
			case StatePackage.STATECHART__INITIAL:
				setInitial((InitialState)newValue);
				return;
			case StatePackage.STATECHART__FINAL:
				setFinal((FinalState)newValue);
				return;
			case StatePackage.STATECHART__HOST:
				setHost((EObject)newValue);
				return;
			case StatePackage.STATECHART__CURRENT_STATE:
				setCurrentState((State)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatePackage.STATECHART__STATES:
				getStates().clear();
				return;
			case StatePackage.STATECHART__INITIAL:
				setInitial((InitialState)null);
				return;
			case StatePackage.STATECHART__FINAL:
				setFinal((FinalState)null);
				return;
			case StatePackage.STATECHART__HOST:
				setHost((EObject)null);
				return;
			case StatePackage.STATECHART__CURRENT_STATE:
				setCurrentState((State)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatePackage.STATECHART__STATES:
				return states != null && !states.isEmpty();
			case StatePackage.STATECHART__INITIAL:
				return initial != null;
			case StatePackage.STATECHART__FINAL:
				return final_ != null;
			case StatePackage.STATECHART__HOST:
				return host != null;
			case StatePackage.STATECHART__CURRENT_STATE:
				return currentState != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case StatePackage.STATECHART___RUN:
				run();
				return null;
			case StatePackage.STATECHART___DO_TRANSITION__STATE:
				doTransition((State)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}
	
	public Timer getTimer() {
		if(timer==null)
			timer = new Timer();
		return timer;
	}
	
	private Timer timer = null;
	
	private Engine henshinEngine = null;
	private EGraph hostGraph = null;
	
	public Engine getEngine() {
		if(henshinEngine==null) {
			henshinEngine = new EngineImpl();
		}
		return henshinEngine;
	}
	
	public EGraph getHostGraph() {
		if(hostGraph==null && this.host!=null) {
			hostGraph = new EGraphImpl(this.host);
		}
		
		return hostGraph;
	}
	
	private Logger logger = java.util.logging.Logger.getLogger("Statechart");
	public Logger getLogger() {
		return logger;
	}

} //STATECHARTImpl
