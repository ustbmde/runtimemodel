/**
 */
package edu.ustb.sei.state.impl;

import edu.ustb.sei.state.State;
import edu.ustb.sei.state.StatePackage;
import edu.ustb.sei.state.TimeGuard;
import edu.ustb.sei.state.Transition;

import java.lang.reflect.InvocationTargetException;
import java.util.Timer;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.henshin.interpreter.EGraph;
import org.eclipse.emf.henshin.interpreter.Engine;
import org.eclipse.emf.henshin.model.Rule;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.state.impl.TransitionImpl#getNext <em>Next</em>}</li>
 *   <li>{@link edu.ustb.sei.state.impl.TransitionImpl#getTimeGuard <em>Time Guard</em>}</li>
 *   <li>{@link edu.ustb.sei.state.impl.TransitionImpl#getTimeGuardType <em>Time Guard Type</em>}</li>
 *   <li>{@link edu.ustb.sei.state.impl.TransitionImpl#getModelGuard <em>Model Guard</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransitionImpl extends MinimalEObjectImpl.Container implements Transition {
	/**
	 * The cached value of the '{@link #getNext() <em>Next</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNext()
	 * @generated
	 * @ordered
	 */
	protected State next;

	/**
	 * The default value of the '{@link #getTimeGuard() <em>Time Guard</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeGuard()
	 * @generated
	 * @ordered
	 */
	protected static final long TIME_GUARD_EDEFAULT = 0L;

	/**
	 * The cached value of the '{@link #getTimeGuard() <em>Time Guard</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeGuard()
	 * @generated
	 * @ordered
	 */
	protected long timeGuard = TIME_GUARD_EDEFAULT;

	/**
	 * The default value of the '{@link #getTimeGuardType() <em>Time Guard Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeGuardType()
	 * @generated
	 * @ordered
	 */
	protected static final TimeGuard TIME_GUARD_TYPE_EDEFAULT = TimeGuard.AFTER;

	/**
	 * The cached value of the '{@link #getTimeGuardType() <em>Time Guard Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimeGuardType()
	 * @generated
	 * @ordered
	 */
	protected TimeGuard timeGuardType = TIME_GUARD_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getModelGuard() <em>Model Guard</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModelGuard()
	 * @generated
	 * @ordered
	 */
	protected Rule modelGuard;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatePackage.Literals.TRANSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State getNext() {
		if (next != null && next.eIsProxy()) {
			InternalEObject oldNext = (InternalEObject)next;
			next = (State)eResolveProxy(oldNext);
			if (next != oldNext) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatePackage.TRANSITION__NEXT, oldNext, next));
			}
		}
		return next;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public State basicGetNext() {
		return next;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNext(State newNext) {
		State oldNext = next;
		next = newNext;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatePackage.TRANSITION__NEXT, oldNext, next));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getTimeGuard() {
		return timeGuard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeGuard(long newTimeGuard) {
		long oldTimeGuard = timeGuard;
		timeGuard = newTimeGuard;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatePackage.TRANSITION__TIME_GUARD, oldTimeGuard, timeGuard));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeGuard getTimeGuardType() {
		return timeGuardType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeGuardType(TimeGuard newTimeGuardType) {
		TimeGuard oldTimeGuardType = timeGuardType;
		timeGuardType = newTimeGuardType == null ? TIME_GUARD_TYPE_EDEFAULT : newTimeGuardType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatePackage.TRANSITION__TIME_GUARD_TYPE, oldTimeGuardType, timeGuardType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Rule getModelGuard() {
		if (modelGuard != null && modelGuard.eIsProxy()) {
			InternalEObject oldModelGuard = (InternalEObject)modelGuard;
			modelGuard = (Rule)eResolveProxy(oldModelGuard);
			if (modelGuard != oldModelGuard) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, StatePackage.TRANSITION__MODEL_GUARD, oldModelGuard, modelGuard));
			}
		}
		return modelGuard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Rule basicGetModelGuard() {
		return modelGuard;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModelGuard(Rule newModelGuard) {
		Rule oldModelGuard = modelGuard;
		modelGuard = newModelGuard;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatePackage.TRANSITION__MODEL_GUARD, oldModelGuard, modelGuard));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void watch() {
		StatechartImpl statechart = (StatechartImpl) EcoreUtil.getRootContainer(this);
		Timer timer = statechart.getTimer();
		
		if(this.getTimeGuard()<=0) {
			if(this.getTimeGuardType()==TimeGuard.AFTER)
				this.setAvailableTemporally(true);
			else 
				this.setAvailableTemporally(false);
		} else {
			guard = new edu.ustb.sei.state.struct.TimeGuard(this);
			if(this.getTimeGuardType()==TimeGuard.AFTER)
				this.setAvailableTemporally(false);
			else 
				this.setAvailableTemporally(true);
			timer.schedule(guard, this.getTimeGuard());
		}
		
		
	}
	
	private edu.ustb.sei.state.struct.TimeGuard guard = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void stop() {
		if(guard!=null) {
			guard.cancel();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean canTrigger() {
		if(this.isAvailableTemporally()) {
			Rule rule = this.getModelGuard();
			if(rule==null)
				return true;
			else {
				StatechartImpl chart = (StatechartImpl) EcoreUtil.getRootContainer(this);
				Engine engine = chart.getEngine();
				if(engine==null)
					return false;
				EGraph graph = chart.getHostGraph();
				if(graph==null)
					return false;
				
				if(engine.findMatches(rule, graph, null) != null)
					return true;
				else
					return false;
			}
		} else
			return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatePackage.TRANSITION__NEXT:
				if (resolve) return getNext();
				return basicGetNext();
			case StatePackage.TRANSITION__TIME_GUARD:
				return getTimeGuard();
			case StatePackage.TRANSITION__TIME_GUARD_TYPE:
				return getTimeGuardType();
			case StatePackage.TRANSITION__MODEL_GUARD:
				if (resolve) return getModelGuard();
				return basicGetModelGuard();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatePackage.TRANSITION__NEXT:
				setNext((State)newValue);
				return;
			case StatePackage.TRANSITION__TIME_GUARD:
				setTimeGuard((Long)newValue);
				return;
			case StatePackage.TRANSITION__TIME_GUARD_TYPE:
				setTimeGuardType((TimeGuard)newValue);
				return;
			case StatePackage.TRANSITION__MODEL_GUARD:
				setModelGuard((Rule)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatePackage.TRANSITION__NEXT:
				setNext((State)null);
				return;
			case StatePackage.TRANSITION__TIME_GUARD:
				setTimeGuard(TIME_GUARD_EDEFAULT);
				return;
			case StatePackage.TRANSITION__TIME_GUARD_TYPE:
				setTimeGuardType(TIME_GUARD_TYPE_EDEFAULT);
				return;
			case StatePackage.TRANSITION__MODEL_GUARD:
				setModelGuard((Rule)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatePackage.TRANSITION__NEXT:
				return next != null;
			case StatePackage.TRANSITION__TIME_GUARD:
				return timeGuard != TIME_GUARD_EDEFAULT;
			case StatePackage.TRANSITION__TIME_GUARD_TYPE:
				return timeGuardType != TIME_GUARD_TYPE_EDEFAULT;
			case StatePackage.TRANSITION__MODEL_GUARD:
				return modelGuard != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case StatePackage.TRANSITION___WATCH:
				watch();
				return null;
			case StatePackage.TRANSITION___STOP:
				stop();
				return null;
			case StatePackage.TRANSITION___CAN_TRIGGER:
				return canTrigger();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (timeGuard: ");
		result.append(timeGuard);
		result.append(", timeGuardType: ");
		result.append(timeGuardType);
		result.append(')');
		return result.toString();
	}

	
	private boolean availableTemporally;

	public boolean isAvailableTemporally() {
		return availableTemporally;
	}

	public void setAvailableTemporally(boolean availableTemporally) {
		this.availableTemporally = availableTemporally;
	}
} //TransitionImpl
