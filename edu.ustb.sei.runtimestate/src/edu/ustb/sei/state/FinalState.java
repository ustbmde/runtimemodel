/**
 */
package edu.ustb.sei.state;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Final State</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.state.StatePackage#getFinalState()
 * @model
 * @generated
 */
public interface FinalState extends State {
} // FinalState
