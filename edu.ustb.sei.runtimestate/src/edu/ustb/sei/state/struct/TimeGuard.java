package edu.ustb.sei.state.struct;

import java.util.TimerTask;

import edu.ustb.sei.state.impl.TransitionImpl;

public class TimeGuard extends TimerTask {
	public TimeGuard(TransitionImpl transition) {
		super();
		this.transition = transition;
	}


	private TransitionImpl transition;

	@Override
	public void run() {
		this.transition.setAvailableTemporally(!this.transition.isAvailableTemporally());
	}

}
