/**
 */
package edu.ustb.sei.state;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Initial State</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.state.StatePackage#getInitialState()
 * @model
 * @generated
 */
public interface InitialState extends State {
} // InitialState
