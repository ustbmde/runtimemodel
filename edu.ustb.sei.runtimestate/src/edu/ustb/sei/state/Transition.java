/**
 */
package edu.ustb.sei.state;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.henshin.model.Rule;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.state.Transition#getNext <em>Next</em>}</li>
 *   <li>{@link edu.ustb.sei.state.Transition#getTimeGuard <em>Time Guard</em>}</li>
 *   <li>{@link edu.ustb.sei.state.Transition#getTimeGuardType <em>Time Guard Type</em>}</li>
 *   <li>{@link edu.ustb.sei.state.Transition#getModelGuard <em>Model Guard</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.state.StatePackage#getTransition()
 * @model
 * @generated
 */
public interface Transition extends EObject {
	/**
	 * Returns the value of the '<em><b>Next</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Next</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next</em>' reference.
	 * @see #setNext(State)
	 * @see edu.ustb.sei.state.StatePackage#getTransition_Next()
	 * @model required="true"
	 * @generated
	 */
	State getNext();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.state.Transition#getNext <em>Next</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Next</em>' reference.
	 * @see #getNext()
	 * @generated
	 */
	void setNext(State value);

	/**
	 * Returns the value of the '<em><b>Time Guard</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Guard</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Guard</em>' attribute.
	 * @see #setTimeGuard(long)
	 * @see edu.ustb.sei.state.StatePackage#getTransition_TimeGuard()
	 * @model default="0" required="true"
	 * @generated
	 */
	long getTimeGuard();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.state.Transition#getTimeGuard <em>Time Guard</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Guard</em>' attribute.
	 * @see #getTimeGuard()
	 * @generated
	 */
	void setTimeGuard(long value);

	/**
	 * Returns the value of the '<em><b>Time Guard Type</b></em>' attribute.
	 * The default value is <code>"after"</code>.
	 * The literals are from the enumeration {@link edu.ustb.sei.state.TimeGuard}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Guard Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Guard Type</em>' attribute.
	 * @see edu.ustb.sei.state.TimeGuard
	 * @see #setTimeGuardType(TimeGuard)
	 * @see edu.ustb.sei.state.StatePackage#getTransition_TimeGuardType()
	 * @model default="after" required="true"
	 * @generated
	 */
	TimeGuard getTimeGuardType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.state.Transition#getTimeGuardType <em>Time Guard Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Guard Type</em>' attribute.
	 * @see edu.ustb.sei.state.TimeGuard
	 * @see #getTimeGuardType()
	 * @generated
	 */
	void setTimeGuardType(TimeGuard value);

	/**
	 * Returns the value of the '<em><b>Model Guard</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Model Guard</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Model Guard</em>' reference.
	 * @see #setModelGuard(Rule)
	 * @see edu.ustb.sei.state.StatePackage#getTransition_ModelGuard()
	 * @model
	 * @generated
	 */
	Rule getModelGuard();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.state.Transition#getModelGuard <em>Model Guard</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Model Guard</em>' reference.
	 * @see #getModelGuard()
	 * @generated
	 */
	void setModelGuard(Rule value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void watch();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void stop();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean canTrigger();

} // Transition
