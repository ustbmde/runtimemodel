/**
 */
package edu.ustb.sei.mde.mobile.swt.provider;


import edu.ustb.sei.mde.mobile.swt.Composite;
import edu.ustb.sei.mde.mobile.swt.SwtFactory;
import edu.ustb.sei.mde.mobile.swt.SwtPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link edu.ustb.sei.mde.mobile.swt.Composite} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class CompositeItemProvider extends ScrollableItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(SwtPackage.Literals.COMPOSITE__CHILDREN);
			childrenFeatures.add(SwtPackage.Literals.COMPOSITE__LAYOUT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Composite.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Composite"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		Object labelValue = ((Composite)object).getSwtObject();
		String label = labelValue == null ? null : labelValue.toString();
		return label == null || label.length() == 0 ?
			getString("_UI_Composite_type") :
			getString("_UI_Composite_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Composite.class)) {
			case SwtPackage.COMPOSITE__CHILDREN:
			case SwtPackage.COMPOSITE__LAYOUT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(SwtPackage.Literals.COMPOSITE__CHILDREN,
				 SwtFactory.eINSTANCE.createButton()));

		newChildDescriptors.add
			(createChildParameter
				(SwtPackage.Literals.COMPOSITE__CHILDREN,
				 SwtFactory.eINSTANCE.createLabel()));

		newChildDescriptors.add
			(createChildParameter
				(SwtPackage.Literals.COMPOSITE__CHILDREN,
				 SwtFactory.eINSTANCE.createComposite()));

		newChildDescriptors.add
			(createChildParameter
				(SwtPackage.Literals.COMPOSITE__CHILDREN,
				 SwtFactory.eINSTANCE.createList()));

		newChildDescriptors.add
			(createChildParameter
				(SwtPackage.Literals.COMPOSITE__CHILDREN,
				 SwtFactory.eINSTANCE.createText()));

		newChildDescriptors.add
			(createChildParameter
				(SwtPackage.Literals.COMPOSITE__CHILDREN,
				 SwtFactory.eINSTANCE.createCCombo()));

		newChildDescriptors.add
			(createChildParameter
				(SwtPackage.Literals.COMPOSITE__CHILDREN,
				 SwtFactory.eINSTANCE.createShell()));

		newChildDescriptors.add
			(createChildParameter
				(SwtPackage.Literals.COMPOSITE__LAYOUT,
				 SwtFactory.eINSTANCE.createGridLayout()));
	}

}
