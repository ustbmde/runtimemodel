/**
 * generated by Xtext 2.11.0
 */
package edu.ustb.sei.modeladapter.ide;

import com.google.inject.Guice;
import com.google.inject.Injector;
import edu.ustb.sei.modeladapter.RuntimeModelCodeRuntimeModule;
import edu.ustb.sei.modeladapter.RuntimeModelCodeStandaloneSetup;
import edu.ustb.sei.modeladapter.ide.RuntimeModelCodeIdeModule;
import org.eclipse.xtext.util.Modules2;

/**
 * Initialization support for running Xtext languages as language servers.
 */
@SuppressWarnings("all")
public class RuntimeModelCodeIdeSetup extends RuntimeModelCodeStandaloneSetup {
  @Override
  public Injector createInjector() {
    RuntimeModelCodeRuntimeModule _runtimeModelCodeRuntimeModule = new RuntimeModelCodeRuntimeModule();
    RuntimeModelCodeIdeModule _runtimeModelCodeIdeModule = new RuntimeModelCodeIdeModule();
    return Guice.createInjector(Modules2.mixin(_runtimeModelCodeRuntimeModule, _runtimeModelCodeIdeModule));
  }
}
