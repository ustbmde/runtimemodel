/**
 */
package edu.ustb.sei.mde.mobile.swt.impl;

import edu.ustb.sei.mde.mobile.swt.LayoutData;
import edu.ustb.sei.mde.mobile.swt.SwtPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Layout Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class LayoutDataImpl extends SWTElementImpl implements LayoutData {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LayoutDataImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SwtPackage.Literals.LAYOUT_DATA;
	}

} //LayoutDataImpl
