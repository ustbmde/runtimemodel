/**
 */
package edu.ustb.sei.mde.mobile.swt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scrollable</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getScrollable()
 * @model abstract="true"
 * @generated
 */
public interface Scrollable extends Control {
} // Scrollable
