/**
 */
package edu.ustb.sei.mde.mobile.swt;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Widget</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getWidget()
 * @model abstract="true"
 * @generated
 */
public interface Widget extends SWTElement {
} // Widget
