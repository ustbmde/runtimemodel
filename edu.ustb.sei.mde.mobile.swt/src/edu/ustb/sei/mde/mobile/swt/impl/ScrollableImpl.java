/**
 */
package edu.ustb.sei.mde.mobile.swt.impl;

import edu.ustb.sei.mde.mobile.swt.Scrollable;
import edu.ustb.sei.mde.mobile.swt.SwtPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Scrollable</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ScrollableImpl extends ControlImpl implements Scrollable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScrollableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SwtPackage.Literals.SCROLLABLE;
	}

} //ScrollableImpl
