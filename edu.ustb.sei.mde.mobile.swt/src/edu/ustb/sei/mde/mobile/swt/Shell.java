/**
 */
package edu.ustb.sei.mde.mobile.swt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Shell</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.swt.Shell#getText <em>Text</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getShell()
 * @model
 * @generated
 */
public interface Shell extends Composite {

	/**
	 * Returns the value of the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Text</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Text</em>' attribute.
	 * @see #setText(String)
	 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getShell_Text()
	 * @model
	 * @generated
	 */
	String getText();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mobile.swt.Shell#getText <em>Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Text</em>' attribute.
	 * @see #getText()
	 * @generated
	 */
	void setText(String value);
} // Shell
