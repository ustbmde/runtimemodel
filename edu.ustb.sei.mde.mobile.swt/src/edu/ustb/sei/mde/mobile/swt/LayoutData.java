/**
 */
package edu.ustb.sei.mde.mobile.swt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Layout Data</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getLayoutData()
 * @model abstract="true"
 * @generated
 */
public interface LayoutData extends SWTElement {
} // LayoutData
