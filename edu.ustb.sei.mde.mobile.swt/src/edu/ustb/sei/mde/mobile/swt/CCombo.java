/**
 */
package edu.ustb.sei.mde.mobile.swt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CCombo</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getCCombo()
 * @model
 * @generated
 */
public interface CCombo extends Composite {
} // CCombo
