/**
 */
package edu.ustb.sei.mde.mobile.swt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>List</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getList()
 * @model
 * @generated
 */
public interface List extends Composite {
} // List
