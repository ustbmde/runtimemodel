/**
 */
package edu.ustb.sei.mde.mobile.swt;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.swt.Composite#getChildren <em>Children</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.swt.Composite#getLayout <em>Layout</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getComposite()
 * @model
 * @generated
 */
public interface Composite extends Scrollable {

	/**
	 * Returns the value of the '<em><b>Children</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.mobile.swt.Control}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' containment reference list.
	 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getComposite_Children()
	 * @model containment="true"
	 * @generated
	 */
	EList<Control> getChildren();

	/**
	 * Returns the value of the '<em><b>Layout</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Layout</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Layout</em>' containment reference.
	 * @see #setLayout(Layout)
	 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getComposite_Layout()
	 * @model containment="true"
	 * @generated
	 */
	Layout getLayout();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mobile.swt.Composite#getLayout <em>Layout</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Layout</em>' containment reference.
	 * @see #getLayout()
	 * @generated
	 */
	void setLayout(Layout value);
} // Composite
