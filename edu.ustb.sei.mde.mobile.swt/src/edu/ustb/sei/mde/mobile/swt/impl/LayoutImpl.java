/**
 */
package edu.ustb.sei.mde.mobile.swt.impl;

import edu.ustb.sei.mde.mobile.swt.Layout;
import edu.ustb.sei.mde.mobile.swt.SwtPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Layout</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class LayoutImpl extends SWTElementImpl implements Layout {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LayoutImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SwtPackage.Literals.LAYOUT;
	}

} //LayoutImpl
