/**
 */
package edu.ustb.sei.mde.mobile.swt;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Layout</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getLayout()
 * @model abstract="true"
 * @generated
 */
public interface Layout extends SWTElement {
} // Layout
