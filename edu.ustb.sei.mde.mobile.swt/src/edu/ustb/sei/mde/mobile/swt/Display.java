/**
 */
package edu.ustb.sei.mde.mobile.swt;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Display</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.swt.Display#getShells <em>Shells</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getDisplay()
 * @model
 * @generated
 */
public interface Display extends SWTElement {

	/**
	 * Returns the value of the '<em><b>Shells</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.mobile.swt.Shell}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shells</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shells</em>' containment reference list.
	 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getDisplay_Shells()
	 * @model containment="true"
	 * @generated
	 */
	EList<Shell> getShells();
} // Display
