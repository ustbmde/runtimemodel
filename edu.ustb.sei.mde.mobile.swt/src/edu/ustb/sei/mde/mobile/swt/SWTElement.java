/**
 */
package edu.ustb.sei.mde.mobile.swt;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SWT Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.swt.SWTElement#getSwtObject <em>Swt Object</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getSWTElement()
 * @model abstract="true"
 * @generated
 */
public interface SWTElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Swt Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Swt Object</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Swt Object</em>' attribute.
	 * @see #setSwtObject(Object)
	 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getSWTElement_SwtObject()
	 * @model transient="true"
	 * @generated
	 */
	Object getSwtObject();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mobile.swt.SWTElement#getSwtObject <em>Swt Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Swt Object</em>' attribute.
	 * @see #getSwtObject()
	 * @generated
	 */
	void setSwtObject(Object value);

} // SWTElement
