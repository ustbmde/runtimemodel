/**
 */
package edu.ustb.sei.mde.mobile.swt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Grid Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.swt.GridData#getHorizontalAlignment <em>Horizontal Alignment</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.swt.GridData#getVerticalAlignment <em>Vertical Alignment</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.swt.GridData#isGrabExcessHorizontalSpace <em>Grab Excess Horizontal Space</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.swt.GridData#isGrabExcessVerticalSpace <em>Grab Excess Vertical Space</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.swt.GridData#getHorizontalSpan <em>Horizontal Span</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.swt.GridData#getVerticalSpan <em>Vertical Span</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getGridData()
 * @model
 * @generated
 */
public interface GridData extends LayoutData {

	/**
	 * Returns the value of the '<em><b>Horizontal Alignment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Horizontal Alignment</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Horizontal Alignment</em>' attribute.
	 * @see #setHorizontalAlignment(int)
	 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getGridData_HorizontalAlignment()
	 * @model
	 * @generated
	 */
	int getHorizontalAlignment();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mobile.swt.GridData#getHorizontalAlignment <em>Horizontal Alignment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Horizontal Alignment</em>' attribute.
	 * @see #getHorizontalAlignment()
	 * @generated
	 */
	void setHorizontalAlignment(int value);

	/**
	 * Returns the value of the '<em><b>Vertical Alignment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vertical Alignment</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vertical Alignment</em>' attribute.
	 * @see #setVerticalAlignment(int)
	 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getGridData_VerticalAlignment()
	 * @model
	 * @generated
	 */
	int getVerticalAlignment();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mobile.swt.GridData#getVerticalAlignment <em>Vertical Alignment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vertical Alignment</em>' attribute.
	 * @see #getVerticalAlignment()
	 * @generated
	 */
	void setVerticalAlignment(int value);

	/**
	 * Returns the value of the '<em><b>Grab Excess Horizontal Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Grab Excess Horizontal Space</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Grab Excess Horizontal Space</em>' attribute.
	 * @see #setGrabExcessHorizontalSpace(boolean)
	 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getGridData_GrabExcessHorizontalSpace()
	 * @model
	 * @generated
	 */
	boolean isGrabExcessHorizontalSpace();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mobile.swt.GridData#isGrabExcessHorizontalSpace <em>Grab Excess Horizontal Space</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Grab Excess Horizontal Space</em>' attribute.
	 * @see #isGrabExcessHorizontalSpace()
	 * @generated
	 */
	void setGrabExcessHorizontalSpace(boolean value);

	/**
	 * Returns the value of the '<em><b>Grab Excess Vertical Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Grab Excess Vertical Space</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Grab Excess Vertical Space</em>' attribute.
	 * @see #setGrabExcessVerticalSpace(boolean)
	 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getGridData_GrabExcessVerticalSpace()
	 * @model
	 * @generated
	 */
	boolean isGrabExcessVerticalSpace();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mobile.swt.GridData#isGrabExcessVerticalSpace <em>Grab Excess Vertical Space</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Grab Excess Vertical Space</em>' attribute.
	 * @see #isGrabExcessVerticalSpace()
	 * @generated
	 */
	void setGrabExcessVerticalSpace(boolean value);

	/**
	 * Returns the value of the '<em><b>Horizontal Span</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Horizontal Span</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Horizontal Span</em>' attribute.
	 * @see #setHorizontalSpan(int)
	 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getGridData_HorizontalSpan()
	 * @model
	 * @generated
	 */
	int getHorizontalSpan();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mobile.swt.GridData#getHorizontalSpan <em>Horizontal Span</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Horizontal Span</em>' attribute.
	 * @see #getHorizontalSpan()
	 * @generated
	 */
	void setHorizontalSpan(int value);

	/**
	 * Returns the value of the '<em><b>Vertical Span</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vertical Span</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vertical Span</em>' attribute.
	 * @see #setVerticalSpan(int)
	 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getGridData_VerticalSpan()
	 * @model
	 * @generated
	 */
	int getVerticalSpan();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mobile.swt.GridData#getVerticalSpan <em>Vertical Span</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vertical Span</em>' attribute.
	 * @see #getVerticalSpan()
	 * @generated
	 */
	void setVerticalSpan(int value);
} // GridData
