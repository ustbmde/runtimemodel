/**
 */
package edu.ustb.sei.mde.mobile.swt.impl;

import edu.ustb.sei.mde.mobile.swt.GridLayout;
import edu.ustb.sei.mde.mobile.swt.SwtPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Grid Layout</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.swt.impl.GridLayoutImpl#getNumOfColumns <em>Num Of Columns</em>}</li>
 * </ul>
 *
 * @generated
 */
public class GridLayoutImpl extends LayoutImpl implements GridLayout {
	/**
	 * The default value of the '{@link #getNumOfColumns() <em>Num Of Columns</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumOfColumns()
	 * @generated
	 * @ordered
	 */
	protected static final int NUM_OF_COLUMNS_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getNumOfColumns() <em>Num Of Columns</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumOfColumns()
	 * @generated
	 * @ordered
	 */
	protected int numOfColumns = NUM_OF_COLUMNS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GridLayoutImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SwtPackage.Literals.GRID_LAYOUT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumOfColumns() {
		return numOfColumns;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNumOfColumns(int newNumOfColumns) {
		int oldNumOfColumns = numOfColumns;
		numOfColumns = newNumOfColumns;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SwtPackage.GRID_LAYOUT__NUM_OF_COLUMNS, oldNumOfColumns, numOfColumns));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case SwtPackage.GRID_LAYOUT__NUM_OF_COLUMNS:
				return getNumOfColumns();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case SwtPackage.GRID_LAYOUT__NUM_OF_COLUMNS:
				setNumOfColumns((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case SwtPackage.GRID_LAYOUT__NUM_OF_COLUMNS:
				setNumOfColumns(NUM_OF_COLUMNS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case SwtPackage.GRID_LAYOUT__NUM_OF_COLUMNS:
				return numOfColumns != NUM_OF_COLUMNS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (numOfColumns: ");
		result.append(numOfColumns);
		result.append(')');
		return result.toString();
	}

} //GridLayoutImpl
