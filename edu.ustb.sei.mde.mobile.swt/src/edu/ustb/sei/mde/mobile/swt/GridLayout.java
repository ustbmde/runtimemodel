/**
 */
package edu.ustb.sei.mde.mobile.swt;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Grid Layout</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.swt.GridLayout#getNumOfColumns <em>Num Of Columns</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getGridLayout()
 * @model
 * @generated
 */
public interface GridLayout extends Layout {

	/**
	 * Returns the value of the '<em><b>Num Of Columns</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Num Of Columns</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Num Of Columns</em>' attribute.
	 * @see #setNumOfColumns(int)
	 * @see edu.ustb.sei.mde.mobile.swt.SwtPackage#getGridLayout_NumOfColumns()
	 * @model
	 * @generated
	 */
	int getNumOfColumns();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mobile.swt.GridLayout#getNumOfColumns <em>Num Of Columns</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Num Of Columns</em>' attribute.
	 * @see #getNumOfColumns()
	 * @generated
	 */
	void setNumOfColumns(int value);
} // GridLayout
