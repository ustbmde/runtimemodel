/**
 */
package edu.ustb.sei.mde.mobile.swt;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.mobile.swt.SwtFactory
 * @model kind="package"
 * @generated
 */
public interface SwtPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "swt";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.ustb.edu.cn/sei/mde/swt";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "swt";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SwtPackage eINSTANCE = edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl.init();

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.swt.impl.SWTElementImpl <em>SWT Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.swt.impl.SWTElementImpl
	 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getSWTElement()
	 * @generated
	 */
	int SWT_ELEMENT = 13;

	/**
	 * The feature id for the '<em><b>Swt Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWT_ELEMENT__SWT_OBJECT = 0;

	/**
	 * The number of structural features of the '<em>SWT Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWT_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>SWT Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SWT_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.swt.impl.WidgetImpl <em>Widget</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.swt.impl.WidgetImpl
	 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getWidget()
	 * @generated
	 */
	int WIDGET = 0;

	/**
	 * The feature id for the '<em><b>Swt Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WIDGET__SWT_OBJECT = SWT_ELEMENT__SWT_OBJECT;

	/**
	 * The number of structural features of the '<em>Widget</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WIDGET_FEATURE_COUNT = SWT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Widget</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WIDGET_OPERATION_COUNT = SWT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.swt.impl.ControlImpl <em>Control</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.swt.impl.ControlImpl
	 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getControl()
	 * @generated
	 */
	int CONTROL = 1;

	/**
	 * The feature id for the '<em><b>Swt Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL__SWT_OBJECT = WIDGET__SWT_OBJECT;

	/**
	 * The feature id for the '<em><b>Layout Data</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL__LAYOUT_DATA = WIDGET_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Control</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_FEATURE_COUNT = WIDGET_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Control</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTROL_OPERATION_COUNT = WIDGET_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.swt.impl.ScrollableImpl <em>Scrollable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.swt.impl.ScrollableImpl
	 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getScrollable()
	 * @generated
	 */
	int SCROLLABLE = 2;

	/**
	 * The feature id for the '<em><b>Swt Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCROLLABLE__SWT_OBJECT = CONTROL__SWT_OBJECT;

	/**
	 * The feature id for the '<em><b>Layout Data</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCROLLABLE__LAYOUT_DATA = CONTROL__LAYOUT_DATA;

	/**
	 * The number of structural features of the '<em>Scrollable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCROLLABLE_FEATURE_COUNT = CONTROL_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Scrollable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCROLLABLE_OPERATION_COUNT = CONTROL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.swt.impl.ButtonImpl <em>Button</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.swt.impl.ButtonImpl
	 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getButton()
	 * @generated
	 */
	int BUTTON = 3;

	/**
	 * The feature id for the '<em><b>Swt Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUTTON__SWT_OBJECT = CONTROL__SWT_OBJECT;

	/**
	 * The feature id for the '<em><b>Layout Data</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUTTON__LAYOUT_DATA = CONTROL__LAYOUT_DATA;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUTTON__TEXT = CONTROL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Button</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUTTON_FEATURE_COUNT = CONTROL_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Button</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUTTON_OPERATION_COUNT = CONTROL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.swt.impl.LabelImpl <em>Label</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.swt.impl.LabelImpl
	 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getLabel()
	 * @generated
	 */
	int LABEL = 4;

	/**
	 * The feature id for the '<em><b>Swt Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__SWT_OBJECT = CONTROL__SWT_OBJECT;

	/**
	 * The feature id for the '<em><b>Layout Data</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__LAYOUT_DATA = CONTROL__LAYOUT_DATA;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__TEXT = CONTROL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Label</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL_FEATURE_COUNT = CONTROL_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Label</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL_OPERATION_COUNT = CONTROL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.swt.impl.CompositeImpl <em>Composite</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.swt.impl.CompositeImpl
	 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getComposite()
	 * @generated
	 */
	int COMPOSITE = 5;

	/**
	 * The feature id for the '<em><b>Swt Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE__SWT_OBJECT = SCROLLABLE__SWT_OBJECT;

	/**
	 * The feature id for the '<em><b>Layout Data</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE__LAYOUT_DATA = SCROLLABLE__LAYOUT_DATA;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE__CHILDREN = SCROLLABLE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Layout</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE__LAYOUT = SCROLLABLE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Composite</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_FEATURE_COUNT = SCROLLABLE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Composite</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_OPERATION_COUNT = SCROLLABLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.swt.impl.ListImpl <em>List</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.swt.impl.ListImpl
	 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getList()
	 * @generated
	 */
	int LIST = 6;

	/**
	 * The feature id for the '<em><b>Swt Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST__SWT_OBJECT = COMPOSITE__SWT_OBJECT;

	/**
	 * The feature id for the '<em><b>Layout Data</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST__LAYOUT_DATA = COMPOSITE__LAYOUT_DATA;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST__CHILDREN = COMPOSITE__CHILDREN;

	/**
	 * The feature id for the '<em><b>Layout</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST__LAYOUT = COMPOSITE__LAYOUT;

	/**
	 * The number of structural features of the '<em>List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_FEATURE_COUNT = COMPOSITE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LIST_OPERATION_COUNT = COMPOSITE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.swt.impl.TextImpl <em>Text</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.swt.impl.TextImpl
	 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getText()
	 * @generated
	 */
	int TEXT = 7;

	/**
	 * The feature id for the '<em><b>Swt Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEXT__SWT_OBJECT = COMPOSITE__SWT_OBJECT;

	/**
	 * The feature id for the '<em><b>Layout Data</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEXT__LAYOUT_DATA = COMPOSITE__LAYOUT_DATA;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEXT__CHILDREN = COMPOSITE__CHILDREN;

	/**
	 * The feature id for the '<em><b>Layout</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEXT__LAYOUT = COMPOSITE__LAYOUT;

	/**
	 * The feature id for the '<em><b>Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEXT__DATA = COMPOSITE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Text</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEXT_FEATURE_COUNT = COMPOSITE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Text</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEXT_OPERATION_COUNT = COMPOSITE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.swt.impl.CComboImpl <em>CCombo</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.swt.impl.CComboImpl
	 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getCCombo()
	 * @generated
	 */
	int CCOMBO = 8;

	/**
	 * The feature id for the '<em><b>Swt Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCOMBO__SWT_OBJECT = COMPOSITE__SWT_OBJECT;

	/**
	 * The feature id for the '<em><b>Layout Data</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCOMBO__LAYOUT_DATA = COMPOSITE__LAYOUT_DATA;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCOMBO__CHILDREN = COMPOSITE__CHILDREN;

	/**
	 * The feature id for the '<em><b>Layout</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCOMBO__LAYOUT = COMPOSITE__LAYOUT;

	/**
	 * The number of structural features of the '<em>CCombo</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCOMBO_FEATURE_COUNT = COMPOSITE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>CCombo</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CCOMBO_OPERATION_COUNT = COMPOSITE_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.swt.impl.DisplayImpl <em>Display</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.swt.impl.DisplayImpl
	 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getDisplay()
	 * @generated
	 */
	int DISPLAY = 9;

	/**
	 * The feature id for the '<em><b>Swt Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPLAY__SWT_OBJECT = SWT_ELEMENT__SWT_OBJECT;

	/**
	 * The feature id for the '<em><b>Shells</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPLAY__SHELLS = SWT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Display</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPLAY_FEATURE_COUNT = SWT_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Display</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISPLAY_OPERATION_COUNT = SWT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.swt.impl.ShellImpl <em>Shell</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.swt.impl.ShellImpl
	 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getShell()
	 * @generated
	 */
	int SHELL = 10;

	/**
	 * The feature id for the '<em><b>Swt Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHELL__SWT_OBJECT = COMPOSITE__SWT_OBJECT;

	/**
	 * The feature id for the '<em><b>Layout Data</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHELL__LAYOUT_DATA = COMPOSITE__LAYOUT_DATA;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHELL__CHILDREN = COMPOSITE__CHILDREN;

	/**
	 * The feature id for the '<em><b>Layout</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHELL__LAYOUT = COMPOSITE__LAYOUT;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHELL__TEXT = COMPOSITE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Shell</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHELL_FEATURE_COUNT = COMPOSITE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Shell</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SHELL_OPERATION_COUNT = COMPOSITE_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.swt.impl.LayoutImpl <em>Layout</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.swt.impl.LayoutImpl
	 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getLayout()
	 * @generated
	 */
	int LAYOUT = 11;

	/**
	 * The feature id for the '<em><b>Swt Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAYOUT__SWT_OBJECT = SWT_ELEMENT__SWT_OBJECT;

	/**
	 * The number of structural features of the '<em>Layout</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAYOUT_FEATURE_COUNT = SWT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Layout</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAYOUT_OPERATION_COUNT = SWT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.swt.impl.GridLayoutImpl <em>Grid Layout</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.swt.impl.GridLayoutImpl
	 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getGridLayout()
	 * @generated
	 */
	int GRID_LAYOUT = 12;

	/**
	 * The feature id for the '<em><b>Swt Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_LAYOUT__SWT_OBJECT = LAYOUT__SWT_OBJECT;

	/**
	 * The feature id for the '<em><b>Num Of Columns</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_LAYOUT__NUM_OF_COLUMNS = LAYOUT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Grid Layout</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_LAYOUT_FEATURE_COUNT = LAYOUT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Grid Layout</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_LAYOUT_OPERATION_COUNT = LAYOUT_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.swt.impl.LayoutDataImpl <em>Layout Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.swt.impl.LayoutDataImpl
	 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getLayoutData()
	 * @generated
	 */
	int LAYOUT_DATA = 15;

	/**
	 * The feature id for the '<em><b>Swt Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAYOUT_DATA__SWT_OBJECT = SWT_ELEMENT__SWT_OBJECT;

	/**
	 * The number of structural features of the '<em>Layout Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAYOUT_DATA_FEATURE_COUNT = SWT_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Layout Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LAYOUT_DATA_OPERATION_COUNT = SWT_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.swt.impl.GridDataImpl <em>Grid Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.swt.impl.GridDataImpl
	 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getGridData()
	 * @generated
	 */
	int GRID_DATA = 14;

	/**
	 * The feature id for the '<em><b>Swt Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_DATA__SWT_OBJECT = LAYOUT_DATA__SWT_OBJECT;

	/**
	 * The feature id for the '<em><b>Horizontal Alignment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_DATA__HORIZONTAL_ALIGNMENT = LAYOUT_DATA_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Vertical Alignment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_DATA__VERTICAL_ALIGNMENT = LAYOUT_DATA_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Grab Excess Horizontal Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_DATA__GRAB_EXCESS_HORIZONTAL_SPACE = LAYOUT_DATA_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Grab Excess Vertical Space</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_DATA__GRAB_EXCESS_VERTICAL_SPACE = LAYOUT_DATA_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Horizontal Span</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_DATA__HORIZONTAL_SPAN = LAYOUT_DATA_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Vertical Span</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_DATA__VERTICAL_SPAN = LAYOUT_DATA_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Grid Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_DATA_FEATURE_COUNT = LAYOUT_DATA_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Grid Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_DATA_OPERATION_COUNT = LAYOUT_DATA_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.swt.Widget <em>Widget</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Widget</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.Widget
	 * @generated
	 */
	EClass getWidget();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.swt.Control <em>Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Control</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.Control
	 * @generated
	 */
	EClass getControl();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.mobile.swt.Control#getLayoutData <em>Layout Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Layout Data</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.Control#getLayoutData()
	 * @see #getControl()
	 * @generated
	 */
	EReference getControl_LayoutData();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.swt.Scrollable <em>Scrollable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scrollable</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.Scrollable
	 * @generated
	 */
	EClass getScrollable();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.swt.Button <em>Button</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Button</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.Button
	 * @generated
	 */
	EClass getButton();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.swt.Button#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.Button#getText()
	 * @see #getButton()
	 * @generated
	 */
	EAttribute getButton_Text();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.swt.Label <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Label</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.Label
	 * @generated
	 */
	EClass getLabel();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.swt.Label#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.Label#getText()
	 * @see #getLabel()
	 * @generated
	 */
	EAttribute getLabel_Text();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.swt.Composite <em>Composite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composite</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.Composite
	 * @generated
	 */
	EClass getComposite();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.mobile.swt.Composite#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Children</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.Composite#getChildren()
	 * @see #getComposite()
	 * @generated
	 */
	EReference getComposite_Children();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.mobile.swt.Composite#getLayout <em>Layout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Layout</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.Composite#getLayout()
	 * @see #getComposite()
	 * @generated
	 */
	EReference getComposite_Layout();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.swt.List <em>List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>List</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.List
	 * @generated
	 */
	EClass getList();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.swt.Text <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Text</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.Text
	 * @generated
	 */
	EClass getText();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.swt.Text#getData <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Data</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.Text#getData()
	 * @see #getText()
	 * @generated
	 */
	EAttribute getText_Data();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.swt.CCombo <em>CCombo</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CCombo</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.CCombo
	 * @generated
	 */
	EClass getCCombo();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.swt.Display <em>Display</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Display</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.Display
	 * @generated
	 */
	EClass getDisplay();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.mobile.swt.Display#getShells <em>Shells</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Shells</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.Display#getShells()
	 * @see #getDisplay()
	 * @generated
	 */
	EReference getDisplay_Shells();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.swt.Shell <em>Shell</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Shell</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.Shell
	 * @generated
	 */
	EClass getShell();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.swt.Shell#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.Shell#getText()
	 * @see #getShell()
	 * @generated
	 */
	EAttribute getShell_Text();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.swt.Layout <em>Layout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Layout</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.Layout
	 * @generated
	 */
	EClass getLayout();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.swt.GridLayout <em>Grid Layout</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Grid Layout</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.GridLayout
	 * @generated
	 */
	EClass getGridLayout();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.swt.GridLayout#getNumOfColumns <em>Num Of Columns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Of Columns</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.GridLayout#getNumOfColumns()
	 * @see #getGridLayout()
	 * @generated
	 */
	EAttribute getGridLayout_NumOfColumns();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.swt.SWTElement <em>SWT Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SWT Element</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.SWTElement
	 * @generated
	 */
	EClass getSWTElement();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.swt.SWTElement#getSwtObject <em>Swt Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Swt Object</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.SWTElement#getSwtObject()
	 * @see #getSWTElement()
	 * @generated
	 */
	EAttribute getSWTElement_SwtObject();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.swt.GridData <em>Grid Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Grid Data</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.GridData
	 * @generated
	 */
	EClass getGridData();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.swt.GridData#getHorizontalAlignment <em>Horizontal Alignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Horizontal Alignment</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.GridData#getHorizontalAlignment()
	 * @see #getGridData()
	 * @generated
	 */
	EAttribute getGridData_HorizontalAlignment();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.swt.GridData#getVerticalAlignment <em>Vertical Alignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Vertical Alignment</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.GridData#getVerticalAlignment()
	 * @see #getGridData()
	 * @generated
	 */
	EAttribute getGridData_VerticalAlignment();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.swt.GridData#isGrabExcessHorizontalSpace <em>Grab Excess Horizontal Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Grab Excess Horizontal Space</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.GridData#isGrabExcessHorizontalSpace()
	 * @see #getGridData()
	 * @generated
	 */
	EAttribute getGridData_GrabExcessHorizontalSpace();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.swt.GridData#isGrabExcessVerticalSpace <em>Grab Excess Vertical Space</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Grab Excess Vertical Space</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.GridData#isGrabExcessVerticalSpace()
	 * @see #getGridData()
	 * @generated
	 */
	EAttribute getGridData_GrabExcessVerticalSpace();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.swt.GridData#getHorizontalSpan <em>Horizontal Span</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Horizontal Span</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.GridData#getHorizontalSpan()
	 * @see #getGridData()
	 * @generated
	 */
	EAttribute getGridData_HorizontalSpan();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.swt.GridData#getVerticalSpan <em>Vertical Span</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Vertical Span</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.GridData#getVerticalSpan()
	 * @see #getGridData()
	 * @generated
	 */
	EAttribute getGridData_VerticalSpan();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.swt.LayoutData <em>Layout Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Layout Data</em>'.
	 * @see edu.ustb.sei.mde.mobile.swt.LayoutData
	 * @generated
	 */
	EClass getLayoutData();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SwtFactory getSwtFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.swt.impl.WidgetImpl <em>Widget</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.swt.impl.WidgetImpl
		 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getWidget()
		 * @generated
		 */
		EClass WIDGET = eINSTANCE.getWidget();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.swt.impl.ControlImpl <em>Control</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.swt.impl.ControlImpl
		 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getControl()
		 * @generated
		 */
		EClass CONTROL = eINSTANCE.getControl();

		/**
		 * The meta object literal for the '<em><b>Layout Data</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTROL__LAYOUT_DATA = eINSTANCE.getControl_LayoutData();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.swt.impl.ScrollableImpl <em>Scrollable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.swt.impl.ScrollableImpl
		 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getScrollable()
		 * @generated
		 */
		EClass SCROLLABLE = eINSTANCE.getScrollable();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.swt.impl.ButtonImpl <em>Button</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.swt.impl.ButtonImpl
		 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getButton()
		 * @generated
		 */
		EClass BUTTON = eINSTANCE.getButton();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BUTTON__TEXT = eINSTANCE.getButton_Text();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.swt.impl.LabelImpl <em>Label</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.swt.impl.LabelImpl
		 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getLabel()
		 * @generated
		 */
		EClass LABEL = eINSTANCE.getLabel();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LABEL__TEXT = eINSTANCE.getLabel_Text();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.swt.impl.CompositeImpl <em>Composite</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.swt.impl.CompositeImpl
		 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getComposite()
		 * @generated
		 */
		EClass COMPOSITE = eINSTANCE.getComposite();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE__CHILDREN = eINSTANCE.getComposite_Children();

		/**
		 * The meta object literal for the '<em><b>Layout</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE__LAYOUT = eINSTANCE.getComposite_Layout();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.swt.impl.ListImpl <em>List</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.swt.impl.ListImpl
		 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getList()
		 * @generated
		 */
		EClass LIST = eINSTANCE.getList();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.swt.impl.TextImpl <em>Text</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.swt.impl.TextImpl
		 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getText()
		 * @generated
		 */
		EClass TEXT = eINSTANCE.getText();

		/**
		 * The meta object literal for the '<em><b>Data</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEXT__DATA = eINSTANCE.getText_Data();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.swt.impl.CComboImpl <em>CCombo</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.swt.impl.CComboImpl
		 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getCCombo()
		 * @generated
		 */
		EClass CCOMBO = eINSTANCE.getCCombo();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.swt.impl.DisplayImpl <em>Display</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.swt.impl.DisplayImpl
		 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getDisplay()
		 * @generated
		 */
		EClass DISPLAY = eINSTANCE.getDisplay();

		/**
		 * The meta object literal for the '<em><b>Shells</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DISPLAY__SHELLS = eINSTANCE.getDisplay_Shells();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.swt.impl.ShellImpl <em>Shell</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.swt.impl.ShellImpl
		 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getShell()
		 * @generated
		 */
		EClass SHELL = eINSTANCE.getShell();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SHELL__TEXT = eINSTANCE.getShell_Text();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.swt.impl.LayoutImpl <em>Layout</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.swt.impl.LayoutImpl
		 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getLayout()
		 * @generated
		 */
		EClass LAYOUT = eINSTANCE.getLayout();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.swt.impl.GridLayoutImpl <em>Grid Layout</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.swt.impl.GridLayoutImpl
		 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getGridLayout()
		 * @generated
		 */
		EClass GRID_LAYOUT = eINSTANCE.getGridLayout();

		/**
		 * The meta object literal for the '<em><b>Num Of Columns</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRID_LAYOUT__NUM_OF_COLUMNS = eINSTANCE.getGridLayout_NumOfColumns();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.swt.impl.SWTElementImpl <em>SWT Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.swt.impl.SWTElementImpl
		 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getSWTElement()
		 * @generated
		 */
		EClass SWT_ELEMENT = eINSTANCE.getSWTElement();

		/**
		 * The meta object literal for the '<em><b>Swt Object</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SWT_ELEMENT__SWT_OBJECT = eINSTANCE.getSWTElement_SwtObject();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.swt.impl.GridDataImpl <em>Grid Data</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.swt.impl.GridDataImpl
		 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getGridData()
		 * @generated
		 */
		EClass GRID_DATA = eINSTANCE.getGridData();

		/**
		 * The meta object literal for the '<em><b>Horizontal Alignment</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRID_DATA__HORIZONTAL_ALIGNMENT = eINSTANCE.getGridData_HorizontalAlignment();

		/**
		 * The meta object literal for the '<em><b>Vertical Alignment</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRID_DATA__VERTICAL_ALIGNMENT = eINSTANCE.getGridData_VerticalAlignment();

		/**
		 * The meta object literal for the '<em><b>Grab Excess Horizontal Space</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRID_DATA__GRAB_EXCESS_HORIZONTAL_SPACE = eINSTANCE.getGridData_GrabExcessHorizontalSpace();

		/**
		 * The meta object literal for the '<em><b>Grab Excess Vertical Space</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRID_DATA__GRAB_EXCESS_VERTICAL_SPACE = eINSTANCE.getGridData_GrabExcessVerticalSpace();

		/**
		 * The meta object literal for the '<em><b>Horizontal Span</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRID_DATA__HORIZONTAL_SPAN = eINSTANCE.getGridData_HorizontalSpan();

		/**
		 * The meta object literal for the '<em><b>Vertical Span</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRID_DATA__VERTICAL_SPAN = eINSTANCE.getGridData_VerticalSpan();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.swt.impl.LayoutDataImpl <em>Layout Data</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.swt.impl.LayoutDataImpl
		 * @see edu.ustb.sei.mde.mobile.swt.impl.SwtPackageImpl#getLayoutData()
		 * @generated
		 */
		EClass LAYOUT_DATA = eINSTANCE.getLayoutData();

	}

} //SwtPackage
