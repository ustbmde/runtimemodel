package edu.ustb.sei.mde.mobile.file.actions;

import java.io.File;
import java.util.function.Supplier;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.action.Action;

import edu.ustb.sei.mde.mobile.datastructure.operation.OperationScheduler;
import edu.ustb.sei.mde.mobile.datastructure.operation.ValueProvider;
import edu.ustb.sei.mde.mobile.filemodel.FileModelUtil;

public class UpdateSourceAction extends Action {
	protected Supplier<Resource> resourceSupplier;

	public UpdateSourceAction(Supplier<Resource> resourceSupplier) {
		super("Update Source");
		this.resourceSupplier = resourceSupplier;
	}
	
	@Override
	public void run() {
		Resource resource = this.resourceSupplier.get();
		edu.ustb.sei.mde.mobile.file.Folder eFolder = (edu.ustb.sei.mde.mobile.file.Folder) resource.getContents().get(0);
		if(eFolder.getActualPath()==null) {
			
		} else {
			String path = eFolder.getActualPath();
			File jFolder = new File(path);
			@SuppressWarnings("unchecked")
			ValueProvider<?,?,File> provider = FileModelUtil.instance.put(jFolder, eFolder);
			OperationScheduler s = FileModelUtil.instance.createScheduler();
			s.schedule(provider);
			s.execute(provider);
		}
	}
}
