package edu.ustb.sei.mde.mobile.file.actions;

import java.io.File;
import java.util.function.Supplier;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.dialogs.MessageDialog;

import edu.ustb.sei.mde.mobile.file.Folder;
import edu.ustb.sei.mde.mobile.filemodel.FileModelUtil;


public class GenerateViewAction extends Action {
	protected Supplier<Resource> resourceSupplier;

	public GenerateViewAction(Supplier<Resource> resourceSupplier) {
		super("Get View");
		this.resourceSupplier = resourceSupplier;
	}

	@Override
	public void run() {
		Resource resource = this.resourceSupplier.get();
		EObject root = null;
		if(resource.getContents().isEmpty()==false) root = resource.getContents().get(0);
		
		if(root==null || !(root instanceof Folder)) {
			MessageDialog.openError(null, "Error in get", "This model bridge requires a root element with actualPath");
		} else {
			String path = ((Folder)root).getActualPath();
			File jFolder = new File(path);
			@SuppressWarnings("unchecked")
			edu.ustb.sei.mde.mobile.file.Folder newRoot = (edu.ustb.sei.mde.mobile.file.Folder) FileModelUtil.instance.get(jFolder);
			resource.getContents().clear();
			resource.getContents().add(newRoot);
		}
		
	}
}
