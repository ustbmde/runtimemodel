package edu.ustb.sei.mde.mobile.javamodel.actions;

import edu.ustb.sei.mde.mobile.javamodel.util.JavamodelUtil;

import java.util.function.Supplier;

import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jdt.core.IJavaModel;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.action.Action;

public class GenerateViewAction extends Action {
	protected Supplier<Resource> resourceSupplier;

	public GenerateViewAction(Supplier<Resource> resourceSupplier) {
		super("Get View");
		this.resourceSupplier = resourceSupplier;
	}

	@Override
	public void run() {
		Resource resource = this.resourceSupplier.get();
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IJavaModel java = JavaCore.create(root);
		@SuppressWarnings("unchecked")
		EObject eJava = JavamodelUtil.instance.get(java);
		resource.getContents().clear();
		resource.getContents().add(eJava);
	}
}
