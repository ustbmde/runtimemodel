package edu.ustb.sei.mde.mobile.javamodel.actions;

import java.util.function.Supplier;

import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jdt.core.IJavaModel;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.action.Action;

import edu.ustb.sei.mde.mobile.datastructure.operation.OperationScheduler;
import edu.ustb.sei.mde.mobile.datastructure.operation.ValueProvider;
import edu.ustb.sei.mde.mobile.javamodel.JavaModel;
import edu.ustb.sei.mde.mobile.javamodel.util.JavamodelUtil;

public class UpdateSourceAction extends Action {
	protected Supplier<Resource> resourceSupplier;

	public UpdateSourceAction(Supplier<Resource> resourceSupplier) {
		super("Update Source");
		this.resourceSupplier = resourceSupplier;
	}
	
	@Override
	public void run() {
		Resource resource = this.resourceSupplier.get();
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IJavaModel java = JavaCore.create(root);
		JavaModel eJava = (JavaModel) resource.getContents().get(0);
		@SuppressWarnings("unchecked")
		ValueProvider<?,?,?> provider = JavamodelUtil.instance.put(java, eJava);
		OperationScheduler s = JavamodelUtil.instance.createScheduler();
		s.schedule(provider);
		s.execute(provider);
	}
}
