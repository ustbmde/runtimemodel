/**
 */
package edu.ustb.sei.mde.runtimemodel.generator.provider;


import edu.ustb.sei.mde.runtimemodel.generator.ClassAdapter;
import edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter;
import edu.ustb.sei.mde.runtimemodel.generator.GeneratorFactory;
import edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link edu.ustb.sei.mde.runtimemodel.generator.FeatureAdapter} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class FeatureAdapterItemProvider 
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureAdapterItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAdaptedFeaturePropertyDescriptor(object);
			addGetPropertyDescriptor(object);
			addPostPropertyDescriptor(object);
			addPutPropertyDescriptor(object);
			addDeletePropertyDescriptor(object);
			addIdPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Adapted Feature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addAdaptedFeaturePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(new ItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FeatureAdapter_adaptedFeature_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FeatureAdapter_adaptedFeature_feature", "_UI_FeatureAdapter_type"),
				 GeneratorPackage.Literals.FEATURE_ADAPTER__ADAPTED_FEATURE,
				 true,
				 false,
				 true,
				 null,
				 getString("_UI_ConfigurationPropertyCategory"),
				 null){
				@Override
				protected Collection<?> getComboBoxObjects(Object object) {
					FeatureAdapter a = (FeatureAdapter) object;
					ClassAdapter c = (ClassAdapter)a.eContainer();
					if(c.getAdaptedClass()==null)
						return Collections.emptyList();
					else
						return new ArrayList<EStructuralFeature>(c.getAdaptedClass().getEAllStructuralFeatures());
				}
		});
	}

	/**
	 * This adds a property descriptor for the Get feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addGetPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FeatureAdapter_get_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FeatureAdapter_get_feature", "_UI_FeatureAdapter_type"),
				 GeneratorPackage.Literals.FEATURE_ADAPTER__GET,
				 true,
				 true,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_ServicePropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Post feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPostPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FeatureAdapter_post_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FeatureAdapter_post_feature", "_UI_FeatureAdapter_type"),
				 GeneratorPackage.Literals.FEATURE_ADAPTER__POST,
				 true,
				 true,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_ServicePropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Put feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPutPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FeatureAdapter_put_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FeatureAdapter_put_feature", "_UI_FeatureAdapter_type"),
				 GeneratorPackage.Literals.FEATURE_ADAPTER__PUT,
				 true,
				 true,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_ServicePropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Delete feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDeletePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FeatureAdapter_delete_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FeatureAdapter_delete_feature", "_UI_FeatureAdapter_type"),
				 GeneratorPackage.Literals.FEATURE_ADAPTER__DELETE,
				 true,
				 true,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 getString("_UI_ServicePropertyCategory"),
				 null));
	}

	/**
	 * This adds a property descriptor for the Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_FeatureAdapter_id_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_FeatureAdapter_id_feature", "_UI_FeatureAdapter_type"),
				 GeneratorPackage.Literals.FEATURE_ADAPTER__ID,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				 getString("_UI_ConfigurationPropertyCategory"),
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(GeneratorPackage.Literals.FEATURE_ADAPTER__CONVERSION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns FeatureAdapter.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/FeatureAdapter"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		FeatureAdapter featureAdapter = (FeatureAdapter)object;
		EStructuralFeature feature = featureAdapter.getAdaptedFeature();
		String label = feature == null ? "unkonwn" : feature.getName();
		
		if(featureAdapter.isId()) {
			return "ID "+label;
		} else
			return "adapt EStructuralFeature " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(FeatureAdapter.class)) {
			case GeneratorPackage.FEATURE_ADAPTER__GET:
			case GeneratorPackage.FEATURE_ADAPTER__POST:
			case GeneratorPackage.FEATURE_ADAPTER__PUT:
			case GeneratorPackage.FEATURE_ADAPTER__DELETE:
			case GeneratorPackage.FEATURE_ADAPTER__ID:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case GeneratorPackage.FEATURE_ADAPTER__CONVERSION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(GeneratorPackage.Literals.FEATURE_ADAPTER__CONVERSION,
				 GeneratorFactory.eINSTANCE.createConversion()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return AdapterGeneratorEditPlugin.INSTANCE;
	}

}
