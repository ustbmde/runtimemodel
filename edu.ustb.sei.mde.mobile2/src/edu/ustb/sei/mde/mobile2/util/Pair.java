package edu.ustb.sei.mde.mobile2.util;

public class Pair<F, S> {
	Pair(F first, S second) {
		super();
		this.first = first;
		this.second = second;
	}
	
	public F first;
	public S second;
	
	static public <A,B> Pair<A,B> pair(A f, B s) {
		return new Pair<A,B>(f,s);
	}
}
