package edu.ustb.sei.mde.mobile2.util;

/**
 * A helper class for computing the position of a source object by assuming that SourceObject and SourcePool are not available.
 * @author hexiao
 *
 */
public interface Position {
	public static final String DOT = ".";
	public static final String SEP = "/";
	public static final String ROOT = "root";


	String getString();
	Position append(String segment, int index);
	static public Position root() {
		return new RootPosition();
	}
	
	abstract class PositionImpl implements Position {
		abstract protected StringBuilder builder();
		abstract protected void fillPosition();
		
		@Override
		public Position append(String segment, int index) {
			SegmentPosition sp = new SegmentPosition(segment, index, this);
			return sp;
		}
	}
	
	class RootPosition extends PositionImpl implements Position {

		@Override
		public String getString() {
			return ROOT;
		}
		
		private StringBuilder builder = new StringBuilder();

		@Override
		protected StringBuilder builder() {
			return builder;
		}

		@Override
		protected void fillPosition() {
			builder.append(getString());
		}
		
	}
	
	class SegmentPosition extends PositionImpl implements Position {
		public SegmentPosition(String segment, int index, PositionImpl prev) {
			super();
			this.segment = segment;
			this.index = index;
			this.prev = prev;
			this.builder = prev.builder();
		}
		private String segment;
		private int index;
		private PositionImpl prev;
		private StringBuilder builder;
		
		
		
		@Override
		public String getString() {
			fillPosition();
			return builder().toString();
		}
		@Override
		protected StringBuilder builder() {
			return builder;
		}
		@Override
		protected void fillPosition() {
			prev.fillPosition();
			builder.append(SEP).append(segment).append(DOT).append(index);
		}
	}
	
	

}
