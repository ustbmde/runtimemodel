package edu.ustb.sei.mde.mobile2.util;

@FunctionalInterface
public interface TripleFunction<A,B,C,T> {
	T apply(A a,B b,C c);
}