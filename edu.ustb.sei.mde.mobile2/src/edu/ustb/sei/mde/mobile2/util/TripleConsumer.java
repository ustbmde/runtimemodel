package edu.ustb.sei.mde.mobile2.util;

@FunctionalInterface
public interface TripleConsumer<A,B,C> {
	void accept(A a,B b,C c);
}