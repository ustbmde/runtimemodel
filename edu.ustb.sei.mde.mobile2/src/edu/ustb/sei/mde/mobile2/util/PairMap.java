package edu.ustb.sei.mde.mobile2.util;

import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PairMap<F,S,U> {
	private Map<F,Map<S,U>> map;
	
	public PairMap() {
		map = new ConcurrentHashMap<>();
	}
	
	public boolean put(F f, S s, U u) {
		Map<S,U> su = map.get(f);
		if(su==null) {
			synchronized (f) {
				su = map.get(f);
				if(su==null) {
					su = new ConcurrentHashMap<>();
					map.put(f, su);
				}
			}
		}
		
		return su.put(s, u)==null;
	}
	
	public U get(F f, S s) {
		Map<S,U> su = map.getOrDefault(f, Collections.emptyMap());
		return su.get(s);
	}

}
