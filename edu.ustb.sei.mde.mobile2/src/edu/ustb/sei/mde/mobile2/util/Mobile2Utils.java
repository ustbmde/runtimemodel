package edu.ustb.sei.mde.mobile2.util;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.mde.mobile2.core.edits.CreateObjectEdit;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObjectProvider;
import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;
import edu.ustb.sei.mde.mobile2.system.Environment;

public class Mobile2Utils {
	static public boolean notChanged(SourceProvider<?> post, SourceProvider<?> original, boolean actionOnReload) {
		if(equals(post, original)) {
			if(post instanceof SourceObjectProvider && ((SourceObjectProvider) post).hasReplacement()) {
				if( ((SourceObjectProvider)post).getFinalProvider() instanceof CreateObjectEdit) return false; // new
				else return !actionOnReload; // reload
			} else return true;
		} else return false; // not equal
	}
	
	static public boolean equals(SourceProvider<?> l, SourceProvider<?> r) {
		if(l==r || (l!=null && r!=null && l.getCore()==r.getCore())) return true;
		else if(l!=null && r!=null) {
			if(l.isReady() && r.isReady()) {
				return equals(l.getValue(), r.getValue());
			} else {
				return false; // unknown
			}
		} else {
			return false;
		}
	}
	
	static public boolean equals(SourceProvider<?> l, Object r) {
		if(!l.isReady()) return false;
		return equals(l.getValue(), r);
	}
	
	static public boolean equals(Object l, Object r) {
		return l==r || (l!=null && l.equals(r));
	}

	public static boolean belongToView(EObject v, Environment env) {
		return v!=null && v.eResource() != null;
	}

	public static boolean equalsOrIn(Object v1, Object v2) {
		if(v2 instanceof Collection) {
			if(v1 == null) return false;
			else return ((Collection<?>) v2).contains(v1);
		} else return equals(v1,v2);
	}
	
	static private ConcurrentHashMap<Object, Long> timer = new ConcurrentHashMap<>();
	public static void timerStart(Object key) {
		timer.put(key, System.nanoTime());
	}
	
	public static long timerStop(Object key, Object acc) {
		long end = System.nanoTime();
		Long start = timer.remove(key);
		long used = 0;
		if(start!=null) {
			used = end-start;
		} else
			System.out.println("timer incorrect "+key);
		
//		synchronized (acc) {
			long total = timer.getOrDefault(acc, 0L);
//			if(total==0) System.out.println("new acc: "+acc);
			total += used;
			timer.put(acc, total);
//		}
		
		return used;
	}

	public static Long getTimer(Object key) {
		return timer.getOrDefault(key,0L)/1000000;
	}
	
	public static void resetTimer() {
		timer.clear();
	}
	
	public static void resetTimer(String key) {
		timer.remove(key);
	}
}
