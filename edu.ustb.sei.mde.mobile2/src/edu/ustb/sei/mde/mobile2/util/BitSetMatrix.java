package edu.ustb.sei.mde.mobile2.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLongArray;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.IntStream;

import com.ibm.icu.impl.Assert;

public class BitSetMatrix {	
	static public long[] BITS = buildBITS();
	
	
	private volatile long[][] bitmap;
	private long[][] connectiveMap;
	
	private final int width; // x
	private final int height; // y
	
	public BitSetMatrix(int width, int height) {
		this.width = width;
		this.height = height;

		bitmap = new long[height][];
		connectiveMap = new long[height][];

		for(int i=0;i<height;i++) {
			bitmap[i] = new long[(width+63) >> 6];
			Arrays.fill(bitmap[i], 0);
		}
	}
	
	private static long[] buildBITS() {
		long[] bits = new long[64];
		for(int i=0;i<64;i++) bits[i] = 1L << i;
		return bits;
	}

	public void set(int col, int row) {
		if(col>=width || row >=height) 
			System.out.println("out of bound!!");

		
		int word = col >> 6;
		int posInWord = col & 0x3F;
		
		synchronized (bitmap[row]) {

		bitmap[row][word] |= BITS[posInWord];

		}
		
	}
	
	public boolean get(int col, int row) {
		int word = col >> 6;
		int posInWord = col & 0x3F;

		return (bitmap[row][word] & BITS[posInWord])!=0;
	}
	

	public int countCol(int row) {
		int sum = 0;
		for(int i=0, s = bitmap[row].length; i<s; i++) {
			sum += BitCount(bitmap[row][i]);
		}
		return sum;
	}
	
	static int BitCountInt(int n) 
	{
	    int tmp = n - ((n >>> 1) & 033333333333) - ((n >>> 2) & 011111111111);
	    return ((tmp + (tmp >>> 3)) & 030707070707) % 63;
	}
	
	static int BitCount(long n) {
		return BitCountInt((int)(n&0xFFFFFFFF)) + BitCountInt((int) (n >>> 32));
	}
	
	public int countRow(int col) {
		int sum = 0;
		for(int i=0, s = bitmap.length;i<s;i++) {
			sum += (get(col, i) ? 1 : 0);
		}
		return sum;
	}
	
//	public void floyd(int from, int to) {
////		for(int i=from; i<to;i++) 
////			for(int j=from; j<to;j++)
////				get(i,j);
//		
//		for(int k=from;k<to;k++) {
//			for(int i=from;i<to;i++) {
//				boolean ik = get(i,k);
//				if(ik==false) continue;
//				for(int j=from;j<to;j++) {
//					boolean ij = get(i, j);
//					if(ij) continue;
//					boolean kj = get(k,j);
//					if(kj) set(i,j); // (ik & kj) | ij
//				}
//			}
//		}
//	}
	
	// based on performance evaluation, at present, a few edges were added by floyd algorithm
	// that means we can use search-on-demand strategy
	public void floyd() {
		int size = Math.min(width, height);
		if(size==0) return;
		
		int roundedSize = (size + 63) / 64;
		final boolean[] kjArr = new boolean[height];
		for(int k=0;k<size;k++) {
			for(int j=0;j<size;j++) {
				kjArr[j] = get(k,j);
			}
			
			for(int i=0;i<roundedSize;i++) {
				final long ik = bitmap[k][i];
				if(ik==0) continue;
				for(int j=0;j<size;j++) {
					final boolean kj = kjArr[j];
					if(kj)  bitmap[j][i] |= ik;
				}
			}
		}
	}
	
	public boolean checkReachable(int from, int to) {
		long[] fromIDs = searchReachableTo(to);
		int wordID = from >> 6;
		int pos = from & 0x3F;
		return (fromIDs[wordID] & BITS[pos])!=0;
	}
	
	private long[] searchReachableTo(int row) {
		if(connectiveMap[row]==null) {
			final int wordLength = bitmap[row].length;
			connectiveMap[row] = new long[wordLength]; // init
			
			for(int cWord = 0; cWord < wordLength ; cWord++) {
				final long word = bitmap[row][cWord]; // scan directly reachable
				if(word==0) continue;
				
				connectiveMap[row][cWord] = word; // initialize word
				
				for(int posInWord = 0; posInWord < 64 ; posInWord ++) { // for each reachable
					if((word & BITS[posInWord])!=0) {
						final int nextID = (cWord << 6) | posInWord; // restore index
						final long[] nextFrom = searchReachableTo(nextID); // get all reachable to nextID
						for(int i = 0; i < nextFrom.length ; i++) {
							connectiveMap[row][cWord] |= nextFrom[i]; // merge result
						}
					}
						
				}
			}
		}
		return connectiveMap[row];
	}
	
	
	
	public void floyd2() {
		int size = Math.min(width, height);
		if(size==0) return;
		
		int roundedSize = (size + 63) / 64;
		final long[] kjArr = new long[height];
		for(int k=0;k<size;k++) {
			for(int j=0;j<size;j++) kjArr[j] = get(k,j) ? -1L : 0L;
			for(int j=0;j<size;j++) {
				final long kj = kjArr[j];
				for(int i=0;i<roundedSize;i++) {
					final long ik = bitmap[k][i];
					bitmap[j][i] |= (ik & kj);
				}
			}
		}
	}
	
	public void parallelFloyd() {
		int size = Math.min(width, height);
		if(size==0) return;
		
		int roundedSize = (size + 63) / 64;

		int batchCounts = Math.min(height, 8);
		int batchSize = (size + batchCounts - 1) / batchCounts;
		
		int[] batchStart = new int[batchCounts];
		int[] batchEnd = new int[batchCounts];
		
		for(int i=0;i<batchCounts;i++) {
			batchStart[i] = i * batchSize;
			batchEnd[i] =  Math.min(batchStart[i] + batchSize, size);
		}
		
		final boolean[] kjArr = new boolean[height];
		for(int mid=0;mid<size;mid++) {
			final int fmid = mid;
			for(int row=0;row<size;row++) kjArr[row] = get(mid,row);
			for(int i=0;i<roundedSize;i++) {
				final int col = i;
				final long ik = bitmap[fmid][i];
				if(ik==0) continue;
				IntStream.range(0, batchCounts).parallel().forEach(batchID->{
					for(int j=batchStart[batchID];j<batchEnd[batchID];j++) {
						final boolean kj = kjArr[j];
						if(kj)  bitmap[j][col] |= ik;
					}
				});
			}
		}
		
	}
	
	
	public int[] countAllRows() {
		int[] result = new int[width];
		for(long[] row : bitmap) {
			int wordId = 0;
			for(long word : row) {
				if(word!=0) {
					int base = wordId << 6;
					if((word & 0xFFFFFFFF)!=0) {
						for(int col = base, posInWord = 0; col < width && posInWord < 32 ; col ++, posInWord++) {
							if((word & BITS[posInWord])!=0) result[col]++;
						}					
					}
					
					if((word >>32 )!=0) {
						for(int col = base + 32, posInWord = 32; col < width && posInWord < 64 ; col ++, posInWord++) {
							if((word & BITS[posInWord])!=0) result[col]++;
						}
					}
				}
				wordId ++;
			}
		}
		
		return result;
	}

	
	public static void main(String[] args) {
		BitSetMatrix martixA = new BitSetMatrix(1000, 1000);
		BitSetMatrix martixB = new BitSetMatrix(1000, 1000);
		Random rand = new Random();
		
		for(int i=0;i<1000;i++) {
			for(int j=0;j<1000;j++) {
				if(martixA.get(j, i)==false) {
					int bound = rand.nextInt(100);
					if(bound>70) {
						martixA.set(i, j);
						martixB.set(i, j);
					}
				}
			}
		}
				
		int[] allRowDeg = martixA.countAllRows();
		for(int i=0;i<allRowDeg.length;i++) {
			if(allRowDeg[i]!=martixA.countRow(i)) {
				System.out.println("error in count rows");
			}
		}

		martixA.floyd();
		martixB.parallelFloyd();
		
		for(int i=0;i<1000;i++) {
			for(int j=0;j<1000;j++) {
				if(martixA.get(i, j)!=martixB.get(i, j)) {
					System.out.println("error at ("+i+","+j+")");
				}
			}
		}
		
		
		System.out.println("finished");
	}
}



class BitMapArray {
	private char[] data;
	BitMapArray(int size) {
		data = new char[size];
		clear();
	}
	
	public void set(int index) {
		data[index] = 1;
	}
	
	public boolean get(int index) {
		return data[index]!=0;
	}
	public int size() {
		return data.length;
	}
	public void clear() {
		Arrays.fill(data, (char) 0);
	}
}