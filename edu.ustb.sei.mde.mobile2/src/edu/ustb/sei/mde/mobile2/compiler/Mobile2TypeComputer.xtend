package edu.ustb.sei.mde.mobile2.compiler

import edu.ustb.sei.mde.mobile2.mobile.ClassMapping
import edu.ustb.sei.mde.mobile2.mobile.DataMapping
import edu.ustb.sei.mde.mobile2.mobile.EnumMapping
import edu.ustb.sei.mde.mobile2.mobile.FeatureMapping
import edu.ustb.sei.mde.mobile2.mobile.Function
import edu.ustb.sei.mde.mobile2.mobile.InMover
import edu.ustb.sei.mde.mobile2.mobile.Inserter
import edu.ustb.sei.mde.mobile2.mobile.OutMover
import edu.ustb.sei.mde.mobile2.mobile.Remover
import edu.ustb.sei.mde.mobile2.mobile.Setter
import edu.ustb.sei.mde.mobile2.mobile.SpecialVarExp
import edu.ustb.sei.mde.mobile2.mobile.TypeMapping
import java.util.List
import org.eclipse.emf.ecore.EEnumLiteral
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.common.types.JvmTypeReference
import org.eclipse.xtext.xbase.typesystem.computation.ITypeComputationState
import org.eclipse.xtext.xbase.typesystem.computation.XbaseTypeComputer
import org.eclipse.xtext.xbase.typesystem.references.LightweightTypeReference
import org.eclipse.xtext.xbase.typesystem.references.ParameterizedTypeReference
import edu.ustb.sei.mde.mobile2.mobile.ViewModel
import org.eclipse.xtext.xtext.generator.util.GenModelUtil2
import org.eclipse.emf.ecore.EClassifier
import org.eclipse.emf.ecore.EEnum
import org.eclipse.emf.ecore.EClass
import edu.ustb.sei.mde.mobile2.mobile.MobileSystem
import edu.ustb.sei.mde.mobile2.mobile.MapSource
import edu.ustb.sei.mde.mobile2.mobile.Modifier

class Mobile2TypeComputer extends XbaseTypeComputer {
	
	def dispatch void computeTypes(SpecialVarExp s, ITypeComputationState state) {
		val type = if(s.set===MapSource.CONTEXT) {
			switch s.name {
				case 'source': getRawTypeForName(s.classMapping.javaType.qualifiedName,state)
				case 'view': s.classMapping.ecoreType.typeRef(s.viewModel, state)
				case 'value': s.computeFeatureSourceValueType(state) //
				case 'oldValue': s.computeFeatureSourceValueType(state)
				case 'viewValue': s.computeFeatureViewValueType(state) //
				case 'from': getRawTypeForName(Object, state)
				case 'to': getRawTypeForName(EObject, state)
				case 'position': getRawTypeForName(int, state)
				case 'oldPosition': getRawTypeForName(int, state)
				default: getRawTypeForName(Object, state)
			}
		} else {
			val clsMapping = s.classMapping
			val f = clsMapping.featureMappings.findFirst[f|f.feature.name.equals(s.name)]
			if(f!==null) {
				val type = getRawTypeForName(f.valueSynchronizer.javaType.qualifiedName,state)
				if(f.featureMapping.feature.many) {
					makeArrayType(state, type)
				} else {
					type
				}
			} else {				
				getRawTypeForName(Object, state)
			}
		}
		
		state.acceptActualType(type);
	}
	
	protected def ParameterizedTypeReference makeArrayType(ITypeComputationState state, LightweightTypeReference type) {
		val owner = state.getReferenceOwner();
		val array = owner.newParameterizedTypeReference(owner.newReferenceTo(List).type);
		array.addTypeArgument(type);
		array
	}
	
	def LightweightTypeReference computeSourceObjectType(SpecialVarExp s, ITypeComputationState state) {
		val clsMapping = s.classMapping
		getRawTypeForName(clsMapping.javaType.qualifiedName, state)
	}
	
	def LightweightTypeReference computeFeatureViewValueType(SpecialVarExp s, ITypeComputationState state) {
		val func = s.function
		val feaMapping = func.featureMapping
		val view = feaMapping.viewModel
		
		val type = feaMapping.valueSynchronizer.featureEcoreType(view, state)
		
		if(func instanceof Setter) {
			if(feaMapping.featureMapping.feature.many) {
				makeArrayType(state, type)
			} else {
				type
			}
		} else if(func instanceof Inserter || func instanceof Remover 
			|| func instanceof InMover || func instanceof OutMover 
			|| func instanceof Modifier
		) {
			type
		} else {
			getRawTypeForName(Object, state)
		}
	}
	
	def ViewModel getViewModel(EObject o) {
		if(o===null) null
		else if(o instanceof MobileSystem) o.view
		else o.eContainer.viewModel
	}
	
	def LightweightTypeReference computeFeatureSourceValueType(SpecialVarExp s, ITypeComputationState state) {
		val func = s.function
		val feaMapping = func.featureMapping
		val type = getRawTypeForName(feaMapping.valueSynchronizer.javaType.qualifiedName,state)
		
		if(func instanceof Setter) {
			if(feaMapping.featureMapping.feature.many) {
				makeArrayType(state, type)
			} else {
				type
			}
		} else if(func instanceof Inserter || func instanceof Remover 
			|| func instanceof InMover || func instanceof OutMover 
			|| func instanceof Modifier
		) {
			type
		} else {
			getRawTypeForName(Object, state)
		}
	}
	
	def LightweightTypeReference featureEcoreType(TypeMapping m, ViewModel view, ITypeComputationState state) {
		if(m instanceof ClassMapping) m.ecoreType.typeRef(view, state)
		else if(m instanceof DataMapping) m.ecoreType.typeRef(view,state)
		else if(m instanceof EnumMapping) m.ecoreType.typeRef(view, state)
		else getRawTypeForName(Object, state)
	}
	
	def JvmTypeReference sourceFeatureType(FeatureMapping m) {
		m.valueSynchronizer.javaType
	}
	
	def ClassMapping getClassMapping(EObject e) {
		if(e===null) null
		else if(e instanceof ClassMapping) e
		else e.eContainer.classMapping
	}
	
	def FeatureMapping getFeatureMapping(EObject e) {
		if(e===null) null
		else if(e instanceof FeatureMapping) e
		else e.eContainer.featureMapping
	}
	
	def Function getFunction(EObject e) {
		if(e===null) null
		else if(e instanceof Function) e
		else e.eContainer.function
	}
	
	def LightweightTypeReference typeRef(EClassifier cls, ViewModel adapter, ITypeComputationState state) {
		if(cls instanceof EEnum || cls instanceof EClass) {
			return getRawTypeForName(adapter.basePackageName+'.'+cls.name, state);
		} else {
			return cls.instanceClass.toObjectType(state);
		}
	}
	
	def LightweightTypeReference toObjectType(Class<?> clazz, ITypeComputationState state) {
		if(clazz===null) return getRawTypeForName(Object, state);
		switch clazz {
			case int:
				getRawTypeForName(Integer,state)
			case boolean:
				getRawTypeForName(Boolean,state)
			case char:
				getRawTypeForName(Character,state)
			case long:
				getRawTypeForName(Long,state)
			case byte:
				getRawTypeForName(Byte,state)
			case float:
				getRawTypeForName(Float,state)
			case double:
				getRawTypeForName(Double,state)
			default:
				getRawTypeForName(clazz,state)
		}
	}
	
	def basePackageName(ViewModel a) {
		try{
			val genPackage = GenModelUtil2.getGenPackage(a.metamodel, a.metamodel.eResource.resourceSet);
			return genPackage.basePackage;
		} catch(Exception e){
			return a.basePackage;
		}
	}
}