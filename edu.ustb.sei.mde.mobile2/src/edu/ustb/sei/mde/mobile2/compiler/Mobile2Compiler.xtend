package edu.ustb.sei.mde.mobile2.compiler

import org.eclipse.xtext.xbase.compiler.XbaseCompiler
import org.eclipse.xtext.xbase.XExpression
import org.eclipse.xtext.xbase.compiler.output.ITreeAppendable
import org.eclipse.xtext.xbase.XStringLiteral
import edu.ustb.sei.mde.mobile2.mobile.SpecialVarExp
import edu.ustb.sei.mde.mobile2.mobile.Function
import org.eclipse.emf.ecore.EObject
import edu.ustb.sei.mde.mobile2.core.edits.EditContext
import edu.ustb.sei.mde.mobile2.mobile.MapSource

class Mobile2Compiler extends XbaseCompiler {
	
	override protected internalToConvertedExpression(XExpression obj, ITreeAppendable appendable) {
		if(obj instanceof SpecialVarExp) {
			if(obj.checkContext(obj.functionContext)) {
				if(appendable.hasName(obj)) {
					appendable.append(appendable.getName(obj))
				} else {
					if(obj.set===MapSource.CONTEXT) {
						  appendable.append("getContext().getValueForKey(\"").append(obj.name).append("\")").append(obj.genGetValue)					
					} else {
						appendable.append("((Map<Object, SourceProvider<?>>)getContext().getValueForKey(").append(EditContext.canonicalName).append(".CONSTRUCTOR_PARAM)")
						  .append(").get(\"").append(obj.name).append("\")").append(obj.genGetValue)
					}
				}
			} else {
				appendable.append('__error')
			}
		} else super.internalToConvertedExpression(obj, appendable)
	}
	
	override protected doInternalToJavaStatement(XExpression obj, ITreeAppendable appendable, boolean isReferenced) {
		if(obj instanceof SpecialVarExp) {
			if(obj.checkContext(obj.functionContext)) {
				val expectedType = getType(obj);
				val varName = appendable.declareSyntheticVariable(obj, obj.name);
				if(obj.set===MapSource.CONTEXT) {
					appendable.append(expectedType.qualifiedName).append(" ").append(varName).append(" = ")
					  .append("getContext().getValueForKey(\"").append(obj.name).append("\")").append(obj.genGetValue).append(";")					
				} else {
					appendable.append(expectedType.qualifiedName).append(" ").append(varName).append(" = ")
					  .append("((Map<Object, SourceProvider<?>>)getContext().getValueForKey(").append(EditContext.canonicalName).append(".CONSTRUCTOR_PARAM)")
					  .append(").get(\"").append(obj.name).append("\")").append(obj.genGetValue).append(";")
				}
			} else {
				appendable.append('__error')
			}
		} else super.doInternalToJavaStatement(obj, appendable, isReferenced)
	}
	
	def Function getFunctionContext(EObject o) {
		if(o===null) null
		else if(o instanceof Function) o
		else o.eContainer.functionContext;
	}
	
	def boolean checkContext(SpecialVarExp v, Function f) {
		return true;
	}
	
	def String genGetValue(SpecialVarExp e) {
		if(e.set===MapSource.CONTEXT) {
			switch e.name {
				case 'source': '.getValue()'
				case 'value': '.getValue()'
				case 'oldValue': '.getValue()'
				case 'from': '.getValue()'
				default: ''
			}
		} else {
			'.getValue()'
		}
	}
}