package edu.ustb.sei.mde.mobile2.system;

public enum Priority {
	leftFirst,
	rightFirst,
	unknown;
	
	public Priority opposite() {
		if(this==leftFirst) return rightFirst;
		else if(this==rightFirst) return leftFirst;
		else return unknown;
	}
}
