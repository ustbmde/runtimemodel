package edu.ustb.sei.mde.mobile2.system;

import java.io.IOException;
import java.io.PrintStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMIResource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.xtext.xbase.lib.Pair;

import edu.ustb.sei.mde.mobile2.core.ElementBX;
import edu.ustb.sei.mde.mobile2.core.SystemBX;
import edu.ustb.sei.mde.mobile2.core.edits.Edit;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObject;
import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;
import edu.ustb.sei.mde.mobile2.util.Mobile2Utils;
import edu.ustb.sei.mde.mobile2.util.Position;

public abstract class Mobile2System {
	protected SystemBX bxInstance;
	protected Environment rootEnvironment;
	protected ResourceSet resourceSet;
	protected Resource viewResource;
	protected EditScheduler scheduler;
	
	protected PrintStream print;

	public Mobile2System() {
		resourceSet = new ResourceSetImpl();
		resourceSet.getPackageRegistry().put(EcorePackage.eNS_URI, EcorePackage.eINSTANCE);
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		
		initResourceSet(resourceSet);
		bxInstance = buildBX(); 
		scheduler = buildScheduler(this);
		
		print = System.out; // default
	}
	
	protected abstract EditScheduler buildScheduler(Mobile2System mobile2System);

	public EditScheduler getScheduler() {
		return scheduler;
	}
	
	public void test() {
		
	}

	protected abstract SystemBX buildBX();
	protected abstract void initResourceSet(ResourceSet set);
	
	public ResourceSet getResourceSet() {
		return resourceSet;
	}
	
	@SuppressWarnings("unchecked")
	public <S extends SystemBX> S getSystemBX() {
		return (S) bxInstance;
	}
	
	public Environment getRootEnvironment() {
		return rootEnvironment;
	}
	
	public Environment initializeEnvironment() {
		Environment env = Environment.init(this.getSystemBX(), (e)->{			
			autoInitEnvironment(e);
			internalInitEnvironment(e);
		});
	    return env;
	}
	
	protected void autoInitEnvironment(Environment env) {}

	protected void internalInitEnvironment(Environment env) {}

	// called automatically by state-based get
	public void initializeForStateBasedGet(Object rootSource) {
		this.rootEnvironment = initializeEnvironment();
		this.rootEnvironment.initializeStateBasedGet(rootSource);
	}
	
	// before every second delta-based get
	public void initializeForDeltaBasedGet(Object rootSource) {
		// delta based get requires a resource
		assert this.rootEnvironment!=null;
		this.rootEnvironment.initializeDeltaBasedGet(rootSource, this.getViewResource());
	}
	
	public EObject getState(Object rootSource) {
		Mobile2Utils.timerStart("GET_INIT_STATE");
		initializeForStateBasedGet(rootSource);
		Mobile2Utils.timerStop("GET_INIT_STATE","GET_INIT_STATE_TOTAL");
		ElementBX bx = getSystemBX().getRootBX();
		Mobile2Utils.timerStart("GET_STATE");
		SourceObject rootObject = this.getRootEnvironment().getSourcePool().getSourceObject(rootSource);
		EObject rootView = bx.get(rootObject, getRootEnvironment());
		Mobile2Utils.timerStop("GET_STATE", "GET_STATE_TOTAL");
		return rootView;
	}

	/**
	 * Before calling getStateDelta, setViewResource must be called.
	 * @param rootSource
	 */
	public EObject getDelta(Object rootSource) {
		if(this.getViewResource()==null) {
			throw new RuntimeException("setViewResource is not called!");
		}
		
		if(this.rootEnvironment == null) {
			initializeForStateBasedGet(rootSource);
		} else {
			initializeForDeltaBasedGet(rootSource);
		}
		
		ElementBX bx = getSystemBX().getRootBX();
		SourceObject rootObject = this.getRootEnvironment().getSourcePool().getSourceObject(rootSource);
		EObject rootView = bx.get(rootObject, getRootEnvironment());
		return rootView;
	}
	
	public Set<Edit<?>> putState(Object sourceRoot, Resource viewResource) {
		System.out.println("initialize");
		initializeForStateBasedPut(sourceRoot, viewResource);
		
		EObject rootView = viewResource.getContents().isEmpty() ? null : viewResource.getContents().get(0);

		ElementBX bx = getSystemBX().getRootBX();
		
		SourceObject rootObject = null;
		if(sourceRoot!=null) rootObject = this.getRootEnvironment().getSourcePool().getSourceObject(sourceRoot);
		else rootObject = (SourceObject) this.getRootEnvironment().getSourcePool().findToBeObject(rootView);
		
		System.out.println("put");
		SourceProvider<Object> result = bx.put(rootObject, rootView, this.rootEnvironment);
		
		System.out.println("do remaining");
		this.rootEnvironment.completeTasks();
		
		System.out.println("collect edits");
		if(result instanceof Edit) {
			Set<Edit<?>> allEdits = ((Edit<Object>) result).collectEdits();
			return allEdits;
		} else {
			return Collections.emptySet();
		}
	}
	
	public Set<Edit<?>> putDelta(Object sourceRoot) {
		System.out.println("initialize");
		if(this.getViewResource()==null) {
			throw new RuntimeException("setViewResource is not called!");
		}
		
		Mobile2Utils.timerStart("PUT_INIT_DELTA");
		if(this.rootEnvironment==null) {
			initializeForStateBasedPut(sourceRoot, this.getViewResource());
		} else {
			initializeForDeltaBasedPut(sourceRoot);
		}
		Mobile2Utils.timerStop("PUT_INIT_DELTA","PUT_INIT_DELTA_TOTAL");
		
		System.out.println("put");
		
		Mobile2Utils.timerStart("PUT_DELTA");
		ElementBX bx = getSystemBX().getRootBX();
		EObject rootView = viewResource.getContents().isEmpty() ? null : viewResource.getContents().get(0);

		SourceObject rootObject = null;
		if(sourceRoot!=null) rootObject = this.getRootEnvironment().getSourcePool().getSourceObject(sourceRoot);
		else rootObject = (SourceObject) this.getRootEnvironment().getSourcePool().findToBeObject(rootView);
		
		Mobile2Utils.timerStart("All");

		
		SourceProvider<Object> result = bx.put(rootObject, rootView, this.rootEnvironment);
		System.out.println("do remaining");
		
		this.rootEnvironment.completeTasks();
		
		Mobile2Utils.timerStop("PUT_DELTA","PUT_DELTA_TOTAL");
		long putTime = Mobile2Utils.timerStop("All", "AllTime");
		
		System.out.println("Time used by put: "+putTime/1000000+" ms");
		
		System.out.println("collect edits");
		if(result instanceof Edit) {
			Set<Edit<?>> allEdits = ((Edit<Object>) result).collectEdits();
			return allEdits;
		} else {
			return Collections.emptySet();
		}
		
	}

	public void initializeForDeltaBasedPut(Object sourceRoot) {
		assert this.rootEnvironment!=null;
		this.rootEnvironment.initializeDeltaBasedPut(sourceRoot, this.getViewResource());
	}

	public void initializeForStateBasedPut(Object sourceRoot, Resource viewRes) {
		this.rootEnvironment = initializeEnvironment();
		this.rootEnvironment.initializeStateBasedPut(sourceRoot, viewRes);
	}

	public void save(EObject root, Resource res) {
		Map<String, Object> option = new HashMap<>();
		option.put(XMIResource.OPTION_SCHEMA_LOCATION, true);
		try {
			res.getContents().clear();
			res.getContents().add(root);
			res.save(option);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public Resource getViewResource() {
		return viewResource;
	}

	public void setViewResource(Resource viewResource) {
		this.viewResource = viewResource;
	}

	
	// TODO: put之后更新key的逻辑
	public void serializeDeltaTrace(String uri) {
		this.getRootEnvironment().serializeTrace(uri);
	}
	
	/**
	 * called before the initialization of the first get and put
	 * @param uri
	 * @param sourceRoot
	 * @param viewResource
	 */
	public void initializeFromSerialization(String uri, Object sourceRoot, Resource viewResource) {
		this.getRootEnvironment().initializeFromSerialization(uri, sourceRoot, viewResource);
	}
	
	public void close() {
		this.getRootEnvironment().getTaskPool().close();
	}
	
	static private void position(EObject view, StringBuilder sb) {
		if(view.eContainer()==null) {
			sb.append(Position.ROOT);
		} else {
			position(view.eContainer(),sb);
			sb.append(Position.SEP);

			EReference containment = view.eContainmentFeature();
			sb.append(containment.getName());
			sb.append(Position.DOT);
			
			if(containment.isMany()) {
				List<?> col = (List<?>) view.eContainer().eGet(containment);
				sb.append(col.indexOf(view));
			} else {
				sb.append("0");
			}
		}
	}
	static public String position(EObject view) {
		StringBuilder sb = new StringBuilder();
		position(view,sb);
		return sb.toString();
	}
	
	static public Object localKey(EObject view, Object localKey) {
		return new Pair<>(view.eContainer(), localKey);
	}
	static public Object localKey(Object source, Object localKey) {
		return new SourceLocalKey(source, localKey);
	}
	
	static public Object instanceKey(EObject view) {
		return new ViewInstanceKey();
	}

	public PrintStream getPrint() {
		return print;
	}

	public void setPrint(PrintStream print) {
		this.print = print;
	}
	
}
