package edu.ustb.sei.mde.mobile2.system;

public abstract class Mobile2Task implements Runnable {
	
	final static public Mobile2Task placeHolder = new Mobile2Task() {
		@Override
		protected void compute() {
			throw new RuntimeException("this is a place holder that cannot be run!");
		}
		
	};
	
	enum TaskState {
		waiting,
		running,
		stoped
	}

	public Mobile2Task() {
		state = TaskState.waiting;
	}
	
	private volatile TaskState state;
	
	final public void run() {	
		state = TaskState.running;
		compute();
		synchronized (this) {
			state = TaskState.stoped;
			this.notifyAll();				
		}
	}
	
	public boolean started() {
		return state!=TaskState.running;
	}
	
	public boolean running() {
		return state==TaskState.running;
	}
	
	public boolean finished() {
		return state==TaskState.stoped;
	}
	
	protected abstract void compute();
}
