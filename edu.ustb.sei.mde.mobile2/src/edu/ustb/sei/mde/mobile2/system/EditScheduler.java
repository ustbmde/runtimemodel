package edu.ustb.sei.mde.mobile2.system;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.concurrent.RecursiveTask;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.util.Arrays;

import edu.ustb.sei.mde.mobile2.core.edits.CollectiveEdit;
import edu.ustb.sei.mde.mobile2.core.edits.CreateObjectEdit;
import edu.ustb.sei.mde.mobile2.core.edits.Edit;
import edu.ustb.sei.mde.mobile2.core.edits.Edit.EditKind;
import edu.ustb.sei.mde.mobile2.core.edits.EditContext;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObject;
import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;
import edu.ustb.sei.mde.mobile2.system.EditPredicate.OrPredicate;
import edu.ustb.sei.mde.mobile2.system.SchedulingRule.ApplicationStrategy;
import edu.ustb.sei.mde.mobile2.util.Mobile2Utils;


public class EditScheduler {
	class NaturalRuleFactor {
		public SchedulingRule rule;
		public EditKind leftEdit;
		public EditKind rightEdit;
	}
	
	@SuppressWarnings("serial")
	class RuleList extends ArrayList<SchedulingRule> {
		private Map<org.eclipse.xtext.xbase.lib.Pair<EditKind, EditKind>, List<SchedulingRule>> ruleMaps = new HashMap<>();
		private List<SchedulingRule> initialIterationRules = new ArrayList<>();
		private List<SchedulingRule> treeIterationLeftFirstRules = new ArrayList<>();
		private List<SchedulingRule> treeIterationRightFirstRules = new ArrayList<>();
		private List<SchedulingRule> treeIterationSiblingRules = new ArrayList<>();
		private Set<SchedulingRule> unforgettableLeftFirstRules = null;
		private Set<SchedulingRule> unforgettableRightFirstRules = null;
		
		public void computeNaturalRules() {
			unforgettableLeftFirstRules = computeNaturalRules(treeIterationLeftFirstRules);
			unforgettableRightFirstRules = computeNaturalRules(treeIterationRightFirstRules);
		}
		
		public boolean isForgettableLeftFirstRule(SchedulingRule r) {
			return !unforgettableLeftFirstRules.contains(r);
		}
		public boolean isForgettableRightFirstRule(SchedulingRule r) {
			return !unforgettableRightFirstRules.contains(r);
		}
		
		public boolean checkUseful(Edit<?> edit, List<SchedulingRule> rules, boolean leftOnly) {
			boolean useful = false;
			for(SchedulingRule r : rules) {
				if(r.leftPredicate().match(edit.getContext(), null)) {
					useful = true;
					break;
				} else if(leftOnly==false && r.rightPredicate().match(edit.getContext(), null)) {
					useful = true;
					break;
				}
			}
			return useful;
		}
		
		private Set<SchedulingRule> computeNaturalRules(List<SchedulingRule> rules) {
			List<NaturalRuleFactor> factors = new ArrayList<>();
			rules.forEach(rule->{
				Pattern[] leftPatterns = null;
				Pattern[] rightPatterns = null;
				EditPredicate leftPredicate = rule.leftPredicate();
				EditPredicate rightPredicate = rule.rightPredicate();
				
				leftPatterns = extractPatterns(leftPredicate);
				rightPatterns = extractPatterns(rightPredicate);
				
				Set<EditKind> leftEdits = new HashSet<>();
				Set<EditKind> rightEdits = new HashSet<>();

				for(Pattern l : leftPatterns) {
					leftEdits.clear();
					l.collectEditKinds(leftEdits);
					for(Pattern r : rightPatterns) {
						rightEdits.clear();
						r.collectEditKinds(rightEdits);
						
						leftEdits.forEach(le->{
							rightEdits.forEach(re->{
								NaturalRuleFactor factor = new NaturalRuleFactor();
								factor.leftEdit = le;
								factor.rightEdit = re;
								factor.rule = rule;
								factors.add(factor);
							});
						});
					}
				}
			});
			
			List<NaturalRuleFactor> unforgettableFactors = new ArrayList<>();
			factors.forEach(r->{
				// r := leftEdit & rightEdit
				if(!factors.stream().allMatch(rp->{
					if(r.leftEdit==rp.leftEdit) {
						return factors.stream().anyMatch(rpp->{
							return r.rightEdit==rpp.leftEdit && rp.rightEdit==rpp.rightEdit;
						});
					} else return true;
				})) {
					unforgettableFactors.add(r);
				}
			});
			
			Set<SchedulingRule> unforgettableRules = new HashSet<>();
			unforgettableFactors.forEach(f->{
				unforgettableRules.add(f.rule);
			});
			
			return unforgettableRules;
		}
		
		public List<SchedulingRule> getRulesFor(EditKind left, EditKind right) {
			return ruleMaps.getOrDefault(org.eclipse.xtext.xbase.lib.Pair.of(left, right), Collections.emptyList());
		}
		
		public List<SchedulingRule> getInitialRules() {
			return initialIterationRules;
		}
		
		@Override
		public boolean add(SchedulingRule e) {
			if(e.strategy()==ApplicationStrategy.initialInteration) {
				initialIterationRules.add(e);
				return false;
			} else if(e.strategy()==ApplicationStrategy.treeIteration) {
				if(e.priority()==Priority.leftFirst)
					treeIterationLeftFirstRules.add(e);
				else treeIterationRightFirstRules.add(e);
				return false;
			} else if(e.strategy()==ApplicationStrategy.treeIterationSibling) {
				treeIterationSiblingRules.add(e);
				return false;
			} else {
				Set<EditKind> leftKinds = new HashSet<>();
				Set<EditKind> rightKinds = new HashSet<>();
				e.leftPredicate().collectEditKinds(leftKinds);
				e.rightPredicate().collectEditKinds(rightKinds);
				leftKinds.forEach(l->{
					rightKinds.forEach(r->{
						org.eclipse.xtext.xbase.lib.Pair<EditKind, EditKind> pair = org.eclipse.xtext.xbase.lib.Pair.of(l, r);
						List<SchedulingRule> rules = ruleMaps.get(pair);
						if(rules==null) {
							rules = new ArrayList<>();
							ruleMaps.put(pair, rules);
						}
						rules.add(e);
					});
				});				
				return super.add(e);			
			}
		}

		public List<SchedulingRule> getTreeIterationLeftFirstRules() {
			return treeIterationLeftFirstRules;
		}

		public List<SchedulingRule> getTreeIterationRightFirstRules() {
			return treeIterationRightFirstRules;
		}
	}
	
	protected RuleList semanticRules = new RuleList();
	
	protected Mobile2System system;
	public EditScheduler(Mobile2System system) {
		buildSchedulingRules();
		this.system = system;
	}

	protected void buildSchedulingRules() {
		buildHardSchedulingRules();
		buildCreationRules(); // also hard rule
		buildCustomizedSchedulingRules();
		buildDefaultSchedulingRules();
		
		this.semanticRules.computeNaturalRules();
	}

	

	private Pattern[] extractPatterns(EditPredicate predicate) {
		Pattern[] leftPatterns = null;
		if(predicate instanceof Pattern) {
			leftPatterns = new Pattern[] {(Pattern) predicate};
		} else if(predicate instanceof OrPredicate) {
			leftPatterns = java.util.Arrays.stream(((OrPredicate) predicate).getPredicates()).map(p->(Pattern)p).toArray(size->new Pattern[size]);
		}
		return leftPatterns;
	}

	protected void buildHardSchedulingRules() {
		Pattern formerMoveOut = new Pattern("e1", EditKind.moveOut);
		Pattern laterMoveIn = new Pattern("e2", EditKind.moveIn);
//		ContextPredicate pred1 = ContextPredicate.valueEqual("e1", "viewValue", "e2", "viewValue");
//		SchedulingRule rule1 = SchedulingRule.simpleRule(formerMoveOut, laterMoveIn, pred1, Priority.leftFirst, "[HARD] For the same value, moveOut must be executed before moveIn.");
//		addRule(rule1);
		
		
		SchedulingRule rule1 = SchedulingRule.initialIterationRule(formerMoveOut, (node, rule)->{
			Graph g = node.graph;
			EObject movedValue = node.edit.getContext().getViewValue();
			Set<Edit<?>> edits = g.eValueEditsMap.getOrDefault(movedValue, Collections.emptySet());
			
			edits.forEach(n->{
				EditContext context = n.getContext();
				if(laterMoveIn.match(context, null) && context.getViewValue()==movedValue) {
					Node target = g.editNodeMap.get(n);
					g.addEdge(node, target, rule);
					this.triggerRule(rule);
				}
			});
			return false;
		}, "[HARD] For the same value, moveOut must be executed before moveIn.");
		addRule(rule1);
		
		Pattern former = new Pattern("e1", EditKind.values(), FeatureKind.values());
//		Pattern later = new Pattern("e2", EditKind.values(), FeatureKind.values());
//		ContextPredicate pred2 = ContextPredicate.valueEqual("e1", EditContext.NEXT_EDIT, null, SchedulingRule.RIGHT_EDIT);
//		SchedulingRule rule2 = SchedulingRule.simpleRule(former, later, pred2, Priority.leftFirst, "[HARD] A predefined priority must be satisfied.");
//		addRule(rule2);
		
		SchedulingRule rule2 = SchedulingRule.initialIterationRule(former, (node, rule)->{
			Edit<?> nextEdit = node.edit.getContext().getValueForKey(EditContext.NEXT_EDIT);
			if(nextEdit==null) return false;
			Graph g = node.graph;
			Node target = g.editNodeMap.get(nextEdit);
			g.addEdge(node, target, rule);
			this.triggerRule(rule);
			return false;
		}, "[HARD] A predefined priority must be satisfied.");
		addRule(rule2);
	}

	protected void addRule(SchedulingRule rule) {
		this.semanticRules.add(rule);
		rule.setScheduler(this);
	}

	protected void buildDefaultSchedulingRules() {
		buildSetRules();
		buildReplaceRules();
//		buildReorderRules();
		buildAdditiveRules();
		buildRemovalRules();
		buildNormalRules();
	}

	private void buildCreationRules() {

		Pattern formerCreate = new Pattern("e1", EditKind.create);
		Pattern laterAll = new Pattern("e2", EditKind.viewValueEdits(), FeatureKind.values());
//		ContextPredicate pred = ContextPredicate.or(ContextPredicate.valueEqualOrIn("e1", "viewValue", "e2", "viewValue"), ContextPredicate.toBeContain("e1", "viewValue", "e2", "view"));
//		SchedulingRule rule1 = SchedulingRule.simpleRule(formerCreate, laterAll, pred, Priority.leftFirst, "[CREATION] New objects should be created before being used.");
//		addRule(rule1);
		SchedulingRule rule1 = SchedulingRule.initialIterationRule(formerCreate, (node, rule)->{
			EObject created = node.edit.getContext().getViewValue();
			Set<Edit<?>> edits = node.graph.eValueEditsMap.getOrDefault(created, Collections.emptySet());
			edits.forEach(e2->{
				if(laterAll.match(e2.getContext(), null)) {
					Node target = node.graph.editNodeMap.get(e2);
					node.graph.addEdge(node, target, rule);
					this.triggerRule(rule);
				}
			});
			return true;
		}, "[CREATION] New objects should be created before being used.");
		addRule(rule1);
		
//		Pattern laterCreate = new Pattern("e2", EditKind.create);
//		ContextPredicate pred2 = ContextPredicate.and(new ContextPredicate() {
//			@Override
//			public boolean check(Map<String, Object> context) {
//				return resolveKey(context, "e2", EditContext.CREATION_WITH_PARENT, false);
//			}
//		}, new ContextPredicate() {
//			@Override
//			public boolean check(Map<String, Object> context) {
//				EObject view1 = resolveKey(context, "e1", EditContext.VIEW_VALUE, null);
//				EObject view2 = resolveKey(context, "e2", EditContext.VIEW_VALUE, null);
//				return (view1!=null && view2!=null && view1==view2.eContainer());
//			}
//			
//		});
//		SchedulingRule rule2 = SchedulingRule.simpleRule(formerCreate, laterCreate, pred2, Priority.leftFirst, "[CREATION] Parent creation must be performed first.");
//		addRule(rule2);
		
		EditPredicate createWithParent = EditPredicate.and(new Pattern("e1", EditKind.create),
				new EditPredicate() {
					@Override
					public boolean match(EditContext info, Map<String, Object> context) {
						Boolean requireParent = info.getValueForKey(EditContext.CREATION_WITH_PARENT);
						if(requireParent==null) return false;
						return requireParent;
					}
					
					@Override
					public void fillContext(EditContext info, Map<String, Object> context) {}
					
					@Override
					public void collectEditKinds(Set<EditKind> kinds) {}
				});
		
		SchedulingRule rule2 = SchedulingRule.initialIterationRule(createWithParent, (node, rule)->{
			EObject created = node.edit.getContext().getViewValue();
			Set<Edit<?>> edits = node.graph.eValueEditsMap.getOrDefault(created.eContainer(), Collections.emptySet());
			edits.forEach(e2->{
				EditKind e2Kind = e2.getContext().getValueForKey(EditContext.OPERATION);
				if(e2Kind==EditKind.create) {
					Node source = node.graph.editNodeMap.get(e2);
					node.graph.addEdge(source, node, rule);
					this.triggerRule(rule);
				}
			});
			return true;
		}, "[CREATION] Parent creation must be performed first.");
		addRule(rule2);
		
		SchedulingRule rule3 = SchedulingRule.initialIterationRule(formerCreate, (node,rule)->{
			CreateObjectEdit e1 = (CreateObjectEdit) node.edit;
			Map<Object, SourceProvider<?>> paramMap = e1.getContext().getValueForKey(EditContext.CONSTRUCTOR_PARAM);
			if(paramMap!=null) {
				paramMap.values().forEach(v->{
					if(v instanceof CreateObjectEdit) {
						Node source = node.graph.editNodeMap.get(v);
						node.graph.addEdge(source, node, rule);
						this.triggerRule(rule);
					} else {
						// handle collection
						if(v instanceof CollectiveEdit<?>) {
							((CollectiveEdit<?>) v).collectEdits().forEach(vv->{
								if(vv instanceof CreateObjectEdit) {
									Node source = node.graph.editNodeMap.get(vv);
									node.graph.addEdge(source, node, rule);
									this.triggerRule(rule);
								}
							});
						}
					}
				});
			}
			return true;
		}, "[CREATION] Constructor parameters must be created first");
		addRule(rule3);
	}

//	private void buildReorderRules() {
//		Pattern formerReorder1 = new Pattern("e1", EditKind.reorder);
//		Pattern laterSet = new Pattern("e2", new EditKind[] {EditKind.set}, FeatureKind.values());
//		Pattern laterAdditive = new Pattern("e2", new EditKind[] {EditKind.modify, EditKind.reorder, EditKind.insert, EditKind.moveIn});
////		ContextPredicate pred1 = ContextPredicate.toBeContain("e1", "viewValue", "e2", "view"); //new ToBeValueDependency("e2", "view", "e1", "viewValue");
////		SchedulingRule rule1 = SchedulingRule.simpleRule(formerReorder1, EditPredicate.or(laterSet,laterAdditive), pred1, Priority.leftFirst, "[REORDER] Reordering parent should be performed before the additive change of child.");
//		SchedulingRule rule1 = SchedulingRule.naturalRule(formerReorder1, EditPredicate.or(laterSet,laterAdditive), Priority.leftFirst, "[REORDER] Reordering parent should be performed before the additive change of child.");
//		addRule(rule1);
//
//		Pattern formerReorder2 = new Pattern("e1", EditKind.modify);
//		Pattern laterDestructive = new Pattern("e2", new EditKind[] {EditKind.remove, EditKind.moveOut});
//		ContextPredicate pred2 = ContextPredicate.asIsContain("e1", "value", "e2", "source"); //new AsIsValueDependency("e2", "source", "e1", "value");
//		SchedulingRule rule2 = SchedulingRule.simpleRule(formerReorder2, laterDestructive, pred2, Priority.rightFirst, "[REORDER] Reordering parent should be performed after the destructive change of child.");
//		addRule(rule2);
//	}

	private void buildNormalRules() {
//		ContextPredicate pred = ContextPredicate.valueEqual("e1", "view", "e2", "view");

		Pattern former = new Pattern("e1", EditKind.featureEdits(), new FeatureKind[] {FeatureKind.attribute}); 
		
		Pattern later1 = new Pattern("e2", EditKind.featureEdits(), new FeatureKind[] {FeatureKind.containment}); 
		SchedulingRule rule1 = SchedulingRule.naturalSiblingRule(former, later1, null, Priority.rightFirst, "[NORMAL] Attribute changes are performed last.");
		addRule(rule1);

		Pattern later2 = new Pattern("e2", EditKind.featureEdits(), new FeatureKind[] {FeatureKind.noncontainment}); 
		SchedulingRule rule2 = SchedulingRule.naturalSiblingRule(former, later2, null, Priority.leftFirst, "[NORMAL] Attribute changes are performed before noncontainment changes.");
		addRule(rule2);
	}

	private void buildRemovalRules() {
		Pattern formerRemoval = new Pattern("e1", new EditKind[] {EditKind.remove, EditKind.moveOut});
		Pattern laterRemoval = new Pattern("e2", new EditKind[] {EditKind.remove, EditKind.moveOut});
		ContextPredicate pred1 = ContextPredicate.asIsContain("e1", "value", "e2", "source"); // new AsIsValueDependency("e2", "source", "e1", "value");
		SchedulingRule rule1 = SchedulingRule.simpleRule(formerRemoval, laterRemoval, pred1, Priority.rightFirst, "[REMOVAL] Removal of children should be performed before the removal of parent.");
		addRule(rule1);
	}

	private void buildAdditiveRules() {
		Pattern formerAdditive = new Pattern("e1", new EditKind[] {EditKind.insert, EditKind.moveIn});
		Pattern laterSet = new Pattern("e2", new EditKind[] {EditKind.set}, FeatureKind.values());
		Pattern laterAdditive = new Pattern("e2", new EditKind[] {EditKind.modify, EditKind.reorder, EditKind.insert, EditKind.moveIn});
//		ContextPredicate pred1 = ContextPredicate.toBeContain("e1", "viewValue", "e2", "view");
//		SchedulingRule rule1 = SchedulingRule.simpleRule(formerAdditive, EditPredicate.or(laterSet,laterAdditive), pred1, Priority.leftFirst, "[INSERTION] Insertion of children should be performed after the insertion of parent.");
		SchedulingRule rule1 = SchedulingRule.naturalRule(formerAdditive, EditPredicate.or(laterSet,laterAdditive), Priority.leftFirst, "[INSERTION] Insertion of children should be performed after the insertion of parent.");
		addRule(rule1);
	}

	private void buildReplaceRules() {
		Pattern formerReplace1 = new Pattern("e1", EditKind.modify);
		Pattern laterSet = new Pattern("e2", new EditKind[] {EditKind.set}, FeatureKind.values());
		Pattern laterAdditive = new Pattern("e2", new EditKind[] {EditKind.modify, EditKind.reorder, EditKind.insert, EditKind.moveIn});
//		ContextPredicate pred1 = ContextPredicate.toBeContain("e1", "viewValue", "e2", "view");//new ToBeValueDependency("e2", "view", "e1", "viewValue");
//		SchedulingRule rule1 = SchedulingRule.simpleRule(formerReplace1, EditPredicate.or(laterSet,laterAdditive), pred1, Priority.leftFirst, "[REPLACE] Insertion of children should be performed after the replacement.");
		SchedulingRule rule1 = SchedulingRule.naturalRule(formerReplace1, EditPredicate.or(laterSet,laterAdditive), Priority.leftFirst, "[REPLACE] Insertion of children should be performed after the replacement.");
		addRule(rule1);

		Pattern formerReplace2 = new Pattern("e1", EditKind.modify);
		Pattern laterDestructive = new Pattern("e2", new EditKind[] {EditKind.remove, EditKind.moveOut});
		ContextPredicate pred2 = ContextPredicate.asIsContain("e1", "value", "e2", "source"); //new AsIsValueDependency("e2", "source", "e1", "value");
		SchedulingRule rule2 = SchedulingRule.simpleRule(formerReplace2, laterDestructive, pred2, Priority.rightFirst, "[REPLACE] Removal of children should be performed before the replacement.");
		addRule(rule2);

	}

	private void buildSetRules() {
		Pattern formerSet1 = new Pattern("e1", EditKind.set);
		Pattern laterSet = new Pattern("e2", new EditKind[] {EditKind.set}, FeatureKind.values());
		Pattern laterAdditive = new Pattern("e2", new EditKind[] {EditKind.modify, EditKind.reorder, EditKind.insert, EditKind.moveIn});
//		ContextPredicate pred1 = ContextPredicate.toBeContain("e1", "viewValue", "e2", "view");// new ToBeValueDependency("e2", "view", "e1", "viewValue");
//		SchedulingRule rule1 = SchedulingRule.simpleRule(formerSet1, EditPredicate.or(laterSet,laterAdditive), pred1, Priority.leftFirst, "[SET] Children additive updates should be performed after the setting of parents.");
		SchedulingRule rule1 = SchedulingRule.naturalRule(formerSet1, EditPredicate.or(laterSet,laterAdditive), Priority.leftFirst, "[SET] Children additive updates should be performed after the setting of parents.");
		addRule(rule1);
		
		Pattern formerSet2 = new Pattern("e1", EditKind.set);
		Pattern laterDestructive = new Pattern("e2", new EditKind[] {EditKind.remove, EditKind.moveOut}, FeatureKind.values());
		ContextPredicate pred2 = ContextPredicate.asIsContain("e1", "value", "e2", "source"); //new AsIsValueDependency("e2", "source", "e1", "value");
		SchedulingRule rule2 = SchedulingRule.simpleRule(formerSet2, laterDestructive, pred2, Priority.rightFirst, "[SET] Children destructive updates should be performed before the setting of parents.");
		addRule(rule2);		
	}

	protected void buildCustomizedSchedulingRules() {
	}

	
	public List<Edit<?>> schedule(Set<Edit<?>> edits) {
		if(edits.isEmpty()) {
			System.out.println("No edits!");
			return Collections.emptyList();
		}
		resetCache();
		long start = System.nanoTime();
		Graph graph = buildGraph(edits);
		
		List<Edit<?>> result = topologicalSort(graph);
		long end = System.nanoTime();
		System.out.println("Build graph "+(end-start)/1000000);

		this.cache.ruleUsage.forEach((r,i)->{
			System.out.println(r.information()+"\n\t Used "+i);
			System.out.println();
		});
		graph.dump();
		return result;
	}
	
	private Graph buildGraph(Set<Edit<?>> edits) {
		Environment env = this.system.getRootEnvironment();
		Graph graph = new Graph(env, edits, this);
		initialIteration(graph);
		env.autoStream(graph.nodes).forEach(n->{
//		graph.nodes.stream().forEach(n->{
			if(!n.filtered) {
				if(!this.semanticRules.checkUseful(n.edit, this.semanticRules, false)) {
					graph.filter(n);
				}
			}
		});
		
		long start, time;
		start = System.nanoTime();
		graph.buildGraph(); // pairwise check, very expensive!
		time = System.nanoTime()-start;
		System.out.println("Build graph 0: "+time/1000000);
//		start = System.nanoTime();
//		if(env.isParallel()) graph.edgeMartix.parallelFloyd();
//		else 
//			graph.edgeMartix.floyd();
//		time = System.nanoTime()-start;
//		System.out.println("Build graph 1: "+time/1000000);
		start = System.nanoTime();
		treeIteration(graph);
		treeIterationSibling(graph);
		time = System.nanoTime()-start;
		System.out.println("Build graph 2: "+time/1000000);
		return graph;
	}
	
	private void treeIterationSibling(Graph graph) {
		Optional<EObject> root = graph.eValueEditsMap.keySet().stream().findAny();
		if(root.isEmpty()) return;
		Resource resource = root.get().eResource();
		resource.getAllContents().forEachRemaining(e->{
			Set<Edit<?>> associatedEdits = graph.eValueEditsMap.getOrDefault(e, Collections.emptySet());
			if(associatedEdits.isEmpty()) return;
			List<Edit<?>> editList = associatedEdits.stream().collect(Collectors.toList());
			IntStream first = IntStream.range(0, associatedEdits.size());
			first.forEach(fi->{
				Edit<?> fe = editList.get(fi);
				Node fn = graph.editNodeMap.get(fe);
				IntStream second = IntStream.range(fi+1, associatedEdits.size());
				second.forEach(si->{
					Edit<?> se = editList.get(si);
					Node sn = graph.editNodeMap.get(se);
					
//					if(graph.edgeMartix.get(fn.id, sn.id) || graph.edgeMartix.get(sn.id, fn.id)) return;
					if(graph.edgeMartix.checkReachable(fn.id, sn.id) || graph.edgeMartix.checkReachable(sn.id, fn.id)) return;
					
					for(SchedulingRule r : this.semanticRules.treeIterationSiblingRules) {
						Priority pri = r.compare(fe, se);
						if(pri==Priority.leftFirst) {
							graph.addEdge(fn, sn, r);
							break;
						} else if(pri==Priority.rightFirst) {
							graph.addEdge(sn, fn, r);
							break;
						}
					}
				});
			});
		});
	}
	
	private void treeIteration(Graph graph) {
		// 1. get root EObject
		// 2. traverse the tree as follows
		//    a. keep a collection P of parent edits
		//    b. for current node, get associated edits C
		//    c. pairwise check the elements in P and C and create edges
		//    d. copy P to P'
		//    d' for each p in P', if p is used and p is forgettable for all compatible rules, remove p from P'
		//    e. append C to P'
		//    f. go down
		
		Optional<EObject> root = graph.eValueEditsMap.keySet().stream().findAny();
		if(root.isEmpty()) return;
		
		EObject rootEObject = EcoreUtil.getRootContainer(root.get());
		Set<Edit<?>> parentEdits = Collections.emptySet();
		
		traverseForLeftFirst(rootEObject, parentEdits, graph);
	}

	private void traverseForLeftFirst(EObject rootEObject, Set<Edit<?>> parentEdits, Graph graph) {
		Set<Edit<?>> associatedEdits = graph.eValueEditsMap.getOrDefault(rootEObject, Collections.emptySet()).stream()
				.filter(e->e.getContext().getView()==rootEObject).collect(Collectors.toSet());
		
		List<SchedulingRule> leftFirstRules = this.semanticRules.getTreeIterationLeftFirstRules();
		
		Set<Edit<?>> nextParentEdits = new HashSet<>();
		
		parentEdits.stream().forEach(pe->{
			Node pn = graph.editNodeMap.get(pe);
			boolean used = false;
			
			for(Edit<?> ce : associatedEdits) {
				Node cn = graph.editNodeMap.get(ce);
//				if(graph.edgeMartix.get(cn.id, pn.id)) { // if there is an reverse order
				if(graph.edgeMartix.checkReachable(cn.id, pn.id)) { // if there is an reverse order
					nextParentEdits.add(pe);
				} else {
//					if(graph.edgeMartix.get(pn.id, cn.id)) {
					if(graph.edgeMartix.checkReachable(pn.id, cn.id)) {
						continue;
					} else {
						for(SchedulingRule r : leftFirstRules) {
							if(r.compare(pe, ce)==Priority.leftFirst) {
								graph.addEdge(pn, cn, r);
								used = true;
								break; // no need for extra edge between pn and cn
							}
						}
					}
				}
			}
			boolean forgettable = false; // pn is forgettable if pn is used and no rule claims that pn is unforgettable
			if(used) {
				forgettable = true;
				for(SchedulingRule r : leftFirstRules) {
					if(r.leftPredicate().match(pe.getContext(), null)) {
						if(this.semanticRules.isForgettableLeftFirstRule(r)==false) {
							forgettable = false;
							break;
						}
					}
				}
			} else forgettable = false;
			
			if(!forgettable) {
				nextParentEdits.add(pe);
			}
		});
		
		nextParentEdits.addAll(associatedEdits);
		
//		nextParentEdits = nextParentEdits.parallelStream().filter(e->this.semanticRules.checkUseful(e, leftFirstRules, true)).collect(Collectors.toSet());
		
		for(EObject child : rootEObject.eContents()) {
			traverseForLeftFirst(child, nextParentEdits, graph);
		}
	}

	private void initialIteration(Graph graph) {
		List<SchedulingRule> initialRules = this.semanticRules.getInitialRules();
		graph.nodes.parallelStream().forEach(node->{
			boolean filtered = false;
			for(SchedulingRule r : initialRules) {
				if(r.initialIterate(node)) filtered = true;
			}
			if(filtered) {
				graph.filter(node);
			}
		});
	}

	private List<Edit<?>> topologicalSort(Graph graph) {
		System.out.println("topological sort!");

		long startTime, endTime;
		startTime = System.nanoTime();
		graph.buildNodeDegree();
		
		List<Node> result = new ArrayList<>(graph.nodes.size());
		List<Node> zeroOutNodes = new ArrayList<>(graph.nodes.size());
		graph.nodes.stream().forEach(n->{
			if(n.getTempIncomingDegree()==0) result.add(n);
			else if(graph.hasOutputEdges(n)==false) zeroOutNodes.add(n);
		});
		
//		result.addAll(graph.nodes.stream().filter(n->n.getTempIncomingDegree()==0).collect(Collectors.toList()));
		
//		List<Node> zeroOutNodes = graph.nodes.stream().filter(n->graph.hasOutputEdges(n)==false).collect(Collectors.toList());
		
		endTime = System.nanoTime();
		System.out.println("S1: "+(endTime - startTime)/1000000);
		
		Set<Node> nodesRemain = new HashSet<>(graph.nodes);
		nodesRemain.removeAll(result);
		nodesRemain.removeAll(zeroOutNodes);

		Set<Node> nodesRemoved = new HashSet<>();
		
		startTime = System.nanoTime();
		List<Node> syncResult = Collections.synchronizedList(result);
		Set<Node> syncNodesRemoved = Collections.synchronizedSet(nodesRemoved);
		
		int zeroOuts = 0;
		int start = 0;
		
		int nodeSize = graph.nodes.size() - zeroOutNodes.size();

		while(result.size()<nodeSize) {
			int size = result.size();
			for(int i = start; i<size;i++) {
				Node ni = result.get(i);
				if(graph.hasOutputEdges(ni)) {
					nodesRemoved.clear();
//				for(int j=0;j<graph.nodes.size();j++) {
//				IntStream.range(0, graph.nodes.size()).parallel().forEach(j->{					
//				for(Node tar : nodesRemain) {
					nodesRemain.parallelStream().forEach(tar->{
						if(graph.edgeMartix.get(ni.id, tar.id)) {
//						Node tar = graph.nodes.get(j);
							if(tar.decTempIncomingDegree()==0) {
								syncResult.add(tar);
								syncNodesRemoved.remove(tar);
//							result.add(tar);
//							nodesRemoved.remove(tar);
							}
						}
					});
//				}
					nodesRemain.removeAll(nodesRemoved);
				} else {
					zeroOuts ++;
				}
			}
			if(size==result.size()) {
				System.out.println("Cannot do topological sort!");
				graph.findCircle();
				throw new RuntimeException("Cannot do topological sort!");
			}
			start = size;
		}
		result.addAll(zeroOutNodes);
		endTime = System.nanoTime();
		System.out.println("S2: "+(endTime - startTime)/1000000);
		System.out.println("Zero outs: "+zeroOuts);
		
		return result.stream().map(r->r.edit).collect(Collectors.toList());
	}
	
	static public Random random = new Random();
	
	class SchedulingCache {
		Map<org.eclipse.xtext.xbase.lib.Pair<EditContext, EditPredicate>, Boolean> predicateCache;
		Map<org.eclipse.xtext.xbase.lib.Pair<EObject, EObject>, Boolean> eObjectDependency;
		Map<org.eclipse.xtext.xbase.lib.Pair<SourceObject, SourceObject>, Boolean> objectDependency;
		Map<SchedulingRule, AtomicInteger> ruleUsage;
		Map<EditKind, AtomicInteger> editSortCount;

		SchedulingCache() {
			predicateCache = new ConcurrentHashMap<>();
			eObjectDependency = new ConcurrentHashMap<>();
			objectDependency = new ConcurrentHashMap<>();
			ruleUsage = new ConcurrentHashMap<>();
			editSortCount = new ConcurrentHashMap<>();
		}
		void reset() {
			predicateCache.clear();
			eObjectDependency.clear();
			objectDependency.clear();
			ruleUsage.clear();
			editSortCount.clear();
		}
		Boolean checkCache(EditContext context, EditPredicate predicate) {
			org.eclipse.xtext.xbase.lib.Pair<EditContext, EditPredicate> p = org.eclipse.xtext.xbase.lib.Pair.of(context, predicate);
			return predicateCache.get(p);
		}
		Boolean checkCache(EObject left, EObject right) {
			org.eclipse.xtext.xbase.lib.Pair<EObject, EObject> p = org.eclipse.xtext.xbase.lib.Pair.of(left, right);
			return eObjectDependency.get(p);
		}
		Boolean checkCache(SourceObject left, SourceObject right) {
			org.eclipse.xtext.xbase.lib.Pair<SourceObject, SourceObject> p = org.eclipse.xtext.xbase.lib.Pair.of(left, right);
			return objectDependency.get(p);
		}
		void setCache(EditContext context, EditPredicate predicate, boolean result) {
			org.eclipse.xtext.xbase.lib.Pair<EditContext, EditPredicate> p = org.eclipse.xtext.xbase.lib.Pair.of(context, predicate);
			predicateCache.put(p, result);
		}
		void setCache(EObject left, EObject right, boolean result) {
			org.eclipse.xtext.xbase.lib.Pair<EObject, EObject> p = org.eclipse.xtext.xbase.lib.Pair.of(left, right);
			eObjectDependency.put(p, result);
		}
		void setCache(SourceObject left, SourceObject right, boolean result) {
			org.eclipse.xtext.xbase.lib.Pair<SourceObject, SourceObject> p = org.eclipse.xtext.xbase.lib.Pair.of(left, right);
			objectDependency.put(p, result);
		}
		
		public void triggerRule(SchedulingRule r) {
			AtomicInteger i = this.ruleUsage.get(r);
			if(i==null) {
				synchronized (this.ruleUsage) {
					i = this.ruleUsage.get(r);
					if(i==null) {
						i = new AtomicInteger(0);
						this.ruleUsage.put(r, i);
					}
				}
			}
			i.incrementAndGet();
		}
		

		void triggerEdit(Edit<?> r) {
			EditKind key = r.getContext().getValueForKey(EditContext.OPERATION);
			AtomicInteger i = this.editSortCount.get(key);
			if(i==null) {
				synchronized (this.editSortCount) {
					i = this.editSortCount.get(key);
					if(i==null) {
						i = new AtomicInteger(0);
						this.editSortCount.put(key, i);
					}
				}
			}
			i.incrementAndGet();
		}
	}
	
	SchedulingCache cache = new SchedulingCache();
	protected void resetCache() {
		cache.reset();
	}
	protected Boolean checkCache(EditContext context, EditPredicate predicate) {
		return cache.checkCache(context, predicate);
	}
	protected Boolean checkCache(EObject left, EObject right) {
		return cache.checkCache(left, right);
	}
	protected Boolean checkCache(SourceObject left, SourceObject right) {
		return cache.checkCache(left, right);
	}
	protected void setCache(EditContext context, EditPredicate predicate, boolean result) {
		cache.setCache(context, predicate, result);
	}
	protected void setCache(EObject left, EObject right, boolean result) {
		cache.setCache(left, right, result);
	}
	protected void setCache(SourceObject left, SourceObject right, boolean result) {
		cache.setCache(left, right, result);
	}
	protected void triggerRule(SchedulingRule r) {
		cache.triggerRule(r);
	}
}
