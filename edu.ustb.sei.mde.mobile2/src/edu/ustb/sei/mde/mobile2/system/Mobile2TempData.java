package edu.ustb.sei.mde.mobile2.system;

public abstract class Mobile2TempData {

	public Mobile2TempData() {
	}
	
	
	public abstract void init();
	public abstract void reset();
}
