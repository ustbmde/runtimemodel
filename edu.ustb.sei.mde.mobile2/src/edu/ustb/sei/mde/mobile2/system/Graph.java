package edu.ustb.sei.mde.mobile2.system;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

import edu.ustb.sei.mde.mobile2.core.edits.Edit;
import edu.ustb.sei.mde.mobile2.core.edits.EditContext;
import edu.ustb.sei.mde.mobile2.core.edits.Edit.EditKind;
import edu.ustb.sei.mde.mobile2.util.BitSetMatrix;

class Graph {
		private EditScheduler scheduler;
		private Environment environment;
		public BitSetMatrix edgeMartix;
		private AtomicInteger nextIndex;
		private Collection<Edge> edgeList;
		
		Map<Edit<?>, Node> editNodeMap;
		Map<EObject, Set<Edit<?>>> eValueEditsMap;
		
		protected int nextIndex() {
			return nextIndex.getAndIncrement();
		}
		
		public Graph(Environment environment, Set<Edit<?>> lists, EditScheduler scheduler) {
			this.scheduler = scheduler;
			this.environment = environment;
			edgeMartix = new BitSetMatrix(lists.size(), lists.size());
			
			nodes = environment.autoList(lists.size());
			nextIndex = new AtomicInteger();
			edgeList = new ConcurrentLinkedQueue<>();
			
			editNodeMap = environment.autoMap();
			eValueEditsMap = environment.autoMap();
			
			initializeNodesFromEdits(lists);
			
//			forkBuildGraph(lists);
//			quickBuildGraph(lists);
		}
		
		public void addEdge(Node source, Node target, SchedulingRule reason) {
			edgeMartix.set(source.id, target.id);
			
//			Edge e = new Edge();
//			e.source = source;
//			e.target = target;
//			e.reason = reason;
//			source.addOutgoing(e);
//			edgeList.add(e);
		}
		
		
		public int[] outgoingDegrees;
		
		public void buildNodeDegree() {
			environment.autoStream(nodes).forEach(n->n.initTempIncomingDegree());
			
			outgoingDegrees = edgeMartix.countAllRows();
		}
		
		protected Priority buildEdge(Node f, Node l) {
			Priority priority = Priority.unknown;
			SchedulingRule reason = null;
			
			EditKind leftKind = f.edit.getContext().getValueForKey(EditContext.OPERATION);
			EditKind rightKind = l.edit.getContext().getValueForKey(EditContext.OPERATION);
			
			List<SchedulingRule> rulesForLeftRight = scheduler.semanticRules.getRulesFor(leftKind, rightKind);
			List<SchedulingRule> rulesForRightLeft = scheduler.semanticRules.getRulesFor(rightKind, leftKind);
			
			for(SchedulingRule rr : rulesForLeftRight) {
				reason = rr;
				priority = rr.compare(f.edit, l.edit);
				if(priority!=Priority.unknown) {
					break;
				}
			}
			
			if(priority==Priority.unknown) {
				for(SchedulingRule rr : rulesForRightLeft) {
					reason = rr;
					priority = rr.compare(l.edit, f.edit);
					if(priority!=Priority.unknown) {
						priority = priority.opposite();
						break;
					}
				}
			}
			
			if(priority==Priority.leftFirst) {
				addEdge(f,l,reason);
			} else if(priority==Priority.rightFirst) {
				addEdge(l,f,reason);
			} else {
			}
			
			return priority;
		}
		
//		private void forkBuildGraph(Set<Edit<?>> lists) {
//			ForkBuildGraphAction action = new ForkBuildGraphAction(this, 0, lists.size());
//			ForkJoinPool pool = new ForkJoinPool();
//			
//			pool.invoke(action);
//		}
//		
//		@SuppressWarnings("serial")
//		class ForkBuildGraphAction extends RecursiveAction {
//			public ForkBuildGraphAction(Graph graph, int from, int to) {
//				super();
//				this.graph = graph;
//				this.from = from;
//				this.to = to;
//			}
//			private Graph graph;
//			private int from;
//			private int to;
//			
//			private void doSimpleAction() {
//				IntStream firstStream = IntStream.range(0, nodes.size()).parallel();
//				firstStream.forEach(first->{
//					Node f = nodes.get(first);
//					IntStream secondStream = IntStream.range(first+1, nodes.size()).parallel();
//					secondStream.forEach(second->{
//						Node l = nodes.get(second);
//						buildEdge(f, l);
//					});
//				});
//				
//				graph.edgeMartix.floyd(from, to);
//			}
//			
//			@Override
//			protected void compute() {
//				int size = to-from;
//				if(size<500) {
//					doSimpleAction();
//				} else {
//					int leftFrom = from;
//					int leftTo = (from+to)/2;
//					int rightFrom = leftTo;
//					int rightTo = to;
//					
//					ForkBuildGraphAction firstHalfAction = new ForkBuildGraphAction(graph, leftFrom, leftTo);
//					ForkBuildGraphAction secondHalfAction = new ForkBuildGraphAction(graph, rightFrom, rightTo);
//					
//					invokeAll(firstHalfAction, secondHalfAction);
//					
//					IntStream leftStream = IntStream.range(leftFrom, leftTo).parallel();
////					for(int left=leftFrom; left<leftTo;left++) {
//					leftStream.forEach(left->{
//						Node leftNode = nodes.get(left);
//						IntStream rightStream = IntStream.range(rightFrom, rightTo).parallel();
////						for(int right=rightFrom; right<rightTo; right++) {
//						rightStream.forEach(right->{
//							Node rightNode = nodes.get(right);
////							if(edgeMartix.get(left, right) || edgeMartix.get(right, left)) return;
//							
//							Priority pri = buildEdge(leftNode, rightNode);
////							if(pri==Priority.leftFirst) {
////								edgeMartix.propagate(left, right, leftFrom, leftTo, rightFrom, rightTo);
////							} else if(pri==Priority.rightFirst) {
////								edgeMartix.propagate(right, left, rightFrom, rightTo, leftFrom, leftTo);
////							}
//						});
//					});
//				}
//			}
//
//			
//		}
		
		

		public void buildGraph() {
			if(environment.isParallel()) {
				List<Node> unfilteredNodes = nodes.stream().filter(n->n.filtered==false).collect(Collectors.toList());
				
				IntStream firstStream = IntStream.range(0, unfilteredNodes.size()).parallel();
				firstStream.forEach(first->{
					Node f = unfilteredNodes.get(first);
					IntStream secondStream = IntStream.range(first+1, unfilteredNodes.size()).parallel();
					secondStream.forEach(second->{
						Node l = unfilteredNodes.get(second);
						buildEdge(f, l);
					});
				});
			} else {
				for(int i=0;i<nodes.size();i++) {
					Node f = nodes.get(i);
					for(int j=i+1;j<nodes.size();j++) {
						Node l = nodes.get(j);
						buildEdge(f, l);
					}
				}
			}
		}

		private void initializeNodesFromEdits(Set<Edit<?>> lists) {
			environment.autoStream(lists).forEach(e->{
				Node n = new Node(this, environment);
				n.edit = e;
				nodes.add(n);
				scheduler.cache.triggerEdit(e);
				
				this.editNodeMap.put(e, n);
				
				EObject viewElement = e.getContext().getView();
				if(viewElement!=null)
					associateEdit(viewElement, e);

				Object viewValue = e.getContext().getViewValue();
				
				if(viewValue instanceof Collection) {
					Collection<?> col = (Collection<?>) viewValue;
					for(Object o : col) {
						if(o instanceof EObject)
							associateEdit((EObject) o, e);
					}
				} else {
					if(viewValue instanceof EObject)
						associateEdit((EObject) viewValue, e);
				}
			});
			Collections.sort(nodes, (a,b)->a.id-b.id);
		}
		
		private void associateEdit(EObject eVal, Edit<?> edit) {
			if(eVal==null) {
				System.out.println(edit.getContext());
			}
			Set<Edit<?>> set = eValueEditsMap.get(eVal);
			if(set==null) {
				synchronized (eVal) {
					set = eValueEditsMap.get(eVal);
					if(set==null) {
						set = environment.autoSet();
						eValueEditsMap.put(eVal, set);
					}
				}
			}
			set.add(edit);
		}
		
		
		public void findCircle() {
			Set<Node> v = new HashSet<>();
			Set<Node> t = new HashSet<>();
			
			nodes.forEach(n->{
				findCircle(n, v, t);
			});
		}
		private void findCircle(Node cur, Set<Node> visited, Set<Node> tested) {
			if(tested.contains(cur)) return;
			if(visited.contains(cur)) {
				System.out.println("Circle found at "+cur);
			} else {
				visited.add(cur);
				for(int i=0;i<nodes.size();i++) {
					if(edgeMartix.get(cur.id, i)) {
						Node tar = nodes.get(i);
						findCircle(tar, visited, tested);
					}
				}
				visited.remove(cur);
			}
			tested.add(cur);
		}

		public List<Node> nodes = null;

		public void dump() {
			System.out.println("node count: "+nodes.size());
			System.out.println("\t isolated node count: "+nodes.stream().filter(n->(edgeMartix.countCol(n.id)+edgeMartix.countRow(n.id))==0).count());
//			System.out.println("\t isolated node count: "+nodes.stream().filter(n->n.incomings.isEmpty() && n.outgoings.isEmpty()).count());
			System.out.println("edge count: "+edgeList.size());
			scheduler.cache.editSortCount.forEach((k,i)->{
				System.out.println(k.toString()+": "+i.get());
			});
			
//			nodes.stream().filter(n->(edgeMartix.countCol(n.id)+edgeMartix.countRow(n.id))==0).limit(20).forEach(n->{
//				System.out.println(n.edit.getContext());
//			});
//			nodes.forEach(n->{
//				n.outgoings.forEach(e->{
//					System.out.print(e.source.edit.getId());
//					System.out.print(" is before ");
//					System.out.print(e.target.edit.getId());
//					System.out.println(" due to ");
//					System.out.println(e==null? "transitive edge" : e.reason.information());
//				});
//			});
		}

		public void filter(Node node) {
			node.filtered = true;
		}

		public boolean hasOutputEdges(Node ni) {
			return outgoingDegrees[ni.id]!=0;
		}
		
		
		
	}