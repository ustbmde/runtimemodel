package edu.ustb.sei.mde.mobile2.system;

import java.util.Map;

import edu.ustb.sei.mde.mobile2.core.edits.EditContext;
import edu.ustb.sei.mde.mobile2.util.Mobile2Utils;

public abstract class ContextPredicate {
	abstract public boolean check(Map<String,Object> context);
	protected EditScheduler scheduler;
	public void setEditScheduler(EditScheduler scheduler) {
		this.scheduler = scheduler;
	}
	
	
	@SuppressWarnings("unchecked")
	protected static <T> T resolveKey(Map<String, Object> context, String edit, String key, T defaultValue) {
		if(edit==null) return (T) context.getOrDefault(key,defaultValue);
		EditContext c = (EditContext) context.get(edit);
		if(c!=null) {
			return c.getShortKey(key,defaultValue);
		} else return defaultValue;
	}
	
	static public ContextPredicate implies(ContextPredicate left, ContextPredicate right) {
		return or(not(left), right);
	}
	
	static public ContextPredicate and(ContextPredicate...contextPredicates) {
		return new ContextPredicate() {
			@Override
			public boolean check(Map<String, Object> context) {
				for(ContextPredicate p : contextPredicates) {
					if(!p.check(context)) return false;
				}
				return true;
			}
			
			@Override
			public void setEditScheduler(EditScheduler scheduler) {
				super.setEditScheduler(scheduler);
				for(ContextPredicate p : contextPredicates) 
					p.setEditScheduler(scheduler);
			}
		};
	}
	
	static public ContextPredicate or(ContextPredicate...contextPredicates) {
		return new ContextPredicate() {
			@Override
			public boolean check(Map<String, Object> context) {
				for(ContextPredicate p : contextPredicates) {
					if(p.check(context)) return true;
				}
				return false;
			}
			
			@Override
			public void setEditScheduler(EditScheduler scheduler) {
				super.setEditScheduler(scheduler);
				for(ContextPredicate p : contextPredicates) 
					p.setEditScheduler(scheduler);
			}
		};
	}
	
	static public ContextPredicate not(ContextPredicate contextPredicate) {
		return new ContextPredicate() {
			@Override
			public boolean check(Map<String, Object> context) {
				return !contextPredicate.check(context);
			}
			
			@Override
			public void setEditScheduler(EditScheduler scheduler) {
				super.setEditScheduler(scheduler);
				contextPredicate.setEditScheduler(scheduler);
			}
		};
	}
	
	static public ContextPredicate valueEqualOrIn(String e1, String k1, String e2, String k2) {
		return new ContextPredicate() {
			@Override
			public boolean check(Map<String, Object> context) {
				Object v1 = resolveKey(context, e1, k1,null);
				Object v2 = resolveKey(context, e2, k2,null);
				return Mobile2Utils.equalsOrIn(v1, v2);
			}
		};
	}
	
	static public ContextPredicate toBeContain(String containerEdit, String containerKey, String elementEdit, String elementKey) {
		return new ToBeValueDependency(elementEdit, elementKey, containerEdit, containerKey);
	}
	
	static public ContextPredicate asIsContain(String containerEdit, String containerKey, String elementEdit, String elementKey) {
		return new AsIsValueDependency(elementEdit, elementKey, containerEdit, containerKey);
	}

	public static ContextPredicate valueEqual(String e1, String k1, String e2, String k2) {
		return new ContextPredicate() {
			@Override
			public boolean check(Map<String, Object> context) {
				Object v1 = resolveKey(context, e1, k1,null);
				Object v2 = resolveKey(context, e2, k2,null);
				return Mobile2Utils.equals(v1, v2);
			}
		};
	}
}
