package edu.ustb.sei.mde.mobile2.system;

/**
 * This is a special key for view that is a place holder.
 * @author hexiao
 *
 */
public class ViewInstanceKey {

	public ViewInstanceKey() {
	}

}
