package edu.ustb.sei.mde.mobile2.system;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import edu.ustb.sei.mde.mobile2.core.edits.Edit;

public abstract class SchedulingRule {
	public static final String RIGHT_EDIT = "__RIGHT__EDIT__";
	public static final String LEFT_EDIT = "__LEFT__EDIT__";

	abstract public Priority compare(Edit<?> left, Edit<?> right);
	abstract public String information();
	
	abstract public EditPredicate leftPredicate();
	abstract public EditPredicate rightPredicate();
	abstract public ContextPredicate contextPredicate();
	abstract public Priority priority();
	
	private static class SimpleRule extends SchedulingRule {
		private final ContextPredicate contextPredicate;
		private final EditPredicate rightPredicate;
		private final String reason;
		private final Priority priority;
		private final EditPredicate leftPredicate;

		private SimpleRule(EditPredicate leftPredicate, EditPredicate rightPredicate, ContextPredicate contextPredicate,
				Priority priority, String reason) {
			this.contextPredicate = contextPredicate;
			this.rightPredicate = rightPredicate;
			this.reason = reason;
			this.priority = priority;
			this.leftPredicate = leftPredicate;
		}

		@Override
		public Priority compare(Edit<?> left, Edit<?> right) {
			try {
				Map<String, Object> context = new HashMap<>();
				context.put(LEFT_EDIT, left);
				context.put(RIGHT_EDIT, right);
				if(checkPredicate(left, leftPredicate, context) 
						&& checkPredicate(right, rightPredicate, context) 
						&& checkContext(contextPredicate, context))
					return priority;
				else return Priority.unknown;
			} catch (Exception e) {
				return Priority.unknown;
			}
		}

		@Override
		public String information() {
			return reason;
		}

		@Override
		public EditPredicate leftPredicate() {
			return leftPredicate;
		}

		@Override
		public EditPredicate rightPredicate() {
			return rightPredicate;
		}

		@Override
		public ContextPredicate contextPredicate() {
			return contextPredicate;
		}

		@Override
		public Priority priority() {
			return priority;
		}
	}

	public enum ApplicationStrategy {
		pairwise,
		initialInteration,
		treeIteration,
		treeIterationSibling,
		finalIteration
	}
	
	public ApplicationStrategy strategy() {
		return ApplicationStrategy.pairwise;
	}
	
	public boolean initialIterate(Node node) {
		return false;
	}
	
	
	protected EditScheduler scheduler; 

	public void setScheduler(EditScheduler scheduler) {
		this.scheduler = scheduler;
		if(contextPredicate()!=null)
			contextPredicate().setEditScheduler(scheduler);
	}
	
	protected boolean checkPredicate(Edit<?> edit, EditPredicate predicate,
			Map<String, Object> context) {
		Boolean cachedResult = scheduler.checkCache(edit.getContext(), predicate);
		if(cachedResult==null) {
			cachedResult = predicate.match(edit.getContext(), context);
			scheduler.setCache(edit.getContext(), predicate, cachedResult);
		} else {
			predicate.fillContext(edit.getContext(), context);
		}
		return cachedResult;
	}
	
	protected boolean checkContext(ContextPredicate contextPredicate, Map<String, Object> context) {
		boolean result = contextPredicate.check(context);
		if(result)
			this.scheduler.triggerRule(this);
		return result;
	}
	
	static public SchedulingRule initialIterationRule(EditPredicate predicate, BiFunction<Node, SchedulingRule,Boolean> function, String reason) {
		return new SchedulingRule() {
			@Override
			public boolean initialIterate(Node node) {
				Map<String,Object> map = new HashMap<>();
				if(predicate.match(node.edit.getContext(), map))
					return function.apply(node, this);
				else return false;
			}
			
			
			@Override
			public ApplicationStrategy strategy() {
				return ApplicationStrategy.initialInteration;
			}

			@Override
			public Priority compare(Edit<?> left, Edit<?> right) {
				return Priority.unknown;
			}

			@Override
			public String information() {
				return reason;
			}

			@Override
			public EditPredicate leftPredicate() {
				return predicate;
			}

			@Override
			public EditPredicate rightPredicate() {
				return null;
			}

			@Override
			public ContextPredicate contextPredicate() {
				return null;
			}

			@Override
			public Priority priority() {
				return Priority.unknown;
			}
		};
	}
	
	
	static ContextPredicate naturalPredicate = ContextPredicate.toBeContain("e1", "viewValue", "e2", "view");
	static public SchedulingRule naturalRule(EditPredicate leftPredicate, EditPredicate rightPredicate,	Priority priority, String reason) {
		if(!leftPredicate.isNatural() || !rightPredicate.isNatural()) {
			throw new RuntimeException("This is not a natural rule");
		}
		
		return new SimpleRule(leftPredicate, rightPredicate, naturalPredicate, priority, reason) {
			@Override
			public ApplicationStrategy strategy() {
				return ApplicationStrategy.treeIteration;
			}
		};
	}
	
	static public SchedulingRule simpleRule(EditPredicate leftPredicate, EditPredicate rightPredicate, ContextPredicate contextPredicate,
			Priority priority, String reason) {
		return new SimpleRule(leftPredicate, rightPredicate, contextPredicate, priority, reason);
	}
	
	
	static final private ContextPredicate siblingPredicate = ContextPredicate.valueEqual("e1", "view", "e2", "view");
	static public SchedulingRule naturalSiblingRule(EditPredicate leftPredicate, EditPredicate rightPredicate, ContextPredicate contextPredicate,
			Priority priority, String reason) {
		return new SimpleRule(leftPredicate, rightPredicate, contextPredicate==null ? siblingPredicate : ContextPredicate.and(siblingPredicate, contextPredicate), priority, reason) {
			@Override
			public ApplicationStrategy strategy() {
				return ApplicationStrategy.treeIterationSibling;
			}
		};
	}
}
