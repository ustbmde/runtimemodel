package edu.ustb.sei.mde.mobile2.system;

import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

import edu.ustb.sei.mde.mobile2.core.edits.Edit.EditKind;
import edu.ustb.sei.mde.mobile2.core.edits.EditContext;

public class Pattern extends EditPredicate{
	private String name;
	private EditKind[] editKinds;
	private FeatureKind[] featureKinds;
	private String classMappingName;
	private String featureMappingName;
	
	
	public Pattern(String name, String classMappingName, String featureMappingName, EditKind[] editKinds) {
		this(name, editKinds, null);
		this.classMappingName = classMappingName;
		this.featureMappingName = featureMappingName;
	}
	
	public Pattern(String name, EditKind editKind, FeatureKind featureKind) {
		this(name, new EditKind[] {editKind}, new FeatureKind[] {featureKind});
	}
	
	public Pattern(String name, EditKind editKind) {
		this(name, new EditKind[] {editKind}, new FeatureKind[] {FeatureKind.containment});
	}
	
	public Pattern(String name, EditKind[] editKinds, FeatureKind[] featureKinds) {
		this.editKinds = editKinds;
		this.featureKinds = featureKinds;
		this.name = name;
		this.classMappingName = null; // null means do not check
		this.featureMappingName = null; // null means do not check
	}
	
	public Pattern(String name, EditKind[] kinds) {
		this(name, kinds, new FeatureKind [] {FeatureKind.containment});
	}

	public boolean match(EditContext info, Map<String, Object> context) {
		if(checkNames(info) && checkKind(info) && checkFeatureType(info)) {
			fillContext(info, context);
			return true;
		} else 
			return false;
	}
	
	public void fillContext(EditContext info, Map<String, Object> context) {
		if(context==null) return;
		context.put(name,info);
		context.putIfAbsent("environment", info.getValueForKey(EditContext.ENVIRONMENT));
	}
	
	private boolean checkNames(EditContext info) {
		final boolean cnf = classMappingName==null ? true : classMappingName.equals(info.getClassMappingName());
		final boolean fnf = featureMappingName==null ? true : featureMappingName.equals(info.getFeatureMappingName());
		return cnf && fnf;
	}

	private boolean checkKind(EditContext info) {
		EditKind editKind = info.getValueForKey(EditContext.OPERATION);
		for(EditKind kind : editKinds) {
			if(editKind==kind) {
				return true;
			}
		}
		return false;
	}

	private boolean checkFeatureType(EditContext info) {
		if(editKinds.length==1 && editKinds[0]==EditKind.create) return true;
		if(featureKinds==null) return true;
		EStructuralFeature f = info.getValueForKey(EditContext.FEATURE);
		FeatureKind k = featureKind(f);
		for(FeatureKind kind : featureKinds) {
			if(k==kind) return true;
		}
		return false;
	}
	
	private FeatureKind featureKind(EStructuralFeature f) {
		if(f==null) return null;
		else if(f instanceof EAttribute) return FeatureKind.attribute;
		else if(f instanceof EReference) {
			if(((EReference) f).isContainment()) return FeatureKind.containment;
			else return FeatureKind.noncontainment;
		} else return FeatureKind.containment;
	}

	@Override
	public void collectEditKinds(Set<EditKind> kinds) {
		for(EditKind k : editKinds) kinds.add(k);
	}
	
	@Override
	public boolean isNatural() {
		return true;
	}
}
