package edu.ustb.sei.mde.mobile2.system;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.ustb.sei.mde.mobile2.core.edits.Edit;

class Node {
		public Node(Graph graph, Environment env) {
			this.graph = graph;
			id = graph.nextIndex();
			filtered = false;
			
//			outgoings = new ArrayList<>();
//			incomings = new ArrayList<>();
//			
//			if(env.isParallel()) {
//				outgoings = Collections.synchronizedList(outgoings);
//				incomings = Collections.synchronizedList(incomings);
//			}
		}
		
		public Graph graph;
		public Edit<?> edit;
		public int id;
		boolean filtered;
		
//		public Set<Node> outgoings;
		
//		public List<Edge> outgoings;
//		public List<Edge> incomings;
//		
		
//		public void addIncoming(Edge e) {
//			incomings.add(e);
//			e.source.outgoings.add(e);
//		}
//		
//		public void addOutgoing(Edge e) {
//			outgoings.add(e);
//			e.target.incomings.add(e);
//		}
		
		private volatile int tempIncomingDegree = -1;
		
		public int getTempIncomingDegree() {return tempIncomingDegree;}
		
		public void initTempIncomingDegree() {
			tempIncomingDegree = graph.edgeMartix.countCol(id);
//			outgoings = new HashSet<>();
//			for(int i=0;i<graph.nodes.size();i++) {
//				if(graph.edgeMartix.get(id, i)) 
//					outgoings.add(graph.nodes.get(i));
//			}
		}
		
		synchronized public int decTempIncomingDegree() {
			return --tempIncomingDegree;
		}
	}