package edu.ustb.sei.mde.mobile2.system;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.stream.Stream;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;

import edu.ustb.sei.mde.mobile2.core.ElementBX;
import edu.ustb.sei.mde.mobile2.core.ElementBX.AlignMode;
import edu.ustb.sei.mde.mobile2.core.FeatureBX;
import edu.ustb.sei.mde.mobile2.core.ReferenceBX;
import edu.ustb.sei.mde.mobile2.core.edits.CreateObjectEdit;
import edu.ustb.sei.mde.mobile2.core.providers.FutureSourceObject;
import edu.ustb.sei.mde.mobile2.core.providers.FutureSourceObjectProvider;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObject;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObjectProvider;
import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;
import edu.ustb.sei.mde.mobile2.util.Mobile2Utils;
import edu.ustb.sei.mde.mobile2.util.Pair;
import edu.ustb.sei.mde.mobile2.util.TripleConsumer;
import edu.ustb.sei.mde.mobile2.util.TripleFunction;

/**
 * SourcePool is a snapshot of the system.
 * We cache the object map in source pool.
 * @author hexiao
 *
 */
public class SourcePool {
	// objects that exist in the system now
	private Collection<SourceObject> knownObjects;
	private Map<Object, SourceObject> objectMap;
	private Map<EObject, SourceObject> viewIndexMap; // from view to knownObjects
	
	// from view to null->new or old->new
	private Map<EObject, Pair<SourceObject, FutureSourceObjectProvider>> newObjectMap;
	
	

	private Environment environment;
	
	
	public Collection<SourceObject> getKnownObjects() {
		return knownObjects;
	}
	
	public void addObject(SourceObject so) {
		knownObjects.add(so);
		SourceObject oldValue = objectMap.put(so.getValue(), so);
		if(oldValue!=null) 
			System.out.println("replaced "+so.getValue()+" "+oldValue.getValue());
	}
	
	
	public void dynamicReload(SourceObjectProvider so, Object o) {
		SourceObjectProvider finalProvider = so.getFinalProvider();
		if(finalProvider instanceof SourceObject) {
			assert finalProvider==so;
			reloadObject((SourceObject)so, o);
		} else {
			assert finalProvider instanceof FutureSourceObjectProvider;
			((FutureSourceObjectProvider)finalProvider).setValue(o);
		}
	}
	
	public void reloadObject(SourceObject so, Object o) {
		if(so.getValue()!=null)
			objectMap.remove(so.getValue());
		so.setSourceValue(o);
		objectMap.put(o, so);
	}
	
	public void removeObject(SourceObject so) {
		if(so.getValue()!=null)
			objectMap.remove(so.getValue());
		knownObjects.remove(so);
	}
	
	/**
	 * The method uses buildSourceTree to initialize a source pool. 
	 * In the beginning, objects and object map will be cleared. 
	 * @param root
	 * @param initializer
	 */
	protected void initSourcePool(Object root, TripleConsumer<SourceObject, SourceObject, ElementBX> initializer) {
		knownObjects.clear();
		newObjectMap.clear();
		objectMap.clear();
		
		// TODO: for debug
		AtomicInteger count = new AtomicInteger(0);
		buildSourceTree(null, root, environment.getSystemBX().getRootBX(), (parent, o, bx)->{
			SourceObject so = objectMap.get(o);
			if(so==null) {
				so = (SourceObject) SourceProvider.object(o);
				addObject(so);
				count.incrementAndGet();
			}
			initializer.accept(parent, so, bx);
			return so;
		});
		System.out.println(count.get()+" times");
	}

	public SourcePool(Environment env) {
		knownObjects = env.autoSet();
		objectMap = env.autoMap();
		newObjectMap = env.autoMap();
		this.viewIndexMap = null;
		this.environment = env;
	}
	
	public void buildViewIndex() {
		this.viewIndexMap = this.environment.autoMap();
		this.environment.autoStream(knownObjects).forEach(so->{
			if(so.getViewElement()!=null) {
				this.viewIndexMap.put(so.getViewElement(), so);
			}
		});
	}
	
	public SourceObject getSourceObject(Object object) {
		SourceObject so = this.objectMap.get(object);
		if(so==null) {
			System.out.println(object+" is not managed in SourcePool!");
		}
		return so;
	}
	
	public SourceObjectProvider findAsIsObject(EObject view) {
		return this.viewIndexMap.get(view);
		
//		for(SourceObjectProvider so : knownObjects) {
//			if(Mobile2Utils.equals(so.getViewElement(), view)) 
//				return so;
//		}
//		return null;
//		this.environment.autoStream(knownObjects)
	}
	
	public SourceObjectProvider findToBeObject(EObject view) {
		Pair<SourceObject,FutureSourceObjectProvider> pair = newObjectMap.get(view);
		if(pair!=null) 
			return pair.first;
		else  { 
			SourceObjectProvider res = findAsIsObject(view);
			if(res==null) {
				System.out.println("findToBeObject failed!! thread unsafe!");
			}
			return res;
		}
//		Optional<SourceObject> res = knownObjects.stream().filter(o->Mobile2Utils.equals(o.getViewElement(), view)).findAny();
//		if(res.isPresent()) return res.get();
//		System.out.println("findToBeObject failed!! thread unsafe!");
	}

	// thread safe
	public void registerObjectChange(EObject view, SourceObject asIs, CreateObjectEdit toBe) {
		Pair<SourceObject,FutureSourceObjectProvider> pair = newObjectMap.get(view);
		toBe.setViewElement(view);
		if(pair==null) {
			// assert asIs.isEmpty()==false, i.e., asIs must be an existing SourceObject
			pair = Pair.pair(asIs, toBe);
		} else {
			// assert pair.first.isEmpty() && pair.second==null && asIs==null, i.e., pair.first must be a new empty SourceObject
			assert pair.first.isEmpty() && pair.second==null && (asIs==null || asIs.isEmpty());
			pair.second = toBe;
		}
		pair.first.setReplacement(toBe);
		newObjectMap.put(view, pair);
	} 
	
	// thread unsafe
	public void registerReloadableObject(EObject view, SourceObject asIs) {
		synchronized (view) {			
			Pair<SourceObject,FutureSourceObjectProvider> pair = newObjectMap.get(view);
			if(pair!=null && pair.second!=null) return;
			else {
				if(pair==null) {
					pair = Pair.pair(asIs, null);
					newObjectMap.put(view, pair);
				}
				pair.second = new FutureSourceObject(asIs);
				pair.first.setReplacement(pair.second);
			}
		}
	}
	
	public void initializeStateBasedGet(Object root) {
		initSourcePool(root, (parent, so, bx)->{
			EObject initialView = bx.createView(); // create an initial view element
			so.setViewElement(initialView);
			// TODO: need support for local keys
			Object sourceKey = calculateSourceKeyWithLocalSupport(bx, parent, so.getValue());
			so.setSourceKey(sourceKey);
		});
		
		System.out.println(knownObjects.size()+" known objects");
		System.out.println(objectMap.size()+" objects in map");
		sourceRefreshed();
	}

	public void initializeStateBasedPut(Object root, Resource viewRes) {
		Map<Object,SourceObjectProvider> keyMap = this.environment.autoMap();
		
		initSourcePool(root, (parent, so, bx)->{
			Object sourceKey = calculateSourceKeyWithLocalSupport(bx, parent, so.getValue());
			// TODO: need support for local keys
			so.setSourceKey(sourceKey);
			keyMap.put(so.getSourceKey(), so);
			
		});
		
		Set<SourceObjectProvider> unmatchedSource = this.environment.autoSet(this.knownObjects);
//		Set<EObject> unmatchedView = this.environment.autoSet();	
		ElementBX curBX = this.environment.getSystemBX().getRootBX();
		
		EObject vRoot = viewRes.getContents().isEmpty() ? null : viewRes.getContents().get(0);
		
		navigateInView(vRoot, curBX, (e, bx) -> {
			Optional<SourceObjectProvider> so = bx.findSource(e, unmatchedSource, keyMap, this.environment, AlignMode.initial);
			if(so.isPresent()) {
				SourceObject s = (SourceObject) so.get();
				s.setViewElement(e);
				unmatchedSource.remove(s);
			} else {
//				unmatchedView.add(e);
				SourceObject newObject = (SourceObject) SourceProvider.newObject(e);
				this.newObjectMap.put(e, Pair.pair(newObject, null));
			}
		});
		
		stream(unmatchedSource).forEach(ums->ums.setViewElement(null));

		buildViewIndex();
		sourceRefreshed();
	}

	public void initializeDeltaBasedPut(Object root, Resource viewRes) {
		newObjectMap.clear();
		Set<EObject> matchedView = this.environment.autoSet();
		Set<SourceObjectProvider> unmatchedSource = this.environment.autoSet();
		Map<Object, SourceObjectProvider> keyMap = this.environment.autoMap();
		
		List<SourceObject> removed = updateSourcePool(root, (parent, sourceObject,bx)->{
			// check the view existence
			EObject viewElement = sourceObject.getViewElement();
			Object newKey = calculateSourceKeyWithLocalSupport(bx, parent, sourceObject.getValue());
			// TODO: need support for local keys
			if(viewElement!=null) {
				if(!Mobile2Utils.belongToView(viewElement, environment)) {
					sourceObject.setViewElement(null);
				} else {
					matchedView.add(viewElement);
				}
			} else {
				if(sourceObject.getSourceKey()==null) {
					sourceObject.setSourceKey(newKey);
				}
				unmatchedSource.add(sourceObject);
				keyMap.put(sourceObject.getSourceKey(), sourceObject);
			}
			
			// Refresh the source key. It is safe to do so
			sourceObject.setSourceKey(newKey);
		});
		
		knownObjects.removeAll(removed);
		removed.forEach(r->{
			objectMap.remove(r.getValue());
		});
		
		EObject vRoot = viewRes.getContents().isEmpty() ? null : viewRes.getContents().get(0);
		
		navigateInView(vRoot, this.environment.getSystemBX().getRootBX(), (e, bx)->{
			if(!matchedView.contains(e)) {			
				Optional<SourceObjectProvider> so = bx.findSource(e, unmatchedSource, keyMap, environment, AlignMode.initial);
				if(so.isPresent()) {
					SourceObjectProvider s = so.get();
					s.setViewElement(e);
					unmatchedSource.remove(s);
				} else {
					SourceObject newObject = (SourceObject) SourceProvider.newObject(e);
					this.newObjectMap.put(e, Pair.pair(newObject, null));
				}
			}
		});
		
		buildViewIndex();
		
		sourceRefreshed();
	}
	
	@SuppressWarnings("unchecked")
	private void navigateInView(EObject vRoot, ElementBX curBX, BiConsumer<EObject, ElementBX> action) {
		if(vRoot == null) return;
		ElementBX actualBX  = curBX.getContainer().findBX(vRoot, curBX);
		
		action.accept(vRoot, actualBX);
		
		for(FeatureBX<?, ?, ?, ?> f : actualBX.getFeatureBXs()) {
			EStructuralFeature viewFeature = f.getViewFeature();
			if(viewFeature instanceof EReference && ((EReference) viewFeature).isContainment()) {
				if(viewFeature.isMany()) {
					List<EObject> col = (List<EObject>) vRoot.eGet(viewFeature);
					stream(col).forEach(co->{						
						navigateInView(co, ((ReferenceBX<?, ?>)f).getValueBX(), action);
					});
				} else {
					EObject co = (EObject) vRoot.eGet(viewFeature);
					navigateInView(co, ((ReferenceBX<?, ?>)f).getValueBX(), action);
				}
			}
		}
	}
	
	
	protected List<SourceObject> updateSourcePool(Object root, TripleConsumer<SourceObject, SourceObjectProvider, ElementBX> initializer) {
		Set<SourceObject> checked = this.environment.autoSet();
		
		// update containment
		buildSourceTree(null, root, environment.getSystemBX().getRootBX(), (parent, object, bx)->{
			SourceObject sourceObject = objectMap.get(object);
			if(sourceObject==null) {
				sourceObject = (SourceObject) SourceProvider.object(object);
				addObject(sourceObject);
			}
			checked.add(sourceObject);
			initializer.accept(parent, sourceObject, bx);
			return sourceObject;
		});
		
		// compute objects to be removed
		List<SourceObject> removed = new ArrayList<>(objectMap.size()-checked.size());
		objectMap.entrySet().forEach(e->{
			if(!checked.contains(e.getValue())) {
				removed.add(e.getValue());
			}
		});
		
		return removed;
	}
	
	
	/**
	 * The method <code>buildSourceTree</code> will iterate the source tree to discover new objects and visit existing objects that are in the tree. 
	 * It will ignore all the objects that are not in the tree.
	 * @param root
	 * @param rootBX
	 * @param action
	 * @return
	 */
	protected SourceObject buildSourceTree(SourceObject parent, Object root, ElementBX rootBX, TripleFunction<SourceObject, Object, ElementBX, SourceObject> action) {
		if(root==null) return null;
		ElementBX actualBX = rootBX.getContainer().findBX(root, rootBX);
		SourceObject so = action.apply(parent, root, actualBX);
		
		stream(actualBX.getFeatureBXs()).forEach(feature->{
			if(feature instanceof ReferenceBX) {
				if(((ReferenceBX<?, ?>) feature).isContainment()) {
					if(feature.isMany()) {
						@SuppressWarnings("unchecked")
						List<Object> col = (List<Object>) feature.externalGetWithCache(so, environment);
						so.cacheFeatureValue(feature, col);
						stream(col).forEach(o -> {
							SourceObject co = buildSourceTree(so, o, ((ReferenceBX<?, ?>) feature).getValueBX(), action);
							co.setContainer(so);
						});
					} else {
						Object tar = feature.externalGetWithCache(so, environment);
						so.cacheFeatureValue(feature, tar);
						SourceObject co = buildSourceTree(so, tar, ((ReferenceBX<?, ?>) feature).getValueBX(), action);
						co.setContainer(so);
					}
				}
			}
		});
		
//		for(FeatureBX<?,?,?,?> feature : actualBX.getFeatureBXs()) {
//			if(feature instanceof ReferenceBX) {
//				if(((ReferenceBX<?, ?>) feature).isContainment()) {
//					if(feature.isMany()) {
//						@SuppressWarnings("unchecked")
//						List<Object> col = (List<Object>) feature.externalGetWithCache(so, environment);
//						so.cacheFeatureValue(feature, col);
//						for(Object o : col) {
//							SourceObject co = buildSourceTree(o, ((ReferenceBX<?, ?>) feature).getValueBX(), action);
//							co.setContainer(so);
//						}
//					} else {
//						Object tar = feature.externalGetWithCache(so, environment);
//						so.cacheFeatureValue(feature, tar);
//						SourceObject co = buildSourceTree(tar, ((ReferenceBX<?, ?>) feature).getValueBX(), action);
//						co.setContainer(so);
//					}
//				}
//			}
//		}
		
		return so;
	}
	
	public void dump(PrintStream out) {
		out.println(objectMap.size()+" existing objects");
		objectMap.entrySet().forEach(e->{
			out.print(e.getKey());
			out.print(" => ");
			out.print(e.getValue());
			out.print(" ---> ");
			out.println(e.getValue().getViewElement());
		});
		
		out.println(newObjectMap.size()+" new or changed objects");
		newObjectMap.entrySet().forEach(e->{
			out.print(e.getValue().first);
			out.print(" => ");
			out.print(e.getValue().second);
			out.print(" ---> ");
			out.println(e.getKey());
		});
	}

	public void initializeDeltaBasedGet(Object root, Resource viewRes) {
		newObjectMap.clear();
		
		Set<EObject> matchedView = this.environment.autoSet();
		
		List<SourceObject> removed = updateSourcePool(root, (parent, sourceObject,bx)->{
			EObject viewElement = sourceObject.getViewElement();
			if(viewElement!=null) {
				matchedView.add(viewElement);
			} else {
				viewElement = bx.createView();
				sourceObject.setViewElement(viewElement);
			}
			
			sourceObject.setSourceKey(calculateSourceKeyWithLocalSupport(bx, parent, sourceObject.getValue()));
		});
		
		knownObjects.removeAll(removed);
		removed.forEach(r->{
			objectMap.remove(r.getValue());
		});
		
		Set<EObject> removedView = this.environment.autoSet();
		
		viewRes.getAllContents().forEachRemaining(v->{
			if(!matchedView.contains(v)) removedView.add(v);
		});
		removedView.forEach(r->{
			EcoreUtil.delete(r, false);
		});
		
		sourceRefreshed();
	}
	
	
	public void initialSourcePoolWithLoadedTrace(Object root, Map<String, EObject> keyViewMap) {
		initSourcePool(root, (parent, sourceObject,bx)->{
			// TODO: need support for local keys ??
			Object sourceKey = calculateSourceKeyWithLocalSupport(bx, parent, sourceObject.getValue());
			sourceObject.setSourceKey(sourceKey);
			String keyStr = environment.serializeKey(sourceKey);
			EObject initialView = keyViewMap.get(keyStr); 
			sourceObject.setViewElement(initialView);
		});
		
		sourceRefreshed();
	}
	
	public void commitChanges(Object root) {
		// 1. commit new objects to knownObjects
		// 2. refresh key of sourceObjects
		// 3. reload the object when the key is changed
		sourcePolluted();
		
		Map<Object,SourceObject> changedObjects = this.environment.autoMap();
		
		newObjectMap.values().forEach(pair->{
			removeObject(pair.first);
			
			// handle source instance key
			if(pair.second.getSourceKey() instanceof ViewInstanceKey) {
				pair.second.setSourceKey(pair.second.getValue());
			}
			
			Object sourceValue = pair.second.getValue();
			pair.first.setSourceKey(pair.second.getSourceKey()); 
			pair.first.setSourceValue(sourceValue);
			pair.first.setReplacement(null);
			
			addObject(pair.first);
			
			changedObjects.put(pair.second.getSourceKey(), pair.first);
		});
		
		newObjectMap.clear();
		
		Set<SourceObject> removed = this.environment.autoSet();
		
		knownObjects.stream().forEach(so->{
			if(!Mobile2Utils.belongToView(so.getViewElement(), environment)) {
				removed.add(so);
			} else {
				so.setReplacement(null); // clear replacement
				ElementBX bx = this.environment.resolveBXFor(so.getViewElement());
				Object viewKey = bx.calculateViewKey(so.getViewElement());
				if(viewKey instanceof ViewInstanceKey) {
					// source instance key must have been handled in the first phase
				} else {
					if(!viewKey.equals(so.getSourceKey())) {
						changedObjects.put(viewKey, so);
						so.setSourceKey(viewKey);
					}					
				}
				
			}
		});
		
		removed.forEach(r->{
			removeObject(r);
		});
		
		// all changed objects must be known objects now
		buildSourceTree(null, root, this.environment.getSystemBX().getRootBX(), (parent, object, bx)->{
			Object sk = calculateSourceKeyWithLocalSupport(bx, parent, object);
			SourceObject so = changedObjects.getOrDefault(sk, null);
			
			if(so!=null) {
				reloadObject(so, object);
			} else {
				so = objectMap.get(object);
			}
			
			return so;
		});
		
		sourceRefreshed();
	}
	
	public void sourceRefreshed() {
		sourceRefreshed = true;
	}
	public void sourcePolluted() {
		sourceRefreshed = false;
	}
	protected boolean sourceRefreshed; 
	public boolean isSourceRefreshed() {
		return sourceRefreshed;
	}
	
	public <T> Stream<T> stream(Collection<T> c) {
		return this.environment.autoStream(c);
	}
	
	public Object calculateSourceKeyWithLocalSupport(ElementBX bx, SourceObject parent, Object object) {
		Object rawKey =  bx.calculateSourceKey(object);
//		if(rawKey instanceof SourceLocalKey) {
//			EObject view = null; // resolve key according to parent
//			Object localKey = ((SourceLocalKey) rawKey).localKey;
//		}
		return rawKey;
	}
}