package edu.ustb.sei.mde.mobile2.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.BiConsumer;

public class Mobile2ThreadPool {
	protected boolean parallel;
	private Map<Object, Boolean> scoreboard;
	
	static public Mobile2ThreadPool makePool(int nThreads, boolean parallel) {
		return new ManagedMobile2ThreadPool(nThreads, parallel);
	}
	
	public Mobile2ThreadPool(int nThreads, boolean parallel) {
		runningThreads = new LinkedBlockingQueue<>();
		remainingThreads = new ArrayList<>(8);
		this.parallel = parallel;
		if(parallel) {
			scoreboard = new ConcurrentHashMap<Object, Boolean>();
			this.add = (i,r)->{
				synchronized (remainingThreads) {
					sequentialAdd(r, i);
				}
			};
		} else {
			scoreboard = new HashMap<Object, Boolean>();
			this.add = (i,r) -> {
				sequentialAdd(r, i);
			};
		}
	}
	
	public void close() {
	}
	
	protected LinkedBlockingQueue<Mobile2Task> runningThreads;
	protected List<LinkedBlockingQueue<Mobile2Task>> remainingThreads;
	
	public void execute(Mobile2Task task) {
		if(parallel) {
			runningThreads.add(task);
			new Thread(task).start();
		} else task.run();
	}
	
	public BiConsumer<Integer, Mobile2Task> add;
	
	/**
	 * 
	 * @param round starts from 2!! The runables in the first round have been executed
	 * @param r
	 */
	public void add(int round, Mobile2Task r) {
		int index = round - 2;
		this.add.accept(index, r);
	}

	protected void sequentialAdd(Mobile2Task r, int index) {
		if(remainingThreads.size()<=index) {
			for(int i=remainingThreads.size();i<=index;i++) {
				remainingThreads.add(new LinkedBlockingQueue<Mobile2Task>());
			}
		}
		remainingThreads.get(index).add(r);
	}
	
	public void waitScoreBoard(Object monitor) {
		if(parallel) {
			synchronized (monitor) {
				while(!scoreboard.getOrDefault(monitor, false)) {
					try {
						monitor.wait();
					} catch (InterruptedException e) {
					}
				}
			}
		}
	}
	
	public void fillScoreBoard(Object monitor) {
		if(parallel) {
			synchronized (monitor) {
				scoreboard.put(monitor, true);
				monitor.notifyAll();
			}
		}
	}
	
	
	
	public void doRemaining() {
		waitTillFinish();
		for(LinkedBlockingQueue<Mobile2Task> round : remainingThreads) {
			if(round.isEmpty()==false) {
				for(Mobile2Task r : round) execute(r);
				waitTillFinish();
			}
		}
	}

	protected void waitTillFinish() {
		while(true) {
			Mobile2Task first = this.runningThreads.poll();
			if(first==null) break;
			else {
				if(!first.finished()) {
					synchronized (first) {
						if(first.finished()) continue;
						try {
							first.wait();
						} catch (InterruptedException e) {
						}
					}
				}
			}
		}
//		Iterator<Mobile2Task> it = this.runningThreads.iterator();
//		while(it.hasNext()) {
//			Mobile2Task task = it.next();
//			synchronized (task) {
//				while(!task.finished()) {
//					try {
//						task.wait();
//					} catch (InterruptedException e) {
//					}					
//				}
//			}
//		}
//		this.runningThreads.clear();
	}
}

class ManagedMobile2ThreadPool extends Mobile2ThreadPool {
	private volatile boolean closed;
	
	class Worker extends Thread {
		public Worker(ManagedMobile2ThreadPool pool) {
			super();
			this.pool = pool;
			this.task = null;
		}
		private volatile Mobile2Task task;
		
		public boolean isIdle() {
			return task == null;
		}
		
		@Override
		public void run() {
			while(true) {
//				taskPeekingPermission.lock();
				task = pool.peekTask();
//				taskListChangingPermission.unlock();
				
				if(task!=null) {
					task.run();
					synchronized (this) {
						task = null;
						this.notify();
					}
				} else {
					if(pool.closed) break;
					synchronized (pool) {
						if(!pool.runningThreads.isEmpty()) continue;
						try {
							pool.wait();
						} catch (InterruptedException e) {
						}
					}
				}
			}
		}

		private ManagedMobile2ThreadPool pool;
	}

	@Override
	public void close() {
		closed = true;
	}
	
	public ManagedMobile2ThreadPool(int nThreads, boolean parallel) {
		super(nThreads, parallel);
		taskListChangingPermission = new ReentrantLock();
//		taskPeekingPermission = new ReentrantLock();
		
		closed = false;
		
		if(parallel) {
			workers = new Worker[nThreads];
			for(int i=0;i<nThreads;i++) {
				workers[i] = new Worker(this);
				workers[i].start();
			}
			taskListChanged = true;
		} else {
			workers = new Worker[0];
		}
	}
	
	@Override
	protected void waitTillFinish() {
		if(parallel) {
			boolean shouldWait = true;
			while(shouldWait) {
				taskListChangingPermission.lock();
				taskListChanged=false;
				taskListChangingPermission.unlock();
				
				for(Worker w : workers) {
					synchronized (w) {
						if(!w.isIdle()) {
							try {
								w.wait();
							} catch (InterruptedException e) {
							}
						}
					}
				}

				taskListChangingPermission.lock();
				if(taskListChanged) shouldWait = true;
				else shouldWait = !(this.runningThreads.isEmpty());
				taskListChangingPermission.unlock();
			}
		} else {
			while(true) {
				Mobile2Task task = this.peekTask();
				if(task==null) break;
				task.run();
			}
		}
	}
	
	private Worker[] workers;
	
	private java.util.concurrent.locks.ReentrantLock taskListChangingPermission;
	private volatile boolean taskListChanged;
	
	public Mobile2Task peekTask() {
		Mobile2Task result = null;
		taskListChangingPermission.lock();
		result = runningThreads.poll();
		taskListChanged = true;
		taskListChangingPermission.unlock();
		return result;
	}
	
	@Override
	public void doRemaining() {
		super.doRemaining();
	}
	
	@Override
		public void execute(Mobile2Task task) {
			if(parallel) {
				taskListChangingPermission.lock();
				runningThreads.add(task);
				taskListChanged = true;
				taskListChangingPermission.unlock();
				
				synchronized (this) {
					this.notify();
				}
			} else {
				runningThreads.add(task);
//				task.run();
			}
		}
}