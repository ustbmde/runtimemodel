package edu.ustb.sei.mde.mobile2.system;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.mde.mobile2.core.providers.SourceObjectProvider;
import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;

/**
 * The key is to check the dependencies over view values.
 * First, navigate to view.
 * Second, check the dependency from left to right;
 * @author hexiao
 *
 * right contains left??
 */
public class ToBeValueDependency extends ContextPredicate {
	public ToBeValueDependency(String leftEdit, String leftKey, String rightEdit, String rightKey) {
		super();
		this.leftKey = leftKey;
		this.rightKey = rightKey;
		this.leftEdit = leftEdit;
		this.rightEdit = rightEdit;
	}

	private String leftEdit;
	private String rightEdit;
	private String leftKey;
	private String rightKey;
	
	@Override
	public boolean check(Map<String, Object> context) {
		Object leftRawValue  = resolveKey(context, leftEdit, leftKey,null);
		Object rightRawValue = resolveKey(context, rightEdit, rightKey,null);
		if(leftRawValue == null || rightRawValue == null) return false;
		
		EObject leftView = findView(leftRawValue);
		Collection<EObject> rightViews = findViews(rightRawValue);
		
		if(rightViews.isEmpty()) return false; // short-cut
		return checkWithCache(leftView, rightViews);
		
//		while(leftView!=null) {
//			if(rightViews.contains(leftView)) return true;
//			leftView = leftView.eContainer();
//		}
//		return false;
	}
	
	private boolean checkWithCache(EObject element, Collection<EObject> container) {
		return container.parallelStream().anyMatch(con->checkWithCache(element, con));
//		for(EObject con : container) {
//			if(checkWithCache(element, con)) return true;
//		}
//		return false;
	}
	private boolean checkWithCache(EObject element, EObject con) {
		if(element==con) return true;
		else if(element==null) return false;
		
		Boolean contain = this.scheduler.checkCache(con, element);
		if(contain==null) {
			contain = checkWithCache(element.eContainer(), con);
			this.scheduler.setCache(con, element, contain);
		}

		return contain;
	}

	protected EObject findView(Object rawValue) {
		EObject view = null;
		if(rawValue instanceof EObject) view = (EObject)rawValue;
		else if(rawValue instanceof SourceProvider) {
			rawValue = ((SourceProvider<?>) rawValue).getCore();
			if(rawValue instanceof SourceObjectProvider) {
				view = ((SourceObjectProvider) rawValue).getFinalProvider().getViewElement();
			} else {
				System.out.println("In ToBeValueDependency, rawValue is not a SourceObjectProvider! Return false.");
			}
		}
		return view;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected Collection<EObject> findViews(Object rawValue) {
		Collection<EObject> views = Collections.emptySet();
		if(rawValue instanceof EObject) views = Collections.singleton((EObject)rawValue);
		else if(rawValue instanceof List) views = new HashSet<>((List<EObject>)rawValue); // view value
		else if(rawValue instanceof SourceProvider) {
			rawValue = ((SourceProvider<?>) rawValue).getCore();
			if(rawValue instanceof SourceObjectProvider) {
				views = Collections.singleton(((SourceObjectProvider) rawValue).getFinalProvider().getViewElement());
			} else {
				if(((SourceProvider) rawValue).isReady()) {
					rawValue = ((SourceProvider) rawValue).getValue();
					if(rawValue instanceof List) {
						System.out.println("ToBeValueDependency cannot check original source collections now!");
					}
				}
				System.out.println("In ToBeValueDependency, rawValue is not a SourceObjectProvider! Return false.");
			}
		}
		return views;
	}

}
