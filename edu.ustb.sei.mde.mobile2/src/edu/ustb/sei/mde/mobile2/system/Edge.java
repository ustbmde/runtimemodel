package edu.ustb.sei.mde.mobile2.system;

class Edge {
	public SchedulingRule reason;
	public Node source;
	public Node target;
}