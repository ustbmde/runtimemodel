package edu.ustb.sei.mde.mobile2.system;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;

import edu.ustb.sei.mde.mobile2.core.ElementBX;
import edu.ustb.sei.mde.mobile2.core.SystemBX;
import edu.ustb.sei.mde.mobile2.core.edits.CreateObjectEdit;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObject;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObjectProvider;
import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;

@SuppressWarnings("unused")
public interface Environment {

	Environment newScope(boolean containment);
	EObject getViewParent();
	void setViewParent(EObject p);
	boolean isParallel();
	void setParallel(boolean flag);
	Mobile2ThreadPool getTaskPool();
	
	void completeTasks();
	
	void put(Object key, Object value);
	<T> T get(Object key);
	
//	Resource getViewResource();
//	void setViewResource(Resource v);
	
	SourcePool getSourcePool();
	
	SourceProvider<Object> getSourceParent();
	void setSourceParent(SourceProvider<Object> p);
	
	<T extends Mobile2TempData> T getTempData();
	void setTempData(Mobile2TempData t);
	
	default void resetTempData() {
		Mobile2TempData t = getTempData();
		if(t!=null) {			
			t.reset();
			t.init();
		}
	}
	
	default <T> Stream<T> autoStream(Collection<T> c) {
		if(isParallel())
			return c.parallelStream();
		else return c.stream();
	}
	
	boolean isRefreshMode();
	static public Environment init(SystemBX bx, Consumer<Environment> initializer) {
		RootEnvironmentImpl env = new RootEnvironmentImpl();
		initializer.accept(env);
		env.sourcePool = new SourcePool(env);
		env.systemBX = bx;
		return env;
	}
	
	class EnvironmentImpl implements Environment {
		private Boolean refreshMode;
		private Environment parent;
		private Environment root;
		private EObject viewParent;
		SourceProvider<Object> sourceParent;
		@Override
		public Environment newScope(boolean containment) {
			EnvironmentImpl n = new EnvironmentImpl();
			n.parent = this;
			n.root = this.root;
			n.refreshMode = containment;
			return n;
		}
		@Override
		public EObject getViewParent() {
			return viewParent;
		}
		@Override
		public void setViewParent(EObject p) {
			this.viewParent = p;
		}
		@Override
		public SourcePool getSourcePool() {
			return root.getSourcePool();
		}
		@Override
		public SourceProvider<Object> getSourceParent() {
			return this.sourceParent;
		}
		@Override
		public void setSourceParent(SourceProvider<Object> p) {
			this.sourceParent = p;
		}
		@Override
		public boolean isRefreshMode() {
			return refreshMode;
		}
		@Override
		public ElementBX resolveBXFor(EObject viewElement) {
			return root.resolveBXFor(viewElement);
		}
		@Override
		public ElementBX resolveBXFor(Object sourceElement) {
			return root.resolveBXFor(sourceElement);
		}
		@Override
		public SystemBX getSystemBX() {
			return root.getSystemBX();
		}
		@Override
		public void initializeStateBasedGet(Object object) {
			root.initializeStateBasedGet(object);
		}
		@Override
		public void initializeDeltaBasedGet(Object object, Resource viewRes) {
			root.initializeDeltaBasedGet(object, viewRes);
		}
		@Override
		public void initializeStateBasedPut(Object object, Resource viewRes) {
			root.initializeStateBasedPut(object, viewRes);
		}
		@Override
		public void initializeDeltaBasedPut(Object object, Resource viewRes) {
			root.initializeDeltaBasedPut(object, viewRes);
		}
		@Override
		public void registerObjectChange(SourceObject core, CreateObjectEdit core2, EObject view,
				Object calculateViewKey) {
			root.registerObjectChange(core, core2, view, calculateViewKey);
		}

		@Override
		public void serializeTrace(String uri) {
			this.root.serializeTrace(uri);
		}
		@Override
		public void initializeFromSerialization(String uri, Object sourceRoot, Resource viewResource) {
			root.initializeFromSerialization(uri, sourceRoot, viewResource);
		}
		@Override
		public String serializeKey(Object so) {
			return root.serializeKey(so);
		}
		@Override
		public boolean isParallel() {
			return root.isParallel();
		}
		@Override
		public Mobile2ThreadPool getTaskPool() {
			return parent.getTaskPool();
		}
		@Override
		public void setParallel(boolean flag) {
			root.setParallel(flag);
		}
		@Override
		public void completeTasks() {
			this.root.completeTasks();
		}
		@Override
		public <T extends Mobile2TempData> T getTempData() {
			return root.getTempData();
		}
		@Override
		public void setTempData(Mobile2TempData t) {
			root.setTempData(t);
		}
		@Override
		public void put(Object key, Object value) {
			root.put(key, value);
		}
		@Override
		public <T> T get(Object key) {
			return root.get(key);
		}
	}
	
	class RootEnvironmentImpl implements Environment {
		private SystemBX systemBX;
		private SourcePool sourcePool;
		private Map<Object, Object> data;
		
		RootEnvironmentImpl() {
			sourcePool = null;
			systemBX = null;
			parallel = false;
			data = new HashMap<Object, Object>();
		}

		@Override
		public Environment newScope(boolean containment) {
			EnvironmentImpl n = new EnvironmentImpl();
			n.parent = this;
			n.root = this;
			n.refreshMode = containment;
			return n;
		}

		@Override
		public boolean isRefreshMode() {
			return true;
		}

		@Override
		public EObject getViewParent() {
			return null;
		}

		@Override
		public void setViewParent(EObject p) {
			throw new UnsupportedOperationException("setViewParent is not supported");
		}

		@Override
		public SourcePool getSourcePool() {
			return sourcePool;
		}

		@Override
		public ElementBX resolveBXFor(EObject viewElement) {
			return getSystemBX().findBX(viewElement);
		}

		@Override
		public SystemBX getSystemBX() {
			return systemBX;
		}

		@Override
		public SourceProvider<Object> getSourceParent() {
			return null;
		}
		
		@Override
		public void setSourceParent(SourceProvider<Object> p) {
			throw new UnsupportedOperationException("setSourceParent is not supported");
		}
		
		@Override
		public void registerObjectChange(SourceObject asIs, CreateObjectEdit toBe, EObject view,
				Object calculateViewKey) {
			toBe.setSourceKey(calculateViewKey);
//			asIs.setReplacement(toBe);
//			toBe.setViewElement(view);
			getSourcePool().registerObjectChange(view, asIs, toBe);
		}

		
		@Override
		public void initializeStateBasedGet(Object root) {
			getSourcePool().initializeStateBasedGet(root);
		}

		@Override
		public void initializeDeltaBasedGet(Object root, Resource viewRes) {
			getSourcePool().initializeDeltaBasedGet(root, viewRes);
		}

		@Override
		public void initializeStateBasedPut(Object root, Resource viewRes) {
			getSourcePool().initializeStateBasedPut(root, viewRes);
		}

		@Override
		public void initializeDeltaBasedPut(Object root, Resource viewRes) {
			getSourcePool().initializeDeltaBasedPut(root, viewRes);
		}

		@Override
		public ElementBX resolveBXFor(Object sourceElement) {
			return this.getSystemBX().findBX(sourceElement);
		}

		/**
		 * Precondition: the view resource must be saved and the newObjects is empty
		 */
		@Override
		public void serializeTrace(String uri) {
			try {
				Document document = DocumentHelper.createDocument();
				Element root = document.addElement("pool");
				
				for(SourceObjectProvider so : this.getSourcePool().getKnownObjects()) {
					Element trace = root.addElement("trace");
					Element key = trace.addElement("key");
					key.addText(serializeKey(so.getSourceKey()));
					key.addAttribute("type", so.getSourceKey().getClass().getCanonicalName());
					Element view = trace.addElement("view");
					view.addText(EcoreUtil.getRelativeURIFragmentPath(null, so.getViewElement()));
				}
				
				OutputStream os = new FileOutputStream(uri);
				OutputFormat format = OutputFormat.createPrettyPrint();
				format.setEncoding("utf-8");
				
				XMLWriter xw = new XMLWriter(os,format);
				xw.write(document);
				xw.flush();
				xw.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		

		@Override
		public String serializeKey(Object so) {
			return so.toString();
		}
		
		@Override
		public void initializeFromSerialization(String uri, Object sourceRoot, Resource viewResource) {
			Map<String, EObject> keyViewMap = new HashMap<>();
			try {
				EObject viewRoot = viewResource.getContents().isEmpty() ? null :viewResource.getContents().get(0);
				SAXReader sax = new SAXReader();
				Document doc = sax.read(new File(uri));
				
				Element root = doc.getRootElement();
				if(root.getName().equals("pool")) {
					root.elements("trace").forEach(t->{
						Element key = t.element("key");
						String keyStr = key.getText();
//						String keytype = key.attributeValue("type");
						
						Element view = t.element("view");
						String viewURI = view.getText();
						
						EObject viewElement = viewRoot==null ? null : EcoreUtil.getEObject(viewRoot, viewURI);
						keyViewMap.put(keyStr, viewElement);
					});
				}
			} catch (Exception e) {
			}
			
			getSourcePool().initialSourcePoolWithLoadedTrace(sourceRoot, keyViewMap);
		}

		private boolean parallel = false;
		@Override
		public boolean isParallel() {
			return parallel;
		}

		private Mobile2ThreadPool taskPool;
		@Override
		public Mobile2ThreadPool getTaskPool() {
			if(taskPool==null) {
				taskPool = newFixedThreadPool(isParallel());
			}
			return taskPool;
		}

		@Override
		public void setParallel(boolean flag) {
			this.parallel = flag;
		}

		@Override
		public void completeTasks() {
			this.getTaskPool().doRemaining();
			this.taskPool.close();
			synchronized (this.taskPool) {
				this.taskPool.notifyAll();
			}
			this.taskPool = null;
			this.resetTempData();
		}

		private Mobile2TempData tempData = null;
		
		@SuppressWarnings("unchecked")
		@Override
		public <T extends Mobile2TempData> T getTempData() {
			return (T) tempData;
		}

		@Override
		public void setTempData(Mobile2TempData t) {
			this.tempData = t;
			t.init();
		}

		@Override
		public void put(Object key, Object value) {
			data.put(key, value);
		}

		@SuppressWarnings("unchecked")
		@Override
		public <T> T get(Object key) {
			return (T) data.get(key);
		}
	}
	
	public static Mobile2ThreadPool newFixedThreadPool(boolean parallel) {
		int nThreads = Runtime.getRuntime().availableProcessors();
        return Mobile2ThreadPool.makePool(nThreads, parallel);
    }
	
	ElementBX resolveBXFor(EObject viewElement);
	ElementBX resolveBXFor(Object sourceElement);
		
	/**
	 * ResolveSource returns null when second is new
	 * @param second
	 * @return
	 */
	default SourceObjectProvider resolveSource(EObject second) {
		return getSourcePool().findAsIsObject(second);
	}
	
	/**
	 * Always return the original value of the future source.
	 * Use getFinalVersion() to obtain the future source.
	 * For a new view element, it must return a SourceObject that holds a null value. 
	 *  
	 * @param second
	 * @return
	 */
	default SourceObjectProvider resolveFutureSource(EObject second) {
		return getSourcePool().findToBeObject(second);
	}
	
	
	SystemBX getSystemBX();
	
	/**
	 * source pool will be dropped
	 * 1. construct the source pool and compute the source key
	 * 2. create view elements and register them in source pool
	 * @param root
	 */
	void initializeStateBasedGet(Object root);
	
	/**
	 * source pool will be updated
	 * 1. update the source pool, for new source => create or align view, for removed source => remove it from source pool
	 * 2. update the trace pool by view, for unmatched view => remove it
	 * @param root
	 */
	void initializeDeltaBasedGet(Object root, Resource viewRes);
	
	/**
	 * source pool, trace pool will be dropped
	 * @param root
	 */
	void initializeStateBasedPut(Object root, Resource viewRes);
	void initializeDeltaBasedPut(Object root, Resource viewRes);
	
	void registerObjectChange(SourceObject core, CreateObjectEdit core2, EObject view, Object calculateViewKey);
	
	void serializeTrace(String uri);
	void initializeFromSerialization(String uri, Object sourceRoot, Resource viewResource);
	String serializeKey(Object key);
	
	default <K,V> Map<K,V> autoMap() {
		if(isParallel()) return new ConcurrentHashMap<K, V>();
		else return new HashMap<>();
	}
	default <T> Set<T> autoSet() {
		Set<T> set = new HashSet<>();
		if(isParallel()) return Collections.synchronizedSet(set);
		else return set;
	}
	default <T> Set<T> autoSet(Collection<? extends T> knownObjects) {
		Set<T> set = new HashSet<>(knownObjects);
		if(isParallel()) return Collections.synchronizedSet(set);
		else return set;
	}
	
	default <T> List<T> autoList(int size) {
		List<T> set = new ArrayList<>(size);
		if(isParallel()) return Collections.synchronizedList(set);
		else return set;
	}
}
