package edu.ustb.sei.mde.mobile2.system;

public enum FeatureKind {
	containment,
	noncontainment,
	attribute
}
