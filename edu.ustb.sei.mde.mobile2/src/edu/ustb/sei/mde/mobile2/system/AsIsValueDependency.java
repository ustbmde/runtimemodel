package edu.ustb.sei.mde.mobile2.system;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import edu.ustb.sei.mde.mobile2.core.providers.SourceObject;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObjectProvider;
import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;

/**
 * The key is to navigate to original source and check the dependency via the original dependency link.
 * Newly created objects cannot check as-is dependency. 
 * @author hexiao
 *
 */
public class AsIsValueDependency extends ContextPredicate {
	public AsIsValueDependency(String leftEdit, String leftKey, String rightEdit, String rightKey) {
		super();
		this.leftKey = leftKey;
		this.rightKey = rightKey;
		this.leftEdit = leftEdit;
		this.rightEdit = rightEdit;
	}

	private String leftEdit;
	private String rightEdit;
	private String leftKey;
	private String rightKey;
	

	@Override
	public boolean check(Map<String, Object> context) {
		Object leftRawValue  = resolveKey(context, leftEdit, leftKey, null);
		Object rightRawValue = resolveKey(context, rightEdit, rightKey, null);
		
		Environment env = (Environment) context.get("environment");
		if(leftRawValue == null || rightRawValue == null) return false;
		
		SourceObject leftValue = findObject(leftRawValue);
		Collection<SourceObject> rightValue = findObjects(rightRawValue, env);
		
		if(rightValue.isEmpty()) return false; // short-cut
		return checkWithCache(leftValue, rightValue);
//		while(leftValue!=null) {
//			if(rightValue.contains(leftValue)) return true;
//			leftValue = (SourceObject) leftValue.getContainer();
//		}
//		return false;
	}
	
	private boolean checkWithCache(SourceObject element, Collection<SourceObject> container) {
		return container.parallelStream().anyMatch(con->checkWithCache(element, con));
//		for(SourceObject con : container) {
//			if(checkWithCache(element, con)) return true;
//		}
//		return false;
	}
	private boolean checkWithCache(SourceObject element, SourceObject con) {
		if(element==con) return true;
		else if(element==null) return false;
		
		Boolean contain = this.scheduler.checkCache(con, element);
		if(contain==null) {
			contain = checkWithCache((SourceObject) element.getContainer(), con);
			this.scheduler.setCache(con, element, contain);
		}

		return contain;
	}

	private SourceObject findObject(Object rawValue) {
		SourceObject value = null;
		if(rawValue instanceof SourceObject) value = (SourceObject)rawValue;
		else if(rawValue instanceof SourceProvider) {
			rawValue = ((SourceProvider<?>) rawValue).getCore();
			if(rawValue instanceof SourceObjectProvider) {
				value = (SourceObject) ((SourceObjectProvider) rawValue).getOriginalProvider();
			} else {
				System.out.println("In AsIsValueDependency, rawValue is not a SourceObjectProvider! Return false.");
			}
		}
		return value;
	}
	
	private Collection<SourceObject> findObjects(Object rawValue, Environment env) {
		Collection<SourceObject> value = Collections.emptySet();
		if(rawValue instanceof SourceObject) value = Collections.singleton((SourceObject)rawValue);
		else if(rawValue instanceof SourceProvider) {
			rawValue = ((SourceProvider<?>) rawValue).getCore();
			if(rawValue instanceof SourceObjectProvider) {
				value = Collections.singleton((SourceObject) ((SourceObjectProvider) rawValue).getOriginalProvider());
			} else {
				rawValue = ((SourceProvider<?>) rawValue).getValue();
				if(rawValue instanceof List) {
					if(env==null) {
						System.out.println("AsIsValueDependency requires a key of environment to resolve a list of objects.");
					} else 
						value = ((List<?>) rawValue).stream().map(v->env.getSourcePool().getSourceObject(v)).collect(Collectors.toSet());
				}
				else System.out.println("In AsIsValueDependency, rawValue is not a SourceObjectProvider! Return false.");
			}
		}
		return value;
	}

}
