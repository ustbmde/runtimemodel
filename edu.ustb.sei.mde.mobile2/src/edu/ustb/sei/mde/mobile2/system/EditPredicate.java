package edu.ustb.sei.mde.mobile2.system;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import edu.ustb.sei.mde.mobile2.core.edits.EditContext;
import edu.ustb.sei.mde.mobile2.core.edits.Edit.EditKind;

public abstract class EditPredicate {
	static final class AndPredicate extends EditPredicate {
		private final EditPredicate[] predicates;

		private AndPredicate(EditPredicate[] predicates) {
			this.predicates = predicates;
		}

		@Override
		public boolean match(EditContext info, Map<String, Object> context) {
			for(EditPredicate p : predicates) {
				if(p.match(info, context)) continue;
				else return false;
			}
			return true;
		}

		@Override
		public void collectEditKinds(Set<EditKind> kinds) {
			for(EditPredicate p : predicates) 
				p.collectEditKinds(kinds);
		}

		@Override
		public void fillContext(EditContext info, Map<String, Object> context) {
			predicates[0].fillContext(info, context);
		}
	}

	static final class OrPredicate extends EditPredicate {
		private final EditPredicate[] predicates;

		private OrPredicate(EditPredicate[] predicates) {
			this.predicates = predicates;
		}

		@Override
		public boolean match(EditContext info, Map<String, Object> context) {
			for(EditPredicate p : predicates) {
				Map<String, Object> map = null;
				if(context!=null) map = new HashMap<>();
				if(p.match(info, map)) {
					if(context!=null)
						context.putAll(map);
					return true;
				}
			}
			return false;
		}

		@Override
		public void collectEditKinds(Set<EditKind> kinds) {
			for(EditPredicate p : predicates) 
				p.collectEditKinds(kinds);
		}

		@Override
		public void fillContext(EditContext info, Map<String, Object> context) {
			predicates[0].fillContext(info, context);
		}

		@Override
		public boolean isNatural() {
			for(EditPredicate p : predicates)
				if(!p.isNatural()) return false;
			return true;
		}

		public EditPredicate[] getPredicates() {
			return predicates;
		}
	}

	abstract public boolean match(EditContext info, Map<String, Object> context);
	abstract public void collectEditKinds(Set<EditKind> kinds);
	abstract public void fillContext(EditContext info, Map<String, Object> context);
	
	public boolean isNatural() {
		return false;
	}
	
	static public EditPredicate and(EditPredicate... predicates) {
		return new AndPredicate(predicates);
	}
	
	static public EditPredicate or(EditPredicate... predicates) {
		return new OrPredicate(predicates);
	}
}
