package edu.ustb.sei.mde.mobile2.system;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.xbase.lib.Pair;

public class SourceLocalKey {
	public SourceLocalKey(Object source, Object localKey) {
		super();
		this.source = source;
		this.localKey = localKey;
	}
	public Object source;
	public Object localKey;

	public Pair<EObject, Object> buildLocalKey(EObject v) {
		return new Pair<EObject, Object>(v, localKey);
	}
}
