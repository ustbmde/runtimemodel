package edu.ustb.sei.mde.mobile2.core;

import java.util.ArrayList;
import java.util.List;

public abstract class SystemBXImpl implements SystemBX {
	
	protected List<ElementBX> elementBXs;

	public SystemBXImpl() {
		elementBXs = new ArrayList<ElementBX>();
	}
	
	public void addElement(ElementBX bx) {
		elementBXs.add(bx);
		bx.setContainer(this);
	}

	protected ElementBX root;
	
	@Override
	public ElementBX getRootBX() {
		return root;
	}
	
	public List<ElementBX> allElementBXs() {
		return elementBXs;
	}
}
