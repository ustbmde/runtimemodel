package edu.ustb.sei.mde.mobile2.core;

import edu.ustb.sei.mde.mobile2.system.Environment;

public interface BiGULBX<S, V> extends ValueBX<S, V> {
	
	final static public Replace<?> replaceSingleton = new Replace<>();
	
	@SuppressWarnings("unchecked")
	static public <S> Replace<S> replace() {
		return (Replace<S>) replaceSingleton;
	}
	

	class Replace<S> implements BiGULBX<S, S> {
		@Override
		public String getName() {
			return "BiGUL::Replace";
		}

		@Override
		public S get(S source, Environment env) {
			return source;
		}

		@Override
		public S put(S source, S view, Environment env) {
			return view;
		}
	}
}
