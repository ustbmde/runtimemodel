package edu.ustb.sei.mde.mobile2.core;

public interface AttributeBX<SF,SFE,VF,VFE> extends FeatureBX<SF, SFE, VF, VFE> {
	ValueBX<SFE,VFE> getValueBX();
	
	void setValueBX(ValueBX<SFE,VFE> valueBX);
}
