package edu.ustb.sei.mde.mobile2.core.edits;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;

public interface MergeEdit<T> extends PseudoEdit<T> {
	List<Edit<?>> getMergedEdits();
//	void setMergedEdits(List<Edit<?>> list);
	
	static public <T> MergeEdit<T> merge(EditContext cont, SourceProvider<T> host, List<Edit<?>> edits) {
		MergeEditImpl<T> edit = new MergeEditImpl<>(cont, host);
		edit.setMergedEdits(edits);
		return edit;
	}
	
	class MergeEditImpl<T> extends EditImpl<T> implements MergeEdit<T> {
		private SourceProvider<T> host;
		private List<Edit<?>> edits;

		MergeEditImpl(EditContext context, SourceProvider<T> host) {
			super(context);
			this.host = host;
			this.calculatedValue = host;
		}

		@Override
		public void run() {
			throw new RuntimeException("MergeEdit is not runnable");
		}

		@Override
		public T getValue() {
			return host.getValue();
		}

		@Override
		public boolean isReady() {
			return host.isReady() && edits.stream().allMatch(e->e.isReady());
		}

		@Override
		public List<Edit<?>> getMergedEdits() {
			return edits;
		}

		public void setMergedEdits(List<Edit<?>> list) {
			this.edits = list;
		}

		public SourceProvider<T> getHost() {
			return this.host;
		}
		
		public Set<Edit<?>> collectEdits() {
			Set<Edit<?>> result = new HashSet<>();
			if(host instanceof Edit) 
				result.addAll(((Edit<?>) host).collectEdits());
			if(edits!=null) {
				for(Edit<?> c : edits) {
					result.addAll(c.collectEdits());
				}
			}
			return result;
		}
		
		@Override
		public SourceProvider<T> getCore() {
			if(!(this.host instanceof Edit)) return this.host.getCore();
			else return super.getCore();
		}
		
		@Override
		public Edit<T> getArrayEdit() {
			if(!(this.host instanceof Edit)) return  ((Edit<T>)this.host).getArrayEdit();
			else return super.getArrayEdit();
		}
	}
	
	
}
