package edu.ustb.sei.mde.mobile2.core.edits.impl;

import edu.ustb.sei.mde.mobile2.core.edits.EditContext;
import edu.ustb.sei.mde.mobile2.core.edits.EditImpl;
import edu.ustb.sei.mde.mobile2.core.edits.MoveInValueEdit;

public abstract class MoveInValueEditImpl<T> extends EditImpl<T> implements MoveInValueEdit<T> {

	public MoveInValueEditImpl(EditContext context) {
		super(context);
		this.calculatedValue = context.getValueForKey(EditContext.SOURCE_VALUE);
	}
}
