package edu.ustb.sei.mde.mobile2.core.providers;

import java.util.Collection;

/**
 * FixedValueProvider return a fixed value.
 * @author hexiao
 *
 * @param <T>
 */
public class FixedValue<T> implements SourceValueProvider<T> {
	final private T value;
	
	public T getValue() {
		return value;
	}
	
	@Override
	public boolean isReady() {
		return true;
	}
	
	FixedValue(T value) {
		this.value = value;
	}

	@Override
	public boolean isEmpty() {
		if(value == null) return true;
		else if(value instanceof Collection) {
			return ((Collection<?>) value).isEmpty();
		} else return false;
	} 
}
