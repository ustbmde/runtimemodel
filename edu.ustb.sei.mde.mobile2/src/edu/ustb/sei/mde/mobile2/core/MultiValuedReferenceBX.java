package edu.ustb.sei.mde.mobile2.core;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

public interface MultiValuedReferenceBX extends ReferenceBX<List<Object>, List<EObject>> {

	boolean coarseGrainedUpdate();

}
