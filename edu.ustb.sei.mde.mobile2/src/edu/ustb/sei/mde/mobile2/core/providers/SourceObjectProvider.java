package edu.ustb.sei.mde.mobile2.core.providers;

import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.mde.mobile2.core.edits.CreateObjectEdit;

/**
 * A provider that returns a source object
 * @author hexiao
 *
 */
public interface SourceObjectProvider extends SourceProvider<Object> {
	/**
	 * When SourceObjectProvider is created, a source key is stored.
	 * Given a source key, the provider must be able to re-locate the system object.
	 * If the source object is changed, a new key may be stored.
	 * @return
	 */
	Object getSourceKey();
	
	boolean isChecked();
	
	/**
	 * If hasReplacement(), then return the replacement;
	 * Otherwise, return this.
	 * @return
	 */
	SourceObjectProvider getFinalProvider();
	@Override
	default Object getFinalValue() {
		return getFinalProvider().getValue();
	}
	/**
	 * If this provider is a replacement, return the original value;
	 * Otherwise, return this.
	 * @return
	 */
	default SourceObjectProvider getOriginalProvider() {
		return this;
	}
	
	boolean isReplacement();
	boolean hasReplacement();
	
	default boolean isNewOrReloaded() {
		return ((getValue()==null && this!=NullSourceObject.singleton)||this.hasReplacement());
	}
	
	default boolean isReallyNew() {
		return isNewOrReloaded() && this.getFinalProvider() instanceof CreateObjectEdit;
	}

	void setSourceKey(Object key);
	
	EObject getViewElement();
	void setViewElement(EObject o);
}
