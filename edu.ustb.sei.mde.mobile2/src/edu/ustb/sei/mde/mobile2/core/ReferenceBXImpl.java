package edu.ustb.sei.mde.mobile2.core;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

public abstract class ReferenceBXImpl<SF, VF> extends FeatureBXImpl<SF,Object,VF,EObject> implements ReferenceBX<SF, VF> {

	public ReferenceBXImpl(EStructuralFeature viewFeature, FeatureKind kind) {
		super(viewFeature, kind);
	}

	private ElementBX valueBX;

	public ElementBX getValueBX() {
		return valueBX;
	}

	public void setValueBX(ElementBX valueBX) {
		if(valueBX.getEcoreClass().isSuperTypeOf((EClass)this.getViewFeature().getEType())) {
			this.valueBX = valueBX;
		} else {
			throw new RuntimeException("Type inconsistent! "+this.getName());
		}
	}
	
	@Override
	public boolean isContainment() {
		return ((EReference)getViewFeature()).isContainment();
	}
}
