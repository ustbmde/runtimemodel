package edu.ustb.sei.mde.mobile2.core;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

import edu.ustb.sei.mde.mobile2.core.providers.SourceObject;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObjectProvider;
import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;
import edu.ustb.sei.mde.mobile2.system.Environment;
import edu.ustb.sei.mde.mobile2.util.Pair;

public interface FeatureBX<SF,SFE,VF,VFE> extends MobileBX<Pair<SourceObjectProvider, SourceProvider<SF>>, Pair<EObject, VF>> {
	EStructuralFeature getViewFeature();
	ElementBX getContainer();
	void setContainer(ElementBX e);
	
	/**
	 * This operation is used to convert the view feature value into a view edit action.
	 * Ideally, the featureBX is BX (SO,SF) (VO,VF), and the merging operator will handle the duplicate.
	 * However, the featureBX is now declared as BX SF VF. 
	 * For the source part, we generate a source action (wrapped in a source provider), which takes a source and a view parents as implicit parameters.
	 * For the view part, we provide this operation to facilitate the forward trans
	 * 
	 * @param view
	 * @param value
	 */
	default void runViewEdit(EObject view, Pair<EObject, VF> value) {
		view.eSet(this.getViewFeature(), value.second);
	}
	
	FeatureKind getKind();
	
	
	public enum FeatureKind {
		// 
		mutable("MUTABLE", 0x1),
		directlyComputable("DIRECTLY_COMPUTABLE",0x11), 
		constructor("CONSTRUCTOR",0x110),
		mutableConstructor("CONSTRUCTOR",0x111),
		key("KEY",0x1011); // key feature can only be an attribute or a single valued directly computable reference 
		
		private String name;
		private int code;
		FeatureKind(String name, int code) {
			this.name = name;
			this.code = code;
		}
		public String getName() {
			return name;
		}
		public int getCode() {
			return code;
		}
		
		public boolean isMutable() {
			return (code & 0x01) != 0;
		}
		
		public boolean computable() {
			return (code & 0x10) != 0;
		}
		
		public boolean isConstructor() {
			return (code & 0x100) != 0;
		}
		
		public boolean isKey() {
			return (code & 0x1000) != 0;
		}
	}
	
	// users must provide this method to get the feature values
	SF externalGet(Object object, Environment env);
	default boolean isMany() {
		return getViewFeature().isMany();
	}
	
	default SF externalGetWithCache(SourceObject so, Environment env) {
		EStructuralFeature feature = this.getViewFeature();
		if(feature instanceof EReference && ((EReference) feature).isContainment()) {
			if(env.getSourcePool().isSourceRefreshed()) {
				SourceProvider<SF> value = so.retrieveFeatureValue(this);
				if(value!=null) return value.getValue();
			}
		}
		return externalGet(so.getValue(), env);
	}
}

