package edu.ustb.sei.mde.mobile2.core;

import edu.ustb.sei.mde.mobile2.system.Environment;

/**
 * The common interface for Mobile BX
 * @author hexiao
 *
 * @param <S>
 * @param <V>
 */
public interface MobileBX<S,V> {
	String getName();
	
	/**
	 * The forward transformation that converts a source value into a view value
	 * @param source
	 * @return a view element
	 */
	V get(S source, Environment env);
	
	/**
	 * The back transformation that converts a source value and a view value into a set of edits
	 * @param source
	 * @param view
	 * @return a set of edits
	 */
	S put(S source, V view, Environment env);
}
