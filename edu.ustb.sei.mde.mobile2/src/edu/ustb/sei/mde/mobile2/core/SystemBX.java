package edu.ustb.sei.mde.mobile2.core;

import java.util.List;
import java.util.Optional;

import org.eclipse.emf.ecore.EObject;

public interface SystemBX {
	ElementBX getRootBX();
	List<ElementBX> allElementBXs();
	
	default ElementBX findBX(EObject eObject) {
		Optional<ElementBX> bx = allElementBXs().stream().filter(e->e.isAbstract()==false 
				&& e.canHandle(eObject)).findAny();
		if(bx.isPresent()) return bx.get();
		else return null;
	}
	
	default ElementBX findBX(Object object) {
		Optional<ElementBX> bx = allElementBXs().stream()
				.filter(e->e.isAbstract()==false && e.canHandle(object)).findAny();
		if(bx.isPresent()) return bx.get();
		else return null;
	}
	
	default ElementBX findBX(Object object, ElementBX rootBX) {
		for(ElementBX newRoot : rootBX.getSubBXs()) {
			ElementBX res = findBX(object, newRoot);
			if(res!=null) return res;
		}
		if(!rootBX.isAbstract()&&rootBX.canHandle(object)) 
			return rootBX;
		return null;
	}
	
	default ElementBX findBX(EObject view, ElementBX rootBX) {
		for(ElementBX newRoot : rootBX.getSubBXs()) {
			ElementBX res = findBX(view, newRoot);
			if(res!=null) return res;
		}
		if(!rootBX.isAbstract() && rootBX.canHandle(view)) 
			return rootBX;
		return null;
	}
}
