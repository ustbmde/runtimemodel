package edu.ustb.sei.mde.mobile2.core.providers;

public interface FutureSourceObjectProvider extends SourceObjectProvider {
	// getSourcePool
	// setValue
	// setOriginalProvider
	
	void setOriginalProvider(SourceObject o);
	void setValue(Object value);
	
	@Override
	default boolean isChecked() {
		return true;
	}
}
