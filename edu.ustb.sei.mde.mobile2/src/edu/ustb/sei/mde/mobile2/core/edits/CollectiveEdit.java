package edu.ustb.sei.mde.mobile2.core.edits;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


public interface CollectiveEdit<T> extends PseudoEdit<List<T>> {
	List<Edit<T>> getChildren();
	
	public static <T> CollectiveEdit<T> collect(EditContext cont, List<Edit<T>> children) {
		return collect(cont, children, true);
	}
	
	public static <T> CollectiveEdit<T> collect(EditContext cont, List<Edit<T>> children, boolean ordered) {
		return new CollectiveEditImpl<>(cont, children, ordered);
	}
	
	class CollectiveEditImpl<T> extends EditImpl<List<T>> implements CollectiveEdit<T> {
		private List<Edit<T>> children;
		
		public CollectiveEditImpl(EditContext context, List<Edit<T>> children, boolean ordered) {
			super(context);
			this.children = children;
			
			// FIXME: do we have to chain children edits? it seems not
			Edit<T> prev = null;
			for(Edit<T> cur : children) {
				if(ordered && prev!=null) {
					prev.getContext().bind(EditContext.NEXT_EDIT, cur);
					cur.getContext().bind(EditContext.PREVIOUS_EDIT, prev);
				}
				prev = cur;
			}
		}

		@Override
		public void run() {
			throw new RuntimeException("CollectiveEdit cannot be run");
		}

		@Override
		public List<T> getValue() {
			return getChildren().stream().map(c->c.getValue()).collect(Collectors.toList());
		}

		@Override
		public boolean isReady() {
			return getChildren().stream().allMatch(e->e.isReady());
		}

		@Override
		public List<Edit<T>> getChildren() {
			return children;
		}

		@Override
		public Set<Edit<?>> collectEdits() {
			Set<Edit<?>> result = new HashSet<>();
			for(Edit<?> c : children) {
				result.addAll(c.collectEdits());
			}
			return result;
		}
		
	}
}
