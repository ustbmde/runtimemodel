package edu.ustb.sei.mde.mobile2.core.edits;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;

public interface PresudoEditFactory {
	<SFE> Edit<List<SFE>> requestArrayEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, List<Edit<SFE>> edits, boolean ordered);
	<SFE> Edit<List<SFE>> requestCollectiveEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, SourceProvider<List<SFE>> oldSource, List<EObject> viewValue, List<Edit<SFE>> edits, boolean ordered);
	<SFE, T> Edit<SFE> requestMergeEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, SourceProvider<SFE> host, List<Edit<T>> associated);
	<SFE> Edit<SFE> requestMergeEdit(SourceProvider<Object> sourceObject, EObject viewElement, SourceProvider<SFE> host, List<Edit<?>> associated);
}
