package edu.ustb.sei.mde.mobile2.core.providers;

import org.eclipse.emf.ecore.EObject;

final public class NullSourceObject implements SourceObjectProvider {

	public static final NullSourceObject singleton = new NullSourceObject(); 
	
	NullSourceObject() {}

	@Override
	public Object getValue() {
		return null;
	}

	@Override
	public boolean isReady() {
		return true;
	}

	@Override
	public Object getSourceKey() {
		return null;
	}

	@Override
	public SourceObjectProvider getFinalProvider() {
		return this;
	}

	@Override
	public boolean isReplacement() {
		return false;
	}

	@Override
	public boolean hasReplacement() {
		return false;
	}

	@Override
	public void setSourceKey(Object key) {
		throw new UnsupportedOperationException("NullObject does not support this operation!");
	}

	@Override
	public EObject getViewElement() {
		throw new UnsupportedOperationException("NullObject does not support this operation!");
	}

	@Override
	public void setViewElement(EObject o) {
		throw new UnsupportedOperationException("NullObject does not support this operation!");
	}

	
	@Override
	public boolean isChecked() {
		// unclear whether we should return true or false
		return false;
	}
}
