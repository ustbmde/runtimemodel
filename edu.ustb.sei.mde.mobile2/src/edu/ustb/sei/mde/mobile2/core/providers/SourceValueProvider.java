package edu.ustb.sei.mde.mobile2.core.providers;

/**
 * A provider that returns a primitive value
 * @author hexiao
 *
 */
public interface SourceValueProvider<T> extends SourceProvider<T> {

}
