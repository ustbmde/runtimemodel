package edu.ustb.sei.mde.mobile2.core;


import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import edu.ustb.sei.mde.mobile2.core.edits.Edit;
import edu.ustb.sei.mde.mobile2.core.edits.EditContext;
import edu.ustb.sei.mde.mobile2.core.edits.Edit.EditKind;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObjectProvider;
import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;
import edu.ustb.sei.mde.mobile2.system.Environment;
import edu.ustb.sei.mde.mobile2.util.Mobile2Utils;
import edu.ustb.sei.mde.mobile2.util.Pair;

public abstract class SingleValuedAttributeBXImpl<SFE,VFE> extends AttributeBXImpl<SFE,SFE,VFE,VFE>
		implements SingleValuedAttributeBX<SFE,VFE> {

	public SingleValuedAttributeBXImpl(EStructuralFeature viewFeature, FeatureKind kind) {
		super(viewFeature, kind);
	}

	@Override
	public Pair<EObject,VFE> get(Pair<SourceObjectProvider, SourceProvider<SFE>> source, Environment env) {
		return Pair.pair(null, getValueBX().get(source.second.getValue(), env));
	}

	@Override
	public Pair<SourceObjectProvider, SourceProvider<SFE>> put(Pair<SourceObjectProvider, SourceProvider<SFE>> source, Pair<EObject,VFE> view, Environment env) {
		SFE s = source.second.getValue();
		SFE sp = getValueBX().put(s, view.second, env);
		boolean newVersion = source.first.isReallyNew();
		if(!Mobile2Utils.equals(sp, s) || newVersion) {
			if(this.getKind().isConstructor()) { // in such a case, the value can be computed directly and there is no need to generate a set edit
				return Pair.pair(source.first.getFinalProvider(), SourceProvider.fixedValue(sp));
			} else {
				// for newversion, we may return source
				Edit<SFE> edit = requestSetEdit(source.first.getFinalProvider(), view.first, getViewFeature(), 
						SourceProvider.fixedValue(sp), SourceProvider.fixedValue(s), view.second, env);
				return Pair.pair(source.first.getFinalProvider(), edit);
			}
			
		} else return source;
	}
	
	public Edit<SFE> requestSetEdit(SourceProvider<Object> sourceObject, EObject viewElement,
			EStructuralFeature feature, SourceProvider<SFE> value, SourceProvider<SFE> oldValue, VFE viewValue,
			Environment environment) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, EditKind.set)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement)
				.bind(EditContext.FEATURE, feature)
				.bind(EditContext.SOURCE_VALUE, value)
				.bind(EditContext.OLD_SOURCE_VALUE, oldValue)
				.bind(EditContext.VIEW_VALUE, viewValue)
				.bind(EditContext.ENVIRONMENT, environment);
		return createSetEdit(context);
	}
	
	protected Edit<SFE> createSetEdit(EditContext context) {
		throw new UnsupportedOperationException("createSetEdit is not implmeneted");
	}
}
