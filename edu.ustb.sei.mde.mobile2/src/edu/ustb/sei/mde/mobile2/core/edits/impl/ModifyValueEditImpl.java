package edu.ustb.sei.mde.mobile2.core.edits.impl;

import edu.ustb.sei.mde.mobile2.core.edits.EditContext;
import edu.ustb.sei.mde.mobile2.core.edits.EditImpl;
import edu.ustb.sei.mde.mobile2.core.edits.ModifyValueEdit;

public abstract class ModifyValueEditImpl<T> extends EditImpl<T> implements ModifyValueEdit<T> {

	public ModifyValueEditImpl(EditContext context) {
		super(context);
		this.calculatedValue = context.getValueForKey(EditContext.SOURCE_VALUE);
	}
}
