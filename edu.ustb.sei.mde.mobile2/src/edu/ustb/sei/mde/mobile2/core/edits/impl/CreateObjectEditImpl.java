package edu.ustb.sei.mde.mobile2.core.edits.impl;

import java.util.Collections;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.mde.mobile2.core.edits.CreateObjectEdit;
import edu.ustb.sei.mde.mobile2.core.edits.Edit;
import edu.ustb.sei.mde.mobile2.core.edits.EditContext;
import edu.ustb.sei.mde.mobile2.core.edits.EditImpl;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObject;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObjectProvider;

abstract public class CreateObjectEditImpl extends EditImpl<Object> implements CreateObjectEdit {

	private Object createdValue;
	private Object key;
	private EObject view;
	
	public CreateObjectEditImpl(EditContext context) {
		super(context);
		this.calculatedValue = this;
		this.createdValue = null;
		this.key = null;
		this.view = null;
	}
	
	@Override
	public void run() {
		if(this.createdValue!=null) return;
		else {
			this.createdValue = createObject();
		}
	}
	
	@Override
	public Object getValue() {
		return createdValue;
	}
	
	@Override
	public void setValue(Object value) {
		if(this.createdValue==null) throw new RuntimeException("You are trying to initialize the create value!");
		else {
			this.createdValue = value;
		}
	}
	
	@Override
	public boolean isReady() {
		return this.createdValue!=null;
	}
	
	abstract protected Object createObject();

	@Override
	public Object getSourceKey() {
		return key;
	}

	@Override
	public SourceObjectProvider getFinalProvider() {
		return this;
	}

	@Override
	public boolean isReplacement() {
		return false;
	}

	@Override
	public boolean hasReplacement() {
		return false;
	}

	@Override
	public void setSourceKey(Object key) {
		this.key = key;
	}
	
	@Override
	public EObject getViewElement() {
		return view;
	}
	
	@Override
	public void setViewElement(EObject o) {
		this.view = o;
	}
	
	@Override
	public void setOriginalProvider(SourceObject o) {
		this.originalValue = o;
	}
	
	private SourceObject originalValue = null;
	
	@Override
	public SourceObjectProvider getOriginalProvider() {
		return originalValue;
	}
	
	@Override
	public Set<Edit<?>> collectEdits() {
		return Collections.singleton(this);
	}
}
