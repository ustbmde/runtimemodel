package edu.ustb.sei.mde.mobile2.core;

import java.util.List;

public interface MultiValuedAttributeBX<SFE,VFE> extends AttributeBX<List<SFE>, SFE, List<VFE>,VFE> {
	boolean coarseGrainedUpdate();
}
