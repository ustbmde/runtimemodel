package edu.ustb.sei.mde.mobile2.core.edits;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.ustb.sei.mde.mobile2.core.providers.FixedValue;
import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;

/**
 * An edit for a feature array that consists of a list of edits to all the elements
 * So every edit is an array edit (one of insert, remove, modify, moveIn, moveOut)
 * @author hexiao
 *
 * @param <T>
 */
public interface ArrayEdit<T> extends PseudoEdit<List<T>> {
	List<Edit<T>> getChildren();
	
//	static public <T> ArrayEdit<T> array(EditContext context,List<? extends SourceProvider<T>> children, boolean flatten) {
//		return array(context, children, true, flatten);
//	}
	static public <T> ArrayEdit<T> array(EditContext context,List<? extends SourceProvider<T>> children, boolean ordered) {
		return new ArrayEditImpl<T>(context, children, ordered);
	}
	
	class ArrayEditImpl<T> extends EditImpl<List<T>> implements ArrayEdit<T> {
		ArrayEditImpl(EditContext context,List<? extends SourceProvider<T>> children, boolean ordered) {
			super(context);
			this.children = new ArrayList<>();
			
			Edit<T> prev = null;
			for(SourceProvider<T> c : children) {
				Edit<T> cur = null;
				if(c instanceof Edit) cur = (Edit<T>) c;
				else {
					throw new RuntimeException("ArrayEdit accepts edits as children only!");
				}
				this.children.add(cur);
				Edit<T> curCore = cur.getArrayEdit();
				if(curCore!=null) {
					if(ordered && prev!=null) {
						prev.getContext().bind(EditContext.NEXT_EDIT, curCore);
						curCore.getContext().bind(EditContext.PREVIOUS_EDIT, prev);
					}
					prev = curCore;					
				}
			}
		}

		private List<Edit<T>> children;

		@Override
		public List<T> getValue() {
			List<T> res = children.stream().reduce(null, 
					(l,e)->{
						try {
							T r = e.getValue();
							if(l==null)
								l = new ArrayList<>();
							l.add(r);
						} catch (UnsupportedOperationException err) { // no value return
						}
						return l;
					}, 
					(l,r)->{
						if(l==null && r==null) return null;
						else if(l==null && r!=null) return l;
						else if(l!=null && r==null) return r;
						else {
							l.addAll(r);
							return l;
						}
					});
			return res;
		}

		@Override
		public boolean isReady() {
			return children.stream().allMatch(e->e.isReady());
		}

		@Override
		public void run() {
			throw new RuntimeException("ArrayEdit is not runnable");
		}

		@Override
		public List<Edit<T>> getChildren() {
			return children;
		}

		@Override
		public Set<Edit<?>> collectEdits() {
			Set<Edit<?>> result = new HashSet<>();
			for(Edit<?> c : children) {
				result.addAll(c.collectEdits());
			}
			return result;
		}
		
	}

}
