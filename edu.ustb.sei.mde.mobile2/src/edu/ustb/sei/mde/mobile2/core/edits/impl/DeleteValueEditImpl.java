package edu.ustb.sei.mde.mobile2.core.edits.impl;

import edu.ustb.sei.mde.mobile2.core.edits.DeleteValueEdit;
import edu.ustb.sei.mde.mobile2.core.edits.EditContext;
import edu.ustb.sei.mde.mobile2.core.edits.EditImpl;

public abstract class DeleteValueEditImpl<T> extends EditImpl<T> implements DeleteValueEdit<T> {

	public DeleteValueEditImpl(EditContext context) {
		super(context);
		this.calculatedValue = null;
	}
	
	@Override
	public T getValue() {
		throw new UnsupportedOperationException();
	}
}
