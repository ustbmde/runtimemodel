package edu.ustb.sei.mde.mobile2.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;

import edu.ustb.sei.mde.mobile2.core.edits.CreateObjectEdit;
import edu.ustb.sei.mde.mobile2.core.edits.Edit;
import edu.ustb.sei.mde.mobile2.core.edits.Edit.CompoundEditKind;
import edu.ustb.sei.mde.mobile2.core.edits.Edit.EditKind;
import edu.ustb.sei.mde.mobile2.core.edits.EditContext;
import edu.ustb.sei.mde.mobile2.core.edits.MergeEdit;
import edu.ustb.sei.mde.mobile2.core.edits.NoEdit;
import edu.ustb.sei.mde.mobile2.core.edits.PresudoEditFactory;
import edu.ustb.sei.mde.mobile2.core.providers.NullSourceObject;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObject;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObjectProvider;
import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;
import edu.ustb.sei.mde.mobile2.system.Environment;
import edu.ustb.sei.mde.mobile2.system.Mobile2Task;
import edu.ustb.sei.mde.mobile2.system.Mobile2ThreadPool;
import edu.ustb.sei.mde.mobile2.util.Mobile2Utils;
import edu.ustb.sei.mde.mobile2.util.Pair;

public abstract class ElementBXImpl implements ElementBX, PresudoEditFactory {
	public ElementBXImpl(Class<?> javaClass, EClass ecoreClass) {
		super();
		this.javaClass = javaClass;
		this.ecoreClass = ecoreClass;
		this.features = new ArrayList<>();
		this.subBX = new ArrayList<>();
	}
	
	private SystemBX sysBX;
	
	@Override
	public SystemBX getContainer() {
		return sysBX;
	}
	
	@Override
	public void setContainer(SystemBX sys) {
		this.sysBX = sys;
	}
	
	@Override
	public String getName() {
		return ecoreClass.getName();
	}
	
	private List<FeatureBX<?,?,?,?>> features;

	@Override
	public List<FeatureBX<?, ?, ?, ?>> getFeatureBXs() {
		return features;
	}
	
	public void addFeatureBX(FeatureBX<?, ?, ?, ?> f, boolean inherited) {
		if(inherited==false) {
			f.setContainer(this);			
		}
		int i=0;
		for(i=0;i<features.size();i++) {
			if(features.get(i).getViewFeature()==f.getViewFeature())
				break;
		}
		if(i==features.size()) features.add(f);
		else if(!inherited) features.set(i, f);
	}

	private Class<?> javaClass;
	
	@Override
	public boolean canHandle(Object o) {
		return o==null || (javaClass.isInstance(o) && filter(o));
	}
	
	@Override
	public boolean canHandle(EObject v) {
		return this.ecoreClass.isSuperTypeOf(v.eClass());
	}
	
	public boolean filter(Object o) {
		return true;
	}
	
	@Override
	public Class<?> getJavaClass() {
		return javaClass;
	}

	@Override
	public EClass getEcoreClass() {
		return ecoreClass;
	}

	private EClass ecoreClass;
	
	private List<ElementBX> subBX;
	@Override
	public List<ElementBX> getSubBXs() {
		return subBX;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <SF, SFE, VF, VFE> FeatureBX<SF, SFE, VF, VFE> getFeatureBX(String name) {
		for(FeatureBX<?, ?, ?, ?> f : this.getFeatureBXs()) {
			if(name.equals(f.getName())) return (FeatureBX<SF, SFE, VF, VFE>)f;
		}
		return null;
	}
	
	@Override
	public Optional<SourceObjectProvider> findSource(EObject view, Collection<SourceObjectProvider> candidates, Map<Object, SourceObjectProvider> keyMap, Environment context, AlignMode mode) {
		if(keyMap==null) {
			// used in sequential mode only
			System.out.println("fall in an unexpected branch in findSource");
			return context.autoStream(candidates).filter(s->{
				return isAligned(s.getValue(), view, context, mode);
			}).findAny();
		} else {
			SourceObjectProvider so  = keyMap.get(calculateViewKey(view));
			if(so==null) return Optional.empty();
			else {
				if(isAligned(so.getValue(), view, context, mode))
					return Optional.of(so);
				else return Optional.empty();
			}
		}
	}
	

	@Override
	public EObject get(SourceProvider<Object> source, Environment env) {
		if(isAbstract()) return abstractGet(source, env);
		else return concreteGet(source, env);
	}
	
	protected EObject abstractGet(SourceProvider<Object> source, Environment env) {
		return env.getSystemBX().findBX(source.getValue(), this).get(source, env);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected EObject concreteGet(SourceProvider<Object> source, Environment env) {
		if(source==NullSourceObject.singleton) return null;
		
		EObject view = ((SourceObjectProvider)source).getViewElement();
		
		if(!canHandle(view)) {
			// in this case, view is updated incrementally 
			EObject newView = createView();
			// replace view with newView
			initView(newView, view);
			((SourceObjectProvider)source).setViewElement(newView);
			view = newView;
		} // else view is newly created or view has been updated
		
		if(env.isRefreshMode()) {
			for(FeatureBX feature : this.features) {
				SourceProvider value = this.wrapperGet((SourceObjectProvider)source.getCore(), feature, env);
				Pair viewValue = (Pair) feature.get(Pair.pair(source, value), env);
				feature.runViewEdit(view, viewValue);
			}
		}
		
		return view;
	}

	private void initView(EObject newView, EObject view) {
		EList<EStructuralFeature> oldFeatures = view.eClass().getEAllStructuralFeatures();
		EList<EStructuralFeature> newFeatures = newView.eClass().getEAllStructuralFeatures();
		oldFeatures.forEach(f->{
			if(newFeatures.contains(f)) {
				newView.eSet(f, view.eGet(f));
			}
		});
	}

	@Override
	public EObject createView() {
		return EcoreUtil.create(getEcoreClass());
	}

	
	@Override
	public SourceProvider<Object> put(SourceProvider<Object> source, EObject view, Environment env) {
		if(isAbstract()) return abstractPut(source, view, env);
		else {
			return concretePut(source, view, env);
		}
	}
	
	protected SourceProvider<Object> abstractPut(SourceProvider<Object> source, EObject view, Environment env) {
		return env.getSystemBX().findBX(view).put(source, view, env);
	}

	@SuppressWarnings({ "rawtypes" })
	protected SourceProvider<Object> concretePut(SourceProvider<Object> source, EObject view, Environment env) {
		assert env.isRefreshMode() != false; // we do not call elementBX during backward transformation because trace system is a shortcut
		if(view == null) {
			System.out.println("A null view is found. Please check if it is the root object: "+source.getValue());
			return null; 
		}
		
		if(((SourceObject)source.getCore()).isChecked())
			return ((SourceObject)source.getCore()).getFinalProvider();
		
		/*
		 * 0. check if this object has been converted; if so, we reuse the result; from Sep. 5, 2020, we allow re-entrance
		 * 1. check if source can be handled by this bx, if not the source must be re-created
		 * 2. check the constructor parameters, if any parameter is changed, then the source must be re-created.
		 * 3. synchronize non constructor features
		 */
		
		SourceObjectProvider updatedSource = computeNewSource(source, view, env);
		
		Mobile2ThreadPool taskPool = env.getTaskPool();
		
		
		List<Edit<?>> edits = new ArrayList<>();
		
		// containments
		for(FeatureBX f: getContainmentFeatures()) {
//			Edit edit = computeContainmentEdit(source, view, updatedSource, f, env);
//			edits.add(edit);
			taskPool.execute(new FeatureUpdateTask(taskPool, this, source, view, updatedSource, f, env, edits, true));
		}
		
		// noncontainments and attributes
		for(FeatureBX f: getOtherFeatures()) {
//			Edit edit = computeNoncontainmentEdit(source, view, updatedSource, f, env);
//			edits.add(edit);
			taskPool.add(2, new FeatureUpdateTask(taskPool, this, source, view, updatedSource, f, env, edits, false));
		}
		
		Edit<Object> merge = requestMergeEdit(source, view, updatedSource, edits);
		return merge;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected Edit computeNoncontainmentEdit(SourceProvider<Object> source, EObject view,
			SourceObjectProvider updatedSource, FeatureBX f, Environment env) {
		Edit edit;
		SourceProvider originalValue = wrapperGet((SourceObjectProvider)source.getCore(), f, env);
		Pair<SourceProvider, SourceProvider> updatedValue = (Pair<SourceProvider, SourceProvider>) f.put(Pair.pair(source, originalValue), Pair.pair(view, view.eGet(f.getViewFeature())), env);
		
//		if(f.getKind().isKey()) {
//			if(!Mobile2Utils.equals(updatedValue.second, originalValue)) {
//				env.getSourcePool().registerReloadableObject(view, (SourceObject) source);
//			}
//		}
		
		if(updatedValue.second instanceof Edit) {
			edit = ((Edit) updatedValue.second);
		} else {
			edit = (requestNoEdit(updatedSource, view, f.getViewFeature(), updatedValue.second, originalValue, null, env));
		}
		return edit;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected Edit<?> computeContainmentEdit(SourceProvider<Object> source, EObject view, SourceObjectProvider updatedSource,
			FeatureBX f, Environment env) {
		Edit<?> edit;
		SourceProvider<?> originalValue = wrapperGet((SourceObjectProvider)source.getCore(), f, env);
		Pair<SourceProvider, SourceProvider> updatedValue = (Pair<SourceProvider, SourceProvider>) f.put(Pair.pair(source, originalValue), Pair.pair(view, view.eGet(f.getViewFeature())), env);
		
//		if(f.getKind().isKey()) {
//			if(!Mobile2Utils.equals(updatedValue.second, originalValue)) {
//				env.getSourcePool().registerReloadableObject(view, (SourceObject) source);
//			}
//		}
		
		if(updatedValue.second instanceof Edit) {
			edit = ((Edit<?>) updatedValue.second);
		} else {
			edit = (requestNoEdit(updatedSource, view, f.getViewFeature(), updatedValue.second, originalValue, null, env));
		}
		return edit;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected SourceObjectProvider computeNewSource(SourceProvider<Object> source, EObject view, Environment env) {
		boolean requireCreateSource = false;
		SourceObjectProvider updatedSource = null;
		
		if(source.isEmpty()) requireCreateSource = true;
		else if(canHandle(source.getValue())==false) requireCreateSource = true;
		
		Map<Object, SourceProvider<?>> paramMap = new HashMap<>(); 
		for(FeatureBX f : getConstructorParameters()) {
			// NOTE: In general, constructor parameters must be attributes or containment references.
			// For the case that a constructor parameter is a non-containment reference, parallelization cannot be applied and
			//   the order of synchronizing features is very important.
			// At present, we cannot ensure such order.
			SourceProvider originalValue = wrapperGet((SourceObjectProvider)source.getCore(), f, env);
			Pair<SourceProvider, SourceProvider> updatedValue = (Pair<SourceProvider, SourceProvider>) f.put(Pair.pair(source, originalValue), Pair.pair(view, view.eGet(f.getViewFeature())), env);
			if(!requireCreateSource && !Mobile2Utils.notChanged(updatedValue.second, originalValue, false)) {
				requireCreateSource = true;
			}
			if(updatedValue.second instanceof SourceObjectProvider)
				paramMap.put(f.getName(), ((SourceObjectProvider)updatedValue.second).getFinalProvider());
			else paramMap.put(f.getName(), updatedValue.second);
		}
		
		Object viewKey = this.calculateViewKey(view);
		if(requireCreateSource) {
			// viewKey must be set to updatedSource
			updatedSource = (SourceObjectProvider) requireCreateObjectEdit(viewKey,paramMap, view, env);
		}
		
		if(updatedSource!=null) {
			((SourceObject)source.getCore()).setReplacement(updatedSource);
			// then register <source, updatedSource> into new traces
			env.registerObjectChange((SourceObject)source.getCore(), 
					(CreateObjectEdit) updatedSource.getCore(), view, viewKey);
		} else {
			/*
			 * For the case when there is no need to create a new object but the view key has been changed
			 * Assume that the viewKey is also determined by its parent or a key properties (not a constructor property).
			 * This feature gives users a chance to dynamically replacing the source object once.
			 * NOTE: in show cases, this feature is never used.
			 * DATE: 2020-6-29
			 */
			if(shouldReloadForKey()) {
				if(!Mobile2Utils.equals(((SourceObjectProvider)source).getSourceKey(), viewKey)) {
					env.getSourcePool().registerReloadableObject(view, (SourceObject) source);
					updatedSource = ((SourceObjectProvider)source).getFinalProvider();
					System.out.println("Create future object for "+source.getValue());
				}
			}
			if(updatedSource==null) {
				updatedSource = (SourceObjectProvider) source.getCore();
				// simply set replacement to indicate that source has been checked
				((SourceObject)source.getCore()).setReplacement(updatedSource);
			}
		}
		return updatedSource;
	}

	@SuppressWarnings("rawtypes")
	private List<FeatureBX> constructorParameters;
	
	@SuppressWarnings("rawtypes")
	protected List<FeatureBX> getConstructorParameters() {
		if(constructorParameters==null) {
			constructorParameters = this.getFeatureBXs().stream().filter(f->f.getKind().isConstructor()).collect(Collectors.toList());
		}
		return constructorParameters;
	}
	
	protected List<FeatureBX<?,?,?,?>> containmentFeatures;
	public List<FeatureBX<?, ?, ?, ?>> getContainmentFeatures() {
		if(containmentFeatures==null) {
			containmentFeatures = this.getFeatureBXs().stream().filter(f->f.getKind().isConstructor()==false && 
					f.getViewFeature() instanceof EReference && 
					((EReference)f.getViewFeature()).isContainment()).collect(Collectors.toList());
		}
		return containmentFeatures;
	}
	
	protected List<FeatureBX<?,?,?,?>> otherFeatures;
	public List<FeatureBX<?, ?, ?, ?>> getOtherFeatures() {
		if(otherFeatures==null) {
			otherFeatures = this.getFeatureBXs().stream().filter(f->f.getKind().isConstructor()==false &&
					!(f.getViewFeature() instanceof EReference && ((EReference)f.getViewFeature()).isContainment()))
					.collect(Collectors.toList());
		}
		return otherFeatures;
	}
	
	@SuppressWarnings("rawtypes")
	protected SourceProvider<?> wrapperGet(SourceObjectProvider source, FeatureBX feature, Environment env) {
		SourceProvider sv = null;		
		if(source.isEmpty() || !feature.getContainer().getJavaClass().isInstance(source.getValue())) {
			if(feature.getViewFeature().isMany()) {
				sv = SourceProvider.fixedValue(Collections.emptyList());
			} else {
				if(feature instanceof AttributeBX) {
					sv = SourceProvider.fixedValue(null);
				} else {
					sv = SourceProvider.nullObject();
				}
			}
		} else {
			Object value = feature.externalGetWithCache((SourceObject)source, env);
			if(feature instanceof ReferenceBX) {
				if(feature.isMany()) {
					if(value==null) sv = SourceProvider.fixedValue(Collections.emptyList());
					else sv = SourceProvider.fixedValue(value);
				} else {
					if(value==null) sv = SourceProvider.nullObject();
					else sv = env.getSourcePool().getSourceObject(value);
				}
			} else {
				sv= SourceProvider.fixedValue(value);
			}
		}
		return sv;
	}

	/**
	 * no need to implement
	 */
	@Override
	public <SFE> Edit<List<SFE>> requestArrayEdit(SourceProvider<Object> sourceObject, EObject viewElement,
			EStructuralFeature feature, List<Edit<SFE>> edits, boolean ordered) {
		return null;
	}
	
	/**
	 * no need to implement
	 */
	@Override
	public <SFE> Edit<List<SFE>> requestCollectiveEdit(SourceProvider<Object> sourceObject, EObject viewElement,
			EStructuralFeature feature, SourceProvider<List<SFE>> oldSource, List<EObject> viewValue, List<Edit<SFE>> edits, boolean ordered) {
		return null;
	}
	
	/**
	 * no need to implement
	 */
	@Override
	public <SFE, T> Edit<SFE> requestMergeEdit(SourceProvider<Object> sourceObject, EObject viewElement,
			EStructuralFeature feature, SourceProvider<SFE> host, List<Edit<T>> associated) {
		return null;
	}
	
	@Override
	public <SFE> Edit<SFE> requestMergeEdit(SourceProvider<Object> sourceObject, EObject viewElement,
			SourceProvider<SFE> host, List<Edit<?>> associated) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, CompoundEditKind.merge)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement);
		return MergeEdit.merge(context, host, associated);
	}
	

	public Edit<Object> requireCreateObjectEdit(Object key, Map<Object, SourceProvider<?>> paramMap, EObject view, Environment env) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, EditKind.create)
				.bind(EditContext.CONSTRUCTOR_PARAM, paramMap)
				.bind(EditContext.VIEW_VALUE, view)
				.bind(EditContext.SOURCE_OBJECT, env.getSourceParent())
				.bind(EditContext.ENVIRONMENT, env)
				.bind(EditContext.CREATION_WITH_PARENT, createWithParent());
		CreateObjectEdit edit = createObject(context);
		edit.setSourceKey(key);
		
		return edit;
	}
	
	protected CreateObjectEdit createObject(EditContext context) {
		throw new UnsupportedOperationException("createObject is not implmeneted");
	}
	
	protected boolean createWithParent() {
		return false;
	}

	public <SF, VF> NoEdit<SF> requestNoEdit(SourceProvider<Object> sourceObject, EObject viewElement,
			EStructuralFeature feature, SourceProvider<SF> value, SourceProvider<SF> oldValue, VF viewValue,
			Environment environment) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, EditKind.none)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement)
				.bind(EditContext.FEATURE, feature)
				.bind(EditContext.SOURCE_VALUE, value)
				.bind(EditContext.OLD_SOURCE_VALUE, oldValue)
				.bind(EditContext.VIEW_VALUE, viewValue)
				.bind(EditContext.ENVIRONMENT, environment);
		return NoEdit.noEdit(context, value);
	}
	
	@Override
	public Object calculateSourceKey(Object source) {
		for(ElementBX sub : subBX) {
			if(sub.canHandle(source)) return sub.calculateSourceKey(source);
		}
		return null;
	}
	
	public class FeatureUpdateTask extends Mobile2Task {
		@SuppressWarnings("rawtypes")
		public FeatureUpdateTask(Mobile2ThreadPool taskPool, ElementBXImpl bx, SourceProvider<Object> source, EObject view, SourceObjectProvider updatedSource,
				FeatureBX f, Environment env, List<Edit<?>> edits, boolean containment) {
			super();
			this.bx = bx;
			this.source = source;
			this.view = view;
			this.updatedSource = updatedSource;
			this.f = f;
			this.env = env;
			this.edits = edits;
			this.containment = containment;
			this.parallel = env.isParallel();
			this.taskPool = taskPool;
		}

		ElementBXImpl bx;
		SourceProvider<Object> source;
		EObject view;
		SourceObjectProvider updatedSource;
		@SuppressWarnings("rawtypes")
		FeatureBX f;
		Environment env;
		List<Edit<?>> edits;
		boolean containment;
		boolean parallel;
		Mobile2ThreadPool taskPool;

		@SuppressWarnings("rawtypes")
		@Override
		protected void compute() {
			Edit edit = null;
			if(containment) {
				edit = bx.computeContainmentEdit(source, view, updatedSource, f, env);
			}
			else {
				taskPool.waitScoreBoard(view);
				edit = bx.computeNoncontainmentEdit(source, view, updatedSource, f, env);
			}
			if(parallel) {
				synchronized (edits) {
					edits.add(edit);				
				}
			} else {
				edits.add(edit);
			}
		}		
	}
}


