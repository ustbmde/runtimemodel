package edu.ustb.sei.mde.mobile2.core.providers;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.mde.mobile2.core.FeatureBX;

/**
 * SourceObject represents a source object that is injectively mapped onto a model element. 
 * It supports two functionalities: (1) multiple Java objects that refer to the same source object, and (2) dynamically re-searching Java object.
 * @author hexiao
 *
 */
public class SourceObject implements SourceObjectProvider {
	private Object value;
	private SourceObjectProvider container;
	private Object key;
	private EObject view;
	
	public Object getValue() {
		return value;
	}
	
//	protected void setValue(Object value) {
//		this.value = value;
//	}
	
	@Override
	public boolean isChecked() {
		return (this.replacement!=null);
	}
	
	@Override
	public boolean isReady() {
		return true;
	}
	
	public SourceObject(Object object) {
		this.value = object;
		this.featureCache = new ConcurrentHashMap<FeatureBX<?,?,?,?>, SourceProvider<?>>();
		this.replacement = null;
	}
	
	// From Sep. 5, 2020, replacement may points to itself
	// replacement has three states: null - not been checked, this - checked but no update, FutureSourceObject - checked and updated
	private SourceObjectProvider replacement;
	
	@Override
	public SourceObjectProvider getFinalProvider() {
		if(replacement==null) return this;
		else return replacement;
	}
	
	public void setReplacement(SourceObjectProvider t) {
		if(!(t==null || t instanceof FutureSourceObjectProvider || t==this)) 
			throw new RuntimeException("setReplacement is accepting a source that is not null, this, nor a FutureSourceObjectProvider!");
		this.replacement = t;
		
		if(t!=null && t!=this)
			((FutureSourceObjectProvider)t).setOriginalProvider(this);
	}

	@Override
	public boolean isEmpty() {
		return value == null;
	}

	public SourceObjectProvider getContainer() {
		return container;
	}

	public void setContainer(SourceObjectProvider container) {
		this.container = container;
	}

	@Override
	public Object getSourceKey() {
		return key;
	}
	
	@Override
	public void setSourceKey(Object key) {
		this.key = key;
	}

	private Map<FeatureBX<?, ?, ?, ?>, SourceProvider<?>> featureCache;
	
	public <T> void cacheFeatureValue(FeatureBX<?, ?, ?, ?> feature, T col) {
//		synchronized (featureCache) {			
			featureCache.put(feature, SourceProvider.fixedValue(col));
//		}
	}
	
	@SuppressWarnings("unchecked")
	public <T> SourceProvider<T> retrieveFeatureValue(FeatureBX<?, ?, ?, ?> feature) {
		return (SourceProvider<T>) featureCache.get(feature);
	}

	@Override
	public boolean isReplacement() {
		return false;
	}

	@Override
	public boolean hasReplacement() {
		return this.replacement != null && this.replacement != this;
	}

	@Override
	public EObject getViewElement() {
		return view;
	}

	@Override
	public void setViewElement(EObject o) {
		this.view = o;
	}

	public void setSourceValue(Object sourceValue) {
		this.value = sourceValue;
	}
	
	public Map<FeatureBX<?, ?, ?, ?>, SourceProvider<?>> cacheMap() {
		return featureCache;
	}
}
