package edu.ustb.sei.mde.mobile2.core;

import org.eclipse.emf.ecore.EObject;

public interface ReferenceBX<SF,VF> extends FeatureBX<SF, Object, VF, EObject> {
	ElementBX getValueBX();
	void setValueBX(ElementBX valueBX);
	boolean isContainment();
}
