package edu.ustb.sei.mde.mobile2.core.providers;

import java.util.Collection;
import java.util.Collections;

import org.eclipse.emf.ecore.EObject;

/**
 * A source provider is the common interface to obtain a source value/object.
 * @author hexiao
 *
 */
public interface SourceProvider<T> {
	/**
	 * If no value can be returned, an UnsupportedOperationException should be thrown
	 * @return
	 */
	public T getValue();
	public boolean isReady();
	default T getFinalValue() {
		return getValue();
	}
	/**
	 * @return a fixedValue, a sourceObject, a nullSourceObject, or a createObjectEdit
	 */
	default SourceProvider<T> getCore() {
		return this;
	}
	
	/**
	 * Return true if the value is unknown to be empty or unset.
	 * @return
	 */
	default public boolean isEmpty() {
		return false;
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static <A> FixedValue<A> fixedValue(A sp) {
		if(sp==null) return (FixedValue<A>) empty;
		if(sp instanceof Collection && ((Collection) sp).isEmpty()) 
			return  (FixedValue<A>) emptySet;
		return new FixedValue<A>(sp);
	}
	
	public static SourceObjectProvider object(Object o) {
		return new SourceObject(o);
	}
	
	public static SourceObjectProvider newObject(EObject view) {
		SourceObjectProvider so = object(null);
		so.setViewElement(view);
		return so;
	}
	
	public static SourceObjectProvider nullObject() {
		return NullSourceObject.singleton;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static final FixedValue emptySet = new FixedValue(Collections.emptyList());
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static final FixedValue empty = new FixedValue(null);
}
