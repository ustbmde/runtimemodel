package edu.ustb.sei.mde.mobile2.core;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.mde.mobile2.core.ElementBX.AlignMode;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObject;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObjectProvider;
import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;
import edu.ustb.sei.mde.mobile2.system.Environment;
import edu.ustb.sei.mde.mobile2.util.Mobile2Utils;


/**
 * In put, ElementBX always returns SourceObjectEdit even though there is no change.
 * A SourceObjectEdit is a compound edit that owns a SourceObjectProvider and a set of feature edits.
 * The SourceObjectEdit returns the value of SourceObjectProvider.
 * The SourceObjectProvider may be a SourceObject or a CreateObjectEdit.
 * 
 * First, collect all objects from system along with containment tree.
 * For each object, create a SourceObject.
 * A SourceObject is able to relocate the object from a given key.
 * A CreateObjectEdit holds an internal key from ViewElement
 * 
 * @author hexiao
 */
@SuppressWarnings("unused")
public interface ElementBX extends MobileBX<SourceProvider<Object>, EObject> {

	default boolean isAbstract() {
		return getEcoreClass().isAbstract();
	}
	
	EClass getEcoreClass();

	Class<?> getJavaClass();

	boolean canHandle(Object o);
	boolean canHandle(EObject o);
	boolean shouldReloadForKey();
	EObject createView();

	
	List<FeatureBX<?, ?, ?, ?>> getFeatureBXs();
	<SF,SFE, VF, VFE> FeatureBX<SF,SFE,VF,VFE> getFeatureBX(String name);

	Object calculateSourceKey(Object source);
	Object calculateViewKey(EObject view);
	
	public enum AlignMode {
		initial
	}
	
	/**
	 * isAligned must be compatible with the following implementation:
	 * return this.canHandle(source) && this.canHandle(view) && computeSourceKey(source).equals(computeViewKey(view))
	 * @param source
	 * @param view
	 * @param context
	 * @param mode
	 * @return
	 */
	boolean isAligned(Object source, EObject view, Environment context, AlignMode mode);
	
	Optional<SourceObjectProvider> findSource(EObject view, Collection<SourceObjectProvider> candidates, Map<Object, SourceObjectProvider> keyMap, Environment context, AlignMode mode);
	
	List<ElementBX> getSubBXs();
	
	SystemBX getContainer();
	void setContainer(SystemBX sys);
	
}
