package edu.ustb.sei.mde.mobile2.core;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

import com.ibm.icu.util.Calendar;

import edu.ustb.sei.mde.mobile2.core.edits.CollectiveEdit;
import edu.ustb.sei.mde.mobile2.core.edits.CreateObjectEdit;
import edu.ustb.sei.mde.mobile2.core.edits.Edit;
import edu.ustb.sei.mde.mobile2.core.edits.Edit.EditKind;
import edu.ustb.sei.mde.mobile2.core.edits.EditContext;
import edu.ustb.sei.mde.mobile2.core.edits.MoveOutValueEdit;
import edu.ustb.sei.mde.mobile2.core.edits.NoEdit;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObject;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObjectProvider;
import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;
import edu.ustb.sei.mde.mobile2.system.Environment;
import edu.ustb.sei.mde.mobile2.util.Mobile2Utils;
import edu.ustb.sei.mde.mobile2.util.Pair;

public abstract class MultiValuedReferenceBXImpl extends ReferenceBXImpl<List<Object>, List<EObject>> implements MultiValuedReferenceBX {

	public MultiValuedReferenceBXImpl(EStructuralFeature viewFeature, FeatureKind kind) {
		super(viewFeature, kind);
	}

	@Override
	public Pair<EObject, List<EObject>> get(Pair<SourceObjectProvider, SourceProvider<List<Object>>> source,
			Environment env) {
		Environment newEnv = env.newScope(this.isContainment());
		
		List<Object> s = source.second.getValue();
		List<EObject> v = new ArrayList<>();
		
		for(Object sv : s) {
			EObject vv = getValueBX().get(newEnv.getSourcePool().getSourceObject(sv), newEnv);
			v.add(vv);
		}
		
		return Pair.pair(null, v);
	}

	@Override
	public Pair<SourceObjectProvider, SourceProvider<List<Object>>> put(
			Pair<SourceObjectProvider, SourceProvider<List<Object>>> source, Pair<EObject, List<EObject>> view,
			Environment env) {
		if(source.second.isEmpty() && view.second.isEmpty()) return source;
		
		Environment newEnv = env.newScope(this.isContainment());
		newEnv.setSourceParent(source.first.getFinalProvider());
		
		if(this.isContainment() ) {
			return putContainment(source, view, newEnv);
		} else {
			return putNonContainment(source, view, newEnv);
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected Pair<SourceObjectProvider, SourceProvider<List<Object>>> putContainment(
			Pair<SourceObjectProvider, SourceProvider<List<Object>>> source, Pair<EObject, List<EObject>> view,
			Environment newEnv) {
		
		List<Object> sList = source.second.getValue();
		List<EObject> vList = view.second;
		
		boolean parentUpdated = source.first.isReallyNew();
		
		if(coarseGrainedUpdate() || getKind().isConstructor()) {
			List<Edit<Object>> res = new ArrayList<>(vList.size());
			Set<Object> unmatchedSource = new HashSet<>(sList);
			
			for(int i=0;i<vList.size();i++) {
				Edit edit = computeCoarseGainedEdit(source, view, vList, unmatchedSource, i, newEnv);
				res.add(edit);
			}
			Edit<List<Object>> list = requestCollectiveEdit(source.first.getFinalProvider(), view.first, getViewFeature(), source.second, vList, res, true);
			
			if(getKind().isConstructor() || noActualChange(parentUpdated, source.second, res)) return Pair.pair(source.first, list);
			else {
				List<Edit<Object>> edits = new ArrayList<>(unmatchedSource.size());
				for(Object ums : unmatchedSource) {
					SourceObjectProvider so = newEnv.getSourcePool().getSourceObject(ums);
					
					EObject vv = so.getViewElement(); // ums <-trace->vv
					if(!Mobile2Utils.belongToView(vv, newEnv)) {
						try {
							Edit<Object> delete = requestDeleteEdit(source.first.getFinalProvider(), view.first, getViewFeature(), so, -1, newEnv);
							edits.add(delete);
						} catch (UnsupportedOperationException e) {
						}
					} else {
						EObject vp = vv.eContainer();
						try {
							Edit<Object> moveOut = requestMoveOutEdit(source.first.getFinalProvider(), view.first, getViewFeature(), vp, so, vv, -1, newEnv);
							edits.add(moveOut);
						} catch (UnsupportedOperationException e) {
						}
					}
				}
				SourceProvider<List<Object>> set = requestSetEdit(source.first.getFinalProvider(), view.first, getViewFeature(), list, source.second, vList, newEnv);
				if(edits.isEmpty())
					return Pair.pair(source.first.getFinalProvider(), set);
				else {
					SourceProvider<List<Object>> merged = requestMergeEdit(source.first.getFinalProvider(), view.first, getViewFeature(), set, edits);
					return Pair.pair(source.first.getFinalProvider(), merged);
				}
			}
		} else {
			Set<EObject> vSet = new HashSet<>(vList);
			Set<SourceObjectProvider> removedSource = new HashSet<>(sList.size());
			Map<Object, Integer> sourceOldIndexSet = new HashMap<>(sList.size());
			
			for(int i=0;i<sList.size();i++) {
				Object s = sList.get(i);
				sourceOldIndexSet.put(s, i);
				SourceObject so = newEnv.getSourcePool().getSourceObject(s);
				
				// NOTE: if parentUpdated, then the original object is null or replaced by a new one, 
				// hence, old links are removed
				if(parentUpdated || so.getViewElement()==null || !vSet.contains(so.getViewElement()))
					removedSource.add(so);
			}
			
			List<Edit<Object>> edits = new ArrayList<>(removedSource.size()+vList.size());
			
			Order oldOrderConv = new OriginalOrder();
			for(SourceObjectProvider so : removedSource) {
				int oldIndex = sourceOldIndexSet.get(so.getValue());
				// TODO: do profiling
				int newIndex = oldOrderConv.getNewId(oldIndex);
								
				EObject vv = so.getViewElement(); // ums <-trace->vv
				
				if(!Mobile2Utils.belongToView(vv, newEnv)) {
					Edit<Object> deleteEdit = requestDeleteEdit(source.first.getFinalProvider(), view.first, getViewFeature(), so, newIndex, newEnv);
					// check the children of so to find all objects that are moved out
					List moveOuts = collectMoveOut((SourceObject)so,newEnv);
					if(moveOuts.isEmpty())
						edits.add(deleteEdit);
					else 
						edits.add(requestMergeEdit(source.first.getFinalProvider(), view.first, getViewFeature(), deleteEdit, moveOuts));
					
				} else {
					EObject vp = vv.eContainer();
					edits.add(requestMoveOutEdit(source.first.getFinalProvider(), view.first, getViewFeature(), vp, so, vv, newIndex, newEnv));
				}
				oldOrderConv = new OrderAfterDelete(oldOrderConv, newIndex);
			}

			for(int i=0;i<vList.size();i++) {
				Pair<Edit,Order> pair = computeFineGrainedEdit(source, view, vList, parentUpdated, sourceOldIndexSet, oldOrderConv, i, newEnv);
				oldOrderConv = pair.second;
				edits.add(pair.first);
			}
			
			return Pair.pair(source.first.getFinalProvider(), requestArrayEdit(source.first.getFinalProvider(), view.first, getViewFeature(), edits, isOrdered()));
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private List<Edit> collectMoveOut(SourceObject deletedRoot, Environment env) {
		// collect moveOut edits of the deletedRoot's children
		// return a list of edits without any order
		List<Edit> edits = new ArrayList<>(128);
		
		deletedRoot.cacheMap().forEach((feature,val)->{
			if(!(feature.getViewFeature() instanceof EReference)) return;
			if(!((EReference)feature.getViewFeature()).isContainment()) return;
			if(feature.getKind().isConstructor()) return; // constructor parameter cannot be moved
			
			List<?> vals = null;
			if(feature.isMany()) {
				vals = (List<?>) val.getValue();
				int position = -1;
				boolean ordered = feature.getViewFeature().isOrdered();
				if(ordered) position = 0;
				List<Edit<Object>> moveOutEdits = new ArrayList<>();
				for(Object v : vals) {
					Object rawEdit = collectMoveOut(deletedRoot, feature, v, position, env);
					if(rawEdit instanceof MoveOutValueEdit) {
						moveOutEdits.add((Edit<Object>) rawEdit);
					} else {
						edits.addAll((List<Edit>)rawEdit);
						if(ordered) 
							position ++; // because we didn't produce remove edits
					}
				}
				edits.add(requestArrayEdit(deletedRoot, null, feature.getViewFeature(), moveOutEdits, feature.getViewFeature().isOrdered()));
			} else {
				Object rawEdit = collectMoveOut(deletedRoot, feature, val.getFinalValue(), 0, env);
				if(rawEdit instanceof MoveOutValueEdit) {
					edits.add((Edit)rawEdit);
				} else {
					edits.addAll((List<Edit>)rawEdit);
				}
			}
		});
		
		return edits;
	}

	private Object collectMoveOut(SourceObject deletedRoot, FeatureBX<?, ?, ?, ?> feature, Object val, int position, Environment env) {
		SourceObject so = env.getSourcePool().getSourceObject(val);
		if(so!=null ) {
			EObject vo = so.getViewElement();
			if(Mobile2Utils.belongToView(vo, env)) {
				// move out
				EObject vp = vo.eContainer();
				Edit<Object> moveOut = null;
				
				if(feature.isMany()) {
					moveOut = ((MultiValuedReferenceBXImpl)feature).requestMoveOutEdit(deletedRoot, /* deleted */null, feature.getViewFeature(), vp, so, vo, position, env);							
				} else {
					moveOut = ((SingleValuedReferenceBXImpl)feature).requestMoveOutEdit(deletedRoot, /* deleted */null, feature.getViewFeature(), vp, so, vo, position, env);							
				}
				
				return moveOut;
			} else {
				// delete
				return collectMoveOut(so, env);
			}
		}
		
		return Collections.emptyList();
	}

	protected boolean noActualChange(boolean parentUpdated, SourceProvider<List<Object>> second, List<Edit<Object>> res) {
		if(parentUpdated) return false;
		List<Object> tempList = new ArrayList<>();
		for(Edit<Object> e : res) {
			SourceProvider<Object> core = e.getCore();
			if(core instanceof CreateObjectEdit) return false;
			else if(core.isEmpty()) return false;
			else {
				SourceObjectProvider finalProvider = ((SourceObjectProvider)core).getFinalProvider();
				if(finalProvider.isNewOrReloaded()) return false;
				tempList.add(finalProvider.getValue());
			}
		}
		List<Object> oldValue = second.getValue();
		return oldValue.equals(tempList); // FIXME: too strict for unordered collections
	}

	@SuppressWarnings("rawtypes")
	protected Edit computeCoarseGainedEdit(Pair<SourceObjectProvider, SourceProvider<List<Object>>> source,
			Pair<EObject, List<EObject>> view, List<EObject> vList, Set<Object> unmatchedSource, int i,
			Environment newEnv) {
		Edit edit;
		EObject v = vList.get(i);
		SourceObjectProvider s = newEnv.resolveSource(v);
		SourceProvider<Object> sp = null;
		// if s == null, sp = put null v
		// else sp = put s v
		if(s==null) s = newEnv.resolveFutureSource(v);
		else {
			unmatchedSource.remove(s.getValue());
		}
		sp = getValueBX().put(s, v, newEnv);
		
		if(sp instanceof Edit)
			edit = ((Edit<Object>)sp);
		else 
			edit = (requestNoEdit(source.first.getFinalProvider(), view.first, getViewFeature(), sp, s, v, newEnv));
		return edit;
	}

	@SuppressWarnings("rawtypes")
	protected Pair<Edit,Order> computeFineGrainedEdit(Pair<SourceObjectProvider, SourceProvider<List<Object>>> source,
			Pair<EObject, List<EObject>> view, List<EObject> vList, boolean parentUpdated,
			Map<Object, Integer> sourceOldIndexSet, Order oldOrderConv, int i,
			Environment newEnv) {
		Edit edit;
		Order order = oldOrderConv;
		
		EObject v = vList.get(i);
		SourceObjectProvider s = newEnv.resolveSource(v);
		SourceProvider<Object> sp = null;
		if(s==null) s = newEnv.resolveFutureSource(v);
		sp = getValueBX().put(s, v, newEnv); // If s is new, put must register it into source pool
		
		if(s.isEmpty()) { // new
			edit = (requestInsertEdit(source.first.getFinalProvider(), view.first, getViewFeature(), sp, v, i, newEnv));
			order = new OrderAfterInsert(oldOrderConv, i);
		} else {
			int oldIndex = sourceOldIndexSet.getOrDefault(s.getValue(), -1);
			
			if(oldIndex != -1 && !parentUpdated) {
				// found in sList
				// FIXME: since sp is always the original value, sp==s when sp is intended to be the replacement of s!!
				if(Mobile2Utils.notChanged(sp, s, actionOnReload())) {
					int convertedOldIndex = oldOrderConv.getNewId(oldIndex);
					if(isOrdered() && convertedOldIndex!=i) {
						edit = (requestReorderEdit(source.first.getFinalProvider(), view.first, getViewFeature(), sp, v, i, convertedOldIndex, newEnv));
					} else {								
						edit = (requestRetainEdit(source.first.getFinalProvider(), view.first, getViewFeature(), sp, v, i, convertedOldIndex, newEnv));
					}
					if(convertedOldIndex!=i)
						order = new Reorder(oldOrderConv, convertedOldIndex, i);
				} else {
					int convertedOldIndex = oldOrderConv.getNewId(oldIndex);
					edit = (requestModifyEdit(source.first.getFinalProvider(), view.first, getViewFeature(), sp, s, v, i, convertedOldIndex, newEnv));
					if(convertedOldIndex!=i)
						order = new Reorder(oldOrderConv, convertedOldIndex, i);
				}
			} else {
				// no found in sList, do moveIn
				// when parentUpdated == true, the following code may move the object from the old to the new.
				assert s instanceof SourceObject;
				SourceObjectProvider sParent = ((SourceObject)s).getContainer();
				edit = (requestMoveInEdit(source.first.getFinalProvider(), view.first, getViewFeature(), sParent, sp, s, v, i, newEnv));
				order = new OrderAfterInsert(oldOrderConv, i);
			}
		}
		
		return Pair.pair(edit, order);
	}
	
	

	/**
	 * [s] [v]
	 * for each vi, 
	 *   find sj, syn sj vi at i
	 *     if no sj, create sj from vi
	 * for unmathced sj, delete sj
	 *  
	 * @param source
	 * @param view
	 * @param newEnv
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected Pair<SourceObjectProvider, SourceProvider<List<Object>>> putNonContainment(
			Pair<SourceObjectProvider, SourceProvider<List<Object>>> source, Pair<EObject, List<EObject>> view,
			Environment newEnv) {
		List<Object> sList = source.second.getValue();
		List<EObject> vList = view.second;
		boolean parentUpdated = source.first.isReallyNew();
		
		if(coarseGrainedUpdate()) {
			List<Edit<Object>> res = new ArrayList<>(vList.size());
			for(int i=0;i<vList.size();i++) {
				EObject v = vList.get(i);
				SourceObjectProvider s = newEnv.resolveFutureSource(v);
				assert s!=null;
				SourceObjectProvider sp = s.getFinalProvider();
				if(sp instanceof Edit)
					res.add((Edit<Object>)sp);
				else res.add(requestNoEdit(source.first.getFinalProvider(), view.first, getViewFeature(), sp, s, v, newEnv));
			}
			Edit<List<Object>> list = requestCollectiveEdit(source.first.getFinalProvider(), view.first, getViewFeature(), source.second, vList, res, true);
			return Pair.pair(source.first.getFinalProvider(), 
					requestSetEdit(source.first.getFinalProvider(), view.first, getViewFeature(), list, source.second, vList, newEnv));
		} else {
			Set<EObject> vSet = new HashSet<>(vList);
			Set<SourceObjectProvider> removedSource = new HashSet<>(sList.size());
			Map<Object, Integer> sourceOldIndexSet = new HashMap<>(sList.size());
			
			for(int i=0;i<sList.size();i++) {
				Object s = sList.get(i);
				sourceOldIndexSet.put(s, i);
				SourceObject so = newEnv.getSourcePool().getSourceObject(s);
				
				if(parentUpdated || so.getViewElement()==null || !vSet.contains(so.getViewElement()))
					removedSource.add(so);
			}
			
			List<Edit<Object>> edits = new ArrayList<>(removedSource.size()+vList.size());
			Order oldOrderConv = new OriginalOrder();

			for(SourceObjectProvider so : removedSource) {
				int oldIndex = sourceOldIndexSet.get(so.getValue());
				int newIndex = oldOrderConv.getNewId(oldIndex);
				edits.add(requestDeleteEdit(source.first.getFinalProvider(), view.first, getViewFeature(), so, newIndex, newEnv));
				oldOrderConv = new OrderAfterDelete(oldOrderConv, newIndex);
			}
			
			
			for(int i=0;i<vList.size();i++) {
				EObject v = vList.get(i);
				SourceObjectProvider s = newEnv.resolveFutureSource(v);
				assert s!=null;
				SourceObjectProvider sp = s.getFinalProvider();
				
				
				if(s.isEmpty()) {
					// s is newly created, do insert
					edits.add(requestInsertEdit(source.first.getFinalProvider(), view.first, getViewFeature(), sp, v, i, newEnv));
					oldOrderConv = new OrderAfterInsert(oldOrderConv, i);
				} else {
					int oldIndex = sourceOldIndexSet.getOrDefault(s.getValue(), -1);
					int convertedOldIndex = oldOrderConv.getNewId(oldIndex);
					if(oldIndex != -1 && !parentUpdated) {
						// found in sList
//						sourceOldIndexSet.remove(s.getValue());
						if(Mobile2Utils.notChanged(sp, s, actionOnReload())) {
							if(isOrdered() && convertedOldIndex!=i) {
								edits.add(requestReorderEdit(source.first.getFinalProvider(), view.first, getViewFeature(), sp, v, i, convertedOldIndex, newEnv));
							} else {
								edits.add(requestRetainEdit(source.first.getFinalProvider(), view.first, getViewFeature(), sp, v, i, convertedOldIndex, newEnv));
							}
							oldOrderConv = new Reorder(oldOrderConv, convertedOldIndex, i);
						} else {
							edits.add(requestModifyEdit(source.first.getFinalProvider(), view.first, getViewFeature(), sp, s, v, i, convertedOldIndex, newEnv));
							oldOrderConv = new Reorder(oldOrderConv, convertedOldIndex, i);
						}
					} else {
						// no found in sList, do insert
						assert s instanceof SourceObject;
						edits.add(requestInsertEdit(source.first.getFinalProvider(), view.first, getViewFeature(), sp, v, i, newEnv));
						oldOrderConv = new OrderAfterInsert(oldOrderConv, i);
					}
				}
			}
			
			return Pair.pair(source.first.getFinalProvider(), requestArrayEdit(source.first.getFinalProvider(), view.first, getViewFeature(), edits, isOrdered()));
		}
	}

	public boolean isOrdered() {
		return this.getViewFeature().isOrdered();
	}
	
	
	public Edit<Object> requestNoEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, SourceProvider<Object> value, SourceProvider<Object> oldValue, EObject viewValue, Environment environment) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, EditKind.none)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement)
				.bind(EditContext.FEATURE, feature)
				.bind(EditContext.SOURCE_VALUE, value)
				.bind(EditContext.OLD_SOURCE_VALUE, oldValue)
				.bind(EditContext.VIEW_VALUE, viewValue)
				.bind(EditContext.ENVIRONMENT, environment);
		return NoEdit.noEdit(context, value);
	}
	
	@Deprecated
	final public Edit<Object> requestRetainEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, SourceProvider<Object> value, EObject viewValue, int position, Environment environment) {
		return requestRetainEdit(sourceObject, viewElement, feature, value, viewValue, position, position, environment);
	}
	
	public Edit<Object> requestRetainEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, SourceProvider<Object> value, EObject viewValue, int position, int oldPosition, Environment environment) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, EditKind.retain)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement)
				.bind(EditContext.FEATURE, feature)
				.bind(EditContext.SOURCE_VALUE, value)
				.bind(EditContext.POSITION, position)
				.bind(EditContext.OLD_POSITION, oldPosition)
				.bind(EditContext.VIEW_VALUE, viewValue)
				.bind(EditContext.ENVIRONMENT, environment);
		// FIXME:
		return NoEdit.noEdit(context, value);
	}

	protected Edit<Object> createModifyEdit(EditContext context) {
		EditContext delContext = context.copy();
		delContext.bind(EditContext.OPERATION, EditKind.remove);
		delContext.bind(EditContext.SOURCE_VALUE, delContext.getValueForKey(EditContext.OLD_SOURCE_VALUE));
		delContext.bind(EditContext.POSITION, delContext.getValueForKey(EditContext.OLD_POSITION));
		Edit<Object> del = createDeleteEdit(delContext);
	
		EditContext insContext = context.copy();
		insContext.bind(EditContext.OPERATION, EditKind.insert);
		Edit<Object> ins = createInsertEdit(insContext);

		return requestMergeEdit(context.getValueForKey(EditContext.SOURCE_OBJECT), context.getValueForKey(EditContext.VIEW_ELEMENT), context.getValueForKey(EditContext.FEATURE), ins, Collections.singletonList(del));

	}

	public Edit<List<Object>> requestSetEdit(SourceProvider<Object> sourceObject, EObject viewElement,
			EStructuralFeature feature, SourceProvider<List<Object>> value, SourceProvider<List<Object>> oldValue, List<EObject> viewValue,
			Environment environment) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, EditKind.set)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement)
				.bind(EditContext.FEATURE, feature)
				.bind(EditContext.SOURCE_VALUE, value)
				.bind(EditContext.OLD_SOURCE_VALUE, oldValue)
				.bind(EditContext.VIEW_VALUE, viewValue)
				.bind(EditContext.ENVIRONMENT, environment);
		return createSetEdit(context);
	}
	
	protected Edit<List<Object>> createSetEdit(EditContext context) {
		throw new UnsupportedOperationException("createSetEdit is not implmeneted");
	}

	public Edit<Object> requestInsertEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, SourceProvider<Object> value, EObject viewValue, int position, Environment environment) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, EditKind.insert)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement)
				.bind(EditContext.FEATURE, feature)
				.bind(EditContext.SOURCE_VALUE, value)
				.bind(EditContext.POSITION, position)
				.bind(EditContext.VIEW_VALUE, viewValue)
				.bind(EditContext.ENVIRONMENT, environment);
		return createInsertEdit(context);
	}
	protected Edit<Object> createInsertEdit(EditContext context) {
		throw new UnsupportedOperationException("createInsertEdit is not implmeneted");
	}

	public Edit<Object> requestDeleteEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, SourceProvider<Object> value, int position, Environment environment) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, EditKind.remove)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement)
				.bind(EditContext.FEATURE, feature)
				.bind(EditContext.SOURCE_VALUE, value)
				.bind(EditContext.POSITION, position)
				.bind(EditContext.ENVIRONMENT, environment);
		return createDeleteEdit(context);
	}

	protected Edit<Object> createDeleteEdit(EditContext context) {
		throw new UnsupportedOperationException("createDeleteEdit is not implmeneted");
	}
	
	
	
	/**
	 * @deprecated use modify instead
	 * @param sourceObject
	 * @param viewElement
	 * @param feature
	 * @param value
	 * @param viewValue
	 * @param position
	 * @param oldPosition
	 * @param environment
	 * @return
	 */
	public Edit<Object> requestReorderEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, SourceProvider<Object> value, EObject viewValue, int position, int oldPosition, Environment environment) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, EditKind.reorder)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement)
				.bind(EditContext.FEATURE, feature)
				.bind(EditContext.SOURCE_VALUE, value)
				.bind(EditContext.POSITION, position)
				.bind(EditContext.OLD_POSITION, oldPosition)
				.bind(EditContext.VIEW_VALUE, viewValue)
				.bind(EditContext.ENVIRONMENT, environment);
		return createReorderEdit(context);
	}
	
	/**
	 * @deprecated  use modify instead
	 * @param context
	 * @return
	 */
	@Deprecated()
	protected Edit<Object> createReorderEdit(EditContext context) {
		context.bind(EditContext.OPERATION, EditKind.modify);
		context.bind(EditContext.OLD_SOURCE_VALUE, context.getValueForKey(EditContext.SOURCE_VALUE));
		return createModifyEdit(context);
	}

	public Edit<Object> requestModifyEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, SourceProvider<Object> value, SourceProvider<Object> oldValue, EObject viewValue, int position, int oldPosition, Environment environment) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, EditKind.modify)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement)
				.bind(EditContext.FEATURE, feature)
				.bind(EditContext.SOURCE_VALUE, value)
				.bind(EditContext.OLD_SOURCE_VALUE, oldValue)
				.bind(EditContext.POSITION, position)
				.bind(EditContext.OLD_POSITION, oldPosition)
				.bind(EditContext.VIEW_VALUE, viewValue)
				.bind(EditContext.ENVIRONMENT, environment);
		return createModifyEdit(context);
	}

	public Edit<Object> requestMoveInEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, SourceProvider<Object> fromObject, SourceProvider<Object> value, SourceObjectProvider oldValue, EObject viewValue, int position, Environment environment) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, EditKind.moveIn)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement)
				.bind(EditContext.FEATURE, feature)
				.bind(EditContext.FROM_SOURCE_OBJECT, fromObject)
				.bind(EditContext.SOURCE_VALUE, value)
				.bind(EditContext.OLD_SOURCE_VALUE, oldValue)
				.bind(EditContext.VIEW_VALUE, viewValue)
				.bind(EditContext.POSITION, position)
				.bind(EditContext.ENVIRONMENT, environment);
		return createMoveInEdit(context);
	}
	protected Edit<Object> createMoveInEdit(EditContext context) {
		context.bind(EditContext.OPERATION, EditKind.insert);
		return createInsertEdit(context);
	}

	public Edit<Object> requestMoveOutEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, EObject toElement, SourceProvider<Object> value, EObject viewValue, int position, Environment environment) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, EditKind.moveOut)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement)
				.bind(EditContext.FEATURE, feature)
				.bind(EditContext.TO_VIEW_ELEMENT, toElement)
				.bind(EditContext.SOURCE_VALUE, value)
				.bind(EditContext.VIEW_VALUE, viewValue)
				.bind(EditContext.POSITION, position)
				.bind(EditContext.ENVIRONMENT, environment);
		return createMoveOutEdit(context);
	}

	protected Edit<Object> createMoveOutEdit(EditContext context) {
		context.bind(EditContext.OPERATION, EditKind.remove);
		return createDeleteEdit(context);
	}
}

abstract class Order {
	abstract public int getNewId(int oldId);
	
	protected Order chain;
	
	public Order(Order append) {
		this.chain = append;
	}
}

class OriginalOrder extends Order {
	public OriginalOrder() {
		super(null);
	}

	@Override
	public int getNewId(int oldId) {
		return oldId;
	}
}

class OrderAfterInsert extends Order {
	public OrderAfterInsert(Order append, int insertAt) {
		super(append);
		this.insertAt = insertAt;
	}
	private int insertAt;
	@Override
	public int getNewId(int oldId) {
		int id = chain.getNewId(oldId);
		if(id < insertAt) {
			return id;
		} else {
			return id + 1;
		}
	}
}

class OrderAfterDelete extends Order {
	private int deleteAt;
	public OrderAfterDelete(Order append, int deleteAt) {
		super(append);
		this.deleteAt = deleteAt;
	}
	@Override
	public int getNewId(int oldId) {
		int id = chain.getNewId(oldId);
		if(id < deleteAt) {
			return id;
		} else {
			return id - 1;
		}
	}
}

class Reorder extends Order {
	public Reorder(Order append, int from, int to) {
		super(append);
		this.fromOriginalId = from;
		this.toNewId = to;
	}
	private int fromOriginalId;
	private int toNewId;
	@Override
	public int getNewId(int oldId) {
		int id = chain.getNewId(oldId);
		
		if(fromOriginalId == toNewId) {
			return id;
		} else if(fromOriginalId < toNewId) {
			if(fromOriginalId < id && id <= toNewId) {
				return id - 1;
			} else
				return id;
		} else {
			if(toNewId <= id && id < fromOriginalId) {
				return id + 1;
			} else
				return id;
		}
	}
}
