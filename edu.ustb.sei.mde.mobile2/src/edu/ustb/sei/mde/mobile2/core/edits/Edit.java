package edu.ustb.sei.mde.mobile2.core.edits;

import java.util.List;
import java.util.Set;

import edu.ustb.sei.mde.mobile2.core.edits.EditContext.EditContextImpl;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObjectProvider;
import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;

/**
 * <p> An edit operation that changes the source system. 
 * This interface is intended for further extension, although currently, a fixed set of edits are supported.</p>
 * 
 * <p> For objects, the following edits are supported now. </p>
 * <ul>
 * <li> replace: replace an object with another. </li>
 * <li> create: create a new object. </li>
 * <li> delete: delete an object </li>
 * </ul>
 * 
 * <p> For features, the following edits are supported now. </p>
 * <ul>
 * <li> set: change the value for a feature. </li>
 * <li> insert: insert a value at a specific position. </li>
 * <li> delete: delete a value. </li>
 * <li> replace: replace an old value with a new value. </li>
 * <li> moveIn/moveOut: move a value to a new container. </li>
 * <li> reorder: reorder a value in its container. </li>
 * </ul>
 * 
 * <p> An edit is also a source provider that returns a value after the edit. </p>
 *  
 * @author hexiao
 *
 */
public interface Edit<T>  extends SourceProvider<T> {
	EditContext getContext();
	void run();
	
	static public enum EditKind {
		none,
		create,
		set,
		insert,
		remove,
		moveIn,
		moveOut,
		modify,
		reorder,
		retain;// used in array only as a place holder
		
		static public EditKind[] featureEdits() {
			return new EditKind[] {set, insert, remove, moveIn, moveOut, modify, reorder, retain};
		}
		
		static public EditKind[] viewValueEdits() {
			return new EditKind[] {set, insert, moveIn, modify, retain};
		}
	}
	
	static public enum CompoundEditKind {
		array,
		collector, 
		merge
	}
	
	Set<Edit<?>> collectEdits();
	long getId();
	
	default Edit<T> getArrayEdit() {
		return null;
	}
	
	@SuppressWarnings("unchecked")
	default Edit<?> interpret(org.eclipse.xtext.xbase.lib.Pair<String,Object>... valuePairs) {
		for(org.eclipse.xtext.xbase.lib.Pair<String,Object> pair : valuePairs) {
			String key = EditContextImpl.shortKeyMap.getOrDefault(pair.getKey(), pair.getKey());
			switch(key) {
			case EditContext.SOURCE_OBJECT: 
			case EditContext.FROM_SOURCE_OBJECT: 
			{
				this.getContext().bind(key, SourceProvider.object(pair.getValue()));
				break;
			}
			case EditContext.SOURCE_VALUE:
			case EditContext.OLD_SOURCE_VALUE:
			{
				if(getContext().getValueForKey(pair.getKey()) instanceof SourceObjectProvider)
					this.getContext().bind(key, SourceProvider.object(pair.getValue()));
				else
					this.getContext().bind(key, SourceProvider.fixedValue(pair.getValue()));
				break;
			}
			}
		}
		return this;
	}
	default void mergeEffect(final List<Edit<?>> e) {}
}
