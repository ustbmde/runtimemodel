package edu.ustb.sei.mde.mobile2.core;

import org.eclipse.emf.ecore.EStructuralFeature;

public abstract class AttributeBXImpl<SF, SFE, VF, VFE> extends FeatureBXImpl<SF, SFE, VF, VFE> implements AttributeBX<SF, SFE, VF, VFE> {

	public AttributeBXImpl(EStructuralFeature viewFeature, FeatureKind kind) {
		super(viewFeature, kind);
	}
	
	private ValueBX<SFE,VFE> valueBX;

	public ValueBX<SFE,VFE> getValueBX() {
		return valueBX;
	}

	public void setValueBX(ValueBX<SFE,VFE> valueBX) {
		this.valueBX = valueBX;
	}
}
