package edu.ustb.sei.mde.mobile2.core.parallel;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.mde.mobile2.core.ElementBXImpl;
import edu.ustb.sei.mde.mobile2.core.FeatureBX;
import edu.ustb.sei.mde.mobile2.core.ElementBXImpl.FeatureUpdateTask;
import edu.ustb.sei.mde.mobile2.core.edits.Edit;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObject;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObjectProvider;
import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;
import edu.ustb.sei.mde.mobile2.system.Environment;
import edu.ustb.sei.mde.mobile2.system.Mobile2ThreadPool;

public abstract class ParallelElementBXImpl extends ElementBXImpl {

	public ParallelElementBXImpl(Class<?> javaClass, EClass ecoreClass) {
		super(javaClass, ecoreClass);
		// TODO Auto-generated constructor stub
	}
	
	@SuppressWarnings({ "rawtypes" })
	protected SourceProvider<Object> concretePut(SourceProvider<Object> source, EObject view, Environment env) {
		assert env.isRefreshMode() != false; // we do not call elementBX during backward transformation because trace system is a shortcut
		
		if(view == null) {
			System.out.println("A null view is found. Please check if it is the root object: "+source.getValue());
			return null; 
		}
		
		/*
		 * 1. check if source can be handled by this bx, if not the source must be re-created
		 * 2. check the constructor parameters, if any parameter is changed, then the source must be re-created.
		 * 3. synchronize non constructor features
		 */
		
		SourceObjectProvider updatedSource;
		synchronized(source.getCore()) {
			if(((SourceObject)source.getCore()).isChecked())
				return ((SourceObject)source.getCore()).getFinalProvider();
			
			updatedSource = computeNewSource(source, view, env);			
		}
		
		Mobile2ThreadPool taskPool = env.getTaskPool();
		taskPool.fillScoreBoard(view);
		
		List<Edit<?>> edits = new ArrayList<>();
		
		// containments
		for(FeatureBX f: getContainmentFeatures()) {
			taskPool.execute(new FeatureUpdateTask(taskPool, this, source, view, updatedSource, f, env, edits, true));
		}
		
		// noncontainments and attributes
		for(FeatureBX f: getOtherFeatures()) {
			taskPool.add(2, new FeatureUpdateTask(taskPool, this, source, view, updatedSource, f, env, edits, false));
		}
		
		Edit<Object> merge = requestMergeEdit(source, view, updatedSource, edits);
		
		return merge;
	}

}
