package edu.ustb.sei.mde.mobile2.core.providers;

public class FutureSourceObject extends SourceObject implements FutureSourceObjectProvider {

	public FutureSourceObject(SourceObject object) {
		super(object.getValue());
		this.setSourceValue(object.getValue());
		object.setReplacement(this);
		this.setViewElement(object.getViewElement());
	}
	
	private SourceObject originalProvider;

	@Override
	public void setOriginalProvider(SourceObject o) {
		originalProvider = o;
	}
	
	@Override
	public SourceObjectProvider getOriginalProvider() {
		return originalProvider;
	}
	
	@Override
	public void setReplacement(SourceObjectProvider t) {
		// do nothing
	}
	
	@Override
	public boolean isReplacement() {
		return true;
	}
	
	@Override
	public void setValue(Object value) {
		this.setSourceValue(value);
	}

}
