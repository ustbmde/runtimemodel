package edu.ustb.sei.mde.mobile2.core.edits;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import edu.ustb.sei.mde.mobile2.system.Environment;

public interface EditContext {
	static public EditContext newContext() {
		return new EditContextImpl();
	}
	
	public static final String OPERATION = "__EDIT__CONTEXT__OPERATION__";
	
	public static final String SOURCE_OBJECT = "__EDIT__CONTEXT__SOURCE__OBJECT__";
	public static final String VIEW_ELEMENT = "__EDIT__CONTEXT__VIEW__ELEMENT__";
	public static final String FEATURE = "__EDIT__CONTEXT__FEATURE__";
	
	
	public static final String CONSTRUCTOR_PARAM = "__EDIT__CONTEXT__CONSTRUCTOR__PARAM__"; // for create
	
	public static final String FROM_SOURCE_OBJECT = "__EDIT__CONTEXT__FROM__SOURCE__OBJECT__"; // for moveIn/moveOut
	public static final String TO_VIEW_ELEMENT = "__EDIT__CONTEXT__TO__VIEW__ELEMENT__"; // for moveIn/moveOut
	
	public static final String SOURCE_VALUE = "__EDIT__CONTEXT__SOURCE__VALUE__"; // for set/insert/delete/moveIn/moveOut value
	public static final String OLD_SOURCE_VALUE = "__EDIT__CONTEXT__OLD__SOURCE_VALUE__"; // for replace value
	
	public static final String POSITION = "__EDIT__CONTEXT__POSITION__"; // for replace/insert/delete/moveIn/moveOut
	public static final String OLD_POSITION = "__EDIT__CONTEXT__OLD__POSITION__"; // for replace/insert/delete/moveIn/moveOut
	
	public static final String PREVIOUS_EDIT = "__EDIT__CONTEXT__PREVIOUS__EDIT__"; // for compound edits
	public static final String NEXT_EDIT = "__EDIT__CONTEXT__NEXT__EDIT__"; // for compound edits

	public static final String VIEW_VALUE = "__EDIT__CONTEXT__VIEW__VALUE__";

	public static final String ENVIRONMENT = "__EDIT__CONTEXT__ENVIRONMENT__";
	
	public static final String CREATION_WITH_PARENT = "__EDIT__CONTEXT__CREATION__WITH__PARENT__";
	
	static final public String[] printKeys = {OPERATION, SOURCE_OBJECT, VIEW_ELEMENT, FEATURE, FROM_SOURCE_OBJECT, TO_VIEW_ELEMENT, SOURCE_VALUE, OLD_SOURCE_VALUE, POSITION, OLD_POSITION, VIEW_VALUE};
	
	
	
	
	EditContext bind(String key, Object value);
	<T> T getValueForKey(String key);
	
	// ACCESSORS
	default EObject getView() {
		return getValueForKey(VIEW_ELEMENT);
	}
	default <T> T getViewValue() {
		return getValueForKey(VIEW_VALUE);
	}
	default EStructuralFeature getFeature() {
		return getValueForKey(FEATURE);
	}
	default int getPosition() {
		return getValueForKey(POSITION);
	}
	default int getOldPosition() {
		return getValueForKey(OLD_POSITION);
	}
	
	default Object getSource() {
		return getValueForKey(SOURCE_OBJECT);
	}
	default <T> T getValue() {
		return getValueForKey(SOURCE_VALUE);
	}
	default <T> T getOldValue() {
		return getValueForKey(OLD_SOURCE_VALUE);
	}
	default Object getFromSource() {
		return getValueForKey(FROM_SOURCE_OBJECT);
	}
	default EObject getToView() {
		return getValueForKey(TO_VIEW_ELEMENT);
	}
	default Environment getEnvironment() {
		return getValueForKey(ENVIRONMENT);
	}
	// END
	
	
	class EditContextImpl implements EditContext {
		static final public Map<String, String> shortKeyMap;
		static {
			shortKeyMap = new HashMap<>();
			shortKeyMap.put("source", SOURCE_OBJECT);
			shortKeyMap.put("view", VIEW_ELEMENT);
			shortKeyMap.put("fromSource", FROM_SOURCE_OBJECT);
			shortKeyMap.put("toView", TO_VIEW_ELEMENT);
			shortKeyMap.put("value", SOURCE_VALUE);
			shortKeyMap.put("oldValue", OLD_SOURCE_VALUE);
			shortKeyMap.put("viewValue", VIEW_VALUE);
			shortKeyMap.put("position", POSITION);
			shortKeyMap.put("oldPosition", OLD_POSITION);
			shortKeyMap.put("feature", FEATURE);
		}
		
		private Map<String, Object> keyValueMap;
		public EditContextImpl() {
			keyValueMap = new HashMap<String, Object>();
		}
		@Override
		public EditContext bind(String key, Object value) {
			this.keyValueMap.put(key, value);
			return this;
		}
		@SuppressWarnings("unchecked")
		@Override
		public <T> T getValueForKey(String key) {
			return (T) this.keyValueMap.get(key);
		}
		@Override
		public EditContext copy() {
			EditContextImpl copy = new EditContextImpl();
			copy.keyValueMap.putAll(this.keyValueMap);
			return copy;
		}
		
		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("[\n");
			for(String sk : printKeys) {
				Object v = getValueForKey(sk);
				if(sk!=null) sb.append("\t").append(sk).append("=>").append(v).append("\n"); 
			}
			sb.append("]\n");
			return sb.toString();
		}
		@Override
		public <T> T getShortKey(String shortKey, T defaultValue) {
			String fullKey = shortKeyMap.getOrDefault(shortKey, shortKey);
			T res = getValueForKey(fullKey);
			if(res==null) return defaultValue;
			else return res;
		}
		
		@Override
		public String getClassMappingName() {
			return classMappingName;
		}
		@Override
		public void setClassMappingName(String classMappingName) {
			this.classMappingName = classMappingName;
		}

		@Override
		public String getFeatureMappingName() {
			return featureMappingName;
		}
		@Override
		public void setFeatureMappingName(String featureMappingName) {
			this.featureMappingName = featureMappingName;
		}

		private String classMappingName = null;
		private String featureMappingName = null;
	}


	EditContext copy();
	<T> T getShortKey(String key, T defaultValue);
	
	void setFeatureMappingName(String featureMappingName);
	String getFeatureMappingName();
	void setClassMappingName(String classMappingName);
	String getClassMappingName();
}