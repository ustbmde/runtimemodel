package edu.ustb.sei.mde.mobile2.core.edits.impl;

import edu.ustb.sei.mde.mobile2.core.edits.EditContext;
import edu.ustb.sei.mde.mobile2.core.edits.EditImpl;
import edu.ustb.sei.mde.mobile2.core.edits.SetValueEdit;

public abstract class SetValueEditImpl<T> extends EditImpl<T> implements SetValueEdit<T> {

	public SetValueEditImpl(EditContext context) {
		super(context);
		this.calculatedValue = context.getValueForKey(EditContext.SOURCE_VALUE);
	}
}
