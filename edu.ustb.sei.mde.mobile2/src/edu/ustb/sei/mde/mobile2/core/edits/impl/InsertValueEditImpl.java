package edu.ustb.sei.mde.mobile2.core.edits.impl;

import edu.ustb.sei.mde.mobile2.core.edits.EditContext;
import edu.ustb.sei.mde.mobile2.core.edits.EditImpl;
import edu.ustb.sei.mde.mobile2.core.edits.InsertValueEdit;

public abstract class InsertValueEditImpl<T> extends EditImpl<T> implements InsertValueEdit<T> {

	public InsertValueEditImpl(EditContext context) {
		super(context);
		this.calculatedValue = context.getValueForKey(EditContext.SOURCE_VALUE);
	}
}
