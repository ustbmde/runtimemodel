package edu.ustb.sei.mde.mobile2.core.edits;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObject;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObjectProvider;
import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;
import edu.ustb.sei.mde.mobile2.system.SourcePool;
import edu.ustb.sei.mde.mobile2.util.Pair;

public abstract class EditImpl<T> extends Object implements Edit<T> {

	private EditContext context;
	
	private long id;
	
	static long count = 0;
	
	public EditImpl(EditContext context) {
		id = count ++;
		this.context = context;
		this.calculatedValue = null;
		extraEditConfigure();
	}

	@Override
	public long getId() {
		return id;
	}

	@Override
	public EditContext getContext() {
		return context;
	}
	
	protected SourceProvider<T> calculatedValue;
	
	@Override
	public boolean isReady() {
		return calculatedValue==null || calculatedValue.isReady();
	}
	
	@Override
	public T getValue() {
		if(calculatedValue!=null) return calculatedValue.getValue();
		else return null;
	}
	
	@Override
	public SourceProvider<T> getCore() {
		if(calculatedValue==null || calculatedValue==this) return this;
		else return calculatedValue.getCore();
	}
	
	@Override
	public boolean isEmpty() {
		return calculatedValue==null || calculatedValue.isEmpty();
	}
	
	@Override
	public Set<Edit<?>> collectEdits() {
		Set<Edit<?>> edits = new HashSet<>();
		if(calculatedValue!=null) {
			if(calculatedValue instanceof Edit)
				edits.addAll(((Edit<?>) calculatedValue).collectEdits());
			else if(calculatedValue instanceof Collection) {
				((Collection<?>) calculatedValue).forEach(e->{
					if(e instanceof Edit) {
						edits.addAll(((Edit<?>) e).collectEdits());
					}
				});
			}
		}
		edits.add(this);
		return edits;
	}
	
	protected void extraEditConfigure() {
	}
	
	@Override
	public String toString() {
		return this.getClass().getName().concat("@")+getId();
	}
	
	
	/*
	 * Helper functions for post procession
	 */
	 
	public void reload(SourceObjectProvider sourceObject, Object value) throws Exception {
		try {
			this.getContext().getEnvironment().getSourcePool().dynamicReload(sourceObject, value);
		} catch (Exception e) {
			throw e;
		}
	}
	protected SourceObjectProvider getSourceObject() {
		return ((SourceObjectProvider) ((SourceProvider<?>)this.getContext().getSource()).getCore()).getFinalProvider();
	}
	protected SourceObjectProvider getValueObject() {
		return ((SourceObjectProvider) ((SourceProvider<?>)this.getContext().getValue()).getCore()).getFinalProvider();
	}
	protected SourceObjectProvider getTobeObject(EObject o) throws Exception {
		try {
			return this.getContext().getEnvironment().getSourcePool().findToBeObject(o);
		} catch (Exception e) {
			throw e;
		}
	}
	
	private List<Pair<EStructuralFeature, List<SourceObjectProvider>>> getToBeChildrenObject(SourceObjectProvider s) {
		SourceObjectProvider so = s.getOriginalProvider();
		if(so.isEmpty()) {
			return Collections.emptyList();
		} else {
			if(so instanceof SourceObject) {
				List<Pair<EStructuralFeature, List<SourceObjectProvider>>> result = new ArrayList<>();
				SourcePool sourcePool = this.context.getEnvironment().getSourcePool();
				
				((SourceObject) so).cacheMap().forEach((f,v)->{
					EStructuralFeature feature = f.getViewFeature();
					List<SourceObjectProvider> vals = new ArrayList<>();
					if(feature.isMany()) {
						Collection<?> objs = (Collection<?>) v.getValue();
						if(objs!=null) {
							objs.forEach(obj->{
								vals.add(sourcePool.getSourceObject(obj).getFinalProvider());
							});
						}
					} else {
						Object obj = v.getValue();
						if(obj!=null) {
							vals.add(sourcePool.getSourceObject(obj).getFinalProvider());
						}
					}
					result.add(Pair.pair(feature, vals));
				});
				return result;
			} else {
				return Collections.emptyList();
			}
		}
	}
	
	
	public void reloadDescendants(SourceObjectProvider sourceRoot, EObject viewRoot, BiFunction<EStructuralFeature, SourceObjectProvider, Boolean> filter, BiFunction<EStructuralFeature, SourceObjectProvider, Object> reloader) {
		HashSet<SourceObjectProvider> reloadedObjects = new HashSet<>();
		reloadDescendantsFromSource(sourceRoot,viewRoot,reloadedObjects,filter,reloader);
		viewRoot.eAllContents().forEachRemaining(childView->{
			EStructuralFeature containerFeature = childView.eContainmentFeature();
			SourceObjectProvider sourceChild;
			try {
				sourceChild = getTobeObject(childView);
				if(filter.apply(containerFeature, sourceChild) && !reloadedObjects.contains(sourceChild)) {
					reloadedObjects.add(sourceChild);
					reload(sourceChild, reloader.apply(containerFeature, sourceChild));
				}
			} catch (Exception e) {
			}
			
		});
	}
	
	private void reloadDescendantsFromSource(SourceObjectProvider sourceRoot, EObject viewRoot, HashSet<SourceObjectProvider> reloadedObjects, BiFunction<EStructuralFeature, SourceObjectProvider, Boolean> filter, BiFunction<EStructuralFeature, SourceObjectProvider, Object> reloader) {
//		assert sourceRoot==null || sourceRoot.getViewElement()==viewRoot;
//		HashSet<SourceObjectProvider> reloadedObjects = new HashSet<>();
		
		this.getToBeChildrenObject(sourceRoot).forEach(pair->{
			pair.second.forEach(sourceChild->{
				if(filter.apply(pair.first, sourceChild) && !reloadedObjects.contains(sourceChild)) {
					reloadedObjects.add(sourceChild);
					// may contain objects that still exist or are deleted
					try {
						reload(sourceChild, reloader.apply(pair.first, sourceChild));
						reloadDescendantsFromSource(sourceChild, sourceChild.getViewElement(), reloadedObjects, filter, reloader);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}				
			});
		});
	}
	
	protected Edit<?> mergeEdit(final Edit<?> edit) {return edit;}
}
