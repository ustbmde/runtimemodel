package edu.ustb.sei.mde.mobile2.core.edits;

import edu.ustb.sei.mde.mobile2.core.providers.FutureSourceObjectProvider;


public interface CreateObjectEdit extends Edit<Object>, FutureSourceObjectProvider {
//	void setOriginalProvider(SourceObject o);
}
