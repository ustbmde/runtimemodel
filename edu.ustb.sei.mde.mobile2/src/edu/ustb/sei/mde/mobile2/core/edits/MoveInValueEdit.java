package edu.ustb.sei.mde.mobile2.core.edits;

public interface MoveInValueEdit<T> extends Edit<T> {
	@Override
	default Edit<T> getArrayEdit() {
		return this;
	}
}
