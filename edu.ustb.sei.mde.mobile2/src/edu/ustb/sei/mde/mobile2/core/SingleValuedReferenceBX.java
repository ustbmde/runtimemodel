package edu.ustb.sei.mde.mobile2.core;

import org.eclipse.emf.ecore.EObject;

public interface SingleValuedReferenceBX extends ReferenceBX<Object, EObject> {

}
