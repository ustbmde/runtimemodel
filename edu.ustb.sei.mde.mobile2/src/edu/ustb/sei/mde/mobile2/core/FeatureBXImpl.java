package edu.ustb.sei.mde.mobile2.core;

import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import edu.ustb.sei.mde.mobile2.core.edits.ArrayEdit;
import edu.ustb.sei.mde.mobile2.core.edits.CollectiveEdit;
import edu.ustb.sei.mde.mobile2.core.edits.Edit;
import edu.ustb.sei.mde.mobile2.core.edits.Edit.CompoundEditKind;
import edu.ustb.sei.mde.mobile2.core.edits.EditContext;
import edu.ustb.sei.mde.mobile2.core.edits.MergeEdit;
import edu.ustb.sei.mde.mobile2.core.edits.PresudoEditFactory;
import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;

public abstract class FeatureBXImpl<SF,SFE,VF,VFE> implements FeatureBX<SF,SFE,VF,VFE>, PresudoEditFactory {
	final private FeatureKind kind;
	private ElementBX container;
	
	@Override
	public FeatureKind getKind() {
		return kind;
	}
	
	public FeatureBXImpl(EStructuralFeature viewFeature, FeatureKind kind) {
		super();
		this.viewFeature = viewFeature;
		this.kind = kind;
	}

	private EStructuralFeature viewFeature;

	@Override
	public EStructuralFeature getViewFeature() {
		return viewFeature;
	}
	
	@Override
	public String getName() {
		return viewFeature.getName();
	}
	

	public ElementBX getContainer() {
		return container;
	}

	public void setContainer(ElementBX container) {
		this.container = container;
	}
	
	@Override
	public <T> Edit<List<T>> requestArrayEdit(SourceProvider<Object> sourceObject, EObject viewElement,
			EStructuralFeature feature, List<Edit<T>> edits, boolean ordered) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, CompoundEditKind.array)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement)
				.bind(EditContext.FEATURE, feature);
		return ArrayEdit.array(context, edits, ordered);
	}
	
	@Override
	public <T> Edit<List<T>> requestCollectiveEdit(SourceProvider<Object> sourceObject, EObject viewElement,
			EStructuralFeature feature, SourceProvider<List<T>> oldSource, List<EObject> viewValue, List<Edit<T>> edits, boolean ordered) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, CompoundEditKind.collector)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement)
				.bind(EditContext.FEATURE, feature)
				.bind(EditContext.SOURCE_VALUE, oldSource) // for simplicity
				.bind(EditContext.OLD_SOURCE_VALUE, oldSource)
				.bind(EditContext.VIEW_VALUE, viewValue);
		return CollectiveEdit.collect(context, edits, ordered);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public <H, T> Edit<H> requestMergeEdit(SourceProvider<Object> sourceObject, EObject viewElement,
			EStructuralFeature feature, SourceProvider<H> host, List<Edit<T>> associated) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, CompoundEditKind.merge)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement)
				.bind(EditContext.FEATURE, feature);
		List edits = associated;
		return MergeEdit.merge(context, host, edits);
	}
	
	@Override
	public <H> Edit<H> requestMergeEdit(SourceProvider<Object> sourceObject, EObject viewElement,
			SourceProvider<H> host, List<Edit<?>> associated) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, CompoundEditKind.merge)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement);
		return MergeEdit.merge(context, host, associated);
	}
	
	public boolean actionOnReload() {
		return false;
	}
	
//	public <SF2, VF2> Edit<SF2> requestNoEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, SourceProvider<SF2> value, SourceProvider<SF2> oldValue, VF2 viewValue, Environment environment) {
//		EditContext context = EditContext.newContext()
//				.bind(EditContext.OPERATION, EditKind.none)
//				.bind(EditContext.SOURCE_OBJECT, sourceObject)
//				.bind(EditContext.VIEW_ELEMENT, viewElement)
//				.bind(EditContext.FEATURE, feature)
//				.bind(EditContext.SOURCE_VALUE, value)
//				.bind(EditContext.OLD_SOURCE_VALUE, oldValue)
//				.bind(EditContext.VIEW_VALUE, viewValue);
//		return NoEdit.noEdit(context, value);
//	}
//	public <SF2, VF2> Edit<SF2> requestRetainEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, SourceProvider<SF2> value, VF2 viewValue, int position, Environment environment) {
//		EditContext context = EditContext.newContext()
//				.bind(EditContext.OPERATION, EditKind.retain)
//				.bind(EditContext.SOURCE_OBJECT, sourceObject)
//				.bind(EditContext.VIEW_ELEMENT, viewElement)
//				.bind(EditContext.FEATURE, feature)
//				.bind(EditContext.SOURCE_VALUE, value)
//				.bind(EditContext.POSITION, position)
//				.bind(EditContext.VIEW_VALUE, viewValue);
//		return NoEdit.noEdit(context, value);
//	}
//	public <SF2, VF2> Edit<SF2> requestRetainEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, SourceProvider<SF2> value, VF2 viewValue, int position, int oldPosition, Environment environment) {
//		EditContext context = EditContext.newContext()
//				.bind(EditContext.OPERATION, EditKind.retain)
//				.bind(EditContext.SOURCE_OBJECT, sourceObject)
//				.bind(EditContext.VIEW_ELEMENT, viewElement)
//				.bind(EditContext.FEATURE, feature)
//				.bind(EditContext.SOURCE_VALUE, value)
//				.bind(EditContext.POSITION, position)
//				.bind(EditContext.OLD_POSITION, oldPosition)
//				.bind(EditContext.VIEW_VALUE, viewValue);
//		return NoEdit.noEdit(context, value);
//	}
//	
//	abstract public <SF2, VF2> Edit<SF2> requestSetEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, SourceProvider<SF2> value, SourceProvider<SF2> oldValue, VF2 viewValue, Environment environment);
//	abstract public <SF2, VF2> Edit<SF2> requestInsertEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, SourceProvider<SF2> value, VF2 viewValue, int position, Environment environment);
//	abstract public <SF2, VF2> Edit<SF2> requestDeleteEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, SourceProvider<SF2> value, int position, Environment environment);
//	
//	public <SF2, VF2> Edit<SF2> requestReplaceEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, SourceProvider<SF2> value, SourceProvider<SF2> oldValue, VF2 viewValue, int position, Environment environment) {
//		Edit<SF2> del = requestDeleteEdit(sourceObject, viewElement, feature, oldValue, position, environment);
//		Edit<SF2> ins = requestInsertEdit(sourceObject, viewElement, feature, value, viewValue, position, environment);
//		return requestMergeEdit(sourceObject, viewElement, feature, ins, Collections.singletonList(del));
//	}
//	public <SF2, VF2> Edit<SF2> requestReplaceEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, SourceProvider<SF2> value, SourceProvider<SF2> oldValue, VF2 viewValue, int position, int oldPosition, Environment environment) {
//		Edit<SF2> del = requestDeleteEdit(sourceObject, viewElement, feature, oldValue, oldPosition, environment);
//		Edit<SF2> ins = requestInsertEdit(sourceObject, viewElement, feature, value, viewValue, position, environment);
//		return requestMergeEdit(sourceObject, viewElement, feature, ins, Collections.singletonList(del));
//	}
//	
//	public <SF2, VF2> Edit<SF2> requestMoveInEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, SourceProvider<Object> fromObject, SourceProvider<SF2> value, VF2 viewValue, int position, Environment environment) {
//		return requestInsertEdit(sourceObject, viewElement, feature, value, viewValue, position, environment);
//	}
//	public <SF2, VF2> Edit<SF2> requestMoveOutEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, EObject toElement, SourceProvider<SF2> value, VF2 viewValue, int position, Environment environment) {
//		return requestDeleteEdit(sourceObject, viewElement, feature, value, position, environment);
//	}
	
}
