package edu.ustb.sei.mde.mobile2.core;

import edu.ustb.sei.mde.mobile2.system.Environment;

public interface BijectiveValueBX<S, V> extends ValueBX<S, V> {
	@Override
	default S put(S source, V view, Environment env) {
		return put(view, env);
	}
	
	S put(V view, Environment env);
}
