package edu.ustb.sei.mde.mobile2.core;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import edu.ustb.sei.mde.mobile2.core.edits.Edit;
import edu.ustb.sei.mde.mobile2.core.edits.EditContext;
import edu.ustb.sei.mde.mobile2.core.edits.NoEdit;
import edu.ustb.sei.mde.mobile2.core.edits.Edit.EditKind;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObjectProvider;
import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;
import edu.ustb.sei.mde.mobile2.system.Environment;
import edu.ustb.sei.mde.mobile2.util.Mobile2Utils;
import edu.ustb.sei.mde.mobile2.util.Pair;

public abstract class MultiValuedAttributeBXImpl<SFE, VFE> extends AttributeBXImpl<List<SFE>, SFE, List<VFE>, VFE>
		implements MultiValuedAttributeBX<SFE, VFE> {

	public MultiValuedAttributeBXImpl(EStructuralFeature viewFeature, FeatureKind kind) {
		super(viewFeature, kind);
	}

	@Override
	public Pair<EObject,List<VFE>> get(Pair<SourceObjectProvider, SourceProvider<List<SFE>>> source, Environment env) {
		ValueBX<SFE,VFE> valueBX = getValueBX();
		List<VFE> list = new ArrayList<VFE>(); // FIXME: may I use ArrayList here?
		for(SFE s : source.second.getValue()) {
			list.add(valueBX.get(s, env));
		}
		return Pair.pair(null, list);
	}

	@Override
	public Pair<SourceObjectProvider, SourceProvider<List<SFE>>> put(Pair<SourceObjectProvider, SourceProvider<List<SFE>>> source, Pair<EObject,List<VFE>> view, Environment env) {
		ValueBX<SFE,VFE> valueBX = getValueBX();
		boolean unchanged = true;
		List<SFE> s = source.second.getValue(); // the value must be ready because attributes will only be converted once. 
		List<SFE> sp = new ArrayList<>();
		final int commonSize = Math.min(s.size(), view.second.size());
		if(s.size()!=view.second.size()) unchanged = false;		
		
		for(int i=0;i<commonSize;i++) {
			SFE sv = s.get(i);
			VFE vv = view.second.get(i);
			
			SFE svp = valueBX.put(sv, vv, env);
			
			if(unchanged && !Mobile2Utils.equals(svp, sv)) 
				unchanged = false;
			
			sp.add(svp);
		}
		
		for(int i=commonSize;i<view.second.size();i++) {
			VFE vv = view.second.get(i);
			SFE svp = valueBX.put(null, vv, env);
			sp.add(svp);
		}
		
		boolean newVersion = source.first.isReallyNew();
		
		if(unchanged && !newVersion) return source; // or NoEdit
		else {
			if(this.getKind().isConstructor()) { // in such a case, the value can be computed directly and there is no need to generate a set edit
				return Pair.pair(source.first, SourceProvider.fixedValue(sp));
			} else {
				if(coarseGrainedUpdate()) {// in such a case, set, rather than insert/delete/replace will be called
					return Pair.pair(source.first.getFinalProvider(), 
							requestSetEdit(source.first.getFinalProvider(), view.first, getViewFeature(), 
									SourceProvider.fixedValue(sp), SourceProvider.fixedValue(s), view.second, env));
				} else {
					List<Edit<SFE>> children = new ArrayList<>();
					
					if(newVersion) {
						// for the case of replacement, we may leave it to user for simplicity
						for(int i=0;i<sp.size();i++) {
							SFE svp = sp.get(i);
							Edit<SFE> e = requestInsertEdit(source.first.getFinalProvider(), view.first, getViewFeature(), 
									SourceProvider.fixedValue(svp), view.second.get(i), i, env);
							children.add(e);
						}
					} else {
						for(int i=0;i<commonSize;i++) {
							SFE sv = s.get(i);
							SFE svp = sp.get(i);
							
							if(Mobile2Utils.equals(svp, sv)) {
								Edit<SFE> e = requestRetainEdit(source.first.getFinalProvider(), view.first, getViewFeature(), 
										SourceProvider.fixedValue(sv), view.second.get(i), i, env);
								children.add(e);
							} else {
								Edit<SFE> e = requestModifyEdit(source.first.getFinalProvider(), view.first, getViewFeature(), 
										SourceProvider.fixedValue(svp), SourceProvider.fixedValue(sv), view.second.get(i), i, env);
								children.add(e); // replace
							}
						}
						
						if(commonSize <= sp.size()) { // keep, replace, and insert
							for(int i=commonSize;i<sp.size();i++) {
								SFE svp = sp.get(i);
								Edit<SFE> e = requestInsertEdit(source.first.getFinalProvider(), view.first, getViewFeature(), 
										SourceProvider.fixedValue(svp), view.second.get(i), i, env);
								children.add(e); // insert
							}
						} else {
							for(int i=commonSize;i<s.size();i++) {
								SFE sv = s.get(i);
								Edit<SFE> e = requestDeleteEdit(source.first.getFinalProvider(), view.first, getViewFeature(), 
										SourceProvider.fixedValue(sv), i, env);
								children.add(e); // delete
							}
						}
					}
					
					
					return Pair.pair(source.first.getFinalProvider(), 
							requestArrayEdit(source.first.getFinalProvider(), view.first, getViewFeature(), children, isOrdered()));
				}
			}
		}
	}
	
	public boolean isOrdered() {
		return this.getViewFeature().isOrdered();
	}

	public Edit<SFE> requestRetainEdit(SourceObjectProvider sourceObject, EObject viewElement,
			EStructuralFeature feature, SourceProvider<SFE> value, VFE viewValue, int position, Environment env) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, EditKind.retain)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement)
				.bind(EditContext.FEATURE, feature)
				.bind(EditContext.SOURCE_VALUE, value)
				.bind(EditContext.POSITION, position)
				.bind(EditContext.OLD_POSITION, position)
				.bind(EditContext.VIEW_VALUE, viewValue)
				.bind(EditContext.ENVIRONMENT, env);
		return NoEdit.noEdit(context, value);
	}
	
	public Edit<SFE> requestModifyEdit(SourceObjectProvider sourceObject, EObject viewElement,
			EStructuralFeature feature, SourceProvider<SFE> value, SourceProvider<SFE> oldValue, VFE viewValue, int position, Environment env) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, EditKind.modify)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement)
				.bind(EditContext.FEATURE, feature)
				.bind(EditContext.SOURCE_VALUE, value)
				.bind(EditContext.OLD_SOURCE_VALUE, oldValue)
				.bind(EditContext.POSITION, position)
				.bind(EditContext.OLD_POSITION, position)
				.bind(EditContext.VIEW_VALUE, viewValue)
				.bind(EditContext.ENVIRONMENT, env);
		return createModifyEdit(context);
	}

	protected Edit<SFE> createModifyEdit(EditContext context) {
		throw new UnsupportedOperationException("createReplaceEdit is not implmeneted");
	}

	public Edit<List<SFE>> requestSetEdit(SourceProvider<Object> sourceObject, EObject viewElement,
			EStructuralFeature feature, SourceProvider<List<SFE>> value, SourceProvider<List<SFE>> oldValue, List<VFE> viewValue,
			Environment environment) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, EditKind.set)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement)
				.bind(EditContext.FEATURE, feature)
				.bind(EditContext.SOURCE_VALUE, value)
				.bind(EditContext.OLD_SOURCE_VALUE, oldValue)
				.bind(EditContext.VIEW_VALUE, viewValue)
				.bind(EditContext.ENVIRONMENT, environment);
		return createSetEdit(context);
	}
	
	protected Edit<List<SFE>> createSetEdit(EditContext context) {
		throw new UnsupportedOperationException("createSetEdit is not implmeneted");
	}

	public Edit<SFE> requestInsertEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, SourceProvider<SFE> value, VFE viewValue, int position, Environment environment) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, EditKind.insert)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement)
				.bind(EditContext.FEATURE, feature)
				.bind(EditContext.SOURCE_VALUE, value)
				.bind(EditContext.POSITION, position)
				.bind(EditContext.OLD_POSITION, position)
				.bind(EditContext.VIEW_VALUE, viewValue)
				.bind(EditContext.ENVIRONMENT, environment);
		return createInsertEdit(context);
	}
	protected Edit<SFE> createInsertEdit(EditContext context) {
		throw new UnsupportedOperationException("createInsertEdit is not implmeneted");
	}

	public Edit<SFE> requestDeleteEdit(SourceProvider<Object> sourceObject, EObject viewElement, EStructuralFeature feature, SourceProvider<SFE> value, int position, Environment environment) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, EditKind.remove)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement)
				.bind(EditContext.FEATURE, feature)
				.bind(EditContext.SOURCE_VALUE, value)
				.bind(EditContext.POSITION, position)
				.bind(EditContext.ENVIRONMENT, environment);
		return createDeleteEdit(context);
	}

	protected Edit<SFE> createDeleteEdit(EditContext context) {
		throw new UnsupportedOperationException("createDeleteEdit is not implmeneted");
	}

}
