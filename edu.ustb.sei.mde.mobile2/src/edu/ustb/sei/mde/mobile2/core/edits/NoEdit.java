package edu.ustb.sei.mde.mobile2.core.edits;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;

/**
 * NoEdit represents a non-op edit, i.e., an edit that did nothing.
 * @author hexiao
 *
 * @param <T>
 */
public interface NoEdit<T> extends Edit<T> {
	
	static public <T> NoEdit<T> noEdit(EditContext context, SourceProvider<T> v) {
		return new NoEditImpl<T>(context, v);
	}

	class NoEditImpl<T> extends EditImpl<T> implements NoEdit<T> {
		public NoEditImpl(EditContext context, SourceProvider<T> v) {
			super(context);
			this.calculatedValue = v;
		}

		@Override
		public void run() {
			// do nothing
		}

		@Override
		public Set<Edit<?>> collectEdits() {
			SourceProvider<?> value = this.calculatedValue;
			
			if(value instanceof Edit) {
				return ((Edit<?>) value).collectEdits();
			} else {
				if(value instanceof Collection) {
					Set<Edit<?>> edits = new HashSet<>();
					((Collection<?>) value).forEach(e->{
						if(e instanceof Edit) {
							edits.addAll(((Edit<?>) e).collectEdits());
						}
					});
					return edits;
				} else 
					return Collections.emptySet();
			}
			
		}
	}
}
