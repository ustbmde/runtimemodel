package edu.ustb.sei.mde.mobile2.core.edits.impl;

import edu.ustb.sei.mde.mobile2.core.edits.EditContext;
import edu.ustb.sei.mde.mobile2.core.edits.EditImpl;
import edu.ustb.sei.mde.mobile2.core.edits.MoveOutValueEdit;

public abstract class MoveOutValueEditImpl<T> extends EditImpl<T> implements MoveOutValueEdit<T> {

	public MoveOutValueEditImpl(EditContext context) {
		super(context);
		this.calculatedValue = null;
	}
	
	@Override
	public T getValue() {
		throw new UnsupportedOperationException();
	}
}
