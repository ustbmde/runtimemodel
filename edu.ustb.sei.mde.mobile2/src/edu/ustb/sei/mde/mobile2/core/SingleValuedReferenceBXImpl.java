package edu.ustb.sei.mde.mobile2.core;

import java.util.Collections;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.xtext.xbase.typesystem.util.ActualTypeArgumentCollector;

import edu.ustb.sei.mde.mobile2.core.edits.Edit;
import edu.ustb.sei.mde.mobile2.core.edits.EditContext;
import edu.ustb.sei.mde.mobile2.core.edits.Edit.EditKind;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObject;
import edu.ustb.sei.mde.mobile2.core.providers.SourceObjectProvider;
import edu.ustb.sei.mde.mobile2.core.providers.SourceProvider;
import edu.ustb.sei.mde.mobile2.system.Environment;
import edu.ustb.sei.mde.mobile2.util.Mobile2Utils;
import edu.ustb.sei.mde.mobile2.util.Pair;

public abstract class SingleValuedReferenceBXImpl extends ReferenceBXImpl<Object, EObject> implements SingleValuedReferenceBX {

	public SingleValuedReferenceBXImpl(EStructuralFeature viewFeature, FeatureKind kind) {
		super(viewFeature, kind);
	}

	@Override
	public Pair<EObject, EObject> get(Pair<SourceObjectProvider, SourceProvider<Object>> source, Environment env) {
		Environment newEnv = env.newScope(this.isContainment());
		return Pair.pair(null, getValueBX().get(source.second, newEnv));
	}

	/**
	 * 	<p><b>Input</b>
	 *  source.first may be a SourceObject or a SourceObjectEdit/CreateObjectEdit because we may synchronize new objects with view elements.
	 *  source.second must be ready. If source.first is new, then source.second must be the default value (nullObject).
	 *  view.second may be null.</p>
	 *  </ol>
	 *  </p>
	 */
	@Override
	public Pair<SourceObjectProvider, SourceProvider<Object>> put(
			Pair<SourceObjectProvider, SourceProvider<Object>> source, Pair<EObject, EObject> view, Environment env) {
		
		if(view.second==null && source.second.isEmpty()) return source;
		
		Environment newEnv = env.newScope(this.isContainment());
		newEnv.setSourceParent(source.first.getFinalProvider());
		
		if(((EReference)this.getViewFeature()).isContainment()) {
			return putContainment(source,view,newEnv);
		} else {
			return putNonContainment(source,view,newEnv);
		}
	}

	private Pair<SourceObjectProvider, SourceProvider<Object>> putNonContainment(
			Pair<SourceObjectProvider, SourceProvider<Object>> source, Pair<EObject, EObject> view,
			Environment newEnv) {
		assert isContainment() == false;
		boolean parentUpdated = source.first.isReallyNew();
		if(view.second!=null) {
			// First, find the trace associated with view.second
			SourceObjectProvider sp = newEnv.resolveFutureSource(view.second);
			
			if(sp!=null) {
				if(getKind().isConstructor()) {
					return Pair.pair(source.first.getFinalProvider(), sp);
				} else if(Mobile2Utils.notChanged(sp, source.second, actionOnReload()) && !parentUpdated) {
					return Pair.pair(source.first.getFinalProvider(), sp);
				} else {
					return Pair.pair(source.first.getFinalProvider(), requestSetEdit(source.first.getFinalProvider(), view.first, getViewFeature(), 
							sp, source.second, view.second, newEnv));
				}
			} else {
				throw new RuntimeException("A view element does not have a trace. That is impossible because trace must be initialized before transformation.");
			}
		} else {
			if(source.second!=SourceProvider.nullObject())
				return Pair.pair(source.first.getFinalProvider(), requestSetEdit(source.first.getFinalProvider(), view.first, getViewFeature(), 
					SourceProvider.nullObject(), source.second, view.second, newEnv));
			else return source;
		}
	}

	/**
	 * source.first may be CreateObjectEdit.
	 * source.second may be NullObject but cannot be CreateObjectEdit.
	 * view.second may also be null.
	 * if view.second==null and source.second.isEmpty, then return directly. 
	 *   
	 * @param source
	 * @param view
	 * @param newEnv
	 * @return
	 */
	@SuppressWarnings("unused")
	private Pair<SourceObjectProvider, SourceProvider<Object>> putContainment(
			Pair<SourceObjectProvider, SourceProvider<Object>> source, Pair<EObject, EObject> view,
			Environment newEnv) {
		// for the case that a constructor feature is checked, this must be false because the object has not been updated
		boolean parentUpdated = source.first.isReallyNew();
		
		if(view.second!=null) {
			ElementBX valueBX = getValueBX();
			
			if(source.second.isEmpty()) {
				// in such a case, original source value is absent because it cannot be a newly created object.
				//   find ss <- -> view.second, in such a case, view second cannot be null, because an early check ensures this property
				assert view.second!=null;
				SourceObjectProvider ss = newEnv.resolveSource(view.second);
				SourceProvider<Object> ssp = null;
				if(ss == null) {
					// new view
					ss = newEnv.resolveFutureSource(view.second);//SourceProvider.newObject(view.second);
				}
				ssp = valueBX.put(ss, view.second, newEnv);
				
				if(this.getKind().isConstructor() || noActualChange(parentUpdated, source.second,ssp)) {
					return Pair.pair(source.first.getFinalProvider(), ssp);
				} else {
					return Pair.pair(source.first.getFinalProvider(), requestSetEdit(source.first.getFinalProvider(), view.first, getViewFeature(), 
							ssp, SourceProvider.nullObject(), view.second, newEnv));
				}
			} else {
				// original source value is present
				// First, find the trace associated with view.second
				SourceObjectProvider ss = newEnv.resolveSource(view.second);
				if(ss==null) 
					ss = newEnv.resolveFutureSource(view.second);//SourceProvider.newObject(view.second);
				SourceProvider<Object> ssp = valueBX.put(ss.getCore(), view.second, newEnv);
				
				if(ss != null) {
					// Check if source.second is aligned with view.second by comparing the source key with trace
					// use of equals is right because we just want to compare the original values 
					if(Mobile2Utils.equals(ss, source.second)) { // aligned, there is no need to check moveIn/moveOut
						// Update source.second and get sp			
						// source.second == ss
						// Check if sp is equal to source.second (note that sp may be associated with other feature edits
						if(Mobile2Utils.notChanged(ssp, source.second, actionOnReload()) && !parentUpdated) {
							// If so return sp (and its feature edits)
							return Pair.pair(source.first.getFinalProvider(), ssp);
						} else { // a single set is fine because both the old value and new value are provided
							// set sp
							if(this.getKind().isConstructor()) {
								assert parentUpdated==false;
								return Pair.pair(source.first.getFinalProvider(), ssp); // return sp is fine here, no need to set
							} else {
								// for the case of parentUpdated, we may also leave it to users and return source
								return Pair.pair(source.first.getFinalProvider(), requestSetEdit(source.first.getFinalProvider(), view.first, getViewFeature(), 
										ssp, source.second, view.second, newEnv));
							}
						}
					} else {
						// Not aligned, find ss <- trace						
						// check if source.second is moved to another place
						Edit<Object> moveOut = null;
						
						EObject viewValueS = ((SourceObjectProvider)source.second.getCore()).getViewElement();
						if (viewValueS != null) {
							EObject viewParentS = viewValueS.eContainer();
							if (Mobile2Utils.belongToView(viewParentS, newEnv)) {
								moveOut = requestMoveOutEdit(source.first.getFinalProvider(), view.first, getViewFeature(), viewParentS, source.second, viewValueS, -1, newEnv);
							}
						} else {
							// FIXME: cascade moveOut in containment
						}
						
						// set ssp
						if(this.getKind().isConstructor()) { // no set op is generated
							if(moveOut==null) return Pair.pair(source.first, ssp);
							else {
								return Pair.pair(source.first.getFinalProvider(), 
										requestMergeEdit(source.first.getFinalProvider(), view.first, getViewFeature(), ssp, Collections.singletonList(moveOut)));
							}
						} else { 
							// a set op is required
							Edit<Object> setSsp = null;
							if(moveOut==null) {
								// source.second != nulll => we hope the removal of the old value to be handled
								setSsp = requestSetEdit(source.first.getFinalProvider(), view.first, getViewFeature(), ssp, source.second, view.second, newEnv);
								return Pair.pair(source.first, setSsp);
							} else {
								// old value is null => we do not hope developers handle the removal of the old value
								setSsp = requestSetEdit(source.first.getFinalProvider(), view.first, getViewFeature(), ssp, null, view.second, newEnv);
								return Pair.pair(source.first.getFinalProvider(), 
										requestMergeEdit(source.first.getFinalProvider(), view.first, getViewFeature(), setSsp, Collections.singletonList(moveOut)));
							}
						}
					}
				} else {
					throw new RuntimeException("A view element does not have a trace. That is impossible because trace must be initialized before transformation.");
				}
			}
		} else {
			// view.second == null && source.second.isEmpty == false
			Edit<Object> moveOut = null;
			EObject viewValueS = ((SourceObjectProvider)source.second.getCore()).getViewElement();
			if (viewValueS != null) {
				EObject viewParentS = viewValueS.eContainer();
				if (Mobile2Utils.belongToView(viewParentS, newEnv)) {
					moveOut = requestMoveOutEdit(source.first.getFinalProvider(), view.first, getViewFeature(), viewParentS, source.second, viewValueS, -1, newEnv);
				}
			}
			if(moveOut!=null) {
				if(getKind().isConstructor()) {
					return Pair.pair(source.first, requestMergeEdit(source.first, view.first, getViewFeature(), SourceProvider.nullObject(), Collections.singletonList(moveOut)));
				} else {
					Edit<Object> set = requestSetEdit(source.first.getFinalProvider(), view.first, getViewFeature(), 
							SourceProvider.nullObject(), source.second, view.second, newEnv);
					return Pair.pair(source.first.getFinalProvider(), requestMergeEdit(source.first.getFinalProvider(), view.first, getViewFeature(), set, Collections.singletonList(moveOut)));
				}
			} else {
				if(getKind().isConstructor())
					return Pair.pair(source.first.getFinalProvider(), SourceProvider.nullObject());
				else return Pair.pair(source.first.getFinalProvider(), requestSetEdit(source.first.getFinalProvider(), view.first, getViewFeature(), 
						SourceProvider.nullObject(), source.second, view.second, newEnv));
			}
		}
	}
	
	protected boolean noActualChange(boolean parentUpdated, SourceProvider<Object> second, SourceProvider<Object> ssp) {
		if(parentUpdated) return false;
		if(ssp.getCore() != second) return false;
		if(!(ssp.getCore() instanceof SourceObject)) return false;
		SourceObject core = (SourceObject) ssp.getCore();
		if(core.isNewOrReloaded()) return false;
		return true;
	}

	public Edit<Object> requestSetEdit(SourceProvider<Object> sourceObject, EObject viewElement,
			EStructuralFeature feature, SourceProvider<Object> value, SourceProvider<Object> oldValue, EObject viewValue,
			Environment environment) {
		EditContext context = EditContext.newContext()
				.bind(EditContext.OPERATION, EditKind.set)
				.bind(EditContext.SOURCE_OBJECT, sourceObject)
				.bind(EditContext.VIEW_ELEMENT, viewElement)
				.bind(EditContext.FEATURE, feature)
				.bind(EditContext.SOURCE_VALUE, value)
				.bind(EditContext.OLD_SOURCE_VALUE, oldValue)
				.bind(EditContext.VIEW_VALUE, viewValue)
				.bind(EditContext.ENVIRONMENT, environment);
		return createSetEdit(context);
	}
	
	protected Edit<Object> createSetEdit(EditContext context) {
		throw new UnsupportedOperationException("createSetEdit is not implmeneted");
	}

	
	public Edit<Object> requestMoveOutEdit(SourceProvider<Object> sourceObject, EObject viewElement,
			EStructuralFeature feature, EObject toElement, SourceProvider<Object> value, EObject viewValue, int position,
			Environment environment) {
		return null; // current algorithm allows us to return null; otherwise, we must return NoEdit
	}
	
	protected Edit<Object> createMoveOutEdit(EditContext context) {
		return null;
	}
}
