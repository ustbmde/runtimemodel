package edu.ustb.sei.modeladapter.value

import org.eclipse.xtext.conversion.IValueConverter
import org.eclipse.xtext.conversion.ValueConverterException
import org.eclipse.xtext.nodemodel.INode

class CodeValueConverter implements IValueConverter<String> {
	
	override toString(String value) throws ValueConverterException {
		"⟦"+value+"⟧"
	}
	
	override toValue(String string, INode node) throws ValueConverterException {
		if(string===null) null
		else string.substring(1, string.length-1);
	}
	
}