package edu.ustb.sei.modeladapter.value


import org.eclipse.xtext.conversion.IValueConverter
import org.eclipse.xtext.conversion.ValueConverter
import org.eclipse.xtext.common.services.DefaultTerminalConverters

class ValueConverterService extends  DefaultTerminalConverters {
	
	@ValueConverter(rule = "CODE")
	def IValueConverter<String> getCodeValueConverter () {
		return new CodeValueConverter();
	}
	
}