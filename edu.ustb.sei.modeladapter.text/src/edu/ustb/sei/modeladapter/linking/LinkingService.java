package edu.ustb.sei.modeladapter.linking;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.linking.impl.DefaultLinkingService;
import org.eclipse.xtext.linking.impl.IllegalNodeException;
import org.eclipse.xtext.nodemodel.INode;

import edu.ustb.sei.mde.runtimemodel.generator.GeneratorPackage;

public class LinkingService extends DefaultLinkingService {

	public LinkingService() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<EObject> getLinkedObjects(EObject context, EReference ref, INode node) throws IllegalNodeException {
		if(ref==GeneratorPackage.Literals.GENERATOR_MODEL__ADAPTED_PACKAGE) {
			try {
				String text = node.getText();
				URI uri = URI.createURI(text);
				String fragment = uri.fragment();
				Resource res = context.eResource().getResourceSet().getResource(uri, true);
				if(fragment==null)
					return new ArrayList<EObject>(res.getContents());
				else
					return Collections.singletonList(res.getEObject(fragment));
			} catch (Exception e) {
				throw new IllegalNodeException(node);
			}
			
		} else
			return super.getLinkedObjects(context, ref, node);
	}
}
