package edu.ustb.sei.modeladapter.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import edu.ustb.sei.modeladapter.services.RuntimeModelCodeGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalRuntimeModelCodeParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_URI", "RULE_CODE", "RULE_LONG", "RULE_NAME", "RULE_INT", "RULE_ID", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'package'", "';'", "'import'", "'options'", "'{'", "'directory'", "':'", "'load'", "'refreshing'", "'timer'", "'}'", "'adapter'", "'id'", "'get'", "'post'", "'put'", "'delete'", "'actual'", "'from'", "'to'"
    };
    public static final int RULE_NAME=7;
    public static final int RULE_STRING=10;
    public static final int RULE_SL_COMMENT=12;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_ID=9;
    public static final int RULE_WS=13;
    public static final int RULE_ANY_OTHER=14;
    public static final int RULE_CODE=5;
    public static final int RULE_URI=4;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=8;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=11;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int RULE_LONG=6;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalRuntimeModelCodeParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalRuntimeModelCodeParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalRuntimeModelCodeParser.tokenNames; }
    public String getGrammarFileName() { return "InternalRuntimeModelCode.g"; }



     	private RuntimeModelCodeGrammarAccess grammarAccess;

        public InternalRuntimeModelCodeParser(TokenStream input, RuntimeModelCodeGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "GeneratorModel";
       	}

       	@Override
       	protected RuntimeModelCodeGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleGeneratorModel"
    // InternalRuntimeModelCode.g:64:1: entryRuleGeneratorModel returns [EObject current=null] : iv_ruleGeneratorModel= ruleGeneratorModel EOF ;
    public final EObject entryRuleGeneratorModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGeneratorModel = null;


        try {
            // InternalRuntimeModelCode.g:64:55: (iv_ruleGeneratorModel= ruleGeneratorModel EOF )
            // InternalRuntimeModelCode.g:65:2: iv_ruleGeneratorModel= ruleGeneratorModel EOF
            {
             newCompositeNode(grammarAccess.getGeneratorModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGeneratorModel=ruleGeneratorModel();

            state._fsp--;

             current =iv_ruleGeneratorModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGeneratorModel"


    // $ANTLR start "ruleGeneratorModel"
    // InternalRuntimeModelCode.g:71:1: ruleGeneratorModel returns [EObject current=null] : ( (otherlv_0= 'package' ( (lv_basePackage_1_0= RULE_URI ) ) otherlv_2= ';' )? otherlv_3= 'import' ( (otherlv_4= RULE_URI ) ) otherlv_5= ';' otherlv_6= 'options' otherlv_7= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_9= 'directory' otherlv_10= ':' ( (lv_directory_11_0= RULE_URI ) ) otherlv_12= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'load' otherlv_14= ':' ( (lv_loadMetamodel_15_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'refreshing' otherlv_17= ':' ( (lv_refreshingRate_18_0= RULE_LONG ) ) otherlv_19= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'timer' otherlv_21= ':' ( (lv_timerClass_22_0= RULE_URI ) ) otherlv_23= ';' ) ) ) ) )* ) ) ) otherlv_24= '}' ( (lv_classAdapters_25_0= ruleClassAdapter ) )* ) ;
    public final EObject ruleGeneratorModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_basePackage_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_10=null;
        Token lv_directory_11_0=null;
        Token otherlv_12=null;
        Token otherlv_13=null;
        Token otherlv_14=null;
        Token lv_loadMetamodel_15_0=null;
        Token otherlv_16=null;
        Token otherlv_17=null;
        Token lv_refreshingRate_18_0=null;
        Token otherlv_19=null;
        Token otherlv_20=null;
        Token otherlv_21=null;
        Token lv_timerClass_22_0=null;
        Token otherlv_23=null;
        Token otherlv_24=null;
        EObject lv_classAdapters_25_0 = null;



        	enterRule();

        try {
            // InternalRuntimeModelCode.g:77:2: ( ( (otherlv_0= 'package' ( (lv_basePackage_1_0= RULE_URI ) ) otherlv_2= ';' )? otherlv_3= 'import' ( (otherlv_4= RULE_URI ) ) otherlv_5= ';' otherlv_6= 'options' otherlv_7= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_9= 'directory' otherlv_10= ':' ( (lv_directory_11_0= RULE_URI ) ) otherlv_12= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'load' otherlv_14= ':' ( (lv_loadMetamodel_15_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'refreshing' otherlv_17= ':' ( (lv_refreshingRate_18_0= RULE_LONG ) ) otherlv_19= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'timer' otherlv_21= ':' ( (lv_timerClass_22_0= RULE_URI ) ) otherlv_23= ';' ) ) ) ) )* ) ) ) otherlv_24= '}' ( (lv_classAdapters_25_0= ruleClassAdapter ) )* ) )
            // InternalRuntimeModelCode.g:78:2: ( (otherlv_0= 'package' ( (lv_basePackage_1_0= RULE_URI ) ) otherlv_2= ';' )? otherlv_3= 'import' ( (otherlv_4= RULE_URI ) ) otherlv_5= ';' otherlv_6= 'options' otherlv_7= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_9= 'directory' otherlv_10= ':' ( (lv_directory_11_0= RULE_URI ) ) otherlv_12= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'load' otherlv_14= ':' ( (lv_loadMetamodel_15_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'refreshing' otherlv_17= ':' ( (lv_refreshingRate_18_0= RULE_LONG ) ) otherlv_19= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'timer' otherlv_21= ':' ( (lv_timerClass_22_0= RULE_URI ) ) otherlv_23= ';' ) ) ) ) )* ) ) ) otherlv_24= '}' ( (lv_classAdapters_25_0= ruleClassAdapter ) )* )
            {
            // InternalRuntimeModelCode.g:78:2: ( (otherlv_0= 'package' ( (lv_basePackage_1_0= RULE_URI ) ) otherlv_2= ';' )? otherlv_3= 'import' ( (otherlv_4= RULE_URI ) ) otherlv_5= ';' otherlv_6= 'options' otherlv_7= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_9= 'directory' otherlv_10= ':' ( (lv_directory_11_0= RULE_URI ) ) otherlv_12= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'load' otherlv_14= ':' ( (lv_loadMetamodel_15_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'refreshing' otherlv_17= ':' ( (lv_refreshingRate_18_0= RULE_LONG ) ) otherlv_19= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'timer' otherlv_21= ':' ( (lv_timerClass_22_0= RULE_URI ) ) otherlv_23= ';' ) ) ) ) )* ) ) ) otherlv_24= '}' ( (lv_classAdapters_25_0= ruleClassAdapter ) )* )
            // InternalRuntimeModelCode.g:79:3: (otherlv_0= 'package' ( (lv_basePackage_1_0= RULE_URI ) ) otherlv_2= ';' )? otherlv_3= 'import' ( (otherlv_4= RULE_URI ) ) otherlv_5= ';' otherlv_6= 'options' otherlv_7= '{' ( ( ( ( ({...}? => ( ({...}? => (otherlv_9= 'directory' otherlv_10= ':' ( (lv_directory_11_0= RULE_URI ) ) otherlv_12= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'load' otherlv_14= ':' ( (lv_loadMetamodel_15_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'refreshing' otherlv_17= ':' ( (lv_refreshingRate_18_0= RULE_LONG ) ) otherlv_19= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'timer' otherlv_21= ':' ( (lv_timerClass_22_0= RULE_URI ) ) otherlv_23= ';' ) ) ) ) )* ) ) ) otherlv_24= '}' ( (lv_classAdapters_25_0= ruleClassAdapter ) )*
            {
            // InternalRuntimeModelCode.g:79:3: (otherlv_0= 'package' ( (lv_basePackage_1_0= RULE_URI ) ) otherlv_2= ';' )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==15) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalRuntimeModelCode.g:80:4: otherlv_0= 'package' ( (lv_basePackage_1_0= RULE_URI ) ) otherlv_2= ';'
                    {
                    otherlv_0=(Token)match(input,15,FOLLOW_3); 

                    				newLeafNode(otherlv_0, grammarAccess.getGeneratorModelAccess().getPackageKeyword_0_0());
                    			
                    // InternalRuntimeModelCode.g:84:4: ( (lv_basePackage_1_0= RULE_URI ) )
                    // InternalRuntimeModelCode.g:85:5: (lv_basePackage_1_0= RULE_URI )
                    {
                    // InternalRuntimeModelCode.g:85:5: (lv_basePackage_1_0= RULE_URI )
                    // InternalRuntimeModelCode.g:86:6: lv_basePackage_1_0= RULE_URI
                    {
                    lv_basePackage_1_0=(Token)match(input,RULE_URI,FOLLOW_4); 

                    						newLeafNode(lv_basePackage_1_0, grammarAccess.getGeneratorModelAccess().getBasePackageURITerminalRuleCall_0_1_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getGeneratorModelRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"basePackage",
                    							lv_basePackage_1_0,
                    							"edu.ustb.sei.modeladapter.RuntimeModelCode.URI");
                    					

                    }


                    }

                    otherlv_2=(Token)match(input,16,FOLLOW_5); 

                    				newLeafNode(otherlv_2, grammarAccess.getGeneratorModelAccess().getSemicolonKeyword_0_2());
                    			

                    }
                    break;

            }

            otherlv_3=(Token)match(input,17,FOLLOW_3); 

            			newLeafNode(otherlv_3, grammarAccess.getGeneratorModelAccess().getImportKeyword_1());
            		
            // InternalRuntimeModelCode.g:111:3: ( (otherlv_4= RULE_URI ) )
            // InternalRuntimeModelCode.g:112:4: (otherlv_4= RULE_URI )
            {
            // InternalRuntimeModelCode.g:112:4: (otherlv_4= RULE_URI )
            // InternalRuntimeModelCode.g:113:5: otherlv_4= RULE_URI
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getGeneratorModelRule());
            					}
            				
            otherlv_4=(Token)match(input,RULE_URI,FOLLOW_4); 

            					newLeafNode(otherlv_4, grammarAccess.getGeneratorModelAccess().getAdaptedPackageEPackageCrossReference_2_0());
            				

            }


            }

            otherlv_5=(Token)match(input,16,FOLLOW_6); 

            			newLeafNode(otherlv_5, grammarAccess.getGeneratorModelAccess().getSemicolonKeyword_3());
            		
            otherlv_6=(Token)match(input,18,FOLLOW_7); 

            			newLeafNode(otherlv_6, grammarAccess.getGeneratorModelAccess().getOptionsKeyword_4());
            		
            otherlv_7=(Token)match(input,19,FOLLOW_8); 

            			newLeafNode(otherlv_7, grammarAccess.getGeneratorModelAccess().getLeftCurlyBracketKeyword_5());
            		
            // InternalRuntimeModelCode.g:136:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_9= 'directory' otherlv_10= ':' ( (lv_directory_11_0= RULE_URI ) ) otherlv_12= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'load' otherlv_14= ':' ( (lv_loadMetamodel_15_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'refreshing' otherlv_17= ':' ( (lv_refreshingRate_18_0= RULE_LONG ) ) otherlv_19= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'timer' otherlv_21= ':' ( (lv_timerClass_22_0= RULE_URI ) ) otherlv_23= ';' ) ) ) ) )* ) ) )
            // InternalRuntimeModelCode.g:137:4: ( ( ( ({...}? => ( ({...}? => (otherlv_9= 'directory' otherlv_10= ':' ( (lv_directory_11_0= RULE_URI ) ) otherlv_12= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'load' otherlv_14= ':' ( (lv_loadMetamodel_15_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'refreshing' otherlv_17= ':' ( (lv_refreshingRate_18_0= RULE_LONG ) ) otherlv_19= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'timer' otherlv_21= ':' ( (lv_timerClass_22_0= RULE_URI ) ) otherlv_23= ';' ) ) ) ) )* ) )
            {
            // InternalRuntimeModelCode.g:137:4: ( ( ( ({...}? => ( ({...}? => (otherlv_9= 'directory' otherlv_10= ':' ( (lv_directory_11_0= RULE_URI ) ) otherlv_12= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'load' otherlv_14= ':' ( (lv_loadMetamodel_15_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'refreshing' otherlv_17= ':' ( (lv_refreshingRate_18_0= RULE_LONG ) ) otherlv_19= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'timer' otherlv_21= ':' ( (lv_timerClass_22_0= RULE_URI ) ) otherlv_23= ';' ) ) ) ) )* ) )
            // InternalRuntimeModelCode.g:138:5: ( ( ({...}? => ( ({...}? => (otherlv_9= 'directory' otherlv_10= ':' ( (lv_directory_11_0= RULE_URI ) ) otherlv_12= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'load' otherlv_14= ':' ( (lv_loadMetamodel_15_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'refreshing' otherlv_17= ':' ( (lv_refreshingRate_18_0= RULE_LONG ) ) otherlv_19= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'timer' otherlv_21= ':' ( (lv_timerClass_22_0= RULE_URI ) ) otherlv_23= ';' ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6());
            				
            // InternalRuntimeModelCode.g:141:5: ( ( ({...}? => ( ({...}? => (otherlv_9= 'directory' otherlv_10= ':' ( (lv_directory_11_0= RULE_URI ) ) otherlv_12= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'load' otherlv_14= ':' ( (lv_loadMetamodel_15_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'refreshing' otherlv_17= ':' ( (lv_refreshingRate_18_0= RULE_LONG ) ) otherlv_19= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'timer' otherlv_21= ':' ( (lv_timerClass_22_0= RULE_URI ) ) otherlv_23= ';' ) ) ) ) )* )
            // InternalRuntimeModelCode.g:142:6: ( ({...}? => ( ({...}? => (otherlv_9= 'directory' otherlv_10= ':' ( (lv_directory_11_0= RULE_URI ) ) otherlv_12= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'load' otherlv_14= ':' ( (lv_loadMetamodel_15_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'refreshing' otherlv_17= ':' ( (lv_refreshingRate_18_0= RULE_LONG ) ) otherlv_19= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'timer' otherlv_21= ':' ( (lv_timerClass_22_0= RULE_URI ) ) otherlv_23= ';' ) ) ) ) )*
            {
            // InternalRuntimeModelCode.g:142:6: ( ({...}? => ( ({...}? => (otherlv_9= 'directory' otherlv_10= ':' ( (lv_directory_11_0= RULE_URI ) ) otherlv_12= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_13= 'load' otherlv_14= ':' ( (lv_loadMetamodel_15_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_16= 'refreshing' otherlv_17= ':' ( (lv_refreshingRate_18_0= RULE_LONG ) ) otherlv_19= ';' ) ) ) ) | ({...}? => ( ({...}? => (otherlv_20= 'timer' otherlv_21= ':' ( (lv_timerClass_22_0= RULE_URI ) ) otherlv_23= ';' ) ) ) ) )*
            loop2:
            do {
                int alt2=5;
                int LA2_0 = input.LA(1);

                if ( LA2_0 == 20 && getUnorderedGroupHelper().canSelect(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6(), 0) ) {
                    alt2=1;
                }
                else if ( LA2_0 == 22 && getUnorderedGroupHelper().canSelect(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6(), 1) ) {
                    alt2=2;
                }
                else if ( LA2_0 == 23 && getUnorderedGroupHelper().canSelect(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6(), 2) ) {
                    alt2=3;
                }
                else if ( LA2_0 == 24 && getUnorderedGroupHelper().canSelect(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6(), 3) ) {
                    alt2=4;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalRuntimeModelCode.g:143:4: ({...}? => ( ({...}? => (otherlv_9= 'directory' otherlv_10= ':' ( (lv_directory_11_0= RULE_URI ) ) otherlv_12= ';' ) ) ) )
            	    {
            	    // InternalRuntimeModelCode.g:143:4: ({...}? => ( ({...}? => (otherlv_9= 'directory' otherlv_10= ':' ( (lv_directory_11_0= RULE_URI ) ) otherlv_12= ';' ) ) ) )
            	    // InternalRuntimeModelCode.g:144:5: {...}? => ( ({...}? => (otherlv_9= 'directory' otherlv_10= ':' ( (lv_directory_11_0= RULE_URI ) ) otherlv_12= ';' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleGeneratorModel", "getUnorderedGroupHelper().canSelect(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6(), 0)");
            	    }
            	    // InternalRuntimeModelCode.g:144:111: ( ({...}? => (otherlv_9= 'directory' otherlv_10= ':' ( (lv_directory_11_0= RULE_URI ) ) otherlv_12= ';' ) ) )
            	    // InternalRuntimeModelCode.g:145:6: ({...}? => (otherlv_9= 'directory' otherlv_10= ':' ( (lv_directory_11_0= RULE_URI ) ) otherlv_12= ';' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6(), 0);
            	    					
            	    // InternalRuntimeModelCode.g:148:9: ({...}? => (otherlv_9= 'directory' otherlv_10= ':' ( (lv_directory_11_0= RULE_URI ) ) otherlv_12= ';' ) )
            	    // InternalRuntimeModelCode.g:148:10: {...}? => (otherlv_9= 'directory' otherlv_10= ':' ( (lv_directory_11_0= RULE_URI ) ) otherlv_12= ';' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleGeneratorModel", "true");
            	    }
            	    // InternalRuntimeModelCode.g:148:19: (otherlv_9= 'directory' otherlv_10= ':' ( (lv_directory_11_0= RULE_URI ) ) otherlv_12= ';' )
            	    // InternalRuntimeModelCode.g:148:20: otherlv_9= 'directory' otherlv_10= ':' ( (lv_directory_11_0= RULE_URI ) ) otherlv_12= ';'
            	    {
            	    otherlv_9=(Token)match(input,20,FOLLOW_9); 

            	    									newLeafNode(otherlv_9, grammarAccess.getGeneratorModelAccess().getDirectoryKeyword_6_0_0());
            	    								
            	    otherlv_10=(Token)match(input,21,FOLLOW_3); 

            	    									newLeafNode(otherlv_10, grammarAccess.getGeneratorModelAccess().getColonKeyword_6_0_1());
            	    								
            	    // InternalRuntimeModelCode.g:156:9: ( (lv_directory_11_0= RULE_URI ) )
            	    // InternalRuntimeModelCode.g:157:10: (lv_directory_11_0= RULE_URI )
            	    {
            	    // InternalRuntimeModelCode.g:157:10: (lv_directory_11_0= RULE_URI )
            	    // InternalRuntimeModelCode.g:158:11: lv_directory_11_0= RULE_URI
            	    {
            	    lv_directory_11_0=(Token)match(input,RULE_URI,FOLLOW_4); 

            	    											newLeafNode(lv_directory_11_0, grammarAccess.getGeneratorModelAccess().getDirectoryURITerminalRuleCall_6_0_2_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getGeneratorModelRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"directory",
            	    												lv_directory_11_0,
            	    												"edu.ustb.sei.modeladapter.RuntimeModelCode.URI");
            	    										

            	    }


            	    }

            	    otherlv_12=(Token)match(input,16,FOLLOW_8); 

            	    									newLeafNode(otherlv_12, grammarAccess.getGeneratorModelAccess().getSemicolonKeyword_6_0_3());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalRuntimeModelCode.g:184:4: ({...}? => ( ({...}? => (otherlv_13= 'load' otherlv_14= ':' ( (lv_loadMetamodel_15_0= RULE_CODE ) ) ) ) ) )
            	    {
            	    // InternalRuntimeModelCode.g:184:4: ({...}? => ( ({...}? => (otherlv_13= 'load' otherlv_14= ':' ( (lv_loadMetamodel_15_0= RULE_CODE ) ) ) ) ) )
            	    // InternalRuntimeModelCode.g:185:5: {...}? => ( ({...}? => (otherlv_13= 'load' otherlv_14= ':' ( (lv_loadMetamodel_15_0= RULE_CODE ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleGeneratorModel", "getUnorderedGroupHelper().canSelect(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6(), 1)");
            	    }
            	    // InternalRuntimeModelCode.g:185:111: ( ({...}? => (otherlv_13= 'load' otherlv_14= ':' ( (lv_loadMetamodel_15_0= RULE_CODE ) ) ) ) )
            	    // InternalRuntimeModelCode.g:186:6: ({...}? => (otherlv_13= 'load' otherlv_14= ':' ( (lv_loadMetamodel_15_0= RULE_CODE ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6(), 1);
            	    					
            	    // InternalRuntimeModelCode.g:189:9: ({...}? => (otherlv_13= 'load' otherlv_14= ':' ( (lv_loadMetamodel_15_0= RULE_CODE ) ) ) )
            	    // InternalRuntimeModelCode.g:189:10: {...}? => (otherlv_13= 'load' otherlv_14= ':' ( (lv_loadMetamodel_15_0= RULE_CODE ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleGeneratorModel", "true");
            	    }
            	    // InternalRuntimeModelCode.g:189:19: (otherlv_13= 'load' otherlv_14= ':' ( (lv_loadMetamodel_15_0= RULE_CODE ) ) )
            	    // InternalRuntimeModelCode.g:189:20: otherlv_13= 'load' otherlv_14= ':' ( (lv_loadMetamodel_15_0= RULE_CODE ) )
            	    {
            	    otherlv_13=(Token)match(input,22,FOLLOW_9); 

            	    									newLeafNode(otherlv_13, grammarAccess.getGeneratorModelAccess().getLoadKeyword_6_1_0());
            	    								
            	    otherlv_14=(Token)match(input,21,FOLLOW_10); 

            	    									newLeafNode(otherlv_14, grammarAccess.getGeneratorModelAccess().getColonKeyword_6_1_1());
            	    								
            	    // InternalRuntimeModelCode.g:197:9: ( (lv_loadMetamodel_15_0= RULE_CODE ) )
            	    // InternalRuntimeModelCode.g:198:10: (lv_loadMetamodel_15_0= RULE_CODE )
            	    {
            	    // InternalRuntimeModelCode.g:198:10: (lv_loadMetamodel_15_0= RULE_CODE )
            	    // InternalRuntimeModelCode.g:199:11: lv_loadMetamodel_15_0= RULE_CODE
            	    {
            	    lv_loadMetamodel_15_0=(Token)match(input,RULE_CODE,FOLLOW_8); 

            	    											newLeafNode(lv_loadMetamodel_15_0, grammarAccess.getGeneratorModelAccess().getLoadMetamodelCODETerminalRuleCall_6_1_2_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getGeneratorModelRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"loadMetamodel",
            	    												lv_loadMetamodel_15_0,
            	    												"edu.ustb.sei.modeladapter.RuntimeModelCode.CODE");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalRuntimeModelCode.g:221:4: ({...}? => ( ({...}? => (otherlv_16= 'refreshing' otherlv_17= ':' ( (lv_refreshingRate_18_0= RULE_LONG ) ) otherlv_19= ';' ) ) ) )
            	    {
            	    // InternalRuntimeModelCode.g:221:4: ({...}? => ( ({...}? => (otherlv_16= 'refreshing' otherlv_17= ':' ( (lv_refreshingRate_18_0= RULE_LONG ) ) otherlv_19= ';' ) ) ) )
            	    // InternalRuntimeModelCode.g:222:5: {...}? => ( ({...}? => (otherlv_16= 'refreshing' otherlv_17= ':' ( (lv_refreshingRate_18_0= RULE_LONG ) ) otherlv_19= ';' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6(), 2) ) {
            	        throw new FailedPredicateException(input, "ruleGeneratorModel", "getUnorderedGroupHelper().canSelect(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6(), 2)");
            	    }
            	    // InternalRuntimeModelCode.g:222:111: ( ({...}? => (otherlv_16= 'refreshing' otherlv_17= ':' ( (lv_refreshingRate_18_0= RULE_LONG ) ) otherlv_19= ';' ) ) )
            	    // InternalRuntimeModelCode.g:223:6: ({...}? => (otherlv_16= 'refreshing' otherlv_17= ':' ( (lv_refreshingRate_18_0= RULE_LONG ) ) otherlv_19= ';' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6(), 2);
            	    					
            	    // InternalRuntimeModelCode.g:226:9: ({...}? => (otherlv_16= 'refreshing' otherlv_17= ':' ( (lv_refreshingRate_18_0= RULE_LONG ) ) otherlv_19= ';' ) )
            	    // InternalRuntimeModelCode.g:226:10: {...}? => (otherlv_16= 'refreshing' otherlv_17= ':' ( (lv_refreshingRate_18_0= RULE_LONG ) ) otherlv_19= ';' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleGeneratorModel", "true");
            	    }
            	    // InternalRuntimeModelCode.g:226:19: (otherlv_16= 'refreshing' otherlv_17= ':' ( (lv_refreshingRate_18_0= RULE_LONG ) ) otherlv_19= ';' )
            	    // InternalRuntimeModelCode.g:226:20: otherlv_16= 'refreshing' otherlv_17= ':' ( (lv_refreshingRate_18_0= RULE_LONG ) ) otherlv_19= ';'
            	    {
            	    otherlv_16=(Token)match(input,23,FOLLOW_9); 

            	    									newLeafNode(otherlv_16, grammarAccess.getGeneratorModelAccess().getRefreshingKeyword_6_2_0());
            	    								
            	    otherlv_17=(Token)match(input,21,FOLLOW_11); 

            	    									newLeafNode(otherlv_17, grammarAccess.getGeneratorModelAccess().getColonKeyword_6_2_1());
            	    								
            	    // InternalRuntimeModelCode.g:234:9: ( (lv_refreshingRate_18_0= RULE_LONG ) )
            	    // InternalRuntimeModelCode.g:235:10: (lv_refreshingRate_18_0= RULE_LONG )
            	    {
            	    // InternalRuntimeModelCode.g:235:10: (lv_refreshingRate_18_0= RULE_LONG )
            	    // InternalRuntimeModelCode.g:236:11: lv_refreshingRate_18_0= RULE_LONG
            	    {
            	    lv_refreshingRate_18_0=(Token)match(input,RULE_LONG,FOLLOW_4); 

            	    											newLeafNode(lv_refreshingRate_18_0, grammarAccess.getGeneratorModelAccess().getRefreshingRateLONGTerminalRuleCall_6_2_2_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getGeneratorModelRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"refreshingRate",
            	    												lv_refreshingRate_18_0,
            	    												"edu.ustb.sei.modeladapter.RuntimeModelCode.LONG");
            	    										

            	    }


            	    }

            	    otherlv_19=(Token)match(input,16,FOLLOW_8); 

            	    									newLeafNode(otherlv_19, grammarAccess.getGeneratorModelAccess().getSemicolonKeyword_6_2_3());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // InternalRuntimeModelCode.g:262:4: ({...}? => ( ({...}? => (otherlv_20= 'timer' otherlv_21= ':' ( (lv_timerClass_22_0= RULE_URI ) ) otherlv_23= ';' ) ) ) )
            	    {
            	    // InternalRuntimeModelCode.g:262:4: ({...}? => ( ({...}? => (otherlv_20= 'timer' otherlv_21= ':' ( (lv_timerClass_22_0= RULE_URI ) ) otherlv_23= ';' ) ) ) )
            	    // InternalRuntimeModelCode.g:263:5: {...}? => ( ({...}? => (otherlv_20= 'timer' otherlv_21= ':' ( (lv_timerClass_22_0= RULE_URI ) ) otherlv_23= ';' ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6(), 3) ) {
            	        throw new FailedPredicateException(input, "ruleGeneratorModel", "getUnorderedGroupHelper().canSelect(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6(), 3)");
            	    }
            	    // InternalRuntimeModelCode.g:263:111: ( ({...}? => (otherlv_20= 'timer' otherlv_21= ':' ( (lv_timerClass_22_0= RULE_URI ) ) otherlv_23= ';' ) ) )
            	    // InternalRuntimeModelCode.g:264:6: ({...}? => (otherlv_20= 'timer' otherlv_21= ':' ( (lv_timerClass_22_0= RULE_URI ) ) otherlv_23= ';' ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6(), 3);
            	    					
            	    // InternalRuntimeModelCode.g:267:9: ({...}? => (otherlv_20= 'timer' otherlv_21= ':' ( (lv_timerClass_22_0= RULE_URI ) ) otherlv_23= ';' ) )
            	    // InternalRuntimeModelCode.g:267:10: {...}? => (otherlv_20= 'timer' otherlv_21= ':' ( (lv_timerClass_22_0= RULE_URI ) ) otherlv_23= ';' )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleGeneratorModel", "true");
            	    }
            	    // InternalRuntimeModelCode.g:267:19: (otherlv_20= 'timer' otherlv_21= ':' ( (lv_timerClass_22_0= RULE_URI ) ) otherlv_23= ';' )
            	    // InternalRuntimeModelCode.g:267:20: otherlv_20= 'timer' otherlv_21= ':' ( (lv_timerClass_22_0= RULE_URI ) ) otherlv_23= ';'
            	    {
            	    otherlv_20=(Token)match(input,24,FOLLOW_9); 

            	    									newLeafNode(otherlv_20, grammarAccess.getGeneratorModelAccess().getTimerKeyword_6_3_0());
            	    								
            	    otherlv_21=(Token)match(input,21,FOLLOW_3); 

            	    									newLeafNode(otherlv_21, grammarAccess.getGeneratorModelAccess().getColonKeyword_6_3_1());
            	    								
            	    // InternalRuntimeModelCode.g:275:9: ( (lv_timerClass_22_0= RULE_URI ) )
            	    // InternalRuntimeModelCode.g:276:10: (lv_timerClass_22_0= RULE_URI )
            	    {
            	    // InternalRuntimeModelCode.g:276:10: (lv_timerClass_22_0= RULE_URI )
            	    // InternalRuntimeModelCode.g:277:11: lv_timerClass_22_0= RULE_URI
            	    {
            	    lv_timerClass_22_0=(Token)match(input,RULE_URI,FOLLOW_4); 

            	    											newLeafNode(lv_timerClass_22_0, grammarAccess.getGeneratorModelAccess().getTimerClassURITerminalRuleCall_6_3_2_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getGeneratorModelRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"timerClass",
            	    												lv_timerClass_22_0,
            	    												"edu.ustb.sei.modeladapter.RuntimeModelCode.URI");
            	    										

            	    }


            	    }

            	    otherlv_23=(Token)match(input,16,FOLLOW_8); 

            	    									newLeafNode(otherlv_23, grammarAccess.getGeneratorModelAccess().getSemicolonKeyword_6_3_3());
            	    								

            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getGeneratorModelAccess().getUnorderedGroup_6());
            				

            }

            otherlv_24=(Token)match(input,25,FOLLOW_12); 

            			newLeafNode(otherlv_24, grammarAccess.getGeneratorModelAccess().getRightCurlyBracketKeyword_7());
            		
            // InternalRuntimeModelCode.g:314:3: ( (lv_classAdapters_25_0= ruleClassAdapter ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==26) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalRuntimeModelCode.g:315:4: (lv_classAdapters_25_0= ruleClassAdapter )
            	    {
            	    // InternalRuntimeModelCode.g:315:4: (lv_classAdapters_25_0= ruleClassAdapter )
            	    // InternalRuntimeModelCode.g:316:5: lv_classAdapters_25_0= ruleClassAdapter
            	    {

            	    					newCompositeNode(grammarAccess.getGeneratorModelAccess().getClassAdaptersClassAdapterParserRuleCall_8_0());
            	    				
            	    pushFollow(FOLLOW_12);
            	    lv_classAdapters_25_0=ruleClassAdapter();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getGeneratorModelRule());
            	    					}
            	    					add(
            	    						current,
            	    						"classAdapters",
            	    						lv_classAdapters_25_0,
            	    						"edu.ustb.sei.modeladapter.RuntimeModelCode.ClassAdapter");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGeneratorModel"


    // $ANTLR start "entryRuleClassAdapter"
    // InternalRuntimeModelCode.g:337:1: entryRuleClassAdapter returns [EObject current=null] : iv_ruleClassAdapter= ruleClassAdapter EOF ;
    public final EObject entryRuleClassAdapter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClassAdapter = null;


        try {
            // InternalRuntimeModelCode.g:337:53: (iv_ruleClassAdapter= ruleClassAdapter EOF )
            // InternalRuntimeModelCode.g:338:2: iv_ruleClassAdapter= ruleClassAdapter EOF
            {
             newCompositeNode(grammarAccess.getClassAdapterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleClassAdapter=ruleClassAdapter();

            state._fsp--;

             current =iv_ruleClassAdapter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClassAdapter"


    // $ANTLR start "ruleClassAdapter"
    // InternalRuntimeModelCode.g:344:1: ruleClassAdapter returns [EObject current=null] : (otherlv_0= 'adapter' ( (otherlv_1= RULE_NAME ) ) otherlv_2= '{' (otherlv_3= 'import' ( (lv_imports_4_0= RULE_URI ) ) otherlv_5= ';' )* ( (lv_featureAdapters_6_0= ruleFeatureAdapter ) )* otherlv_7= '}' ) ;
    public final EObject ruleClassAdapter() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token lv_imports_4_0=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        EObject lv_featureAdapters_6_0 = null;



        	enterRule();

        try {
            // InternalRuntimeModelCode.g:350:2: ( (otherlv_0= 'adapter' ( (otherlv_1= RULE_NAME ) ) otherlv_2= '{' (otherlv_3= 'import' ( (lv_imports_4_0= RULE_URI ) ) otherlv_5= ';' )* ( (lv_featureAdapters_6_0= ruleFeatureAdapter ) )* otherlv_7= '}' ) )
            // InternalRuntimeModelCode.g:351:2: (otherlv_0= 'adapter' ( (otherlv_1= RULE_NAME ) ) otherlv_2= '{' (otherlv_3= 'import' ( (lv_imports_4_0= RULE_URI ) ) otherlv_5= ';' )* ( (lv_featureAdapters_6_0= ruleFeatureAdapter ) )* otherlv_7= '}' )
            {
            // InternalRuntimeModelCode.g:351:2: (otherlv_0= 'adapter' ( (otherlv_1= RULE_NAME ) ) otherlv_2= '{' (otherlv_3= 'import' ( (lv_imports_4_0= RULE_URI ) ) otherlv_5= ';' )* ( (lv_featureAdapters_6_0= ruleFeatureAdapter ) )* otherlv_7= '}' )
            // InternalRuntimeModelCode.g:352:3: otherlv_0= 'adapter' ( (otherlv_1= RULE_NAME ) ) otherlv_2= '{' (otherlv_3= 'import' ( (lv_imports_4_0= RULE_URI ) ) otherlv_5= ';' )* ( (lv_featureAdapters_6_0= ruleFeatureAdapter ) )* otherlv_7= '}'
            {
            otherlv_0=(Token)match(input,26,FOLLOW_13); 

            			newLeafNode(otherlv_0, grammarAccess.getClassAdapterAccess().getAdapterKeyword_0());
            		
            // InternalRuntimeModelCode.g:356:3: ( (otherlv_1= RULE_NAME ) )
            // InternalRuntimeModelCode.g:357:4: (otherlv_1= RULE_NAME )
            {
            // InternalRuntimeModelCode.g:357:4: (otherlv_1= RULE_NAME )
            // InternalRuntimeModelCode.g:358:5: otherlv_1= RULE_NAME
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getClassAdapterRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_NAME,FOLLOW_7); 

            					newLeafNode(otherlv_1, grammarAccess.getClassAdapterAccess().getAdaptedClassEClassCrossReference_1_0());
            				

            }


            }

            otherlv_2=(Token)match(input,19,FOLLOW_14); 

            			newLeafNode(otherlv_2, grammarAccess.getClassAdapterAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalRuntimeModelCode.g:373:3: (otherlv_3= 'import' ( (lv_imports_4_0= RULE_URI ) ) otherlv_5= ';' )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==17) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalRuntimeModelCode.g:374:4: otherlv_3= 'import' ( (lv_imports_4_0= RULE_URI ) ) otherlv_5= ';'
            	    {
            	    otherlv_3=(Token)match(input,17,FOLLOW_3); 

            	    				newLeafNode(otherlv_3, grammarAccess.getClassAdapterAccess().getImportKeyword_3_0());
            	    			
            	    // InternalRuntimeModelCode.g:378:4: ( (lv_imports_4_0= RULE_URI ) )
            	    // InternalRuntimeModelCode.g:379:5: (lv_imports_4_0= RULE_URI )
            	    {
            	    // InternalRuntimeModelCode.g:379:5: (lv_imports_4_0= RULE_URI )
            	    // InternalRuntimeModelCode.g:380:6: lv_imports_4_0= RULE_URI
            	    {
            	    lv_imports_4_0=(Token)match(input,RULE_URI,FOLLOW_4); 

            	    						newLeafNode(lv_imports_4_0, grammarAccess.getClassAdapterAccess().getImportsURITerminalRuleCall_3_1_0());
            	    					

            	    						if (current==null) {
            	    							current = createModelElement(grammarAccess.getClassAdapterRule());
            	    						}
            	    						addWithLastConsumed(
            	    							current,
            	    							"imports",
            	    							lv_imports_4_0,
            	    							"edu.ustb.sei.modeladapter.RuntimeModelCode.URI");
            	    					

            	    }


            	    }

            	    otherlv_5=(Token)match(input,16,FOLLOW_14); 

            	    				newLeafNode(otherlv_5, grammarAccess.getClassAdapterAccess().getSemicolonKeyword_3_2());
            	    			

            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

            // InternalRuntimeModelCode.g:401:3: ( (lv_featureAdapters_6_0= ruleFeatureAdapter ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_NAME||LA5_0==27) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalRuntimeModelCode.g:402:4: (lv_featureAdapters_6_0= ruleFeatureAdapter )
            	    {
            	    // InternalRuntimeModelCode.g:402:4: (lv_featureAdapters_6_0= ruleFeatureAdapter )
            	    // InternalRuntimeModelCode.g:403:5: lv_featureAdapters_6_0= ruleFeatureAdapter
            	    {

            	    					newCompositeNode(grammarAccess.getClassAdapterAccess().getFeatureAdaptersFeatureAdapterParserRuleCall_4_0());
            	    				
            	    pushFollow(FOLLOW_15);
            	    lv_featureAdapters_6_0=ruleFeatureAdapter();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getClassAdapterRule());
            	    					}
            	    					add(
            	    						current,
            	    						"featureAdapters",
            	    						lv_featureAdapters_6_0,
            	    						"edu.ustb.sei.modeladapter.RuntimeModelCode.FeatureAdapter");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            otherlv_7=(Token)match(input,25,FOLLOW_2); 

            			newLeafNode(otherlv_7, grammarAccess.getClassAdapterAccess().getRightCurlyBracketKeyword_5());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClassAdapter"


    // $ANTLR start "entryRuleFeatureAdapter"
    // InternalRuntimeModelCode.g:428:1: entryRuleFeatureAdapter returns [EObject current=null] : iv_ruleFeatureAdapter= ruleFeatureAdapter EOF ;
    public final EObject entryRuleFeatureAdapter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureAdapter = null;


        try {
            // InternalRuntimeModelCode.g:428:55: (iv_ruleFeatureAdapter= ruleFeatureAdapter EOF )
            // InternalRuntimeModelCode.g:429:2: iv_ruleFeatureAdapter= ruleFeatureAdapter EOF
            {
             newCompositeNode(grammarAccess.getFeatureAdapterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFeatureAdapter=ruleFeatureAdapter();

            state._fsp--;

             current =iv_ruleFeatureAdapter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureAdapter"


    // $ANTLR start "ruleFeatureAdapter"
    // InternalRuntimeModelCode.g:435:1: ruleFeatureAdapter returns [EObject current=null] : ( ( (lv_id_0_0= 'id' ) )? ( (otherlv_1= RULE_NAME ) ) ( (lv_conversion_2_0= ruleConversion ) )? ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'get' ( (lv_get_5_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'post' ( (lv_post_7_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'put' ( (lv_put_9_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'delete' ( (lv_delete_11_0= RULE_CODE ) ) ) ) ) ) )* ) ) ) ) ;
    public final EObject ruleFeatureAdapter() throws RecognitionException {
        EObject current = null;

        Token lv_id_0_0=null;
        Token otherlv_1=null;
        Token otherlv_4=null;
        Token lv_get_5_0=null;
        Token otherlv_6=null;
        Token lv_post_7_0=null;
        Token otherlv_8=null;
        Token lv_put_9_0=null;
        Token otherlv_10=null;
        Token lv_delete_11_0=null;
        EObject lv_conversion_2_0 = null;



        	enterRule();

        try {
            // InternalRuntimeModelCode.g:441:2: ( ( ( (lv_id_0_0= 'id' ) )? ( (otherlv_1= RULE_NAME ) ) ( (lv_conversion_2_0= ruleConversion ) )? ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'get' ( (lv_get_5_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'post' ( (lv_post_7_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'put' ( (lv_put_9_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'delete' ( (lv_delete_11_0= RULE_CODE ) ) ) ) ) ) )* ) ) ) ) )
            // InternalRuntimeModelCode.g:442:2: ( ( (lv_id_0_0= 'id' ) )? ( (otherlv_1= RULE_NAME ) ) ( (lv_conversion_2_0= ruleConversion ) )? ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'get' ( (lv_get_5_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'post' ( (lv_post_7_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'put' ( (lv_put_9_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'delete' ( (lv_delete_11_0= RULE_CODE ) ) ) ) ) ) )* ) ) ) )
            {
            // InternalRuntimeModelCode.g:442:2: ( ( (lv_id_0_0= 'id' ) )? ( (otherlv_1= RULE_NAME ) ) ( (lv_conversion_2_0= ruleConversion ) )? ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'get' ( (lv_get_5_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'post' ( (lv_post_7_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'put' ( (lv_put_9_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'delete' ( (lv_delete_11_0= RULE_CODE ) ) ) ) ) ) )* ) ) ) )
            // InternalRuntimeModelCode.g:443:3: ( (lv_id_0_0= 'id' ) )? ( (otherlv_1= RULE_NAME ) ) ( (lv_conversion_2_0= ruleConversion ) )? ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'get' ( (lv_get_5_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'post' ( (lv_post_7_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'put' ( (lv_put_9_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'delete' ( (lv_delete_11_0= RULE_CODE ) ) ) ) ) ) )* ) ) )
            {
            // InternalRuntimeModelCode.g:443:3: ( (lv_id_0_0= 'id' ) )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==27) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalRuntimeModelCode.g:444:4: (lv_id_0_0= 'id' )
                    {
                    // InternalRuntimeModelCode.g:444:4: (lv_id_0_0= 'id' )
                    // InternalRuntimeModelCode.g:445:5: lv_id_0_0= 'id'
                    {
                    lv_id_0_0=(Token)match(input,27,FOLLOW_13); 

                    					newLeafNode(lv_id_0_0, grammarAccess.getFeatureAdapterAccess().getIdIdKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getFeatureAdapterRule());
                    					}
                    					setWithLastConsumed(current, "id", true, "id");
                    				

                    }


                    }
                    break;

            }

            // InternalRuntimeModelCode.g:457:3: ( (otherlv_1= RULE_NAME ) )
            // InternalRuntimeModelCode.g:458:4: (otherlv_1= RULE_NAME )
            {
            // InternalRuntimeModelCode.g:458:4: (otherlv_1= RULE_NAME )
            // InternalRuntimeModelCode.g:459:5: otherlv_1= RULE_NAME
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFeatureAdapterRule());
            					}
            				
            otherlv_1=(Token)match(input,RULE_NAME,FOLLOW_16); 

            					newLeafNode(otherlv_1, grammarAccess.getFeatureAdapterAccess().getAdaptedFeatureEStructuralFeatureCrossReference_1_0());
            				

            }


            }

            // InternalRuntimeModelCode.g:470:3: ( (lv_conversion_2_0= ruleConversion ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==32) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalRuntimeModelCode.g:471:4: (lv_conversion_2_0= ruleConversion )
                    {
                    // InternalRuntimeModelCode.g:471:4: (lv_conversion_2_0= ruleConversion )
                    // InternalRuntimeModelCode.g:472:5: lv_conversion_2_0= ruleConversion
                    {

                    					newCompositeNode(grammarAccess.getFeatureAdapterAccess().getConversionConversionParserRuleCall_2_0());
                    				
                    pushFollow(FOLLOW_17);
                    lv_conversion_2_0=ruleConversion();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getFeatureAdapterRule());
                    					}
                    					set(
                    						current,
                    						"conversion",
                    						lv_conversion_2_0,
                    						"edu.ustb.sei.modeladapter.RuntimeModelCode.Conversion");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }

            // InternalRuntimeModelCode.g:489:3: ( ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'get' ( (lv_get_5_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'post' ( (lv_post_7_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'put' ( (lv_put_9_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'delete' ( (lv_delete_11_0= RULE_CODE ) ) ) ) ) ) )* ) ) )
            // InternalRuntimeModelCode.g:490:4: ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'get' ( (lv_get_5_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'post' ( (lv_post_7_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'put' ( (lv_put_9_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'delete' ( (lv_delete_11_0= RULE_CODE ) ) ) ) ) ) )* ) )
            {
            // InternalRuntimeModelCode.g:490:4: ( ( ( ({...}? => ( ({...}? => (otherlv_4= 'get' ( (lv_get_5_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'post' ( (lv_post_7_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'put' ( (lv_put_9_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'delete' ( (lv_delete_11_0= RULE_CODE ) ) ) ) ) ) )* ) )
            // InternalRuntimeModelCode.g:491:5: ( ( ({...}? => ( ({...}? => (otherlv_4= 'get' ( (lv_get_5_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'post' ( (lv_post_7_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'put' ( (lv_put_9_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'delete' ( (lv_delete_11_0= RULE_CODE ) ) ) ) ) ) )* )
            {
             
            				  getUnorderedGroupHelper().enter(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3());
            				
            // InternalRuntimeModelCode.g:494:5: ( ( ({...}? => ( ({...}? => (otherlv_4= 'get' ( (lv_get_5_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'post' ( (lv_post_7_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'put' ( (lv_put_9_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'delete' ( (lv_delete_11_0= RULE_CODE ) ) ) ) ) ) )* )
            // InternalRuntimeModelCode.g:495:6: ( ({...}? => ( ({...}? => (otherlv_4= 'get' ( (lv_get_5_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'post' ( (lv_post_7_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'put' ( (lv_put_9_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'delete' ( (lv_delete_11_0= RULE_CODE ) ) ) ) ) ) )*
            {
            // InternalRuntimeModelCode.g:495:6: ( ({...}? => ( ({...}? => (otherlv_4= 'get' ( (lv_get_5_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_6= 'post' ( (lv_post_7_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_8= 'put' ( (lv_put_9_0= RULE_CODE ) ) ) ) ) ) | ({...}? => ( ({...}? => (otherlv_10= 'delete' ( (lv_delete_11_0= RULE_CODE ) ) ) ) ) ) )*
            loop8:
            do {
                int alt8=5;
                int LA8_0 = input.LA(1);

                if ( LA8_0 == 28 && getUnorderedGroupHelper().canSelect(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3(), 0) ) {
                    alt8=1;
                }
                else if ( LA8_0 == 29 && getUnorderedGroupHelper().canSelect(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3(), 1) ) {
                    alt8=2;
                }
                else if ( LA8_0 == 30 && getUnorderedGroupHelper().canSelect(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3(), 2) ) {
                    alt8=3;
                }
                else if ( LA8_0 == 31 && getUnorderedGroupHelper().canSelect(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3(), 3) ) {
                    alt8=4;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalRuntimeModelCode.g:496:4: ({...}? => ( ({...}? => (otherlv_4= 'get' ( (lv_get_5_0= RULE_CODE ) ) ) ) ) )
            	    {
            	    // InternalRuntimeModelCode.g:496:4: ({...}? => ( ({...}? => (otherlv_4= 'get' ( (lv_get_5_0= RULE_CODE ) ) ) ) ) )
            	    // InternalRuntimeModelCode.g:497:5: {...}? => ( ({...}? => (otherlv_4= 'get' ( (lv_get_5_0= RULE_CODE ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3(), 0) ) {
            	        throw new FailedPredicateException(input, "ruleFeatureAdapter", "getUnorderedGroupHelper().canSelect(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3(), 0)");
            	    }
            	    // InternalRuntimeModelCode.g:497:111: ( ({...}? => (otherlv_4= 'get' ( (lv_get_5_0= RULE_CODE ) ) ) ) )
            	    // InternalRuntimeModelCode.g:498:6: ({...}? => (otherlv_4= 'get' ( (lv_get_5_0= RULE_CODE ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3(), 0);
            	    					
            	    // InternalRuntimeModelCode.g:501:9: ({...}? => (otherlv_4= 'get' ( (lv_get_5_0= RULE_CODE ) ) ) )
            	    // InternalRuntimeModelCode.g:501:10: {...}? => (otherlv_4= 'get' ( (lv_get_5_0= RULE_CODE ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleFeatureAdapter", "true");
            	    }
            	    // InternalRuntimeModelCode.g:501:19: (otherlv_4= 'get' ( (lv_get_5_0= RULE_CODE ) ) )
            	    // InternalRuntimeModelCode.g:501:20: otherlv_4= 'get' ( (lv_get_5_0= RULE_CODE ) )
            	    {
            	    otherlv_4=(Token)match(input,28,FOLLOW_10); 

            	    									newLeafNode(otherlv_4, grammarAccess.getFeatureAdapterAccess().getGetKeyword_3_0_0());
            	    								
            	    // InternalRuntimeModelCode.g:505:9: ( (lv_get_5_0= RULE_CODE ) )
            	    // InternalRuntimeModelCode.g:506:10: (lv_get_5_0= RULE_CODE )
            	    {
            	    // InternalRuntimeModelCode.g:506:10: (lv_get_5_0= RULE_CODE )
            	    // InternalRuntimeModelCode.g:507:11: lv_get_5_0= RULE_CODE
            	    {
            	    lv_get_5_0=(Token)match(input,RULE_CODE,FOLLOW_17); 

            	    											newLeafNode(lv_get_5_0, grammarAccess.getFeatureAdapterAccess().getGetCODETerminalRuleCall_3_0_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getFeatureAdapterRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"get",
            	    												lv_get_5_0,
            	    												"edu.ustb.sei.modeladapter.RuntimeModelCode.CODE");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalRuntimeModelCode.g:529:4: ({...}? => ( ({...}? => (otherlv_6= 'post' ( (lv_post_7_0= RULE_CODE ) ) ) ) ) )
            	    {
            	    // InternalRuntimeModelCode.g:529:4: ({...}? => ( ({...}? => (otherlv_6= 'post' ( (lv_post_7_0= RULE_CODE ) ) ) ) ) )
            	    // InternalRuntimeModelCode.g:530:5: {...}? => ( ({...}? => (otherlv_6= 'post' ( (lv_post_7_0= RULE_CODE ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3(), 1) ) {
            	        throw new FailedPredicateException(input, "ruleFeatureAdapter", "getUnorderedGroupHelper().canSelect(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3(), 1)");
            	    }
            	    // InternalRuntimeModelCode.g:530:111: ( ({...}? => (otherlv_6= 'post' ( (lv_post_7_0= RULE_CODE ) ) ) ) )
            	    // InternalRuntimeModelCode.g:531:6: ({...}? => (otherlv_6= 'post' ( (lv_post_7_0= RULE_CODE ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3(), 1);
            	    					
            	    // InternalRuntimeModelCode.g:534:9: ({...}? => (otherlv_6= 'post' ( (lv_post_7_0= RULE_CODE ) ) ) )
            	    // InternalRuntimeModelCode.g:534:10: {...}? => (otherlv_6= 'post' ( (lv_post_7_0= RULE_CODE ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleFeatureAdapter", "true");
            	    }
            	    // InternalRuntimeModelCode.g:534:19: (otherlv_6= 'post' ( (lv_post_7_0= RULE_CODE ) ) )
            	    // InternalRuntimeModelCode.g:534:20: otherlv_6= 'post' ( (lv_post_7_0= RULE_CODE ) )
            	    {
            	    otherlv_6=(Token)match(input,29,FOLLOW_10); 

            	    									newLeafNode(otherlv_6, grammarAccess.getFeatureAdapterAccess().getPostKeyword_3_1_0());
            	    								
            	    // InternalRuntimeModelCode.g:538:9: ( (lv_post_7_0= RULE_CODE ) )
            	    // InternalRuntimeModelCode.g:539:10: (lv_post_7_0= RULE_CODE )
            	    {
            	    // InternalRuntimeModelCode.g:539:10: (lv_post_7_0= RULE_CODE )
            	    // InternalRuntimeModelCode.g:540:11: lv_post_7_0= RULE_CODE
            	    {
            	    lv_post_7_0=(Token)match(input,RULE_CODE,FOLLOW_17); 

            	    											newLeafNode(lv_post_7_0, grammarAccess.getFeatureAdapterAccess().getPostCODETerminalRuleCall_3_1_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getFeatureAdapterRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"post",
            	    												lv_post_7_0,
            	    												"edu.ustb.sei.modeladapter.RuntimeModelCode.CODE");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalRuntimeModelCode.g:562:4: ({...}? => ( ({...}? => (otherlv_8= 'put' ( (lv_put_9_0= RULE_CODE ) ) ) ) ) )
            	    {
            	    // InternalRuntimeModelCode.g:562:4: ({...}? => ( ({...}? => (otherlv_8= 'put' ( (lv_put_9_0= RULE_CODE ) ) ) ) ) )
            	    // InternalRuntimeModelCode.g:563:5: {...}? => ( ({...}? => (otherlv_8= 'put' ( (lv_put_9_0= RULE_CODE ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3(), 2) ) {
            	        throw new FailedPredicateException(input, "ruleFeatureAdapter", "getUnorderedGroupHelper().canSelect(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3(), 2)");
            	    }
            	    // InternalRuntimeModelCode.g:563:111: ( ({...}? => (otherlv_8= 'put' ( (lv_put_9_0= RULE_CODE ) ) ) ) )
            	    // InternalRuntimeModelCode.g:564:6: ({...}? => (otherlv_8= 'put' ( (lv_put_9_0= RULE_CODE ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3(), 2);
            	    					
            	    // InternalRuntimeModelCode.g:567:9: ({...}? => (otherlv_8= 'put' ( (lv_put_9_0= RULE_CODE ) ) ) )
            	    // InternalRuntimeModelCode.g:567:10: {...}? => (otherlv_8= 'put' ( (lv_put_9_0= RULE_CODE ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleFeatureAdapter", "true");
            	    }
            	    // InternalRuntimeModelCode.g:567:19: (otherlv_8= 'put' ( (lv_put_9_0= RULE_CODE ) ) )
            	    // InternalRuntimeModelCode.g:567:20: otherlv_8= 'put' ( (lv_put_9_0= RULE_CODE ) )
            	    {
            	    otherlv_8=(Token)match(input,30,FOLLOW_10); 

            	    									newLeafNode(otherlv_8, grammarAccess.getFeatureAdapterAccess().getPutKeyword_3_2_0());
            	    								
            	    // InternalRuntimeModelCode.g:571:9: ( (lv_put_9_0= RULE_CODE ) )
            	    // InternalRuntimeModelCode.g:572:10: (lv_put_9_0= RULE_CODE )
            	    {
            	    // InternalRuntimeModelCode.g:572:10: (lv_put_9_0= RULE_CODE )
            	    // InternalRuntimeModelCode.g:573:11: lv_put_9_0= RULE_CODE
            	    {
            	    lv_put_9_0=(Token)match(input,RULE_CODE,FOLLOW_17); 

            	    											newLeafNode(lv_put_9_0, grammarAccess.getFeatureAdapterAccess().getPutCODETerminalRuleCall_3_2_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getFeatureAdapterRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"put",
            	    												lv_put_9_0,
            	    												"edu.ustb.sei.modeladapter.RuntimeModelCode.CODE");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // InternalRuntimeModelCode.g:595:4: ({...}? => ( ({...}? => (otherlv_10= 'delete' ( (lv_delete_11_0= RULE_CODE ) ) ) ) ) )
            	    {
            	    // InternalRuntimeModelCode.g:595:4: ({...}? => ( ({...}? => (otherlv_10= 'delete' ( (lv_delete_11_0= RULE_CODE ) ) ) ) ) )
            	    // InternalRuntimeModelCode.g:596:5: {...}? => ( ({...}? => (otherlv_10= 'delete' ( (lv_delete_11_0= RULE_CODE ) ) ) ) )
            	    {
            	    if ( ! getUnorderedGroupHelper().canSelect(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3(), 3) ) {
            	        throw new FailedPredicateException(input, "ruleFeatureAdapter", "getUnorderedGroupHelper().canSelect(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3(), 3)");
            	    }
            	    // InternalRuntimeModelCode.g:596:111: ( ({...}? => (otherlv_10= 'delete' ( (lv_delete_11_0= RULE_CODE ) ) ) ) )
            	    // InternalRuntimeModelCode.g:597:6: ({...}? => (otherlv_10= 'delete' ( (lv_delete_11_0= RULE_CODE ) ) ) )
            	    {

            	    						getUnorderedGroupHelper().select(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3(), 3);
            	    					
            	    // InternalRuntimeModelCode.g:600:9: ({...}? => (otherlv_10= 'delete' ( (lv_delete_11_0= RULE_CODE ) ) ) )
            	    // InternalRuntimeModelCode.g:600:10: {...}? => (otherlv_10= 'delete' ( (lv_delete_11_0= RULE_CODE ) ) )
            	    {
            	    if ( !((true)) ) {
            	        throw new FailedPredicateException(input, "ruleFeatureAdapter", "true");
            	    }
            	    // InternalRuntimeModelCode.g:600:19: (otherlv_10= 'delete' ( (lv_delete_11_0= RULE_CODE ) ) )
            	    // InternalRuntimeModelCode.g:600:20: otherlv_10= 'delete' ( (lv_delete_11_0= RULE_CODE ) )
            	    {
            	    otherlv_10=(Token)match(input,31,FOLLOW_10); 

            	    									newLeafNode(otherlv_10, grammarAccess.getFeatureAdapterAccess().getDeleteKeyword_3_3_0());
            	    								
            	    // InternalRuntimeModelCode.g:604:9: ( (lv_delete_11_0= RULE_CODE ) )
            	    // InternalRuntimeModelCode.g:605:10: (lv_delete_11_0= RULE_CODE )
            	    {
            	    // InternalRuntimeModelCode.g:605:10: (lv_delete_11_0= RULE_CODE )
            	    // InternalRuntimeModelCode.g:606:11: lv_delete_11_0= RULE_CODE
            	    {
            	    lv_delete_11_0=(Token)match(input,RULE_CODE,FOLLOW_17); 

            	    											newLeafNode(lv_delete_11_0, grammarAccess.getFeatureAdapterAccess().getDeleteCODETerminalRuleCall_3_3_1_0());
            	    										

            	    											if (current==null) {
            	    												current = createModelElement(grammarAccess.getFeatureAdapterRule());
            	    											}
            	    											setWithLastConsumed(
            	    												current,
            	    												"delete",
            	    												lv_delete_11_0,
            	    												"edu.ustb.sei.modeladapter.RuntimeModelCode.CODE");
            	    										

            	    }


            	    }


            	    }


            	    }

            	     
            	    						getUnorderedGroupHelper().returnFromSelection(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3());
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);


            }


            }

             
            				  getUnorderedGroupHelper().leave(grammarAccess.getFeatureAdapterAccess().getUnorderedGroup_3());
            				

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureAdapter"


    // $ANTLR start "entryRuleConversion"
    // InternalRuntimeModelCode.g:639:1: entryRuleConversion returns [EObject current=null] : iv_ruleConversion= ruleConversion EOF ;
    public final EObject entryRuleConversion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConversion = null;


        try {
            // InternalRuntimeModelCode.g:639:51: (iv_ruleConversion= ruleConversion EOF )
            // InternalRuntimeModelCode.g:640:2: iv_ruleConversion= ruleConversion EOF
            {
             newCompositeNode(grammarAccess.getConversionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConversion=ruleConversion();

            state._fsp--;

             current =iv_ruleConversion; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConversion"


    // $ANTLR start "ruleConversion"
    // InternalRuntimeModelCode.g:646:1: ruleConversion returns [EObject current=null] : (otherlv_0= 'actual' ( ( (lv_physicalType_1_1= RULE_NAME | lv_physicalType_1_2= RULE_URI ) ) ) otherlv_2= 'from' ( (lv_physicalToLogical_3_0= RULE_CODE ) ) otherlv_4= 'to' ( (lv_logicalToPhysical_5_0= RULE_CODE ) ) ) ;
    public final EObject ruleConversion() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_physicalType_1_1=null;
        Token lv_physicalType_1_2=null;
        Token otherlv_2=null;
        Token lv_physicalToLogical_3_0=null;
        Token otherlv_4=null;
        Token lv_logicalToPhysical_5_0=null;


        	enterRule();

        try {
            // InternalRuntimeModelCode.g:652:2: ( (otherlv_0= 'actual' ( ( (lv_physicalType_1_1= RULE_NAME | lv_physicalType_1_2= RULE_URI ) ) ) otherlv_2= 'from' ( (lv_physicalToLogical_3_0= RULE_CODE ) ) otherlv_4= 'to' ( (lv_logicalToPhysical_5_0= RULE_CODE ) ) ) )
            // InternalRuntimeModelCode.g:653:2: (otherlv_0= 'actual' ( ( (lv_physicalType_1_1= RULE_NAME | lv_physicalType_1_2= RULE_URI ) ) ) otherlv_2= 'from' ( (lv_physicalToLogical_3_0= RULE_CODE ) ) otherlv_4= 'to' ( (lv_logicalToPhysical_5_0= RULE_CODE ) ) )
            {
            // InternalRuntimeModelCode.g:653:2: (otherlv_0= 'actual' ( ( (lv_physicalType_1_1= RULE_NAME | lv_physicalType_1_2= RULE_URI ) ) ) otherlv_2= 'from' ( (lv_physicalToLogical_3_0= RULE_CODE ) ) otherlv_4= 'to' ( (lv_logicalToPhysical_5_0= RULE_CODE ) ) )
            // InternalRuntimeModelCode.g:654:3: otherlv_0= 'actual' ( ( (lv_physicalType_1_1= RULE_NAME | lv_physicalType_1_2= RULE_URI ) ) ) otherlv_2= 'from' ( (lv_physicalToLogical_3_0= RULE_CODE ) ) otherlv_4= 'to' ( (lv_logicalToPhysical_5_0= RULE_CODE ) )
            {
            otherlv_0=(Token)match(input,32,FOLLOW_18); 

            			newLeafNode(otherlv_0, grammarAccess.getConversionAccess().getActualKeyword_0());
            		
            // InternalRuntimeModelCode.g:658:3: ( ( (lv_physicalType_1_1= RULE_NAME | lv_physicalType_1_2= RULE_URI ) ) )
            // InternalRuntimeModelCode.g:659:4: ( (lv_physicalType_1_1= RULE_NAME | lv_physicalType_1_2= RULE_URI ) )
            {
            // InternalRuntimeModelCode.g:659:4: ( (lv_physicalType_1_1= RULE_NAME | lv_physicalType_1_2= RULE_URI ) )
            // InternalRuntimeModelCode.g:660:5: (lv_physicalType_1_1= RULE_NAME | lv_physicalType_1_2= RULE_URI )
            {
            // InternalRuntimeModelCode.g:660:5: (lv_physicalType_1_1= RULE_NAME | lv_physicalType_1_2= RULE_URI )
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( (LA9_0==RULE_NAME) ) {
                alt9=1;
            }
            else if ( (LA9_0==RULE_URI) ) {
                alt9=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }
            switch (alt9) {
                case 1 :
                    // InternalRuntimeModelCode.g:661:6: lv_physicalType_1_1= RULE_NAME
                    {
                    lv_physicalType_1_1=(Token)match(input,RULE_NAME,FOLLOW_19); 

                    						newLeafNode(lv_physicalType_1_1, grammarAccess.getConversionAccess().getPhysicalTypeNAMETerminalRuleCall_1_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getConversionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"physicalType",
                    							lv_physicalType_1_1,
                    							"edu.ustb.sei.modeladapter.RuntimeModelCode.NAME");
                    					

                    }
                    break;
                case 2 :
                    // InternalRuntimeModelCode.g:676:6: lv_physicalType_1_2= RULE_URI
                    {
                    lv_physicalType_1_2=(Token)match(input,RULE_URI,FOLLOW_19); 

                    						newLeafNode(lv_physicalType_1_2, grammarAccess.getConversionAccess().getPhysicalTypeURITerminalRuleCall_1_0_1());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getConversionRule());
                    						}
                    						setWithLastConsumed(
                    							current,
                    							"physicalType",
                    							lv_physicalType_1_2,
                    							"edu.ustb.sei.modeladapter.RuntimeModelCode.URI");
                    					

                    }
                    break;

            }


            }


            }

            otherlv_2=(Token)match(input,33,FOLLOW_10); 

            			newLeafNode(otherlv_2, grammarAccess.getConversionAccess().getFromKeyword_2());
            		
            // InternalRuntimeModelCode.g:697:3: ( (lv_physicalToLogical_3_0= RULE_CODE ) )
            // InternalRuntimeModelCode.g:698:4: (lv_physicalToLogical_3_0= RULE_CODE )
            {
            // InternalRuntimeModelCode.g:698:4: (lv_physicalToLogical_3_0= RULE_CODE )
            // InternalRuntimeModelCode.g:699:5: lv_physicalToLogical_3_0= RULE_CODE
            {
            lv_physicalToLogical_3_0=(Token)match(input,RULE_CODE,FOLLOW_20); 

            					newLeafNode(lv_physicalToLogical_3_0, grammarAccess.getConversionAccess().getPhysicalToLogicalCODETerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getConversionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"physicalToLogical",
            						lv_physicalToLogical_3_0,
            						"edu.ustb.sei.modeladapter.RuntimeModelCode.CODE");
            				

            }


            }

            otherlv_4=(Token)match(input,34,FOLLOW_10); 

            			newLeafNode(otherlv_4, grammarAccess.getConversionAccess().getToKeyword_4());
            		
            // InternalRuntimeModelCode.g:719:3: ( (lv_logicalToPhysical_5_0= RULE_CODE ) )
            // InternalRuntimeModelCode.g:720:4: (lv_logicalToPhysical_5_0= RULE_CODE )
            {
            // InternalRuntimeModelCode.g:720:4: (lv_logicalToPhysical_5_0= RULE_CODE )
            // InternalRuntimeModelCode.g:721:5: lv_logicalToPhysical_5_0= RULE_CODE
            {
            lv_logicalToPhysical_5_0=(Token)match(input,RULE_CODE,FOLLOW_2); 

            					newLeafNode(lv_logicalToPhysical_5_0, grammarAccess.getConversionAccess().getLogicalToPhysicalCODETerminalRuleCall_5_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getConversionRule());
            					}
            					setWithLastConsumed(
            						current,
            						"logicalToPhysical",
            						lv_logicalToPhysical_5_0,
            						"edu.ustb.sei.modeladapter.RuntimeModelCode.CODE");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConversion"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000040000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000003D00000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000004000002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x000000000A020080L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x000000000A000080L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x00000001F0000002L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x00000000F0000002L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000000090L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000200000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000400000000L});

}
