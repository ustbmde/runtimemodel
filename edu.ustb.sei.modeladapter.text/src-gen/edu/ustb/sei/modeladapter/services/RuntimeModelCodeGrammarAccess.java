/*
 * generated by Xtext 2.11.0
 */
package edu.ustb.sei.modeladapter.services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import java.util.List;
import org.eclipse.xtext.Alternatives;
import org.eclipse.xtext.Assignment;
import org.eclipse.xtext.CrossReference;
import org.eclipse.xtext.Grammar;
import org.eclipse.xtext.GrammarUtil;
import org.eclipse.xtext.Group;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.ParserRule;
import org.eclipse.xtext.RuleCall;
import org.eclipse.xtext.TerminalRule;
import org.eclipse.xtext.UnorderedGroup;
import org.eclipse.xtext.common.services.TerminalsGrammarAccess;
import org.eclipse.xtext.service.AbstractElementFinder.AbstractGrammarElementFinder;
import org.eclipse.xtext.service.GrammarProvider;

@Singleton
public class RuntimeModelCodeGrammarAccess extends AbstractGrammarElementFinder {
	
	public class GeneratorModelElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "edu.ustb.sei.modeladapter.RuntimeModelCode.GeneratorModel");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Group cGroup_0 = (Group)cGroup.eContents().get(0);
		private final Keyword cPackageKeyword_0_0 = (Keyword)cGroup_0.eContents().get(0);
		private final Assignment cBasePackageAssignment_0_1 = (Assignment)cGroup_0.eContents().get(1);
		private final RuleCall cBasePackageURITerminalRuleCall_0_1_0 = (RuleCall)cBasePackageAssignment_0_1.eContents().get(0);
		private final Keyword cSemicolonKeyword_0_2 = (Keyword)cGroup_0.eContents().get(2);
		private final Keyword cImportKeyword_1 = (Keyword)cGroup.eContents().get(1);
		private final Assignment cAdaptedPackageAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final CrossReference cAdaptedPackageEPackageCrossReference_2_0 = (CrossReference)cAdaptedPackageAssignment_2.eContents().get(0);
		private final RuleCall cAdaptedPackageEPackageURITerminalRuleCall_2_0_1 = (RuleCall)cAdaptedPackageEPackageCrossReference_2_0.eContents().get(1);
		private final Keyword cSemicolonKeyword_3 = (Keyword)cGroup.eContents().get(3);
		private final Keyword cOptionsKeyword_4 = (Keyword)cGroup.eContents().get(4);
		private final Keyword cLeftCurlyBracketKeyword_5 = (Keyword)cGroup.eContents().get(5);
		private final UnorderedGroup cUnorderedGroup_6 = (UnorderedGroup)cGroup.eContents().get(6);
		private final Group cGroup_6_0 = (Group)cUnorderedGroup_6.eContents().get(0);
		private final Keyword cDirectoryKeyword_6_0_0 = (Keyword)cGroup_6_0.eContents().get(0);
		private final Keyword cColonKeyword_6_0_1 = (Keyword)cGroup_6_0.eContents().get(1);
		private final Assignment cDirectoryAssignment_6_0_2 = (Assignment)cGroup_6_0.eContents().get(2);
		private final RuleCall cDirectoryURITerminalRuleCall_6_0_2_0 = (RuleCall)cDirectoryAssignment_6_0_2.eContents().get(0);
		private final Keyword cSemicolonKeyword_6_0_3 = (Keyword)cGroup_6_0.eContents().get(3);
		private final Group cGroup_6_1 = (Group)cUnorderedGroup_6.eContents().get(1);
		private final Keyword cLoadKeyword_6_1_0 = (Keyword)cGroup_6_1.eContents().get(0);
		private final Keyword cColonKeyword_6_1_1 = (Keyword)cGroup_6_1.eContents().get(1);
		private final Assignment cLoadMetamodelAssignment_6_1_2 = (Assignment)cGroup_6_1.eContents().get(2);
		private final RuleCall cLoadMetamodelCODETerminalRuleCall_6_1_2_0 = (RuleCall)cLoadMetamodelAssignment_6_1_2.eContents().get(0);
		private final Group cGroup_6_2 = (Group)cUnorderedGroup_6.eContents().get(2);
		private final Keyword cRefreshingKeyword_6_2_0 = (Keyword)cGroup_6_2.eContents().get(0);
		private final Keyword cColonKeyword_6_2_1 = (Keyword)cGroup_6_2.eContents().get(1);
		private final Assignment cRefreshingRateAssignment_6_2_2 = (Assignment)cGroup_6_2.eContents().get(2);
		private final RuleCall cRefreshingRateLONGTerminalRuleCall_6_2_2_0 = (RuleCall)cRefreshingRateAssignment_6_2_2.eContents().get(0);
		private final Keyword cSemicolonKeyword_6_2_3 = (Keyword)cGroup_6_2.eContents().get(3);
		private final Group cGroup_6_3 = (Group)cUnorderedGroup_6.eContents().get(3);
		private final Keyword cTimerKeyword_6_3_0 = (Keyword)cGroup_6_3.eContents().get(0);
		private final Keyword cColonKeyword_6_3_1 = (Keyword)cGroup_6_3.eContents().get(1);
		private final Assignment cTimerClassAssignment_6_3_2 = (Assignment)cGroup_6_3.eContents().get(2);
		private final RuleCall cTimerClassURITerminalRuleCall_6_3_2_0 = (RuleCall)cTimerClassAssignment_6_3_2.eContents().get(0);
		private final Keyword cSemicolonKeyword_6_3_3 = (Keyword)cGroup_6_3.eContents().get(3);
		private final Keyword cRightCurlyBracketKeyword_7 = (Keyword)cGroup.eContents().get(7);
		private final Assignment cClassAdaptersAssignment_8 = (Assignment)cGroup.eContents().get(8);
		private final RuleCall cClassAdaptersClassAdapterParserRuleCall_8_0 = (RuleCall)cClassAdaptersAssignment_8.eContents().get(0);
		
		//GeneratorModel:
		//	('package' basePackage=URI ';')?
		//	'import' adaptedPackage=[ecore::EPackage|URI] ';'
		//	'options' '{' (('directory' ':' directory=URI ';')? & ('load' ':' loadMetamodel=CODE)? & ('refreshing' ':'
		//	refreshingRate=LONG ';')? & ('timer' ':' timerClass=URI ';')?)
		//	//('classAdapters' '{' classAdapters+=ClassAdapter ( "," classAdapters+=ClassAdapter)* '}' )?
		//	'}'
		//	classAdapters+=ClassAdapter*;
		@Override public ParserRule getRule() { return rule; }
		
		//('package' basePackage=URI ';')? 'import' adaptedPackage=[ecore::EPackage|URI] ';' 'options' '{' (('directory' ':'
		//directory=URI ';')? & ('load' ':' loadMetamodel=CODE)? & ('refreshing' ':' refreshingRate=LONG ';')? & ('timer' ':'
		//timerClass=URI ';')?) //('classAdapters' '{' classAdapters+=ClassAdapter ( "," classAdapters+=ClassAdapter)* '}' )?
		//'}' classAdapters+=ClassAdapter*
		public Group getGroup() { return cGroup; }
		
		//('package' basePackage=URI ';')?
		public Group getGroup_0() { return cGroup_0; }
		
		//'package'
		public Keyword getPackageKeyword_0_0() { return cPackageKeyword_0_0; }
		
		//basePackage=URI
		public Assignment getBasePackageAssignment_0_1() { return cBasePackageAssignment_0_1; }
		
		//URI
		public RuleCall getBasePackageURITerminalRuleCall_0_1_0() { return cBasePackageURITerminalRuleCall_0_1_0; }
		
		//';'
		public Keyword getSemicolonKeyword_0_2() { return cSemicolonKeyword_0_2; }
		
		//'import'
		public Keyword getImportKeyword_1() { return cImportKeyword_1; }
		
		//adaptedPackage=[ecore::EPackage|URI]
		public Assignment getAdaptedPackageAssignment_2() { return cAdaptedPackageAssignment_2; }
		
		//[ecore::EPackage|URI]
		public CrossReference getAdaptedPackageEPackageCrossReference_2_0() { return cAdaptedPackageEPackageCrossReference_2_0; }
		
		//URI
		public RuleCall getAdaptedPackageEPackageURITerminalRuleCall_2_0_1() { return cAdaptedPackageEPackageURITerminalRuleCall_2_0_1; }
		
		//';'
		public Keyword getSemicolonKeyword_3() { return cSemicolonKeyword_3; }
		
		//'options'
		public Keyword getOptionsKeyword_4() { return cOptionsKeyword_4; }
		
		//'{'
		public Keyword getLeftCurlyBracketKeyword_5() { return cLeftCurlyBracketKeyword_5; }
		
		//('directory' ':' directory=URI ';')? & ('load' ':' loadMetamodel=CODE)? & ('refreshing' ':' refreshingRate=LONG ';')? &
		//('timer' ':' timerClass=URI ';')?
		public UnorderedGroup getUnorderedGroup_6() { return cUnorderedGroup_6; }
		
		//('directory' ':' directory=URI ';')?
		public Group getGroup_6_0() { return cGroup_6_0; }
		
		//'directory'
		public Keyword getDirectoryKeyword_6_0_0() { return cDirectoryKeyword_6_0_0; }
		
		//':'
		public Keyword getColonKeyword_6_0_1() { return cColonKeyword_6_0_1; }
		
		//directory=URI
		public Assignment getDirectoryAssignment_6_0_2() { return cDirectoryAssignment_6_0_2; }
		
		//URI
		public RuleCall getDirectoryURITerminalRuleCall_6_0_2_0() { return cDirectoryURITerminalRuleCall_6_0_2_0; }
		
		//';'
		public Keyword getSemicolonKeyword_6_0_3() { return cSemicolonKeyword_6_0_3; }
		
		//('load' ':' loadMetamodel=CODE)?
		public Group getGroup_6_1() { return cGroup_6_1; }
		
		//'load'
		public Keyword getLoadKeyword_6_1_0() { return cLoadKeyword_6_1_0; }
		
		//':'
		public Keyword getColonKeyword_6_1_1() { return cColonKeyword_6_1_1; }
		
		//loadMetamodel=CODE
		public Assignment getLoadMetamodelAssignment_6_1_2() { return cLoadMetamodelAssignment_6_1_2; }
		
		//CODE
		public RuleCall getLoadMetamodelCODETerminalRuleCall_6_1_2_0() { return cLoadMetamodelCODETerminalRuleCall_6_1_2_0; }
		
		//('refreshing' ':' refreshingRate=LONG ';')?
		public Group getGroup_6_2() { return cGroup_6_2; }
		
		//'refreshing'
		public Keyword getRefreshingKeyword_6_2_0() { return cRefreshingKeyword_6_2_0; }
		
		//':'
		public Keyword getColonKeyword_6_2_1() { return cColonKeyword_6_2_1; }
		
		//refreshingRate=LONG
		public Assignment getRefreshingRateAssignment_6_2_2() { return cRefreshingRateAssignment_6_2_2; }
		
		//LONG
		public RuleCall getRefreshingRateLONGTerminalRuleCall_6_2_2_0() { return cRefreshingRateLONGTerminalRuleCall_6_2_2_0; }
		
		//';'
		public Keyword getSemicolonKeyword_6_2_3() { return cSemicolonKeyword_6_2_3; }
		
		//('timer' ':' timerClass=URI ';')?
		public Group getGroup_6_3() { return cGroup_6_3; }
		
		//'timer'
		public Keyword getTimerKeyword_6_3_0() { return cTimerKeyword_6_3_0; }
		
		//':'
		public Keyword getColonKeyword_6_3_1() { return cColonKeyword_6_3_1; }
		
		//timerClass=URI
		public Assignment getTimerClassAssignment_6_3_2() { return cTimerClassAssignment_6_3_2; }
		
		//URI
		public RuleCall getTimerClassURITerminalRuleCall_6_3_2_0() { return cTimerClassURITerminalRuleCall_6_3_2_0; }
		
		//';'
		public Keyword getSemicolonKeyword_6_3_3() { return cSemicolonKeyword_6_3_3; }
		
		////('classAdapters' '{' classAdapters+=ClassAdapter ( "," classAdapters+=ClassAdapter)* '}' )?
		//'}'
		public Keyword getRightCurlyBracketKeyword_7() { return cRightCurlyBracketKeyword_7; }
		
		//classAdapters+=ClassAdapter*
		public Assignment getClassAdaptersAssignment_8() { return cClassAdaptersAssignment_8; }
		
		//ClassAdapter
		public RuleCall getClassAdaptersClassAdapterParserRuleCall_8_0() { return cClassAdaptersClassAdapterParserRuleCall_8_0; }
	}
	public class ClassAdapterElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "edu.ustb.sei.modeladapter.RuntimeModelCode.ClassAdapter");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cAdapterKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cAdaptedClassAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final CrossReference cAdaptedClassEClassCrossReference_1_0 = (CrossReference)cAdaptedClassAssignment_1.eContents().get(0);
		private final RuleCall cAdaptedClassEClassNAMETerminalRuleCall_1_0_1 = (RuleCall)cAdaptedClassEClassCrossReference_1_0.eContents().get(1);
		private final Keyword cLeftCurlyBracketKeyword_2 = (Keyword)cGroup.eContents().get(2);
		private final Group cGroup_3 = (Group)cGroup.eContents().get(3);
		private final Keyword cImportKeyword_3_0 = (Keyword)cGroup_3.eContents().get(0);
		private final Assignment cImportsAssignment_3_1 = (Assignment)cGroup_3.eContents().get(1);
		private final RuleCall cImportsURITerminalRuleCall_3_1_0 = (RuleCall)cImportsAssignment_3_1.eContents().get(0);
		private final Keyword cSemicolonKeyword_3_2 = (Keyword)cGroup_3.eContents().get(2);
		private final Assignment cFeatureAdaptersAssignment_4 = (Assignment)cGroup.eContents().get(4);
		private final RuleCall cFeatureAdaptersFeatureAdapterParserRuleCall_4_0 = (RuleCall)cFeatureAdaptersAssignment_4.eContents().get(0);
		private final Keyword cRightCurlyBracketKeyword_5 = (Keyword)cGroup.eContents().get(5);
		
		//ClassAdapter:
		//	'adapter' adaptedClass=[ecore::EClass|NAME] '{' ('import' imports+=URI ';')*
		//	featureAdapters+=FeatureAdapter*
		//	'}';
		@Override public ParserRule getRule() { return rule; }
		
		//'adapter' adaptedClass=[ecore::EClass|NAME] '{' ('import' imports+=URI ';')* featureAdapters+=FeatureAdapter* '}'
		public Group getGroup() { return cGroup; }
		
		//'adapter'
		public Keyword getAdapterKeyword_0() { return cAdapterKeyword_0; }
		
		//adaptedClass=[ecore::EClass|NAME]
		public Assignment getAdaptedClassAssignment_1() { return cAdaptedClassAssignment_1; }
		
		//[ecore::EClass|NAME]
		public CrossReference getAdaptedClassEClassCrossReference_1_0() { return cAdaptedClassEClassCrossReference_1_0; }
		
		//NAME
		public RuleCall getAdaptedClassEClassNAMETerminalRuleCall_1_0_1() { return cAdaptedClassEClassNAMETerminalRuleCall_1_0_1; }
		
		//'{'
		public Keyword getLeftCurlyBracketKeyword_2() { return cLeftCurlyBracketKeyword_2; }
		
		//('import' imports+=URI ';')*
		public Group getGroup_3() { return cGroup_3; }
		
		//'import'
		public Keyword getImportKeyword_3_0() { return cImportKeyword_3_0; }
		
		//imports+=URI
		public Assignment getImportsAssignment_3_1() { return cImportsAssignment_3_1; }
		
		//URI
		public RuleCall getImportsURITerminalRuleCall_3_1_0() { return cImportsURITerminalRuleCall_3_1_0; }
		
		//';'
		public Keyword getSemicolonKeyword_3_2() { return cSemicolonKeyword_3_2; }
		
		//featureAdapters+=FeatureAdapter*
		public Assignment getFeatureAdaptersAssignment_4() { return cFeatureAdaptersAssignment_4; }
		
		//FeatureAdapter
		public RuleCall getFeatureAdaptersFeatureAdapterParserRuleCall_4_0() { return cFeatureAdaptersFeatureAdapterParserRuleCall_4_0; }
		
		//'}'
		public Keyword getRightCurlyBracketKeyword_5() { return cRightCurlyBracketKeyword_5; }
	}
	public class FeatureAdapterElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "edu.ustb.sei.modeladapter.RuntimeModelCode.FeatureAdapter");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Assignment cIdAssignment_0 = (Assignment)cGroup.eContents().get(0);
		private final Keyword cIdIdKeyword_0_0 = (Keyword)cIdAssignment_0.eContents().get(0);
		private final Assignment cAdaptedFeatureAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final CrossReference cAdaptedFeatureEStructuralFeatureCrossReference_1_0 = (CrossReference)cAdaptedFeatureAssignment_1.eContents().get(0);
		private final RuleCall cAdaptedFeatureEStructuralFeatureNAMETerminalRuleCall_1_0_1 = (RuleCall)cAdaptedFeatureEStructuralFeatureCrossReference_1_0.eContents().get(1);
		private final Assignment cConversionAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cConversionConversionParserRuleCall_2_0 = (RuleCall)cConversionAssignment_2.eContents().get(0);
		private final UnorderedGroup cUnorderedGroup_3 = (UnorderedGroup)cGroup.eContents().get(3);
		private final Group cGroup_3_0 = (Group)cUnorderedGroup_3.eContents().get(0);
		private final Keyword cGetKeyword_3_0_0 = (Keyword)cGroup_3_0.eContents().get(0);
		private final Assignment cGetAssignment_3_0_1 = (Assignment)cGroup_3_0.eContents().get(1);
		private final RuleCall cGetCODETerminalRuleCall_3_0_1_0 = (RuleCall)cGetAssignment_3_0_1.eContents().get(0);
		private final Group cGroup_3_1 = (Group)cUnorderedGroup_3.eContents().get(1);
		private final Keyword cPostKeyword_3_1_0 = (Keyword)cGroup_3_1.eContents().get(0);
		private final Assignment cPostAssignment_3_1_1 = (Assignment)cGroup_3_1.eContents().get(1);
		private final RuleCall cPostCODETerminalRuleCall_3_1_1_0 = (RuleCall)cPostAssignment_3_1_1.eContents().get(0);
		private final Group cGroup_3_2 = (Group)cUnorderedGroup_3.eContents().get(2);
		private final Keyword cPutKeyword_3_2_0 = (Keyword)cGroup_3_2.eContents().get(0);
		private final Assignment cPutAssignment_3_2_1 = (Assignment)cGroup_3_2.eContents().get(1);
		private final RuleCall cPutCODETerminalRuleCall_3_2_1_0 = (RuleCall)cPutAssignment_3_2_1.eContents().get(0);
		private final Group cGroup_3_3 = (Group)cUnorderedGroup_3.eContents().get(3);
		private final Keyword cDeleteKeyword_3_3_0 = (Keyword)cGroup_3_3.eContents().get(0);
		private final Assignment cDeleteAssignment_3_3_1 = (Assignment)cGroup_3_3.eContents().get(1);
		private final RuleCall cDeleteCODETerminalRuleCall_3_3_1_0 = (RuleCall)cDeleteAssignment_3_3_1.eContents().get(0);
		
		//FeatureAdapter:
		//	id?='id'? adaptedFeature=[ecore::EStructuralFeature|NAME] conversion=Conversion? (('get' get=CODE)? & ('post'
		//	post=CODE)? & ('put' put=CODE)? & ('delete' delete=CODE)?);
		@Override public ParserRule getRule() { return rule; }
		
		//id?='id'? adaptedFeature=[ecore::EStructuralFeature|NAME] conversion=Conversion? (('get' get=CODE)? & ('post'
		//post=CODE)? & ('put' put=CODE)? & ('delete' delete=CODE)?)
		public Group getGroup() { return cGroup; }
		
		//id?='id'?
		public Assignment getIdAssignment_0() { return cIdAssignment_0; }
		
		//'id'
		public Keyword getIdIdKeyword_0_0() { return cIdIdKeyword_0_0; }
		
		//adaptedFeature=[ecore::EStructuralFeature|NAME]
		public Assignment getAdaptedFeatureAssignment_1() { return cAdaptedFeatureAssignment_1; }
		
		//[ecore::EStructuralFeature|NAME]
		public CrossReference getAdaptedFeatureEStructuralFeatureCrossReference_1_0() { return cAdaptedFeatureEStructuralFeatureCrossReference_1_0; }
		
		//NAME
		public RuleCall getAdaptedFeatureEStructuralFeatureNAMETerminalRuleCall_1_0_1() { return cAdaptedFeatureEStructuralFeatureNAMETerminalRuleCall_1_0_1; }
		
		//conversion=Conversion?
		public Assignment getConversionAssignment_2() { return cConversionAssignment_2; }
		
		//Conversion
		public RuleCall getConversionConversionParserRuleCall_2_0() { return cConversionConversionParserRuleCall_2_0; }
		
		//('get' get=CODE)? & ('post' post=CODE)? & ('put' put=CODE)? & ('delete' delete=CODE)?
		public UnorderedGroup getUnorderedGroup_3() { return cUnorderedGroup_3; }
		
		//('get' get=CODE)?
		public Group getGroup_3_0() { return cGroup_3_0; }
		
		//'get'
		public Keyword getGetKeyword_3_0_0() { return cGetKeyword_3_0_0; }
		
		//get=CODE
		public Assignment getGetAssignment_3_0_1() { return cGetAssignment_3_0_1; }
		
		//CODE
		public RuleCall getGetCODETerminalRuleCall_3_0_1_0() { return cGetCODETerminalRuleCall_3_0_1_0; }
		
		//('post' post=CODE)?
		public Group getGroup_3_1() { return cGroup_3_1; }
		
		//'post'
		public Keyword getPostKeyword_3_1_0() { return cPostKeyword_3_1_0; }
		
		//post=CODE
		public Assignment getPostAssignment_3_1_1() { return cPostAssignment_3_1_1; }
		
		//CODE
		public RuleCall getPostCODETerminalRuleCall_3_1_1_0() { return cPostCODETerminalRuleCall_3_1_1_0; }
		
		//('put' put=CODE)?
		public Group getGroup_3_2() { return cGroup_3_2; }
		
		//'put'
		public Keyword getPutKeyword_3_2_0() { return cPutKeyword_3_2_0; }
		
		//put=CODE
		public Assignment getPutAssignment_3_2_1() { return cPutAssignment_3_2_1; }
		
		//CODE
		public RuleCall getPutCODETerminalRuleCall_3_2_1_0() { return cPutCODETerminalRuleCall_3_2_1_0; }
		
		//('delete' delete=CODE)?
		public Group getGroup_3_3() { return cGroup_3_3; }
		
		//'delete'
		public Keyword getDeleteKeyword_3_3_0() { return cDeleteKeyword_3_3_0; }
		
		//delete=CODE
		public Assignment getDeleteAssignment_3_3_1() { return cDeleteAssignment_3_3_1; }
		
		//CODE
		public RuleCall getDeleteCODETerminalRuleCall_3_3_1_0() { return cDeleteCODETerminalRuleCall_3_3_1_0; }
	}
	public class ConversionElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "edu.ustb.sei.modeladapter.RuntimeModelCode.Conversion");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cActualKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cPhysicalTypeAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final Alternatives cPhysicalTypeAlternatives_1_0 = (Alternatives)cPhysicalTypeAssignment_1.eContents().get(0);
		private final RuleCall cPhysicalTypeNAMETerminalRuleCall_1_0_0 = (RuleCall)cPhysicalTypeAlternatives_1_0.eContents().get(0);
		private final RuleCall cPhysicalTypeURITerminalRuleCall_1_0_1 = (RuleCall)cPhysicalTypeAlternatives_1_0.eContents().get(1);
		private final Keyword cFromKeyword_2 = (Keyword)cGroup.eContents().get(2);
		private final Assignment cPhysicalToLogicalAssignment_3 = (Assignment)cGroup.eContents().get(3);
		private final RuleCall cPhysicalToLogicalCODETerminalRuleCall_3_0 = (RuleCall)cPhysicalToLogicalAssignment_3.eContents().get(0);
		private final Keyword cToKeyword_4 = (Keyword)cGroup.eContents().get(4);
		private final Assignment cLogicalToPhysicalAssignment_5 = (Assignment)cGroup.eContents().get(5);
		private final RuleCall cLogicalToPhysicalCODETerminalRuleCall_5_0 = (RuleCall)cLogicalToPhysicalAssignment_5.eContents().get(0);
		
		//Conversion:
		//	'actual' physicalType=(NAME | URI)
		//	'from' physicalToLogical=CODE
		//	'to' logicalToPhysical=CODE;
		@Override public ParserRule getRule() { return rule; }
		
		//'actual' physicalType=(NAME | URI) 'from' physicalToLogical=CODE 'to' logicalToPhysical=CODE
		public Group getGroup() { return cGroup; }
		
		//'actual'
		public Keyword getActualKeyword_0() { return cActualKeyword_0; }
		
		//physicalType=(NAME | URI)
		public Assignment getPhysicalTypeAssignment_1() { return cPhysicalTypeAssignment_1; }
		
		//(NAME | URI)
		public Alternatives getPhysicalTypeAlternatives_1_0() { return cPhysicalTypeAlternatives_1_0; }
		
		//NAME
		public RuleCall getPhysicalTypeNAMETerminalRuleCall_1_0_0() { return cPhysicalTypeNAMETerminalRuleCall_1_0_0; }
		
		//URI
		public RuleCall getPhysicalTypeURITerminalRuleCall_1_0_1() { return cPhysicalTypeURITerminalRuleCall_1_0_1; }
		
		//'from'
		public Keyword getFromKeyword_2() { return cFromKeyword_2; }
		
		//physicalToLogical=CODE
		public Assignment getPhysicalToLogicalAssignment_3() { return cPhysicalToLogicalAssignment_3; }
		
		//CODE
		public RuleCall getPhysicalToLogicalCODETerminalRuleCall_3_0() { return cPhysicalToLogicalCODETerminalRuleCall_3_0; }
		
		//'to'
		public Keyword getToKeyword_4() { return cToKeyword_4; }
		
		//logicalToPhysical=CODE
		public Assignment getLogicalToPhysicalAssignment_5() { return cLogicalToPhysicalAssignment_5; }
		
		//CODE
		public RuleCall getLogicalToPhysicalCODETerminalRuleCall_5_0() { return cLogicalToPhysicalCODETerminalRuleCall_5_0; }
	}
	
	
	private final GeneratorModelElements pGeneratorModel;
	private final ClassAdapterElements pClassAdapter;
	private final FeatureAdapterElements pFeatureAdapter;
	private final ConversionElements pConversion;
	private final TerminalRule tLONG;
	private final TerminalRule tNAME;
	private final TerminalRule tURI;
	private final TerminalRule tCODE;
	
	private final Grammar grammar;
	
	private final TerminalsGrammarAccess gaTerminals;

	@Inject
	public RuntimeModelCodeGrammarAccess(GrammarProvider grammarProvider,
			TerminalsGrammarAccess gaTerminals) {
		this.grammar = internalFindGrammar(grammarProvider);
		this.gaTerminals = gaTerminals;
		this.pGeneratorModel = new GeneratorModelElements();
		this.pClassAdapter = new ClassAdapterElements();
		this.pFeatureAdapter = new FeatureAdapterElements();
		this.pConversion = new ConversionElements();
		this.tLONG = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "edu.ustb.sei.modeladapter.RuntimeModelCode.LONG");
		this.tNAME = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "edu.ustb.sei.modeladapter.RuntimeModelCode.NAME");
		this.tURI = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "edu.ustb.sei.modeladapter.RuntimeModelCode.URI");
		this.tCODE = (TerminalRule) GrammarUtil.findRuleForName(getGrammar(), "edu.ustb.sei.modeladapter.RuntimeModelCode.CODE");
	}
	
	protected Grammar internalFindGrammar(GrammarProvider grammarProvider) {
		Grammar grammar = grammarProvider.getGrammar(this);
		while (grammar != null) {
			if ("edu.ustb.sei.modeladapter.RuntimeModelCode".equals(grammar.getName())) {
				return grammar;
			}
			List<Grammar> grammars = grammar.getUsedGrammars();
			if (!grammars.isEmpty()) {
				grammar = grammars.iterator().next();
			} else {
				return null;
			}
		}
		return grammar;
	}
	
	@Override
	public Grammar getGrammar() {
		return grammar;
	}
	
	
	public TerminalsGrammarAccess getTerminalsGrammarAccess() {
		return gaTerminals;
	}

	
	//GeneratorModel:
	//	('package' basePackage=URI ';')?
	//	'import' adaptedPackage=[ecore::EPackage|URI] ';'
	//	'options' '{' (('directory' ':' directory=URI ';')? & ('load' ':' loadMetamodel=CODE)? & ('refreshing' ':'
	//	refreshingRate=LONG ';')? & ('timer' ':' timerClass=URI ';')?)
	//	//('classAdapters' '{' classAdapters+=ClassAdapter ( "," classAdapters+=ClassAdapter)* '}' )?
	//	'}'
	//	classAdapters+=ClassAdapter*;
	public GeneratorModelElements getGeneratorModelAccess() {
		return pGeneratorModel;
	}
	
	public ParserRule getGeneratorModelRule() {
		return getGeneratorModelAccess().getRule();
	}
	
	//ClassAdapter:
	//	'adapter' adaptedClass=[ecore::EClass|NAME] '{' ('import' imports+=URI ';')*
	//	featureAdapters+=FeatureAdapter*
	//	'}';
	public ClassAdapterElements getClassAdapterAccess() {
		return pClassAdapter;
	}
	
	public ParserRule getClassAdapterRule() {
		return getClassAdapterAccess().getRule();
	}
	
	//FeatureAdapter:
	//	id?='id'? adaptedFeature=[ecore::EStructuralFeature|NAME] conversion=Conversion? (('get' get=CODE)? & ('post'
	//	post=CODE)? & ('put' put=CODE)? & ('delete' delete=CODE)?);
	public FeatureAdapterElements getFeatureAdapterAccess() {
		return pFeatureAdapter;
	}
	
	public ParserRule getFeatureAdapterRule() {
		return getFeatureAdapterAccess().getRule();
	}
	
	//Conversion:
	//	'actual' physicalType=(NAME | URI)
	//	'from' physicalToLogical=CODE
	//	'to' logicalToPhysical=CODE;
	public ConversionElements getConversionAccess() {
		return pConversion;
	}
	
	public ParserRule getConversionRule() {
		return getConversionAccess().getRule();
	}
	
	//terminal LONG returns ecore::ELong:
	//	INT;
	public TerminalRule getLONGRule() {
		return tLONG;
	}
	
	//terminal NAME:
	//	ID;
	public TerminalRule getNAMERule() {
		return tNAME;
	}
	
	//terminal URI:
	//	'A'..'Z' | 'a'..'z' | '0'..'9' | '-' | '_' | '.' | '~' | '!' | '*' | '\'' | '(' | ')' | ':' | '@' | '&' | '=' | '+' |
	//	'$' | ',' | '/' | '?' | '#' | '[' | ']'+;
	public TerminalRule getURIRule() {
		return tURI;
	}
	
	//terminal CODE:
	//	'⟦'->'⟧';
	public TerminalRule getCODERule() {
		return tCODE;
	}
	
	//terminal ID:
	//	'^'? ('a'..'z' | 'A'..'Z' | '_') ('a'..'z' | 'A'..'Z' | '_' | '0'..'9')*;
	public TerminalRule getIDRule() {
		return gaTerminals.getIDRule();
	}
	
	//terminal INT returns ecore::EInt:
	//	'0'..'9'+;
	public TerminalRule getINTRule() {
		return gaTerminals.getINTRule();
	}
	
	//terminal STRING:
	//	'"' ('\\' . | !('\\' | '"'))* '"' |
	//	"'" ('\\' . | !('\\' | "'"))* "'";
	public TerminalRule getSTRINGRule() {
		return gaTerminals.getSTRINGRule();
	}
	
	//terminal ML_COMMENT:
	//	'/*'->'*/';
	public TerminalRule getML_COMMENTRule() {
		return gaTerminals.getML_COMMENTRule();
	}
	
	//terminal SL_COMMENT:
	//	'//' !('\n' | '\r')* ('\r'? '\n')?;
	public TerminalRule getSL_COMMENTRule() {
		return gaTerminals.getSL_COMMENTRule();
	}
	
	//terminal WS:
	//	' ' | '\t' | '\r' | '\n'+;
	public TerminalRule getWSRule() {
		return gaTerminals.getWSRule();
	}
	
	//terminal ANY_OTHER:
	//	.;
	public TerminalRule getANY_OTHERRule() {
		return gaTerminals.getANY_OTHERRule();
	}
}
