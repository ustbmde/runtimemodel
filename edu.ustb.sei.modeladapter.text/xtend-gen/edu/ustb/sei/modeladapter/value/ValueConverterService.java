package edu.ustb.sei.modeladapter.value;

import edu.ustb.sei.modeladapter.value.CodeValueConverter;
import org.eclipse.xtext.common.services.DefaultTerminalConverters;
import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverter;

@SuppressWarnings("all")
public class ValueConverterService extends DefaultTerminalConverters {
  @ValueConverter(rule = "CODE")
  public IValueConverter<String> getCodeValueConverter() {
    return new CodeValueConverter();
  }
}
