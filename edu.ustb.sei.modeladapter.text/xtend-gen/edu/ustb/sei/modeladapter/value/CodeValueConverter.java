package edu.ustb.sei.modeladapter.value;

import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.nodemodel.INode;

@SuppressWarnings("all")
public class CodeValueConverter implements IValueConverter<String> {
  @Override
  public String toString(final String value) throws ValueConverterException {
    return (("⟦" + value) + "⟧");
  }
  
  @Override
  public String toValue(final String string, final INode node) throws ValueConverterException {
    String _xifexpression = null;
    if ((string == null)) {
      _xifexpression = null;
    } else {
      int _length = string.length();
      int _minus = (_length - 1);
      _xifexpression = string.substring(1, _minus);
    }
    return _xifexpression;
  }
}
