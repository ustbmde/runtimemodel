/**
 */
package edu.ustb.sei.mde.mobile.xml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Document</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.xml.Document#getXmlEncoding <em>Xml Encoding</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.xml.Document#getXmlVersion <em>Xml Version</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mobile.xml.Document#getFilePath <em>File Path</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mobile.xml.XmlPackage#getDocument()
 * @model
 * @generated
 */
public interface Document extends Node, NodeContainer {
	/**
	 * Returns the value of the '<em><b>Xml Encoding</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Xml Encoding</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Xml Encoding</em>' attribute.
	 * @see #setXmlEncoding(String)
	 * @see edu.ustb.sei.mde.mobile.xml.XmlPackage#getDocument_XmlEncoding()
	 * @model
	 * @generated
	 */
	String getXmlEncoding();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mobile.xml.Document#getXmlEncoding <em>Xml Encoding</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Xml Encoding</em>' attribute.
	 * @see #getXmlEncoding()
	 * @generated
	 */
	void setXmlEncoding(String value);

	/**
	 * Returns the value of the '<em><b>Xml Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Xml Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Xml Version</em>' attribute.
	 * @see #setXmlVersion(String)
	 * @see edu.ustb.sei.mde.mobile.xml.XmlPackage#getDocument_XmlVersion()
	 * @model
	 * @generated
	 */
	String getXmlVersion();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mobile.xml.Document#getXmlVersion <em>Xml Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Xml Version</em>' attribute.
	 * @see #getXmlVersion()
	 * @generated
	 */
	void setXmlVersion(String value);

	/**
	 * Returns the value of the '<em><b>File Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>File Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>File Path</em>' attribute.
	 * @see #setFilePath(String)
	 * @see edu.ustb.sei.mde.mobile.xml.XmlPackage#getDocument_FilePath()
	 * @model
	 * @generated
	 */
	String getFilePath();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mobile.xml.Document#getFilePath <em>File Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>File Path</em>' attribute.
	 * @see #getFilePath()
	 * @generated
	 */
	void setFilePath(String value);

} // Document
