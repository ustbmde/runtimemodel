/**
 */
package edu.ustb.sei.mde.mobile.xml.impl;

import edu.ustb.sei.mde.mobile.xml.CDATASection;
import edu.ustb.sei.mde.mobile.xml.XmlPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>CDATA Section</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CDATASectionImpl extends TextImpl implements CDATASection {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CDATASectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return XmlPackage.Literals.CDATA_SECTION;
	}

} //CDATASectionImpl
