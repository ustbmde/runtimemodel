/**
 */
package edu.ustb.sei.mde.mobile.xml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Notation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.mobile.xml.XmlPackage#getNotation()
 * @model
 * @generated
 */
public interface Notation extends Node {
} // Notation
