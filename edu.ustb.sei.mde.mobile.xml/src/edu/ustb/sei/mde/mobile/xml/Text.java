/**
 */
package edu.ustb.sei.mde.mobile.xml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Text</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.mobile.xml.XmlPackage#getText()
 * @model
 * @generated
 */
public interface Text extends CharacterData {
} // Text
