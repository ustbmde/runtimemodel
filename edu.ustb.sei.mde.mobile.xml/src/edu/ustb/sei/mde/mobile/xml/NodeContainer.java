/**
 */
package edu.ustb.sei.mde.mobile.xml;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mobile.xml.NodeContainer#getChildren <em>Children</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.mobile.xml.XmlPackage#getNodeContainer()
 * @model abstract="true"
 * @generated
 */
public interface NodeContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>Children</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.mobile.xml.Node}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' containment reference list.
	 * @see edu.ustb.sei.mde.mobile.xml.XmlPackage#getNodeContainer_Children()
	 * @model containment="true"
	 * @generated
	 */
	EList<Node> getChildren();

} // NodeContainer
