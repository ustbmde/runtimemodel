/**
 */
package edu.ustb.sei.mde.mobile.xml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.mobile.xml.XmlPackage#getNode()
 * @model abstract="true"
 * @generated
 */
public interface Node extends EObject {
} // Node
