/**
 */
package edu.ustb.sei.mde.mobile.xml;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.mobile.xml.XmlFactory
 * @model kind="package"
 * @generated
 */
public interface XmlPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "xml";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.ustb.edu.cn/sei/mde/xml";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "x";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	XmlPackage eINSTANCE = edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl.init();

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.xml.impl.NodeImpl <em>Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.xml.impl.NodeImpl
	 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getNode()
	 * @generated
	 */
	int NODE = 1;

	/**
	 * The number of structural features of the '<em>Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.xml.impl.DocumentImpl <em>Document</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.xml.impl.DocumentImpl
	 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getDocument()
	 * @generated
	 */
	int DOCUMENT = 0;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT__CHILDREN = NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Xml Encoding</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT__XML_ENCODING = NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Xml Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT__XML_VERSION = NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>File Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT__FILE_PATH = NODE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Document</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_FEATURE_COUNT = NODE_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Document</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.xml.impl.ProcessingInstructionImpl <em>Processing Instruction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.xml.impl.ProcessingInstructionImpl
	 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getProcessingInstruction()
	 * @generated
	 */
	int PROCESSING_INSTRUCTION = 2;

	/**
	 * The feature id for the '<em><b>Target</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSING_INSTRUCTION__TARGET = NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSING_INSTRUCTION__DATA = NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Processing Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSING_INSTRUCTION_FEATURE_COUNT = NODE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Processing Instruction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROCESSING_INSTRUCTION_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.xml.impl.EntityReferenceImpl <em>Entity Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.xml.impl.EntityReferenceImpl
	 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getEntityReference()
	 * @generated
	 */
	int ENTITY_REFERENCE = 3;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_REFERENCE__CHILDREN = NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_REFERENCE__NAME = NODE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Entity Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_REFERENCE_FEATURE_COUNT = NODE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Entity Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_REFERENCE_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.xml.impl.ElementImpl <em>Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.xml.impl.ElementImpl
	 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getElement()
	 * @generated
	 */
	int ELEMENT = 4;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__CHILDREN = NODE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__NAME = NODE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Namespace URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__NAMESPACE_URI = NODE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT__ATTRIBUTES = NODE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_FEATURE_COUNT = NODE_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.xml.impl.CharacterDataImpl <em>Character Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.xml.impl.CharacterDataImpl
	 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getCharacterData()
	 * @generated
	 */
	int CHARACTER_DATA = 5;

	/**
	 * The feature id for the '<em><b>Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER_DATA__DATA = NODE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Character Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER_DATA_FEATURE_COUNT = NODE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Character Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHARACTER_DATA_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.xml.impl.TextImpl <em>Text</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.xml.impl.TextImpl
	 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getText()
	 * @generated
	 */
	int TEXT = 6;

	/**
	 * The feature id for the '<em><b>Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEXT__DATA = CHARACTER_DATA__DATA;

	/**
	 * The number of structural features of the '<em>Text</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEXT_FEATURE_COUNT = CHARACTER_DATA_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Text</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEXT_OPERATION_COUNT = CHARACTER_DATA_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.xml.impl.CommentImpl <em>Comment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.xml.impl.CommentImpl
	 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getComment()
	 * @generated
	 */
	int COMMENT = 7;

	/**
	 * The feature id for the '<em><b>Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT__DATA = CHARACTER_DATA__DATA;

	/**
	 * The number of structural features of the '<em>Comment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT_FEATURE_COUNT = CHARACTER_DATA_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Comment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMENT_OPERATION_COUNT = CHARACTER_DATA_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.xml.impl.NotationImpl <em>Notation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.xml.impl.NotationImpl
	 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getNotation()
	 * @generated
	 */
	int NOTATION = 8;

	/**
	 * The number of structural features of the '<em>Notation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTATION_FEATURE_COUNT = NODE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Notation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NOTATION_OPERATION_COUNT = NODE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.xml.impl.AttrImpl <em>Attr</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.xml.impl.AttrImpl
	 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getAttr()
	 * @generated
	 */
	int ATTR = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTR__NAME = 0;

	/**
	 * The feature id for the '<em><b>Namespace URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTR__NAMESPACE_URI = 1;

	/**
	 * The feature id for the '<em><b>Value Literal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTR__VALUE_LITERAL = 2;

	/**
	 * The number of structural features of the '<em>Attr</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTR_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Attr</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.xml.impl.NodeContainerImpl <em>Node Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.xml.impl.NodeContainerImpl
	 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getNodeContainer()
	 * @generated
	 */
	int NODE_CONTAINER = 10;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_CONTAINER__CHILDREN = 0;

	/**
	 * The number of structural features of the '<em>Node Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_CONTAINER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Node Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NODE_CONTAINER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.mobile.xml.impl.CDATASectionImpl <em>CDATA Section</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.mobile.xml.impl.CDATASectionImpl
	 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getCDATASection()
	 * @generated
	 */
	int CDATA_SECTION = 11;

	/**
	 * The feature id for the '<em><b>Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDATA_SECTION__DATA = TEXT__DATA;

	/**
	 * The number of structural features of the '<em>CDATA Section</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDATA_SECTION_FEATURE_COUNT = TEXT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>CDATA Section</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CDATA_SECTION_OPERATION_COUNT = TEXT_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.xml.Document <em>Document</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.Document
	 * @generated
	 */
	EClass getDocument();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.xml.Document#getXmlEncoding <em>Xml Encoding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Xml Encoding</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.Document#getXmlEncoding()
	 * @see #getDocument()
	 * @generated
	 */
	EAttribute getDocument_XmlEncoding();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.xml.Document#getXmlVersion <em>Xml Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Xml Version</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.Document#getXmlVersion()
	 * @see #getDocument()
	 * @generated
	 */
	EAttribute getDocument_XmlVersion();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.xml.Document#getFilePath <em>File Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>File Path</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.Document#getFilePath()
	 * @see #getDocument()
	 * @generated
	 */
	EAttribute getDocument_FilePath();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.xml.Node <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.Node
	 * @generated
	 */
	EClass getNode();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.xml.ProcessingInstruction <em>Processing Instruction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Processing Instruction</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.ProcessingInstruction
	 * @generated
	 */
	EClass getProcessingInstruction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.xml.ProcessingInstruction#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.ProcessingInstruction#getTarget()
	 * @see #getProcessingInstruction()
	 * @generated
	 */
	EAttribute getProcessingInstruction_Target();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.xml.ProcessingInstruction#getData <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Data</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.ProcessingInstruction#getData()
	 * @see #getProcessingInstruction()
	 * @generated
	 */
	EAttribute getProcessingInstruction_Data();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.xml.EntityReference <em>Entity Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entity Reference</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.EntityReference
	 * @generated
	 */
	EClass getEntityReference();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.xml.EntityReference#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.EntityReference#getName()
	 * @see #getEntityReference()
	 * @generated
	 */
	EAttribute getEntityReference_Name();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.xml.Element <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.Element
	 * @generated
	 */
	EClass getElement();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.xml.Element#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.Element#getName()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_Name();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.xml.Element#getNamespaceURI <em>Namespace URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Namespace URI</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.Element#getNamespaceURI()
	 * @see #getElement()
	 * @generated
	 */
	EAttribute getElement_NamespaceURI();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.mobile.xml.Element#getAttributes <em>Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attributes</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.Element#getAttributes()
	 * @see #getElement()
	 * @generated
	 */
	EReference getElement_Attributes();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.xml.CharacterData <em>Character Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Character Data</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.CharacterData
	 * @generated
	 */
	EClass getCharacterData();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.xml.CharacterData#getData <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Data</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.CharacterData#getData()
	 * @see #getCharacterData()
	 * @generated
	 */
	EAttribute getCharacterData_Data();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.xml.Text <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Text</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.Text
	 * @generated
	 */
	EClass getText();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.xml.Comment <em>Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Comment</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.Comment
	 * @generated
	 */
	EClass getComment();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.xml.Notation <em>Notation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Notation</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.Notation
	 * @generated
	 */
	EClass getNotation();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.xml.Attr <em>Attr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attr</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.Attr
	 * @generated
	 */
	EClass getAttr();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.xml.Attr#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.Attr#getName()
	 * @see #getAttr()
	 * @generated
	 */
	EAttribute getAttr_Name();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.xml.Attr#getNamespaceURI <em>Namespace URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Namespace URI</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.Attr#getNamespaceURI()
	 * @see #getAttr()
	 * @generated
	 */
	EAttribute getAttr_NamespaceURI();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.mobile.xml.Attr#getValueLiteral <em>Value Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Literal</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.Attr#getValueLiteral()
	 * @see #getAttr()
	 * @generated
	 */
	EAttribute getAttr_ValueLiteral();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.xml.NodeContainer <em>Node Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Node Container</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.NodeContainer
	 * @generated
	 */
	EClass getNodeContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.mobile.xml.NodeContainer#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Children</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.NodeContainer#getChildren()
	 * @see #getNodeContainer()
	 * @generated
	 */
	EReference getNodeContainer_Children();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.mobile.xml.CDATASection <em>CDATA Section</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>CDATA Section</em>'.
	 * @see edu.ustb.sei.mde.mobile.xml.CDATASection
	 * @generated
	 */
	EClass getCDATASection();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	XmlFactory getXmlFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.xml.impl.DocumentImpl <em>Document</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.xml.impl.DocumentImpl
		 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getDocument()
		 * @generated
		 */
		EClass DOCUMENT = eINSTANCE.getDocument();

		/**
		 * The meta object literal for the '<em><b>Xml Encoding</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT__XML_ENCODING = eINSTANCE.getDocument_XmlEncoding();

		/**
		 * The meta object literal for the '<em><b>Xml Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT__XML_VERSION = eINSTANCE.getDocument_XmlVersion();

		/**
		 * The meta object literal for the '<em><b>File Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT__FILE_PATH = eINSTANCE.getDocument_FilePath();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.xml.impl.NodeImpl <em>Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.xml.impl.NodeImpl
		 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getNode()
		 * @generated
		 */
		EClass NODE = eINSTANCE.getNode();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.xml.impl.ProcessingInstructionImpl <em>Processing Instruction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.xml.impl.ProcessingInstructionImpl
		 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getProcessingInstruction()
		 * @generated
		 */
		EClass PROCESSING_INSTRUCTION = eINSTANCE.getProcessingInstruction();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESSING_INSTRUCTION__TARGET = eINSTANCE.getProcessingInstruction_Target();

		/**
		 * The meta object literal for the '<em><b>Data</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROCESSING_INSTRUCTION__DATA = eINSTANCE.getProcessingInstruction_Data();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.xml.impl.EntityReferenceImpl <em>Entity Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.xml.impl.EntityReferenceImpl
		 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getEntityReference()
		 * @generated
		 */
		EClass ENTITY_REFERENCE = eINSTANCE.getEntityReference();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENTITY_REFERENCE__NAME = eINSTANCE.getEntityReference_Name();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.xml.impl.ElementImpl <em>Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.xml.impl.ElementImpl
		 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getElement()
		 * @generated
		 */
		EClass ELEMENT = eINSTANCE.getElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__NAME = eINSTANCE.getElement_Name();

		/**
		 * The meta object literal for the '<em><b>Namespace URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ELEMENT__NAMESPACE_URI = eINSTANCE.getElement_NamespaceURI();

		/**
		 * The meta object literal for the '<em><b>Attributes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ELEMENT__ATTRIBUTES = eINSTANCE.getElement_Attributes();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.xml.impl.CharacterDataImpl <em>Character Data</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.xml.impl.CharacterDataImpl
		 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getCharacterData()
		 * @generated
		 */
		EClass CHARACTER_DATA = eINSTANCE.getCharacterData();

		/**
		 * The meta object literal for the '<em><b>Data</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CHARACTER_DATA__DATA = eINSTANCE.getCharacterData_Data();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.xml.impl.TextImpl <em>Text</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.xml.impl.TextImpl
		 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getText()
		 * @generated
		 */
		EClass TEXT = eINSTANCE.getText();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.xml.impl.CommentImpl <em>Comment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.xml.impl.CommentImpl
		 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getComment()
		 * @generated
		 */
		EClass COMMENT = eINSTANCE.getComment();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.xml.impl.NotationImpl <em>Notation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.xml.impl.NotationImpl
		 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getNotation()
		 * @generated
		 */
		EClass NOTATION = eINSTANCE.getNotation();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.xml.impl.AttrImpl <em>Attr</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.xml.impl.AttrImpl
		 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getAttr()
		 * @generated
		 */
		EClass ATTR = eINSTANCE.getAttr();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTR__NAME = eINSTANCE.getAttr_Name();

		/**
		 * The meta object literal for the '<em><b>Namespace URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTR__NAMESPACE_URI = eINSTANCE.getAttr_NamespaceURI();

		/**
		 * The meta object literal for the '<em><b>Value Literal</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATTR__VALUE_LITERAL = eINSTANCE.getAttr_ValueLiteral();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.xml.impl.NodeContainerImpl <em>Node Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.xml.impl.NodeContainerImpl
		 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getNodeContainer()
		 * @generated
		 */
		EClass NODE_CONTAINER = eINSTANCE.getNodeContainer();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NODE_CONTAINER__CHILDREN = eINSTANCE.getNodeContainer_Children();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.mobile.xml.impl.CDATASectionImpl <em>CDATA Section</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.mobile.xml.impl.CDATASectionImpl
		 * @see edu.ustb.sei.mde.mobile.xml.impl.XmlPackageImpl#getCDATASection()
		 * @generated
		 */
		EClass CDATA_SECTION = eINSTANCE.getCDATASection();

	}

} //XmlPackage
