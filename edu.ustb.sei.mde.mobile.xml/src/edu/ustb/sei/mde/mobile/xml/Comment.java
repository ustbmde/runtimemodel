/**
 */
package edu.ustb.sei.mde.mobile.xml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Comment</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.mobile.xml.XmlPackage#getComment()
 * @model
 * @generated
 */
public interface Comment extends CharacterData {
} // Comment
