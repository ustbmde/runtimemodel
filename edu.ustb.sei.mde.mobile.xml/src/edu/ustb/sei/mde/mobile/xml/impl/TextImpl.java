/**
 */
package edu.ustb.sei.mde.mobile.xml.impl;

import edu.ustb.sei.mde.mobile.xml.Text;
import edu.ustb.sei.mde.mobile.xml.XmlPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Text</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TextImpl extends CharacterDataImpl implements Text {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TextImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return XmlPackage.Literals.TEXT;
	}

} //TextImpl
