/**
 */
package edu.ustb.sei.mde.mobile.xml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>CDATA Section</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.mobile.xml.XmlPackage#getCDATASection()
 * @model
 * @generated
 */
public interface CDATASection extends Text {
} // CDATASection
