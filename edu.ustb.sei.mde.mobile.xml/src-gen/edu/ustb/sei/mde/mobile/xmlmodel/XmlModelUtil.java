package edu.ustb.sei.mde.mobile.xmlmodel;

import edu.ustb.sei.mde.mobile.datastructure.operation.ValueProvider;
import edu.ustb.sei.mde.mobile.xmlmodel.XmlModelSynchronizer;

@SuppressWarnings("all")
public class XmlModelUtil {
  public static XmlModelSynchronizer instance = new edu.ustb.sei.mde.mobile.xmlmodel.XmlModelSynchronizer();;
  
  public static void apply(final ValueProvider root) {
    edu.ustb.sei.mde.mobile.datastructure.operation.OperationScheduler s = instance.createScheduler();
    s.schedule(root);
    s.execute(root);
  }
}
